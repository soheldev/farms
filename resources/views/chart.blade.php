<h1>Chart page here</h1>

<div id="container" style="width: 75%;">
    <canvas id="canvas"></canvas>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript" src="jscript/graph.js"></script>
<script>
    var barChartData = {
        labels: [
            "January",
            "Febuary",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "Sepetember",
            "October",
            "November",
            "December"
        ],
        datasets: [
            {
                label: "Casual",
                backgroundColor: "pink",
                borderColor: "red",
                borderWidth: 1,
                data: [3]
            },
            {
                label: "Floating",
                backgroundColor: "lightblue",
                borderColor: "blue",
                borderWidth: 1,
                data: [4]
            },
            {
                label: "Sick",
                backgroundColor: "lightgreen",
                borderColor: "green",
                borderWidth: 1,
                data: [5]
            },
            /*{
                label: "Visa",
                backgroundColor: "yellow",
                borderColor: "orange",
                borderWidth: 1,
                data: [6,9,7,3,10,7,4,6]
            }*/
        ]
    };

    var chartOptions = {
        responsive: true,
        legend: {
            position: "top"
        },
        title: {
            display: true,
            text: "Chart.js Bar Chart"
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }

    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: "bar",
            data: barChartData,
            options: chartOptions
        });
    };

</script>