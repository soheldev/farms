@php
    $segment = Request::segment(2);
    $value = Request::segment(3);

@endphp
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item @if($segment == "dashboard")active @endif">
            <a class="nav-link @if($segment == "dashboard") hover-cursor @endif" href="{{url('/master/dashboard')}}">
                <i class="mdi mdi-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>

        <li class="nav-item @if($segment == "client") active @endif">
            <a class="nav-link" href="{{url('/master/client/list')}}">
                <i class="mdi mdi mdi-account menu-icon"></i>
                <span class="menu-title">Manage Clients</span>
            </a>
        </li>

        {{--<li class="nav-item @if($segment == "farm")active @endif">
            <a class="nav-link" href="{{url('/master/farm/list')}}">
                <i class="mdi mdi-flower menu-icon"></i>
                <span class="menu-title">Manage Farms</span>
            </a>
        </li>--}}

        {{--<li class="nav-item">
            <a class="nav-link @if($segment == "setting") hover-cursor @endif" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="mdi mdi-settings menu-icon"></i>
                <span class="menu-title">Manage Setting</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse @if($segment == "setting") show @endif" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link @if($value == "global") active @endif" href="{{url('/admin/setting/global')}}">Manage Global Values</a></li>
                    <li class="nav-item"> <a class="nav-link @if($value == "banner") active @endif" href="{{url('/admin/setting/banner')}}">Manage Banner Images</a></li>
                    <li class="nav-item"> <a class="nav-link @if($value == "country") active @endif" href="{{url('/admin/setting/country')}}">Manage Countries</a></li>
                    <li class="nav-item"> <a class="nav-link @if($value == "state") active @endif" href="{{url('/admin/setting/state')}}">Manage States</a></li>
                    <li class="nav-item"> <a class="nav-link @if($value == "city") active @endif" href="{{url('/admin/setting/city')}}">Manage Cities</a></li>
                </ul>
            </div>
        </li>--}}

        {{--<li class="nav-item @if($segment == "breed")active @endif">
            <a class="nav-link" href="{{url('/admin/breed/list')}}">
                <i class="mdi mdi-dna menu-icon"></i>
                <span class="menu-title">Manage Breed</span>
            </a>
        </li>--}}

        {{--<li class="nav-item @if($segment == "vaccine") active @endif">
            <a class="nav-link" href="{{url('/admin/vaccine/list')}}">
                <i class="mdi mdi-medical-bag menu-icon"></i>
                <span class="menu-title">Manage Vaccine</span>
            </a>
        </li>--}}

        {{--<li class="nav-item @if($segment == "disease") active @endif">
            <a class="nav-link" href="{{url('/admin/disease/list')}}">
                <i class="mdi mdi-medical-bag menu-icon"></i>
                <span class="menu-title">Manage Disease</span>
            </a>
        </li>--}}

        {{--<li class="nav-item @if($segment == "customer") active @endif">
            <a class="nav-link" href="{{url('/admin/customer/list')}}">
                <i class="mdi mdi mdi-account menu-icon"></i>
                <span class="menu-title">Manage Customers</span>
            </a>
        </li>--}}

        {{--<li class="nav-item @if($segment == "animal") active @endif">
            <a class="nav-link" href="{{url('/admin/animal/list')}}">
                <i class="mdi mdi-cow menu-icon"></i>
                <span class="menu-title">Manage Animals</span>
            </a>
        </li>--}}

        {{--<li class="nav-item @if($segment == "vaccine") active @endif">
            <a class="nav-link @if($segment == "vaccine") hover-cursor @endif" href="{{url('/admin/vaccine/list')}}">
                <i class="mdi mdi-medical-bag menu-icon"></i>
                <span class="menu-title">Manage Vaccine</span>
            </a>
        </li>--}}
        {{--<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="mdi mdi-circle-outline menu-icon"></i>
                <span class="menu-title">UI Elements</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">Buttons</a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/ui-features/typography.html">Typography</a></li>
                </ul>
            </div>
        </li>--}}
        {{--<li class="nav-item">
            <a class="nav-link" href="pages/forms/basic_elements.html">
                <i class="mdi mdi-view-headline menu-icon"></i>
                <span class="menu-title">Form elements</span>
            </a>
        </li>--}}
        {{--<li class="nav-item">
            <a class="nav-link" href="pages/charts/chartjs.html">
                <i class="mdi mdi-chart-pie menu-icon"></i>
                <span class="menu-title">Charts</span>
            </a>
        </li>--}}
        {{--<li class="nav-item">
            <a class="nav-link" href="pages/tables/basic-table.html">
                <i class="mdi mdi-grid-large menu-icon"></i>
                <span class="menu-title">Tables</span>
            </a>
        </li>--}}
        {{--<li class="nav-item">
            <a class="nav-link" href="pages/icons/mdi.html">
                <i class="mdi mdi-emoticon menu-icon"></i>
                <span class="menu-title">Icons</span>
            </a>
        </li>--}}
        {{--<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
                <i class="mdi mdi-account menu-icon"></i>
                <span class="menu-title">User Pages</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="pages/samples/login.html"> Login </a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/samples/login-2.html"> Login 2 </a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/samples/register.html"> Register </a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/samples/register-2.html"> Register 2 </a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/samples/lock-screen.html"> Lockscreen </a></li>
                </ul>
            </div>
        </li>--}}
        {{--<li class="nav-item">
            <a class="nav-link" href="documentation/documentation.html">
                <i class="mdi mdi-file-document-box-outline menu-icon"></i>
                <span class="menu-title">Documentation</span>
            </a>
        </li>--}}
    </ul>
</nav>