@php
    $segment = Request::segment(2);
    $value = Request::segment(3);

@endphp
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item @if($segment == "dashboard")active @endif">
            <a class="nav-link @if($segment == "dashboard") hover-cursor @endif" href="{{url('/vendor/dashboard')}}">
                <i class="mdi mdi-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>

        @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2')
            <li class="nav-item">
                <a class="nav-link @if($segment == "setting") hover-cursor @endif" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                    <i class="mdi mdi-settings menu-icon"></i>
                    <span class="menu-title">Manage Setting</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse @if($segment == "setting") show @endif" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item"> <a class="nav-link @if($value == "roles") active @endif" href="{{url('/vendor/setting/roles')}}">Manage Roles</a></li>
                        <li class="nav-item"> <a class="nav-link @if($value == "users") active @endif" href="{{url('/vendor/setting/users')}}">Manage Users</a></li>
                    </ul>
                </div>
            </li>
        @endif


        {{--<li class="nav-item @if($segment == "farm")active @endif">
            <a class="nav-link" href="{{url('/admin/farm/list')}}">
                <i class="mdi mdi-flower menu-icon"></i>
                <span class="menu-title">Manage Farms</span>
            </a>
        </li>--}}

        @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.breeds') == true)
            <li class="nav-item @if($segment == "breed")active @endif">
                <a class="nav-link" href="{{url('/vendor/breed/list')}}">
                    <i class="mdi mdi-dna menu-icon"></i>
                    <span class="menu-title">Manage Breed</span>
                </a>
            </li>
        @endif


        @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.vaccine') == true)
        <li class="nav-item @if($segment == "vaccine") active @endif">
            <a class="nav-link" href="{{url('/vendor/vaccine/list')}}">
                <i class="mdi mdi-medical-bag menu-icon"></i>
                <span class="menu-title">Manage Vaccine</span>
            </a>
        </li>
        @endif

        @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.deworming') == true)
        <li class="nav-item @if($segment == "deworming") active @endif">
            <a class="nav-link" href="{{url('/vendor/deworming/agent/list')}}">
                <i class="mdi mdi-medical-bag menu-icon"></i>
                <span class="menu-title">Manage Deworming</span>
            </a>
        </li>
        @endif

        {{--<li class="nav-item">
            <a class="nav-link @if($segment == "deworming") hover-cursor @endif" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="mdi mdi-medical-bag menu-icon"></i>
                <span class="menu-title">Manage Deworming</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse @if($segment == "deworming") show @endif" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link @if($value == "agent") active @endif" href="{{url('/vendor/deworming/agent/list')}}">Add Deworming Agent</a></li>
                    <li class="nav-item"> <a class="nav-link @if($value == "sequence") active @endif" href="{{url('/vendor/deworming/sequence/list')}}">Deworming Sequence</a></li>
                </ul>
            </div>
        </li>--}}
        @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.disease') == true)
            <li class="nav-item @if($segment == "disease") active @endif">
                <a class="nav-link" href="{{url('/vendor/disease/list')}}">
                    <i class="mdi mdi-medical-bag menu-icon"></i>
                    <span class="menu-title">Manage Disease</span>
                </a>
            </li>
        @endif


        @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.customer') == true)
            <li class="nav-item @if($segment == "customer") active @endif">
                <a class="nav-link" href="{{url('/vendor/customer/list')}}">
                    <i class="mdi mdi-account menu-icon"></i>
                    <span class="menu-title">Manage Customers</span>
                </a>
            </li>
        @endif


        {{--<li class="nav-item @if($segment == "animal") active @endif">
            <a class="nav-link" href="{{url('/vendor/animal/list')}}">
                <i class="mdi mdi-cow menu-icon"></i>
                <span class="menu-title">Manage Animals</span>
            </a>
        </li>--}}

        @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.animal') == true || \App\Http\Helpers\CheckPermission::hasPermission('view.exitanimal') == true)
            <li class="nav-item">
                <a class="nav-link @if($segment == "animal") hover-cursor @endif" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                    <i class="mdi mdi-cow menu-icon"></i>
                    <span class="menu-title">Manage Animals</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse @if($segment == "animal") show @endif" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                        @if(\App\Http\Helpers\CheckPermission::hasPermission('view.animal') == true)
                            <li class="nav-item"> <a class="nav-link @if($value == "list") active @endif" href="{{url('/vendor/animal/list')}}">Manage Active Animal</a></li>
                        @endif
                        @if(\App\Http\Helpers\CheckPermission::hasPermission('view.exitanimal') == true)
                             <li class="nav-item"> <a class="nav-link @if($value == "exited") active @endif" href="{{url('/vendor/animal/exited')}}">Manage Exited Animal</a></li>
                        @endif
                    </ul>
                </div>
            </li>
        @endif


        {{--<li class="nav-item @if($segment == "progress") active @endif">
            <a class="nav-link" href="{{url('/vendor/progress/list')}}">
                <i class="mdi mdi-chart-line menu-icon"></i>
                <span class="menu-title">Manage Animals Progress</span>
            </a>
        </li>--}}


        @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.pendingprogress') == true || \App\Http\Helpers\CheckPermission::hasPermission('view.updateprogress') == true || \App\Http\Helpers\CheckPermission::hasPermission('view.progress') == true)
            <li class="nav-item">
                <a class="nav-link @if($segment == "progress") hover-cursor @endif" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                    <i class="mdi mdi-chart-line menu-icon"></i>
                    <span class="menu-title">Manage Animals Progress</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse @if($segment == "progress") show @endif" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                        @if(\App\Http\Helpers\CheckPermission::hasPermission('view.pendingprogress') == true)
                            <li class="nav-item"> <a class="nav-link @if($value == "pending") active @endif" href="{{url('/vendor/progress/pending/list')}}">Manage Pending Progress</a></li>
                        @endif
                        @if(\App\Http\Helpers\CheckPermission::hasPermission('view.updateprogress') == true)
                             <li class="nav-item"> <a class="nav-link @if($value == "capture") active @endif" href="{{url('/vendor/progress/capture/list')}}">Update Animal Progress</a></li>
                        @endif
                        @if(\App\Http\Helpers\CheckPermission::hasPermission('view.progress') == true)
                             <li class="nav-item"> <a class="nav-link @if($value == "view") active @endif" href="{{url('/vendor/progress/view/list')}}">View Animal Progress</a></li>
                        @endif
                    </ul>
                </div>
            </li>
        @endif


        @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.pendingpayment') == true || \App\Http\Helpers\CheckPermission::hasPermission('view.receivedpayment') == true)
            <li class="nav-item">
                <a class="nav-link @if($segment == "payment") hover-cursor @endif" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                    <i class="mdi mdi-cash menu-icon"></i>
                    <span class="menu-title">Manage Payments</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse @if($segment == "payment") show @endif" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                        @if(\App\Http\Helpers\CheckPermission::hasPermission('view.pendingpayment') == true)
                            <li class="nav-item"> <a class="nav-link @if($value == "pending") active @endif" href="{{url('/vendor/payment/pending/list')}}">Manage Pending Payment</a></li>
                        @endif
                        @if(\App\Http\Helpers\CheckPermission::hasPermission('view.receivedpayment') == true)
                             <li class="nav-item"> <a class="nav-link @if($value == "received") active @endif" href="{{url('/vendor/payment/received/list')}}">Manage Received Payment</a></li>
                        @endif
                    </ul>
                </div>
            </li>
        @endif


        @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2')
            <li class="nav-item">
                <a class="nav-link @if($segment == "utility") hover-cursor @endif" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                    <i class="mdi mdi-cash menu-icon"></i>
                    <span class="menu-title">Manage Utility</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse @if($segment == "utility") show @endif" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item"> <a class="nav-link @if($value == "vaccine") active @endif" href="{{url('/vendor/utility/vaccine')}}">Vaccine Utility</a></li>
                    </ul>
                </div>
            </li>
        @endif



        {{--<li class="nav-item @if($segment == "payment") active @endif">
            <a class="nav-link" href="{{url('/vendor/payment/list')}}">
                <i class="mdi mdi-cash menu-icon"></i>
                <span class="menu-title">Manage Payments</span>
            </a>
        </li>--}}


        {{--<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="mdi mdi-circle-outline menu-icon"></i>
                <span class="menu-title">UI Elements</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">Buttons</a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/ui-features/typography.html">Typography</a></li>
                </ul>
            </div>
        </li>--}}
        {{--<li class="nav-item">
            <a class="nav-link" href="pages/forms/basic_elements.html">
                <i class="mdi mdi-view-headline menu-icon"></i>
                <span class="menu-title">Form elements</span>
            </a>
        </li>--}}
        {{--<li class="nav-item">
            <a class="nav-link" href="pages/charts/chartjs.html">
                <i class="mdi mdi-chart-pie menu-icon"></i>
                <span class="menu-title">Charts</span>
            </a>
        </li>--}}
        {{--<li class="nav-item">
            <a class="nav-link" href="pages/tables/basic-table.html">
                <i class="mdi mdi-grid-large menu-icon"></i>
                <span class="menu-title">Tables</span>
            </a>
        </li>--}}
        {{--<li class="nav-item">
            <a class="nav-link" href="pages/icons/mdi.html">
                <i class="mdi mdi-emoticon menu-icon"></i>
                <span class="menu-title">Icons</span>
            </a>
        </li>--}}
        {{--<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
                <i class="mdi mdi-account menu-icon"></i>
                <span class="menu-title">User Pages</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="pages/samples/login.html"> Login </a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/samples/login-2.html"> Login 2 </a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/samples/register.html"> Register </a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/samples/register-2.html"> Register 2 </a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/samples/lock-screen.html"> Lockscreen </a></li>
                </ul>
            </div>
        </li>--}}
        {{--<li class="nav-item">
            <a class="nav-link" href="documentation/documentation.html">
                <i class="mdi mdi-file-document-box-outline menu-icon"></i>
                <span class="menu-title">Documentation</span>
            </a>
        </li>--}}
    </ul>
</nav>