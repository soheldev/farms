<div class="db-head clearfix">
    <div class="bur-menu pull-left">
        <a href="javascript:void(0);"><i class="fa fa-bars"></i></a>
    </div>
    <div class="db-head-menu pull-right">
        <ul class="">
            <li class="db-head-user relative">
                <a href="javascript:void(0);">
                    {{Auth::user()->name}}
                    <span>
                        @if(isset(Auth::user()->userInformation->profile_image))
                            <img src="{{url('/public/farm/'.Auth::user()->userInformation->profile_image)}}" alt="profile"/>
                        @else
                            <img src="{{url('/public/media/backend')}}/images/faces/face5.jpg" alt="profile"/>
                        @endif
                        {{--<img src="{{url('/public/media/backend')}}/assets/images/slide-1.jpeg">--}}
                    </span>
                </a>
                <div class="user-det-menu animate">
                    <ul>
                        <li><a href="{{url('/vendor/profile')}}"> <i class="fa fa-user-o"></i>My Profile</a></li>
                        <li class="divider" role="separator"></li>
                        <li><a href="{{url('/vendor/logout')}}"> <i class="fa fa-key"></i>Log Out</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>