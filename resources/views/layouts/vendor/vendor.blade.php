<html>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Dashboard</title>
<head>
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
<!-- Css File -->
<link rel="stylesheet" type="text/css" href="{{url('/public/media/backend')}}/assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="{{url('/public/media/backend')}}/assets/css/animated.css">
<link rel="stylesheet" type="text/css" href="{{url('/public/media/backend')}}/assets/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="{{url('/public/media/backend')}}/assets/css/owl.theme.css">
<link rel="stylesheet" type="text/css" href="{{url('/public/media/backend')}}/assets/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="{{url('/public/media/backend')}}/assets/css/jquery.bxslider.css">
<link rel="stylesheet" type="text/css" href="{{url('/public/media/backend')}}/assets/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="{{url('/public/media/backend')}}/assets/css/main.css">
<link rel="stylesheet" type="text/css" href="{{url('/public/media/backend')}}/assets/css/responsive.css">
    <link rel="stylesheet" href="{{url('/public/media/backend')}}/assets/css/jquery-ui.css">
    <link rel="stylesheet" href="{{url('/public/media/backend')}}/assets/css/select2.min.css">
</head>
<body>
<div class="main-container relative clearfix">
    @include('layouts.vendor.vendor_side_nav')
    <div class="right-panel pull-left">
        @include('layouts.vendor.vendor_header')
        @yield('content')
    </div>
</div>

<!-- Js scroll Script-->
<script>
    (function($){
        $(window).on("load",function(){
            $(".content").mCustomScrollbar();
        });
    })(jQuery);
</script>
<script src="{{url('/public/media/backend')}}/assets/js/jquery.js"></script>
<script src="{{url('/public/media/backend')}}/assets/js/bootstrap.min.js"></script>
<script src="{{url('/public/media/backend')}}/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="{{url('/public/media/backend')}}/assets/js/wow.js"></script>
<script src="{{url('/public/media/backend')}}/assets/js/owl.carousel.min.js"></script>
<script src="{{url('/public/media/backend')}}/assets/js/jquery.bxslider.min.js"></script>
<script src="{{url('/public/media/backend')}}/assets/js/custom.js"></script>
<script src="{{url('/public/media/backend')}}/assets/js/jquery.validate.js"></script>
<script src="{{url('/public/media/backend')}}/assets/js/jquery-ui.js"></script>
<script src="{{url('/public/media/backend')}}/assets/js/sweet-alert.js"></script>
<script src="{{url('/public/media/backend')}}/assets/js/select2.min.js"></script>
@yield('jcontent')
</body>
</html>