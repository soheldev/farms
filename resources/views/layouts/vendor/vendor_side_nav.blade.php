@php
    $segment = Request::segment(2);
    $value = Request::segment(3);

@endphp

<div class="left-panel pull-left">
    <div class="logo">
        <a href="{{url("/vendor/dashboard")}}">{{Auth::user()->name}}</a>
    </div>
    <div class="db-menu-list">
        <ul>
            <li class="active">
                <a href="{{url("/vendor/dashboard")}}"><i class="fa fa-home"></i> Home</a>
            </li>
            @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2')
                <li>
                    <div class="panel-menu panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseOne">
                                <i class="fa fa-cog"></i> Manage Setting
                            </a>
                            <div id="collapseOne" class="panel-collapse @if($segment == "setting") collapse in @else collapse @endif" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="{{url('/vendor/setting/roles')}}"> Manage Roles</a></li>
                                        <li><a href="{{url('/vendor/setting/users')}}"> Manage Users</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endif

            @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.breeds') == true)
                <li>
                    <a href="{{url('/vendor/breed/list')}}"><i class="fa fa-users"></i> Manage Breed</a>
                </li>
            @endif

            @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.vaccine') == true)
                <li>
                    <a href="{{url('/vendor/vaccine/list')}}"><i class="fa fa-medkit"></i> Manage Vaccine</a>
                </li>
            @endif

            @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.deworming') == true)
                <li>
                    <a href="{{url('/vendor/deworming/agent/list')}}"><i class="fa fa-heartbeat"></i> Manage Deworming</a>
                </li>
            @endif

            @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.disease') == true)
                <li>
                    <a href="{{url('/vendor/disease/list')}}"><i class="fa fa-stethoscope"></i> Manage Disease</a>
                </li>
            @endif

            @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.customer') == true)
                <li>
                    <a href="{{url('/vendor/customer/list')}}"><i class="fa fa-user-circle-o"></i> Manage Customers</a>
                </li>
            @endif

            @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.animal') == true || \App\Http\Helpers\CheckPermission::hasPermission('view.exitanimal') == true)
                <li>
                    <div class="panel-menu panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <a data-toggle="collapse" class="collapsed" data-parent="#accordion1" href="#collapseOne1">
                                <i class="fa fa-users"></i> Manage Animal
                            </a>
                            <div id="collapseOne1" class="panel-collapse @if($segment == "animal") collapse in @else collapse @endif" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        @if(\App\Http\Helpers\CheckPermission::hasPermission('view.animal') == true)
                                            <li><a href="{{url('/vendor/animal/list')}}"> Manage Active Animal</a></li>
                                        @endif
                                        @if(\App\Http\Helpers\CheckPermission::hasPermission('view.exitanimal') == true)
                                             <li><a href="{{url('/vendor/animal/exited')}}"> Manage Exited Animal</a></li>
                                        @endif

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endif

            @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.pendingprogress') == true || \App\Http\Helpers\CheckPermission::hasPermission('view.updateprogress') == true || \App\Http\Helpers\CheckPermission::hasPermission('view.progress') == true)
                <li>
                    <div class="panel-menu panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <a data-toggle="collapse" class="collapsed" data-parent="#accordion2" href="#collapseOne2">
                                <i class="fa fa-line-chart"></i> Manage Animals Progress
                            </a>
                            <div id="collapseOne2" class="panel-collapse @if($segment == "progress") collapse in @else collapse @endif" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        @if(\App\Http\Helpers\CheckPermission::hasPermission('view.pendingprogress') == true)
                                            <li><a href="{{url('/vendor/progress/pending/list')}}"> Manage Pending Progress</a></li>
                                        @endif
                                        @if(\App\Http\Helpers\CheckPermission::hasPermission('view.updateprogress') == true)
                                            <li><a href="{{url('/vendor/progress/capture/list')}}"> Update Animal Progress</a></li>
                                        @endif
                                        @if(\App\Http\Helpers\CheckPermission::hasPermission('view.progress') == true)
                                             <li><a href="{{url('/vendor/progress/view/list')}}"> View Animal Progress</a></li>
                                        @endif

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endif

            @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.pendingpayment') == true || \App\Http\Helpers\CheckPermission::hasPermission('view.receivedpayment') == true)
                <li>
                    <div class="panel-menu panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <a data-toggle="collapse" class="collapsed" data-parent="#accordion3" href="#collapseOne3">
                                <i class="fa fa-money"></i> Manage Payments
                            </a>
                            <div id="collapseOne3" class="panel-collapse @if($segment == "payment") collapse in @else collapse @endif" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        @if(\App\Http\Helpers\CheckPermission::hasPermission('view.pendingpayment') == true)
                                            <li><a href="{{url('/vendor/payment/pending/list')}}"> Manage Pending Payment</a></li>
                                        @endif
                                        @if(\App\Http\Helpers\CheckPermission::hasPermission('view.receivedpayment') == true)
                                            <li><a href="{{url('/vendor/payment/received/list')}}"> Manage Received Payment</a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endif

            @if(\Illuminate\Support\Facades\Auth::user()->user_type == '2')
                <li>
                    <div class="panel-menu panel-group" id="accordion4" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <a data-toggle="collapse" class="collapsed" data-parent="#accordion4" href="#collapseOne4">
                                <i class="fa fa-bars"></i> Manage Utility
                            </a>
                            <div id="collapseOne4" class="panel-collapse @if($segment == "utility") collapse in @else collapse @endif" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="{{url('/vendor/utility/vaccine')}}"> Vaccine Utility</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

            @endif

            {{--<li>
                <div class="panel-menu panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                    <div class="panel">
                        <a data-toggle="collapse" class="collapsed" data-parent="#accordion1" href="#collapseOne1">
                            <i class="fa fa-home"></i> Manage Animal
                        </a>
                        <div id="collapseOne1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <ul>
                                    <li><a href="table-list.html"> Manage Active Animal</a></li>
                                    <li><a href=""> Manage Exited Animal</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </li>--}}
            {{--<li>
                <a href="javascript:void(0);"><i class="fa fa-home"></i> Manage Vaccine</a>
            </li>--}}
            {{--<li>
                <a href="javascript:void(0);"><i class="fa fa-home"></i> Manage Customers</a>
            </li>--}}
        </ul>
    </div>
</div>