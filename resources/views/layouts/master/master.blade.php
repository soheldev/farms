<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Master Dashboard</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{url('/public/media/backend')}}/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="{{url('/public/media/backend')}}/vendors/base/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="{{url('/public/media/backend')}}/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{url('/public/media/backend')}}/css/style.css">
    <link rel="stylesheet" href="{{url('/public/media/backend')}}/css/jquery-ui.css">
    <link rel="stylesheet" href="{{url('/public/media/backend')}}/css/select2.min.css">
    {{--<link rel="stylesheet" href="{{url('/public/media/backend')}}/css/font-awesome.min.css">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{url('/public/media/backend')}}/images/favicon.png" />
</head>
<body>

<style>
    .btn {
        line-height: 0 !important;
    }
</style>

<div class="container-scroller">
    @include('layouts.master.master_header')
    <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:partials/_sidebar.html -->
            @include('layouts.master.master_side_nav')
        <!-- partial -->
            {{--<a class="nav-link" id="sales-tab" data-toggle="tab" href="#sales" role="tab" aria-controls="sales" aria-selected="false">Sales</a>--}}
            <div class="main-panel">
                @yield('content')
                @include('layouts.master.master_footer')
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->



<!-- plugins:js -->
<script src="{{url('/public/media/backend')}}/vendors/base/vendor.bundle.base.js"></script>
<!-- endinject -->
<script src="{{url('/public/media/backend')}}/js/jquery-3.4.1.js"></script>
<script src="{{url('/public/media/backend')}}/js/jquery-ui.js"></script>
<script src="{{url('/public/media/backend')}}/js/select2.min.js"></script>
<!-- Plugin js for this page-->
<script src="{{url('/public/media/backend')}}/vendors/chart.js/Chart.min.js"></script>
<script src="{{url('/public/media/backend')}}/vendors/datatables.net/jquery.dataTables.js"></script>
<script src="{{url('/public/media/backend')}}/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{url('/public/media/backend')}}/js/off-canvas.js"></script>
<script src="{{url('/public/media/backend')}}/js/hoverable-collapse.js"></script>
<script src="{{url('/public/media/backend')}}/js/template.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{url('/public/media/backend')}}/js/dashboard.js"></script>
<script src="{{url('/public/media/backend')}}/js/data-table.js"></script>
<script src="{{url('/public/media/backend')}}/js/jquery.dataTables.js"></script>
<script src="{{url('/public/media/backend')}}/js/dataTables.bootstrap4.js"></script>
<script src="{{url('/public/media/backend')}}/js/jquery.validate.js"></script>
<script src="{{url('/public/media/backend')}}/js/sweet-alert.js"></script>
<!-- End custom js for this page-->
@yield('jcontent')
</body>

</html>