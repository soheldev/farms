<?php

namespace Sohel\Banner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sohel\Banner\Model\Banner;

class BannerController extends Controller
{
    //
    public function index()
    {
        return view('banner::banner-list');
    }

    public function fetchBannerData(Request $request)
    {
        $data = Banner::skip($request->skip)->limit(5)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function createBanner(Request $request)
    {
        if($request->method() == 'GET')
        {
            return view('banner::banner-add');
        }
        else
        {
            $create_banner_image = new Banner();
            if($request->hasFile('banner_image'))
            {
                if (! \File::exists(public_path()."/banner")) {
                    \File::makeDirectory(public_path()."/banner");
                }
                $imageName = time().'.'.request()->banner_image->getClientOriginalExtension();
                request()->banner_image->move(public_path('/banner/'), $imageName);
                $create_banner_image->image = $imageName;
            }
            $create_banner_image->title = $request->title;
            $create_banner_image->save();
            return redirect('/admin/setting/banner')->with('success','Banner image inserted successfully...');
        }
    }

    public function updateBanner(Request $request, $id)
    {
        $update_banner_image = Banner::find($id);
        if($request->method() == 'GET')
        {
            return view('banner::banner-edit',compact('update_banner_image'));
        }
        else
        {
            if($request->hasFile('banner_image'))
            {
                $old_image = $update_banner_image->image;
                $imageName = time().'.'.request()->banner_image->getClientOriginalExtension();
                request()->banner_image->move(public_path('/banner/'), $imageName);
                $update_banner_image->image = $imageName;
                unlink(public_path('/banner/'.$old_image));
            }
            $update_banner_image->title = $request->title;
            $update_banner_image->save();
            return redirect('/admin/setting/banner')->with('success','Banner image updated successfully...');
        }
    }

    public function deleteBanner(Request $request)
    {
        $id = base64_decode($request->id);
        if(isset($id))
        {
            $delete_banner_image = Banner::find($id);
            if(isset($delete_banner_image))
            {
                $delete_image = $delete_banner_image->image;
                unlink(public_path('/banner/'.$delete_image));
                if($delete_banner_image->delete())
                {
                    $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Banner image deleted successfully...');
                    return json_encode($result);
                }
                else
                {
                    $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something went wrong...');
                    return json_encode($result);
                }

            }
        }
    }
}
