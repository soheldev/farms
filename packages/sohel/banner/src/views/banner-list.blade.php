@extends('layouts.admin.admin')
@section('content')
    <style>
        .my-custom-scrollbar {
            position: relative;
            height: 400px;
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>

    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-muted mb-0 hover-cursor" href="{{url('/admin/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-primary mb-0 hover-cursor">List Banner Images</p>
                        </div>
                        <br>
                        <h4 class="card-title">List Banner Images</h4>
                        {{--<p class="card-description">
                            Add class <code>.table-bordered</code>
                        </p>--}}
                        <div class="pull-right">
                            <a type="button" class="btn btn-success btn-rounded" href="{{url('/admin/setting/banner/add')}}">Add New</a>
                        </div>
                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Update</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>

                                <tbody id="banner_image_table">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('jcontent')
    <script>
        var site_path = '{{url('/')}}';
        var skip = 0;
        var i = 0;
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/banner-image-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {
                            html_content += '<tr>';
                            html_content += '<td>'+this.id+'</td>';
                            html_content += '<td><img src="'+site_path+'/public/banner/'+this.image+'"></td>';
                            html_content += '<td>'+this.title+'</td>';
                            html_content += '<td><a type="button" href="'+site_path+'/admin/setting/banner/edit/'+this.id+'" class="btn btn-primary btn-rounded">Update</a></td>';
                            html_content += '<td><a type="button" data-attribute-id="'+this.id+'" class="btn btn-danger btn-rounded delete">Delete</a></td>';
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="5"><center>No Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#banner_image_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 5;
                    fetchRecord(skip);
                }
            };
            $('body').on('click','.delete',function () {
                var id = btoa($(this).attr('data-attribute-id'));
                var _this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to delete this banner image!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#71c016',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function(result){
                    if (result.value) {

                        $.ajax({
                            url: site_path + '/delete/banner-image',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                id: id,
                            },
                            success: function (response) {
                                if(response.status == '1')
                                {
                                    _this.parent('td').parent('tr').remove();
                                }
                                Swal.fire({
                                    icon: response.icon,
                                    title: response.title,
                                    text: response.text,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            },
                            beforeSend: function () {

                            },
                            complete: function () {

                            },
                            error: function () {

                            }


                        });
                    }
            })
            });
        });
    </script>
@endsection