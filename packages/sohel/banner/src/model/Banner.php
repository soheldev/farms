<?php

namespace Sohel\Banner\Model;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //
    protected $table = 'banners';
}
