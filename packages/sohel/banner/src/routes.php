<?php
Route::get('/admin/setting/banner','Sohel\Banner\BannerController@index')->middleware(['web','auth']);
Route::post('/fetch/banner-image-data','Sohel\Banner\BannerController@fetchBannerData')->middleware(['web','auth']);
Route::get('/admin/setting/banner/add','Sohel\Banner\BannerController@createBanner')->middleware(['web','auth']);
Route::post('/admin/setting/banner/add','Sohel\Banner\BannerController@createBanner')->middleware(['web','auth']);
Route::get('/admin/setting/banner/edit/{id}','Sohel\Banner\BannerController@updateBanner')->middleware(['web','auth']);
Route::post('/admin/setting/banner/edit/{id}','Sohel\Banner\BannerController@updateBanner')->middleware(['web','auth']);
Route::post('/delete/banner-image','Sohel\Banner\BannerController@deleteBanner')->middleware(['web','auth']);