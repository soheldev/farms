@extends('layouts.admin.admin')

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url('/admin/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url('/admin/farm/list')}}">List Farm&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Edit Farm</p>
                        </div>
                        <br>
                        <form class="forms-sample" id="update_farm_form" method="POST" action="" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="exampleInputName1">Farm Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="farm_name" name="farm_name" placeholder="Enter Farm Name" value="{{$edit_farm->users->name}}" autocomplete="off">
                                @error('farm_name')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Farm Owner First Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="farm_owner_first_name" name="farm_owner_first_name" placeholder="Enter Farm Owner First Name" value="{{$edit_farm->first_name}}" autocomplete="off">
                                @error('farm_owner_first_name')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Farm Owner Last Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="farm_owner_last_name" name="farm_owner_last_name" placeholder="Enter Farm Owner Last Name" value="{{$edit_farm->last_name}}" autocomplete="off">
                                @error('farm_owner_last_name')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Contact Number<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="Enter Contact Number" value="{{$edit_farm->mobile_number}}" autocomplete="off">
                                @error('contact_number')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Email Id<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="email_id" name="email_id" placeholder="Enter Email Id" value="{{$edit_farm->users->email}}" autocomplete="off">
                                @error('email_id')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Monthly Rent<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="monthly_rent" name="monthly_rent" placeholder="Enter Monthly Rent Amount" value="{{$edit_farm->monthly_rent}}" autocomplete="off">
                                @error('monthly_rent')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Address Line 1</label>
                                <textarea class="form-control" id="address_line_one" name="address_line_one" placeholder="Enter Address Line One" value="" rows="5">{{$edit_farm->address_line_one}}</textarea>
                                @error('address_line_one')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Address Line 2</label>
                                <textarea class="form-control" id="address_line_two" name="address_line_two" placeholder="Enter Address Line Two" value="" rows="5">{{$edit_farm->address_line_two}}</textarea>
                                @error('address_line_two')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Pin Code</label>
                                <input type="text" class="form-control" id="pin_code" name="pin_code" placeholder="Enter Pin Code" value="{{$edit_farm->pin_code}}" autocomplete="off">
                                @error('pin_code')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Country<sup style="color: red">*</sup></label>
                                <select class="form-control" id="country_id" name="country_id">
                                    <option value="">Select Country</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}" @if($country->id == $edit_farm->country_id) selected @endif>{{$country->country_name}}</option>
                                    @endforeach
                                </select>
                                @error('country_id')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">State<sup style="color: red">*</sup></label>
                                <select class="form-control" id="state_id" name="state_id">
                                    <option value="">Select State</option>
                                    @foreach($states as $state)
                                        <option value="{{$state->id}}" @if($state->id == $edit_farm->state_id) selected @endif>{{$state->state_name}}</option>
                                    @endforeach
                                </select>
                                @error('state_id')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">City<sup style="color: red">*</sup></label>
                                <select class="form-control" id="city_id" name="city_id">
                                    <option value="">Select City</option>
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}" @if($city->id == $edit_farm->city_id) selected @endif>{{$city->city_name}}</option>
                                    @endforeach
                                </select>
                                @error('city_id')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword4">Profile Picture</label>
                                <input type="file" class="form-control" id="profile_picture" name="profile_picture" accept=".jpeg, .jpg, .png">
                                <br>
                                <label>Preview</label><br>
                                @if(isset($edit_farm->profile_image))
                                    <img id="preview_profile_picture" height="150px" width="150px" src="{{url('/public/farm/'.$edit_farm->profile_image)}}">
                                @else
                                    <img id="preview_profile_picture" height="150px" width="150px" src="{{url('/public/media/backend/images/no-image.jpg')}}">
                                @endif
                                @error('profile_picture')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary mr-2" id="farm_update_btn"><i id="farm_update_loder" style="font-size:15px"></i>  Update</button>
                            <button type="button" class="btn btn-light" id="reset_update_farm_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        var previous_state_id = '{{$edit_farm->state_id}}';
        var previous_city_id = '{{$edit_farm->city_id}}';
        $(function ()
        {
            $('#reset_update_farm_form').click(function () {
                $('#update_farm_form')[0].reset();
            });

            $('#profile_picture').change(function () {
                var uploaded_image = window.URL.createObjectURL(this.files[0]);
                $('#preview_profile_picture').attr('src',uploaded_image);
            });

            $("#farm_owner_first_name").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 65 || key > 90)) {
                    return false;
                }
            });

            $("#farm_owner_last_name").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 65 || key > 90)) {
                    return false;
                }
            });

            $("#contact_number").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105)) {
                    return false;
                }
            });

            $("#monthly_rent").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105)) {
                    return false;
                }
            });

            $('#update_farm_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'farm_name':{
                        required:true,
                    },
                    'farm_owner_first_name':{
                        required:true,
                    },
                    'farm_owner_last_name':{
                        required:true,
                    },
                    'contact_number':{
                        required:true,
                        digits:true,
                        minlength:10,
                        maxlength:10,
                        remote: {
                            url: javascript_site_path+"/admin/check-contact",
                            type: "post",
                            data:{
                                id:'{{$edit_farm->user_id}}',
                                flag:'2'
                            }
                        }
                    },
                    'email_id':{
                        required:true,
                        email:true,
                        remote: {
                            url: javascript_site_path+"/admin/check-email",
                            type: "post",
                            data:{
                                id:'{{$edit_farm->user_id}}',
                                flag:'2'
                            }
                        }
                    },
                    'monthly_rent':{
                        required:true,
                        digits:true,
                    },
                    'country_id':{
                        required:true,
                    },
                    'state_id':{
                        required:true,
                    },
                    'city_id':{
                        required:true,
                    },

                },
                messages:{
                    'farm_name':{
                        required:"Please enter farm name.",
                    },
                    'farm_owner_first_name':{
                        required:"Please enter farm owner first name.",
                    },
                    'farm_owner_last_name':{
                        required:"Please enter farm owner last name.",
                    },
                    'contact_number':{
                        required:"Please enter contact number.",
                        digits:"Please enter valid contact number.",
                        minlength:"Contact number should not be less than 10 digit.",
                        maxlength:"Contact number should not be greater than 10 digit.",
                        remote:"Contact number already exits."
                    },
                    'email_id':{
                        required:"Please enter email id.",
                        email:"Please enter valid email id.",
                        remote:"Email id already exits."
                    },
                    'monthly_rent':{
                        required:"Please enter monthly rental amount.",
                        digits:"Please enter valid rental amount."
                    },
                    'country_id':{
                        required:"Please select country.",
                    },
                    'state_id':{
                        required:"Please select state.",
                    },
                    'city_id':{
                        required:"Please select city.",
                    },
                },
                submitHandler:function (form)
                {
                    $('#farm_update_loder').addClass("fa fa-spinner fa-pulse");
                    $('#farm_update_btn').attr('disabled',true);
                    form.submit();
                }
            });

            function fetchStates(country_id)
            {
                $.ajax({
                    url: javascript_site_path+'/admin/fetch/state-according-to-country',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        country_id: country_id
                    },
                    success:function(response)
                    {
                        var html_content = '';
                        if(response.status == "1")
                        {
                            html_content += '<option value="">Select State</option>';
                            $.each(response.data,function (key,value)
                            {
                                if(value.id == previous_state_id)
                                {
                                    fetchCity(previous_state_id);
                                    html_content += '<option value="'+value.id+'" selected>'+value.state_name+'</option>';
                                }
                                else
                                {
                                    html_content += '<option value="'+value.id+'">'+value.state_name+'</option>';
                                }
                            });
                            $('#state_id').html(html_content);
                        }
                        else
                        {
                            $('#state_id').html('<option value="">No States Found...</option>');
                        }
                    },
                    beforeSend:function () {
                        $('#state_id').html('<option value="">Fetching States...</option>');
                    },
                    complete:function () {

                    },
                    error:function () {

                    }
                });
            }

            function fetchCity(state_id)
            {
                $.ajax({
                    url: javascript_site_path+'/admin/fetch/city-according-to-state',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        state_id: state_id
                    },
                    success:function(response)
                    {
                        var html_content = '';
                        if(response.status == "1")
                        {
                            html_content += '<option value="">Select City</option>';
                            $.each(response.data,function (key,value)
                            {
                                if(value.id == previous_city_id)
                                {
                                    html_content += '<option value="'+value.id+'" selected>'+value.city_name+'</option>';
                                }
                                else
                                {
                                    html_content += '<option value="'+value.id+'">'+value.city_name+'</option>';
                                }
                            });
                            $('#city_id').html(html_content);
                        }
                        else
                        {
                            $('#city_id').html('<option value="">No Cities Found...</option>');
                        }
                    },
                    beforeSend:function () {
                        $('#city_id').html('<option value="">Fetching Cities...</option>');
                    },
                    complete:function () {

                    },
                    error:function () {

                    }
                });
            }

            $('#country_id').change(function () {
                $('#state_id').html('<option value="">Select State</option>');
                $('#city_id').html('<option value="">Select City</option>');
                var country_id = $(this).val();
                fetchStates(country_id)
            });

            $('#state_id').change(function () {
                var state_id = $(this).val();
                fetchCity(state_id);
            });

        });

    </script>
@endsection