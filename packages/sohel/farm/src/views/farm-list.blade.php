@extends('layouts.admin.admin')
@section('content')
    <style>
        .my-custom-scrollbar {
            position: relative;
            height: 400px;
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>

    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url('/admin/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">List Farm</p>
                        </div>
                        <br>
                        <div class="pull-right">
                            <a type="button" class="btn btn-success btn-rounded" href="{{url('/admin/farm/add')}}">Add New</a>
                        </div>
                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>Farm Id</th>
                                    <th>Farm Name</th>
                                    <th>Farm Owner Name</th>
                                    <th>Email Id</th>
                                    <th>Contact No</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                    {{--<th>Update</th>
                                    <th>Delete</th>--}}
                                </tr>
                                </thead>

                                <tbody id="farm_table">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('jcontent')
    <script>
        var site_path = '{{url('/')}}';
        var skip = 0;
        var i = 0;
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/farm-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip
                },
                success:function (response) {
                    console.log(response);
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td><a href="'+site_path+'/admin/farm/view/'+btoa(this.id)+'">'+this.user_unique_id+'</a></td>';
                            html_content += '<td>'+this.users.name+'</td>';
                            html_content += '<td>'+this.first_name+' '+this.last_name+'</td>';
                            html_content += '<td>'+this.users.email+'</td>';
                            html_content += '<td>'+this.mobile_number+'</td>';
                            if(this.user_status == '0')
                            {
                                html_content += '<td><button data-attribute-id="'+this.id+'" data-attribute-status="1" class="btn btn-warning status">Pending</button></td>';
                            }
                            if(this.user_status == '1')
                            {
                                html_content += '<td><button data-attribute-id="'+this.id+'" data-attribute-status="2" class="btn btn-success status">Active</button></td>';
                            }
                            if(this.user_status == '2')
                            {
                                html_content += '<td><button data-attribute-id="'+this.id+'" data-attribute-status="0" class="btn btn-danger status">Blocked</button></td>';
                            }
                            html_content += '<td><center><a title="Edit" href="'+site_path+'/admin/farm/edit/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; <a title="delete" href="javascript:void(0)" class="farm_delete" data-attribute-id="'+this.user_id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></center></td>';

                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="7"><center>No Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#farm_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 5;
                    fetchRecord(skip);
                }
            };
            $('body').on('click','.farm_delete',function () {
                var id = btoa($(this).attr('data-attribute-id'));
                var _this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to delete this farm.!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#71c016',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function(result){
                    if (result.value) {

                        $.ajax({
                            url: site_path + '/delete/farm',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                id: id,
                            },
                            success: function (response) {
                                if(response.status == '1')
                                {
                                    _this.parent('center').parent('td').parent('tr').remove();
                                    if($('#farm_table tr').length == 0)
                                    {
                                        $('#farm_table').append('<tr><td colspan="7" class="no_record_found_row"><center>No Record Found...</center></td></tr>');
                                    }
                                }
                                Swal.fire({
                                    icon: response.icon,
                                    title: response.title,
                                    text: response.text,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            },
                            beforeSend: function () {

                            },
                            complete: function () {

                            },
                            error: function () {

                            }


                        });
                    }
                });
            });

            $('body').on('click','.status',function () {
                var id = $(this).attr('data-attribute-id');
                var status = $(this).attr('data-attribute-status');
                var _this = $(this);
                $.ajax({
                    url: site_path+'/change/status',
                    type: 'POST',
                    dataType: 'JSON',
                    data:{
                        id:id,
                        status:status
                    },
                    success:function (response) {
                        if(status == '0')
                        {
                            _this.removeAttr('class').addClass('btn btn-warning status').text('Pending').attr('data-attribute-status','1');

                        }
                        if(status == '1')
                        {
                            _this.removeAttr('class').addClass('btn btn-success status').text('Active').attr('data-attribute-status','2');
                        }
                        if(status == '2')
                        {
                            _this.removeAttr('class').addClass('btn btn-danger status').text('Blocked').attr('data-attribute-status','0');
                        }
                        Swal.fire({
                            icon: response.icon,
                            title: response.title,
                            text: response.text,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                });
            });
        });
    </script>
@endsection