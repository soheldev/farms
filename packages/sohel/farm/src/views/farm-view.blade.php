@extends('layouts.admin.admin')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url('/admin/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url('/admin/farm/list')}}">List Farm&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">View Farm</p>
                        </div>
                        <br>

                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th rowspan="8">
                                        <div class="text-center">
                                            @if(isset($view_farm->profile_image))
                                                <img style="border-radius: 50%" height="150px" width="150px" src="{{url('/public/farm/'.$view_farm->profile_image)}}">
                                            @else
                                                <img style="border-radius: 50%" height="150px" width="150px" src="{{url('/public/media/backend/images/no-image.jpg')}}">
                                            @endif
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                    </th>
                                    <th>
                                        Farm Id : <span>#{{$view_farm->user_unique_id}}</span>
                                    </th>
                                    <th>
                                        Farm Name : <span>{{$view_farm->users->name}}</span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>Farm Owner Name : <span>{{$view_farm->first_name}} {{$view_farm->last_name}}</span></th>
                                    <th>Email Id : <span>{{$view_farm->users->email}}</span></th>
                                </tr>
                                <tr>
                                    <th>Contact Number : <span>{{$view_farm->mobile_number}}</span></th>
                                    <th>Address Line 1 : <span>{{isset($view_farm->address_line_one)?$view_farm->address_line_one:'N/A'}}</span></th>
                                </tr>
                                <tr>
                                    <th>Address Line 2 : <span>{{isset($view_farm->address_line_two)?$view_farm->address_line_two:'N/A'}}</span></th>
                                    <th>City : <span>{{isset($view_farm->userCity->city_name)?$view_farm->userCity->city_name:'N/A'}}</span></th>
                                </tr>
                                <tr>
                                    <th>State : <span>{{isset($view_farm->userState->state_name)?$view_farm->userState->state_name:'N/A'}}</span></th>
                                    <th>Country : <span>{{isset($view_farm->userCountry->country_name)?$view_farm->userCountry->country_name:'N/A'}}</span></th>
                                </tr>
                                <tr>
                                    <th>Pin Code : <span>{{isset($view_farm->pin_code)?$view_farm->pin_code:'N/A'}}</span></th>
                                    <th>
                                        Status :
                                        @switch($view_farm->user_status)
                                            @case('0')
                                            <span>Pending</span>
                                            @break

                                            @case('1')
                                            <span>Active</span>
                                            @break

                                            @case('2')
                                            <span>Blocked</span>
                                            @break

                                            @default
                                            <span>N/A</span>
                                        @endswitch
                                    </th>
                                </tr>
                                <tr>
                                    <th>Monthly Rental :- <span>Rs {{isset($view_farm->monthly_rent)?$view_farm->monthly_rent.'/- (Per Animal)*':'N/A'}}</span></th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>




                        {{--<div class="row col-12">
                            <div class="col-6">
                                @if(isset($view_farm->profile_image))
                                    <img height="150px" width="150px" src="{{url('/public/farm/'.$view_farm->profile_image)}}">
                                @else
                                    <img height="150px" width="150px" src="{{url('/public/media/backend/images/no-image.jpg')}}">
                                @endif
                            </div>
                        </div>
                        <br>
                        <div class="row col-12">
                            <div class="col-6">
                                <p><h4>Farm Id :- <span>#{{$view_farm->user_unique_id}}</span></h4></p>
                            </div>
                            <div class="col-6">
                                <p><h4>Farm Name :- <span>{{$view_farm->users->name}}</span></h4></p>
                            </div>
                        </div>
                        <br>
                        <div class="row col-12">
                            <div class="col-6">
                                <p><h4>Farm Owner Name:- <span>{{$view_farm->first_name}} {{$view_farm->last_name}}</span></h4></p>
                            </div>
                            <div class="col-6">
                                <p><h4>Email Id :- <span>{{$view_farm->users->email}}</span></h4></p>
                            </div>

                        </div>
                        <br>
                        <div class="row col-12">
                            <div class="col-6">
                                <p><h4>Contact Number :- <span>{{$view_farm->mobile_number}}</span></h4></p>
                            </div>
                            <div class="col-6">
                                <p><h4>Address Line 1 :- <span>{{isset($view_farm->address_line_one)?$view_farm->address_line_one:'N/A'}}</span></h4></p>
                            </div>
                        </div>
                        <br>
                        <div class="row col-12">
                            <div class="col-6">
                                <p><h4>Address Line 2 :- <span>{{isset($view_farm->address_line_two)?$view_farm->address_line_two:'N/A'}}</span></h4></p>
                            </div>
                            <div class="col-6">
                                <p><h4>City :- <span>{{isset($view_farm->userCity->city_name)?$view_farm->userCity->city_name:'N/A'}}</span></h4></p>
                            </div>
                        </div>
                        <br>
                        <div class="row col-12">
                            <div class="col-6">
                                <p><h4>State :- <span>{{isset($view_farm->userState->state_name)?$view_farm->userState->state_name:'N/A'}}</span></h4></p>
                            </div>
                            <div class="col-6">
                                <p><h4>Country :- <span>{{isset($view_farm->userCountry->country_name)?$view_farm->userCountry->country_name:'N/A'}}</span></h4></p>
                            </div>
                        </div>
                        <br>
                        <div class="row col-12">
                            <div class="col-6">
                                <p><h4>Pin Code :- <span>{{isset($view_farm->pin_code)?$view_farm->pin_code:'N/A'}}</span></h4></p>
                            </div>
                            <div class="col-6">
                                <p>
                                <h4>Status :-
                                    @switch($view_farm->user_status)
                                        @case('0')
                                        <span>Pending</span>
                                        @break

                                        @case('1')
                                        <span>Active</span>
                                        @break

                                        @case('2')
                                        <span>Blocked</span>
                                        @break

                                        @default
                                        <span>N/A</span>
                                    @endswitch
                                </h4>
                                </p>
                            </div>
                        </div>
                        <br>
                        <div class="row col-12">
                            <div class="col-6">
                                <p><h4>Monthly Rental :- <span>Rs {{isset($view_farm->monthly_rent)?$view_farm->monthly_rent.'/- (Per Animal)*':'N/A'}}</span></h4></p>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')

@endsection