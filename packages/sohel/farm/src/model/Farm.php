<?php

namespace Sohel\Vaccine\Model;

use Illuminate\Database\Eloquent\Model;

class Vaccine extends Model
{
    //
    protected $table = 'vaccines';
    protected $fillable = ['vaccine_id','vaccine_name','disease','notes'];
}
