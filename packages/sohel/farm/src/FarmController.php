<?php

namespace Sohel\Farm;

use App\Http\Controllers\Controller;
use App\Http\Helpers\UniqueId;
use App\User;
use App\UserInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Sohel\City\Model\City;
use Sohel\Country\Model\Country;
use Sohel\State\Model\State;

class FarmController extends Controller
{
    //
    public function index()
    {
        return view('farm::farm-list');
    }

    public function fetchFarmData(Request $request)
    {
        $data = UserInformation::skip($request->skip)->limit(5)->where('user_type','2')->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function changeStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
        $change_status = UserInformation::find($id);
        $change_status->user_status = $status;
        $change_status->save();
        $result = array('status'=>'1','icon' => 'success','title' => 'Success','text'=>'Status Changed Successfully...');
        return json_encode($result);
    }

    public function fetchcityAccordingToState(Request $request)
    {
        $cities_arr = [];
        $state_id = $request->state_id;
        $cities = City::where('state_id',$state_id)->get();
        if($cities->count() > 0)
        {
            foreach($cities as $key => $city)
            {
                $cities_arr[$key]['id'] = $city->id;
                $cities_arr[$key]['city_name'] = $city->city_name;
            }
            $result = array('status' => '1', 'data' => $cities_arr);
        }
        else
        {
            $result = array('status' => '0', 'data' => $cities_arr);
        }
        return json_encode($result);
    }

    public function checkEmail(Request $request)
    {
        $email_id = $request->email_id;
        $flag = $request->flag;
        if($flag == "1")
        {
            if(isset($email_id))
            {
                $check_email = User::where('email',$email_id)->first();
                if(isset($check_email))
                {
                    return 'false';
                }
                else
                {
                    return 'true';
                }
            }
        }
        else
        {
            if(isset($email_id))
            {
                $id = $request->id;
                $check_email = User::where('email',$email_id)->where('id','<>',$id)->first();
                if(isset($check_email))
                {
                    return 'false';
                }
                else
                {
                    return 'true';
                }
            }
        }

    }

    public function checkContact(Request $request)
    {
        $contact = $request->contact_number;
        $flag = $request->flag;
        if($flag == "1")
        {
            if(isset($contact))
            {
                $check_contact = UserInformation::where('mobile_number',$contact)->first();
                if(isset($check_contact))
                {
                    return 'false';
                }
                else
                {
                    return 'true';
                }
            }
        }
        else
        {
            if(isset($contact))
            {
                $id = $request->id;
                $check_contact = UserInformation::where('mobile_number',$contact)->where('user_id','<>',$id)->first();
                if(isset($check_contact))
                {
                    return 'false';
                }
                else
                {
                    return 'true';
                }
            }
        }
    }

    public function createFarm(Request $request)
    {
        if($request->method() == "GET")
        {
            $countries = Country::all('id','country_name');
            return view('farm::farm-add',compact('countries'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'farm_name' => 'required',
                'farm_owner_first_name' => 'required',
                'farm_owner_last_name' => 'required',
                'contact_number' => 'required',
                'email_id' => 'required',
                'monthly_rent' => 'required',
                'country_id' => 'required',
                'state_id' => 'required',
                'city_id' => 'required',
            ],
                $messages = [
                    'farm_name.required'    => 'Please enter farm name.',
                    'farm_owner_first_name.required'    => 'Please enter farm owner first name.',
                    'farm_owner_last_name.required'    => 'Please enter farm owner last name.',
                    'contact_number.required'    => 'Please enter contact number.',
                    'email_id.required'    => 'Please enter email id.',
                    'monthly_rent.required'    => 'Please enter monthly rent.',
                    'country_id.required'    => 'Please select country.',
                    'state_id.required'    => 'Please select state.',
                    'city_id.required'    => 'Please select city.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $create_user_arr = [];
                $create_user_information_arr = [];
                $create_user_arr['name'] = $request->farm_name;
                $create_user_arr['email'] = $request->email_id;
                $create_user_arr['password'] = bcrypt('123456789');
                $create_user_arr['user_type'] = '2';
                $created_user = User::create($create_user_arr);
                if(isset($created_user))
                {
                    $unique_number = UniqueId::generateUniqueIdentification(10);
                    $farm_id = 'FR'.$unique_number;
                    $create_user_information_arr['user_id'] = $created_user->id;
                    $create_user_information_arr['user_unique_id'] = $farm_id;
                    $create_user_information_arr['first_name'] = $request->farm_owner_first_name;
                    $create_user_information_arr['last_name'] = $request->farm_owner_last_name;
                    $create_user_information_arr['monthly_rent'] = $request->monthly_rent;
                    $create_user_information_arr['country_id'] = $request->country_id;
                    $create_user_information_arr['state_id'] = $request->state_id;
                    $create_user_information_arr['city_id'] = $request->city_id;
                    $create_user_information_arr['address_line_one'] = isset($request->address_line_one)?$request->address_line_one:NULL;
                    $create_user_information_arr['address_line_two'] = isset($request->address_line_two)?$request->address_line_two:NULL;
                    $create_user_information_arr['pin_code'] = isset($request->pin_code)?$request->pin_code:NULL;
                    $create_user_information_arr['mobile_number'] = $request->contact_number;
                    $create_user_information_arr['user_type'] = '2';
                    $create_user_information_arr['user_status'] = '1';
                    if($request->hasFile('profile_picture'))
                    {
                        if (! \File::exists(public_path()."/farm")) {
                            \File::makeDirectory(public_path()."/farm");
                        }
                        $imageName = time().'.'.request()->profile_picture->getClientOriginalExtension();
                        request()->profile_picture->move(public_path('/farm/'), $imageName);
                        $create_user_information_arr['profile_image'] = $imageName;
                    }
                    $created_user_information = UserInformation::create($create_user_information_arr);
                    return redirect('/admin/farm/list')->with(['success' => 'Farm Added Successfully...']);
                }

            }
        }
    }

    public function viewFarm($farm_id)
    {
        $id = base64_decode($farm_id);
        $view_farm = UserInformation::find($id);
        return view('farm::farm-view',compact('view_farm'));
    }

    public function updateFarm($farm_id, Request $request)
    {
        $id = base64_decode($farm_id);
        $edit_farm = UserInformation::find($id);
        if($request->method() == "GET")
        {
            $countries = Country::all('id','country_name');
            $states = State::select('id','state_name')->where('country_id',$edit_farm->country_id)->get();
            $cities = City::select('id','city_name')->where('state_id',$edit_farm->state_id)->get();
            return view('farm::farm-edit',compact('edit_farm','countries','states','cities'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'farm_name' => 'required',
                'farm_owner_first_name' => 'required',
                'farm_owner_last_name' => 'required',
                'contact_number' => 'required',
                'email_id' => 'required',
                'monthly_rent' => 'required',
                'country_id' => 'required',
                'state_id' => 'required',
                'city_id' => 'required',
            ],
                $messages = [
                    'farm_name.required'    => 'Please enter farm name.',
                    'farm_owner_first_name.required'    => 'Please enter farm owner first name.',
                    'farm_owner_last_name.required'    => 'Please enter farm owner last name.',
                    'contact_number.required'    => 'Please enter contact number.',
                    'email_id.required'    => 'Please enter email id.',
                    'monthly_rent.required'    => 'Please enter monthly rent.',
                    'country_id.required'    => 'Please select country.',
                    'state_id.required'    => 'Please select state.',
                    'city_id.required'    => 'Please select city.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $update_user_information_arr = [];
                $update_user_arr = [];
                $update_user_arr['name'] = $request->farm_name;
                $update_user_arr['email'] = $request->email_id;
                $update_user_information_arr['first_name'] = $request->farm_owner_first_name;
                $update_user_information_arr['last_name'] = $request->farm_owner_last_name;
                $update_user_information_arr['monthly_rent'] = $request->monthly_rent;
                $update_user_information_arr['country_id'] = $request->country_id;
                $update_user_information_arr['state_id'] = $request->state_id;
                $update_user_information_arr['city_id'] = $request->city_id;
                $update_user_information_arr['address_line_one'] = isset($request->address_line_one)?$request->address_line_one:NULL;
                $update_user_information_arr['address_line_two'] = isset($request->address_line_two)?$request->address_line_two:NULL;
                $update_user_information_arr['pin_code'] = isset($request->pin_code)?$request->pin_code:NULL;
                $update_user_information_arr['mobile_number'] = $request->contact_number;
                if($request->hasFile('profile_picture'))
                {
                    $old_image = $edit_farm->profile_image;
                    $imageName = time().'.'.request()->profile_picture->getClientOriginalExtension();
                    request()->profile_picture->move(public_path('/farm/'), $imageName);
                    $update_user_information_arr['profile_image'] = $imageName;
                    if(isset($old_image))
                    {
                        unlink(public_path('/farm/'.$old_image));
                    }
                }
                DB::table('users')->where('id',$edit_farm->user_id)->update($update_user_arr);
                DB::table('user_informations')->where('id',$edit_farm->id)->update($update_user_information_arr);
                return redirect('/admin/farm/list')->with(['success' => 'Farm Updated Successfully...']);
            }
        }
    }

    public function deleteFarm(Request $request)
    {
        $id = base64_decode($request->id);
        if(isset($id))
        {
            $delete_farm = User::find($id);
            $farm_image = UserInformation::where('user_id',$id)->first();
            if(isset($delete_farm))
            {
                $delete_farm_image = $farm_image->profile_image;
                if(isset($delete_farm_image))
                {
                    unlink(public_path('/farm/'.$delete_farm_image));
                }
                if($delete_farm->delete())
                {
                    $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Farm deleted successfully...');
                    return json_encode($result);
                }
                else
                {
                    $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something went wrong...');
                    return json_encode($result);
                }

            }
        }
    }
}
