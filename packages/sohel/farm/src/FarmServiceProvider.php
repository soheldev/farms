<?php

namespace Sohel\Farm;

use Illuminate\Support\ServiceProvider;

class FarmServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->make('Sohel\Farm\FarmController');
        $this->loadViewsFrom(__DIR__.'/views', 'farm');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__.'/routes.php';
    }
}
