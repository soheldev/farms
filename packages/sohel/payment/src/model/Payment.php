<?php

namespace Sohel\Payment\Model;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $table = 'payments';
    protected $fillable = ['animal_id','customer_id','amount_due','amount_paid','due_date','payment_date','mode_of_payment','payment_status','notes','added_by','created_at','updated_at'];
    protected $with = ['paymentAnimalInfo','paymentCustomerInfo'];

    public function paymentAnimalInfo()
    {
        return $this->belongsTo('Sohel\Animal\Model\Animal','animal_id','id');
    }

    public function paymentCustomerInfo()
    {
        return $this->belongsTo('Sohel\User\Model\Customer','customer_id','customer_id');
    }
}
