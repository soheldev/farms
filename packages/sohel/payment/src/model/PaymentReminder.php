<?php

namespace Sohel\Payment\Model;

use Illuminate\Database\Eloquent\Model;

class PaymentReminder extends Model
{
    //
    protected $table = 'payments_reminders';
    protected $fillable = ['animal_id','payment_due_date','payment_tracker','added_by'];
    //protected $with = ['animaldisease'];

    public function paymentReminderAnimalInfo()
    {
        return $this->belongsTo('Sohel\Animal\Model\Animal','animal_id','id');
    }

    /*public function animalOwner()
    {
        return $this->belongsTo('Sohel\User\Model\Customer','customer_id','id');
    }*/
}
