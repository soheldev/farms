@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Add Payment</p>
                        </div>
                        <br>

                        <form class="forms-sample" id="add_payment_form" method="POST" action="" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="exampleInputName1">Animal Id<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="animal_id" name="animal_id" placeholder="Animal Id" value="{{$pending_payment_detail->paymentAnimalInfo->prefix.$pending_payment_detail->paymentAnimalInfo->animal_id}}" readonly>
                                @error('animal_id')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Animal Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="animal_name" name="animal_name" placeholder="Animal Name" value="{{$pending_payment_detail->paymentAnimalInfo->animal_name}}" readonly>
                                @error('animal_name')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Customer Id<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="customer_id" name="customer_id" placeholder="Customer Id" value="{{$pending_payment_detail->paymentCustomerInfo->customer_unique_id}}" readonly>
                                @error('customer_id')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Customer Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="customer_name" name="customer_name" placeholder="Customer Name" value="{{$pending_payment_detail->paymentCustomerInfo->first_name.' '.$pending_payment_detail->paymentCustomerInfo->last_name}}" readonly>
                                @error('customer_name')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Due Date<sup style="color: red">*</sup><span> (DD-MM-YYYY)</span></label>
                                <input type="text" class="form-control" id="due_date" name="due_date" placeholder="Due Date" value="{{date("d-m-Y", strtotime($pending_payment_detail->due_date))}}" readonly>
                                @error('due_date')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Amount Due<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="amount_due" name="amount_due" placeholder="Amount Due" value="{{$pending_payment_detail->amount_due}}" readonly>
                                @error('amount_due')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Amount Paid<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="amount_paid" name="amount_paid" placeholder="Amount Paid" value="">
                                @error('amount_paid')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Payment Date<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="payment_date" name="payment_date" placeholder="Select Payment Date" value="">
                                @error('payment_date')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Mode Of Payment<sup style="color: red">*</sup></label>
                                <select class="form-control" id="mode_of_payment" name="mode_of_payment">
                                    <option value="">Select Mode Of Payment</option>
                                    <option value="0">Cash</option>
                                    <option value="1">Google Pay</option>
                                    <option value="2">Phone Pay</option>
                                </select>
                                @error('mode_of_payment')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Notes</label>
                                <textarea class="form-control" id="notes" name="notes" placeholder="Enter Notes" value="" rows="5"></textarea>
                                @error('notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary mr-2" id="payment_submit_btn"><i id="payment_submit_loder" style="font-size:15px"></i>  Submit</button>
                            <button class="btn btn-light" id="reset_payment_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function ()
        {
            $( "#payment_date" ).datepicker({
                dateFormat : 'yy-mm-d',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });


            /*$('#customer_id').select2({
                placeholder: "Select Customer",
                allowClear: true
            });*/


            $('#reset_payment_form').click(function () {
                $('#add_payment_form')[0].reset();
            });


            $("#amount_paid").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $('#add_payment_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'amount_paid':{
                        required:true,
                        digits:true,
                    },
                    'payment_date':{
                        required:true,
                    },
                    'mode_of_payment':{
                        required:true,
                    },

                },
                messages:{
                    'amount_paid':{
                        required:"Please enter paid amount.",
                        digits:"Please enter valid amount"
                    },
                    'payment_date':{
                        required:"Please select date of payment.",
                    },
                    'mode_of_payment':{
                        required:"Please select mode of payment.",
                    }
                },
                submitHandler:function (form) {
                    $('#payment_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#payment_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

        });


    </script>
@endsection