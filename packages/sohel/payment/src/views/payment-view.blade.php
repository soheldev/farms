@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/payment/received/list')}}">List Received Payment&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">View Payment Information</p>
                        </div>
                        <br>
                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>
                                        Payment Id : <span>#{{$view_payment->id}}</span>
                                    </th>
                                    <th>
                                       Animal Id : <span>{{$view_payment->paymentAnimalInfo->prefix.$view_payment->paymentAnimalInfo->animal_id}}</span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Animal Name : <span>{{$view_payment->paymentAnimalInfo->animal_name}}</span>
                                    </th>
                                    <th>
                                        Customer Id : <span>{{$view_payment->paymentCustomerInfo->customer_unique_id}}</span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Customer Name : <span>{{$view_payment->paymentCustomerInfo->first_name.' '.$view_payment->paymentCustomerInfo->last_name}}</span>
                                    </th>
                                    <th>
                                        Customer Contact : <span>{{$view_payment->paymentCustomerInfo->contact_no}}</span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Amount Due : <span>{{$view_payment->amount_due}}</span>
                                    </th>
                                    <th>
                                        Amount Received : <span>{{$view_payment->amount_paid}}</span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Due Date : <span>{{date("d-m-Y", strtotime($view_payment->due_date))}}</span>
                                    </th>
                                    <th>
                                        Payment Date : <span>{{date("d-m-Y", strtotime($view_payment->payment_date))}}</span>
                                    </th>
                                </tr>

                                <tr>
                                    <th>
                                        Mode Of Payment :
                                        @switch($view_payment->mode_of_payment)
                                            @case('0')
                                            <span>Cash</span>
                                            @break

                                            @case('1')
                                            <span>Google Pay</span>
                                            @break

                                            @case('2')
                                            <span>Phone Pay</span>
                                            @break

                                            @default
                                            <span>N/A</span>
                                        @endswitch
                                    </th>
                                    <th>
                                        Payment Status :
                                        @switch($view_payment->payment_status)
                                            @case('0')
                                            <span>Pending</span>
                                            @break

                                            @case('1')
                                            <span>Completed</span>
                                            @break

                                            @case('2')
                                            <span>Cancelled</span>
                                            @break

                                            @default
                                            <span>N/A</span>
                                        @endswitch
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="2">
                                        Notes : <span>{{isset($view_payment->notes)?$view_payment->notes:'N/A'}}</span>
                                    </th>
                                </tr>

                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('jcontent')
<script>
    var javascript_site_path = '{{url('/')}}';
    $(function () {
        //loadAnimalChart();
    });

    function loadAnimalChart()
    {
        var animal_id = '{{Request::segment(4)}}';
        $.ajax({
           url: javascript_site_path+'/get/animal-chart-data',
           type: 'POST',
           //dataType: 'JSON',
           data:{
               animal_id: animal_id
           },
           success:function (response) {
               if(response.status == '0')
               {
                   var cashDepositsCanvas = $("#animal-chart").get(0).getContext("2d");
                   var data = {
                       labels: response.month_arr,
                       datasets: [
                           {
                               label: 'Height',
                               data: response.height_arr,
                               borderColor: [
                                   '#ff4747'
                               ],
                               borderWidth: 2,
                               fill: false,
                               pointBackgroundColor: "#fff"
                           },
                           {
                               label: 'Weight',
                               data: response.weight_arr,
                               borderColor: [
                                   '#4d83ff'
                               ],
                               borderWidth: 2,
                               fill: false,
                               pointBackgroundColor: "#fff"
                           },
                       ]
                   };
                   var options = {
                       scales: {
                           yAxes: [{
                               display: true,
                               gridLines: {
                                   drawBorder: false,
                                   lineWidth: 1,
                                   color: "#e9e9e9",
                                   zeroLineColor: "#e9e9e9",
                               },
                               ticks: {
                                   min: 0,
                                   max: 100,
                                   stepSize: 10,
                                   fontColor: "#6c7383",
                                   fontSize: 16,
                                   fontStyle: 300,
                                   padding: 15
                               }
                           }],
                           xAxes: [{
                               display: true,
                               gridLines: {
                                   drawBorder: false,
                                   lineWidth: 1,
                                   color: "#e9e9e9",
                               },
                               ticks : {
                                   fontColor: "#6c7383",
                                   fontSize: 16,
                                   fontStyle: 300,
                                   padding: 15
                               }
                           }]
                       },
                       legend: {
                           display: false
                       },
                       legendCallback: function(chart) {
                           var text = [];
                           text.push('<ul class="dashboard-chart-legend">');
                           for(var i=0; i < chart.data.datasets.length; i++) {
                               text.push('<li><span style="background-color: ' + chart.data.datasets[i].borderColor[0] + ' "></span>');
                               if (chart.data.datasets[i].label) {
                                   text.push(chart.data.datasets[i].label);
                               }
                           }
                           text.push('</ul>');
                           return text.join("");
                       },
                       elements: {
                           point: {
                               radius: 3
                           },
                           line :{
                               tension: 0
                           }
                       },
                       stepsize: 1,
                       layout : {
                           padding : {
                               top: 0,
                               bottom : -10,
                               left : -10,
                               right: 0
                           }
                       }
                   };
                   var cashDeposits = new Chart(cashDepositsCanvas, {
                       type: 'line',
                       data: data,
                       options: options
                   });
                   document.getElementById('animal-chart-legend').innerHTML = cashDeposits.generateLegend();
               }
               else
               {
                   $("#animal-chart-legend").html('<h3>No Progress Recorded Yet...</h3>');
               }
           },
           beforeSend:function () {
               $("#animal-chart-legend").html('<i class="fa fa-spinner fa-pulse fa-2x"></i>&nbsp;<h3> Fetching Animal Progress</h3>');
           }
        });
    }
</script>
@endsection