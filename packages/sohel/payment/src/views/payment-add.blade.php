@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Add Payment</h2>
                </div>
                <div class="add-customer">
                    <form id="add_payment_form" method="POST" action="" enctype="multipart/form-data" autocomplete="off">


                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="animal_id" name="animal_id"
                                           placeholder="Animal Id"
                                           value="{{$pending_payment_detail->paymentAnimalInfo->prefix.$pending_payment_detail->paymentAnimalInfo->animal_id}}" readonly
                                           class="form-control">
                                    @error('animal_id')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="animal_name" name="animal_name"
                                           class="form-control" placeholder="Animal Name"
                                           value="{{$pending_payment_detail->paymentAnimalInfo->animal_name}}" readonly>
                                    @error('animal_name')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="customer_id" name="customer_id"
                                           placeholder="Customer Id"
                                           value="{{$pending_payment_detail->paymentCustomerInfo->customer_unique_id}}" readonly
                                           class="form-control">
                                    @error('customer_id')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="customer_name" name="customer_name"
                                           class="form-control" placeholder="Customer Name"
                                           value="{{$pending_payment_detail->paymentCustomerInfo->first_name.' '.$pending_payment_detail->paymentCustomerInfo->last_name}}"
                                           readonly>
                                    @error('customer_name')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="due_date" name="due_date"
                                           placeholder="Due Date"
                                           value="{{date("d-m-Y", strtotime($pending_payment_detail->due_date))}}" readonly
                                           class="form-control">
                                    @error('due_date')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="payment_date" name="payment_date"
                                           class="form-control" placeholder="Select Payment Date*">
                                    @error('payment_date')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="amount_due" name="amount_due"
                                           placeholder="Amount Due"
                                           value="{{$pending_payment_detail->amount_due}}" readonly
                                           class="form-control">
                                    @error('amount_due')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="amount_paid" name="amount_paid"
                                           class="form-control" placeholder="Amount Paid*">
                                    @error('amount_paid')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="mode_of_payment" name="mode_of_payment">
                                        <option value="">Select Mode Of Payment*</option>
                                        <option value="0">Cash</option>
                                        <option value="1">Google Pay</option>
                                        <option value="2">Phone Pay</option>
                                    </select>
                                    @error('mode_of_payment')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                    <textarea class="form-control" id="notes" name="notes"
                                              placeholder="Enter Notes" value="" rows="5"></textarea>
                                @error('notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li>
                                            <button type="submit" class="btn cust-btn btn-save"
                                                    id="payment_submit_btn"><i id="payment_submit_loder"
                                                                                        style="font-size:15px"></i> Save
                                            </button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn cust-btn btn-grey"
                                                    id="reset_payment_form">Cancel
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function ()
        {
            $( "#payment_date" ).datepicker({
                dateFormat : 'yy-mm-d',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });


            /*$('#customer_id').select2({
                placeholder: "Select Customer",
                allowClear: true
            });*/


            $('#reset_payment_form').click(function () {
                $('#add_payment_form')[0].reset();
            });


            $("#amount_paid").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $('#add_payment_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'amount_paid':{
                        required:true,
                        digits:true,
                    },
                    'payment_date':{
                        required:true,
                    },
                    'mode_of_payment':{
                        required:true,
                    },

                },
                messages:{
                    'amount_paid':{
                        required:"Please enter paid amount.",
                        digits:"Please enter valid amount"
                    },
                    'payment_date':{
                        required:"Please select date of payment.",
                    },
                    'mode_of_payment':{
                        required:"Please select mode of payment.",
                    }
                },
                submitHandler:function (form) {
                    $('#payment_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#payment_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

        });


    </script>
@endsection