<?php

namespace Sohel\Payment;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CheckPermission;
use App\Http\Helpers\CheckType;
use App\Http\Helpers\UniqueId;
use App\UserInformation;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sohel\Animal\Model\Animal;
use Sohel\Animal\Model\AnimalDisease;
use Sohel\Animal\Model\AnimalVacination;
use Sohel\Animal\Model\CustomerAnimalExit;
use Sohel\Breed\Model\Breed;
use Sohel\City\Model\City;
use Sohel\Country\Model\Country;
use Sohel\Disease\Model\Disease;
use Sohel\Payment\Model\Payment;
use Sohel\Progress\Model\AnimalProgress;
use Sohel\Progress\Model\ProgressReminder;
use Sohel\State\Model\State;
use Sohel\User\Model\Customer;
use Sohel\Vaccine\Model\Vaccine;

class PaymentController extends Controller
{
    public function __construct()
    {
        //$this->checkType();
    }

    public function checkType()
    {
        $user_redirect = '';
        $auth_type = Auth::user()->userInformation->user_type;
        if($auth_type == "1")
        {
            $user_redirect = "/admin";
        }
        else
        {
            $user_redirect = "/vendor";
        }
        return $user_redirect;
    }
    //
    public function index()
    {
        $capture_pending_payment_permission = CheckPermission::hasPermission('capture.pendingpayment');
        return view('payment::payment-pending-list',compact('capture_pending_payment_permission'));
    }

    public function fetchPendingPaymentData(Request $request)
    {
        /*$added_by = Auth::user()->id;
        $data = Payment::where('added_by',$added_by)->where('payment_status','0')->skip($request->skip)->limit(10)->get(['id','animal_id','customer_id','amount_due','due_date','payment_status']);
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }*/
        $added_by = Auth::user()->id;
        $sql = "SELECT p.id,p.amount_due,p.due_date,p.payment_status,a.id AS animal_id,CONCAT(a.prefix,a.animal_id) AS animal_unique_id,a.animal_name,c.id AS customer_id,c.customer_unique_id AS customer_unique_id,CONCAT(c.first_name,' ',c.last_name) AS customer_name,c.contact_no
                FROM payments AS p
                JOIN animals AS a
                ON p.animal_id = a.id
                JOIN customers AS c 
                ON p.customer_id = c.customer_id";
        $sql = $sql." WHERE p.payment_status = '0'";
        if(isset($request->search_value))
        {
            $sql = $sql." AND (CONCAT(a.prefix,a.animal_id) LIKE '%$request->search_value%' OR a.animal_name LIKE '%$request->search_value%' OR c.customer_id LIKE '%$request->search_value%' OR CONCAT(c.first_name,' ',c.last_name) LIKE '%$request->search_value%' OR c.contact_no LIKE '%$request->search_value%')";
        }
        $sql = $sql." LIMIT $request->skip , 10";
        $data = DB::select($sql);
        if(count($data) > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function processPendingPayment($user, $payment_id, Request $request)
    {
        $id = base64_decode($payment_id);
        $pending_payment_detail = Payment::find($id);
        if($request->method() == "GET")
        {
            return view('payment::payment-add',compact('pending_payment_detail'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'amount_paid' => 'required',
                'payment_date' => 'required',
                'mode_of_payment' => 'required',
            ],
                $messages = [
                    'amount_paid.required'    => 'Please enter amount paid.',
                    'payment_date.required'    => 'Please select payment date.',
                    'mode_of_payment.required'    => 'Please select mode of payment.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $process_payment_arr = [];
                $process_payment_arr['amount_paid'] = $request->amount_paid;
                $process_payment_arr['payment_date'] = $request->payment_date;
                $process_payment_arr['mode_of_payment'] = $request->mode_of_payment;
                $process_payment_arr['notes'] = $request->notes;
                $process_payment_arr['payment_status'] = '1';
                DB::table('payments')->where('id',$pending_payment_detail->id)->update($process_payment_arr);
                $type = CheckType::checkAuthType();
                return redirect($type.'/payment/pending/list')->with(['success' => 'Payment Completed Successfully...']);


            }
        }
    }

    public function receivedPaymentListing()
    {
        return view('payment::payment-received-list');
    }

    public function fetchReceivedPaymentData(Request $request)
    {
        $added_by = Auth::user()->id;
        $sql = "SELECT p.id,p.amount_paid,p.payment_status,a.id AS animal_id,CONCAT(a.prefix,a.animal_id) AS animal_unique_id,a.animal_name,c.id AS customer_id,c.customer_unique_id AS customer_unique_id,CONCAT(c.first_name,' ',c.last_name) AS customer_name,c.contact_no
                FROM payments AS p
                JOIN animals AS a
                ON p.animal_id = a.id
                JOIN customers AS c 
                ON p.customer_id = c.customer_id";
        $sql = $sql." WHERE p.payment_status = '1'";
        if(isset($request->search_value))
        {
            $sql = $sql." AND (CONCAT(a.prefix,a.animal_id) LIKE '%$request->search_value%' OR a.animal_name LIKE '%$request->search_value%' OR c.customer_id LIKE '%$request->search_value%' OR CONCAT(c.first_name,' ',c.last_name) LIKE '%$request->search_value%' OR c.contact_no LIKE '%$request->search_value%')";
        }
        $sql = $sql." LIMIT $request->skip , 10";
        $data = DB::select($sql);
        if(count($data) > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }



    public function viewReceivedPayment($user, $payment_id)
    {
        $id = base64_decode($payment_id);
        $view_payment = Payment::find($id);
        return view('payment::payment-view',compact('view_payment'));
    }









    public function fetchAnimalData(Request $request)
    {
        $added_by = Auth::user()->id;
        $search_value = $request->search_value;
        $data = Animal::query();
        //$data = $data->where('added_by',$added_by);
        if(isset($search_value) && $search_value != '')
        {
            $data = $data->where('animal_name', 'LIKE', '%' .$search_value. '%');
        }
        $data = $data->where('status','=','1');
        $data = $data->skip($request->skip)->limit(5);
        $data = $data->get();

        //$data = Animal::skip($request->skip)->limit(5)->where('added_by',$added_by)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }


    public function viewAnimal($type, $animal_id)
    {
        $id = base64_decode($animal_id);
        $view_animal = Animal::find($id);
        return view('animal::animal-view',compact('view_animal'));
    }

    public function createAnimal(Request $request)
    {
        if($request->method() == "GET")
        {
            $added_by = Auth::user()->id;
            $breeds = Breed::where('added_by',$added_by)->get(['id','breed_name']);
            $customers = Customer::where('added_by',$added_by)->get(['id','customer_id','first_name','last_name']);
            return view("animal::animal-add",compact('breeds','customers'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'animal_type' => 'required',
                'animal_name' => 'required',
                'breed_id' => 'required',
                'gender' => 'required',
                'age_in_month' => 'required',
                'entry_weight' => 'required',
                'entry_height' => 'required',
                'entry_date' => 'required',
                'monthly_rent' => 'required',
            ],
                $messages = [
                    'animal_type.required'    => 'Please select animal type.',
                    'animal_name.required'    => 'Please enter animal name.',
                    'breed_id.required'    => 'Please select animal breed.',
                    'gender.required'    => 'Please select animal gender.',
                    'age_in_month.required'    => 'Please enter animal age in months.',
                    'entry_weight.required'    => 'Please enter animal weight.',
                    'entry_height.required'    => 'Please enter animal height.',
                    'entry_date.required'    => 'Please select registration date.',
                    'monthly_rent.required'    => 'Please enter animal monthly rent.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $prefix = $this->generateAnimalPrefix($request->animal_type,$request->gender);
                $unique_number = UniqueId::generateUniqueIdentification(10);
                $animal_id = $unique_number;
                $insert_animal_arr = [];
                if($request->has('is_owned_by_farm'))
                {
                    $insert_animal_arr['customer_id'] = Auth::user()->id;
                    $insert_animal_arr['is_owned_by_farm'] = '1';
                }
                else
                {
                    $insert_animal_arr['customer_id'] = $request->customer_id;
                    $insert_animal_arr['is_owned_by_farm'] = '0';
                }
                $insert_animal_arr['prefix'] = $prefix;
                $insert_animal_arr['animal_id'] = $animal_id;
                $insert_animal_arr['animal_type'] = $request->animal_type;
                $insert_animal_arr['animal_name'] = $request->animal_name;
                $insert_animal_arr['breed_id'] = $request->breed_id;
                $insert_animal_arr['gender'] = $request->gender;
                $insert_animal_arr['age_in_month'] = $request->age_in_month;
                $insert_animal_arr['date_of_birth'] = $request->date_of_birth;
                $insert_animal_arr['entry_date'] = $request->entry_date;
                $insert_animal_arr['entry_weight'] = $request->entry_weight;
                $insert_animal_arr['entry_height'] = $request->entry_height;
                $insert_animal_arr['monthly_rent'] = $request->monthly_rent;
                $insert_animal_arr['notes'] = $request->notes;
                $insert_animal_arr['added_by'] = Auth::user()->id;
                $created_animal = Animal::create($insert_animal_arr);
                if($created_animal)
                {
                    if($request->has('is_payment_done'))
                    {
                        $payment_reminder = new \DateTime($request->entry_date);
                        $payment_reminder->modify('+1 months');
                        $payment_due_date = $payment_reminder->format("Y-m-d");
                    }
                    else
                    {
                        $payment_reminder = new \DateTime($request->entry_date);
                        $payment_due_date = $payment_reminder->format("Y-m-d");
                    }
                    $this->generateAnimalInitialProgress($created_animal, $request);
                    $this->generateAnimalProgressReminder($created_animal, $request);
                    /*$insert_progress_arr = [];
                    $insert_progress_arr['animal_id'] = $created_animal->id;
                    $insert_progress_arr['weight'] = $request->entry_weight;
                    $insert_progress_arr['height'] = $request->entry_height;
                    $insert_progress_arr['capture_date'] = $request->entry_date;
                    $insert_progress_arr['date'] = date('Y-m-d');
                    AnimalProgress::create($insert_progress_arr);*/

                    /*$progress_reminder = new \DateTime($request->entry_date);
                    $progress_reminder->modify('+1 months');
                    $progress_entry_date = $progress_reminder->format("Y-m-d");

                    $progress_reminder_arr = [];
                    $progress_reminder_arr['animal_id'] = $created_animal->id;
                    $progress_reminder_arr['progress_entry_date'] = $progress_entry_date;
                    $progress_reminder_arr['progress_tracker'] = '0';
                    ProgressReminder::create($progress_reminder_arr);*/
                }
                $type = CheckType::checkAuthType();
                return redirect($type.'/animal/list')->with(['success' => 'Animals Added Successfully...']);
            }
        }
    }

    public function generateAnimalInitialProgress($created_animal, $request)
    {
        $insert_progress_arr = [];
        $insert_progress_arr['animal_id'] = $created_animal->id;
        $insert_progress_arr['weight'] = $request->entry_weight;
        $insert_progress_arr['height'] = $request->entry_height;
        $insert_progress_arr['capture_date'] = $request->entry_date;
        $insert_progress_arr['date'] = date('Y-m-d');
        AnimalProgress::create($insert_progress_arr);
    }

    public function generateAnimalProgressReminder($created_animal, $request)
    {
        $progress_reminder = new \DateTime($request->entry_date);
        $progress_reminder->modify('+1 months');
        $progress_entry_date = $progress_reminder->format("Y-m-d");

        $progress_reminder_arr = [];
        $progress_reminder_arr['animal_id'] = $created_animal->id;
        $progress_reminder_arr['progress_entry_date'] = $progress_entry_date;
        $progress_reminder_arr['progress_tracker'] = '0';
        ProgressReminder::create($progress_reminder_arr);
    }


    public function updateAnimal($type, $animal_id,Request $request)
    {

        $id = base64_decode($animal_id);
        $update_animal = Animal::find($id);
        if($request->method() == "GET")
        {
            $added_by = Auth::user()->id;
            $breeds = Breed::where('added_by',$added_by)->get(['id','breed_name']);
            $customers = Customer::where('added_by',$added_by)->get(['id','customer_id','first_name','last_name']);
            return view('animal::animal-edit',compact('update_animal','breeds','customers'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'animal_type' => 'required',
                'animal_name' => 'required',
                'breed_id' => 'required',
                'gender' => 'required',
                'age_in_month' => 'required',
                'entry_weight' => 'required',
                'entry_height' => 'required',
            ],
                $messages = [
                    'animal_type.required'    => 'Please select animal type.',
                    'animal_name.required'    => 'Please enter animal name.',
                    'breed_id.required'    => 'Please select animal breed.',
                    'gender.required'    => 'Please select animal gender.',
                    'age_in_month.required'    => 'Please enter animal age in months.',
                    'entry_weight.required'    => 'Please enter animal weight.',
                    'entry_height.required'    => 'Please enter animal height.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $update_animal_arr = [];
                if($request->animal_type != $update_animal->animal_type || $request->gender != $update_animal->gender)
                {
                    $prefix = $this->generateAnimalPrefix($request->animal_type,$request->gender);
                    $update_animal_arr['prefix'] = $prefix;
                }
                if($request->has('is_owned_by_farm'))
                {
                    $update_animal_arr['customer_id'] = Auth::user()->id;
                    $update_animal_arr['is_owned_by_farm'] = '1';
                }
                else
                {
                    $update_animal_arr['customer_id'] = $request->customer_id;
                    $update_animal_arr['is_owned_by_farm'] = '0';
                }
                $update_animal_arr['animal_type'] = $request->animal_type;
                $update_animal_arr['animal_name'] = $request->animal_name;
                $update_animal_arr['breed_id'] = $request->breed_id;
                $update_animal_arr['gender'] = $request->gender;
                $update_animal_arr['age_in_month'] = $request->age_in_month;
                $update_animal_arr['date_of_birth'] = $request->date_of_birth;
                $update_animal_arr['entry_weight'] = $request->entry_weight;
                $update_animal_arr['entry_height'] = $request->entry_height;
                $update_animal_arr['notes'] = $request->notes;
                DB::table('animals')->where('id',$update_animal->id)->update($update_animal_arr);
                DB::table('animal_progress')->where('animal_id',$update_animal->id)->where('capture_date',$update_animal->entry_date)->update(['weight' => $request->entry_weight, 'height' => $request->entry_height]);
                $type = CheckType::checkAuthType();
                return redirect($type.'/animal/list')->with(['success' => 'Animal Updated Successfully...']);
            }
        }
    }

    public function deleteAnimal(Request $request)
    {
        $id = base64_decode($request->id);
        if(isset($id))
        {
            $delete_animal = Animal::find($id);
            if(isset($delete_animal))
            {
                if($delete_animal->delete())
                {
                    DB::table('animal_progress')->where('animal_id',$id)->delete();
                    DB::table('progress_reminders')->where('animal_id',$id)->delete();
                    $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Animal deleted successfully...');
                    return json_encode($result);
                }
                else
                {
                    $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something went wrong...');
                    return json_encode($result);
                }

            }
        }
    }

    public function exitAnimal($type, $animal_id,Request $request)
    {

        $id = base64_decode($animal_id);
        $exit_animal = Animal::find($id);
        if($request->method() == "GET")
        {
            return view('animal::animal-exit',compact('exit_animal'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'animal_exit_date' => 'required',
                'animal_exit_weight' => 'required',
                'animal_exit_height' => 'required',
                'exit_reason' => 'required',
            ],
                $messages = [
                    'animal_exit_date.required'    => 'Please select animal exit date.',
                    'animal_exit_weight.required'    => 'Please enter animal weight.',
                    'animal_exit_height.required'    => 'Please select animal height.',
                    'exit_reason.required'    => 'Please select animal exit reason.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $exit_animal_arr = [];
                $exit_animal_arr['animal_id'] = $exit_animal->id;
                $exit_animal_arr['customer_id'] = $exit_animal->customer_id;
                $exit_animal_arr['animal_entry_date'] = $request->animal_entry_date;
                $exit_animal_arr['animal_entry_weight'] = $request->animal_entry_weight;
                $exit_animal_arr['animal_entry_height'] = $request->animal_entry_height;
                $exit_animal_arr['animal_exit_date'] = $request->animal_exit_date;
                $exit_animal_arr['animal_exit_weight'] = $request->animal_exit_weight;
                $exit_animal_arr['animal_exit_height'] = $request->animal_exit_height;
                $exit_animal_arr['exit_reason'] = $request->exit_reason;
                $exit_animal_arr['animal_weight_gain'] = $request->animal_weight_gain;
                $exit_animal_arr['animal_height_gain'] = $request->animal_height_gain;
                $exit_animal_arr['notes'] = $request->notes;
                CustomerAnimalExit::create($exit_animal_arr);
                $exit_animal->status = '0';
                $exit_animal->save();
                DB::table('progress_reminders')->where('animal_id',$exit_animal->id)->update(['progress_tracker' => '1']);
                $type = CheckType::checkAuthType();
                return redirect($type.'/animal/list')->with(['success' => 'Animal Exist Successfully...']);
            }
        }
    }

    public function  generateAnimalPrefix($animal_type, $animal_gender)
    {
        $prefix = '';
        if($animal_type == "0" && $animal_gender == "0")
        {
            $prefix = "GA";
        }
        elseif ($animal_type == "0" && $animal_gender == "1")
        {
            $prefix = "GK";
        }
        elseif ($animal_type == "0" && $animal_gender == "2")
        {
            $prefix = "GF";
        }
        elseif ($animal_type == "1" && $animal_gender == "0")
        {
            $prefix = "SA";
        }
        elseif ($animal_type == "1" && $animal_gender == "1")
        {
            $prefix = "SK";
        }
        elseif ($animal_type == "1" && $animal_gender == "2")
        {
            $prefix = "SF";
        }
        return $prefix;
    }

   public function listAnimalVacination($user, $animal_id)
   {
       $id = base64_decode($animal_id);
       $animal_detail = Animal::select('prefix','animal_id')->find($id);
       return view('animal::animal-vacination-list',compact('animal_detail'));
   }

   public function fetchAnimalVaccineData(Request $request)
   {
       $animal_id = base64_decode($request->animal_id);
       $added_by = Auth::user()->id;
       //$search_value = $request->search_value;
       $data = AnimalVacination::query();
       //$data = $data->where('added_by',$added_by);
       /*if(isset($search_value) && $search_value != '')
       {
           $data = $data->where('animal_name', 'LIKE', '%' .$search_value. '%');
       }*/
       $data = $data->where('animal_id',$animal_id);
       $data = $data->skip($request->skip)->limit(5);
       $data = $data->get();

       //$data = Animal::skip($request->skip)->limit(5)->where('added_by',$added_by)->get();
       if($data->count() > 0)
       {
           $result = array('status'=>'1','data'=>$data);
           return json_encode($result);

       }
       else
       {
           $result = array('status'=>'0');
           return json_encode($result);
       }
   }

   public function addAnimalVacination($user,$animal_id,Request $request)
   {
       $id = base64_decode($animal_id);
       $added_by = Auth::user()->id;
       $vacinations = Vaccine::where('added_by',$added_by)->get(['id','vaccine_name']);
       if($request->method() == "GET")
       {
            return view('animal::animal-vacination-add',compact('id','vacinations'));
       }
       else
       {
           $validator = \Validator::make($request->all(), [
               'vaccine_id' => 'required',
               'dose_number' => 'required',
               'vacination_date' => 'required',
               'weight_on_vacination_day' => 'required',
               'quantity' => 'required',
           ],
               $messages = [
                   'vaccine_id.required'    => 'Please select vacination.',
                   'dose_number.required'    => 'Please enter dose number.',
                   'vacination_date.required'    => 'Please select vacination date.',
                   'weight_on_vacination_day.required'    => 'Please enter weight of animal on vacination day.',
                   'quantity.required'    => 'Please enter quantity in ml.',
               ]);
           if ($validator->fails())
           {
               return redirect()->back()
                   ->withErrors($validator)
                   ->withInput();
           }
           else
           {
               $insert_vacination_arr = [];
               $insert_vacination_arr['animal_id'] = $request->animal_id;
               $insert_vacination_arr['vaccine_id'] = $request->vaccine_id;
               $insert_vacination_arr['dose_number'] = $request->dose_number;
               $insert_vacination_arr['vacination_date'] = $request->vacination_date;
               $insert_vacination_arr['weight_on_vacination_day'] = $request->weight_on_vacination_day;
               $insert_vacination_arr['quantity'] = $request->quantity;
               $insert_vacination_arr['notes'] = $request->notes;
               AnimalVacination::create($insert_vacination_arr);
               $type = CheckType::checkAuthType();
               return redirect($type.'/animal/vacination/'.$animal_id)->with(['success' => 'Animal Vaccine Added Successfully...']);
           }
       }
   }

   public function editAnimalVacination($user, $animal_id, $id, Request $request)
   {
       $id = base64_decode($id);
       $added_by = Auth::user()->id;
       $edit_animal_vacination = AnimalVacination::find($id);
       $vacinations = Vaccine::where('added_by',$added_by)->get(['id','vaccine_name']);
       if($request->method() == "GET")
       {
            return view('animal::animal-vacination-edit',compact('edit_animal_vacination','vacinations'));
       }
       else
       {
           $validator = \Validator::make($request->all(), [
               'vaccine_id' => 'required',
               'dose_number' => 'required',
               'vacination_date' => 'required',
               'weight_on_vacination_day' => 'required',
               'quantity' => 'required',
           ],
               $messages = [
                   'vaccine_id.required'    => 'Please select vacination.',
                   'dose_number.required'    => 'Please enter dose number.',
                   'vacination_date.required'    => 'Please select vacination date.',
                   'weight_on_vacination_day.required'    => 'Please enter weight of animal on vacination day.',
                   'quantity.required'    => 'Please enter quantity in ml.',
               ]);
           if ($validator->fails())
           {
               return redirect()->back()
                   ->withErrors($validator)
                   ->withInput();
           }
           else
           {
               $update_vacination_arr = [];
               $update_vacination_arr['vaccine_id'] = $request->vaccine_id;
               $update_vacination_arr['dose_number'] = $request->dose_number;
               $update_vacination_arr['vacination_date'] = $request->vacination_date;
               $update_vacination_arr['weight_on_vacination_day'] = $request->weight_on_vacination_day;
               $update_vacination_arr['quantity'] = $request->quantity;
               $update_vacination_arr['notes'] = $request->notes;
               DB::table('animal_vacinations')->where('id',$edit_animal_vacination->id)->update($update_vacination_arr);
               $type = CheckType::checkAuthType();
               return redirect($type.'/animal/vacination/'.$animal_id)->with(['success' => 'Animal Vaccine Updated Successfully...']);
           }
       }
   }

    public function deleteAnimalVacination(Request $request)
    {
        $id = base64_decode($request->id);
        if(isset($id))
        {
            $delete_animal_vacination = AnimalVacination::find($id);
            if(isset($delete_animal_vacination))
            {
                if($delete_animal_vacination->delete())
                {
                    $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Animal vacination record deleted successfully...');
                    return json_encode($result);
                }
                else
                {
                    $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something went wrong...');
                    return json_encode($result);
                }

            }
        }
    }

    public function listAnimalDisease($user, $animal_id)
    {
        $id = base64_decode($animal_id);
        $animal_detail = Animal::select('prefix','animal_id')->find($id);
        return view('animal::animal-disease-list',compact('animal_detail'));
    }

    public function fetchAnimalDiseaseData(Request $request)
    {
        $animal_id = base64_decode($request->animal_id);
        $added_by = Auth::user()->id;
        //$search_value = $request->search_value;
        $data = AnimalDisease::query();
        //$data = $data->where('added_by',$added_by);
        /*if(isset($search_value) && $search_value != '')
        {
            $data = $data->where('animal_name', 'LIKE', '%' .$search_value. '%');
        }*/
        $data = $data->where('animal_id',$animal_id);
        $data = $data->skip($request->skip)->limit(5);
        $data = $data->get();

        //$data = Animal::skip($request->skip)->limit(5)->where('added_by',$added_by)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function addAnimalDisease($user,$animal_id,Request $request)
    {
        $id = base64_decode($animal_id);
        $added_by = Auth::user()->id;
        $diseases = Disease::where('added_by',$added_by)->get(['id','disease_name']);
        if($request->method() == "GET")
        {
            return view('animal::animal-disease-add',compact('id','diseases'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'disease_id' => 'required',
                'diagnosed_date' => 'required',
                'treatment' => 'required',
            ],
                $messages = [
                    'disease_id.required'    => 'Please select disease.',
                    'diagnosed_date.required'    => 'Please select diagnosed date.',
                    'treatment.required'    => 'Please enter treatment.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $insert_disease_arr = [];
                $insert_disease_arr['animal_id'] = $request->animal_id;
                $insert_disease_arr['disease_id'] = $request->disease_id;
                $insert_disease_arr['diagnosed_date'] = $request->diagnosed_date;
                $insert_disease_arr['treatment'] = $request->treatment;
                $insert_disease_arr['recovered_date'] = isset($request->recovered_date)?$request->recovered_date:NULL;
                $insert_disease_arr['notes'] = isset($request->notes)?$request->notes:NULL;
                AnimalDisease::create($insert_disease_arr);
                $type = CheckType::checkAuthType();
                return redirect($type.'/animal/disease/'.$animal_id)->with(['success' => 'Animal Disease Added Successfully...']);
            }
        }
    }

    public function editAnimalDisease($user, $animal_id, $id, Request $request)
    {
        $id = base64_decode($id);
        $added_by = Auth::user()->id;
        $edit_animal_disease = AnimalDisease::find($id);
        $diseases = Disease::where('added_by',$added_by)->get(['id','disease_name']);
        if($request->method() == "GET")
        {
            return view('animal::animal-disease-edit',compact('edit_animal_disease','diseases'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'disease_id' => 'required',
                'diagnosed_date' => 'required',
                'treatment' => 'required',
            ],
                $messages = [
                    'disease_id.required'    => 'Please select disease.',
                    'diagnosed_date.required'    => 'Please select diagnosed date.',
                    'treatment.required'    => 'Please enter treatment.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $update_disease_arr = [];
                $update_disease_arr['disease_id'] = $request->disease_id;
                $update_disease_arr['diagnosed_date'] = $request->diagnosed_date;
                $update_disease_arr['treatment'] = $request->treatment;
                $update_disease_arr['recovered_date'] = isset($request->recovered_date)?$request->recovered_date:NULL;
                $update_disease_arr['notes'] = isset($request->notes)?$request->notes:NULL;
                DB::table('animal_diseases')->where('id',$edit_animal_disease->id)->update($update_disease_arr);
                $type = CheckType::checkAuthType();
                return redirect($type.'/animal/disease/'.$animal_id)->with(['success' => 'Animal Disease Updated Successfully...']);
            }
        }
    }

    public function deleteAnimalDisease(Request $request)
    {
        $id = base64_decode($request->id);
        if(isset($id))
        {
            $delete_animal_disease = AnimalDisease::find($id);
            if(isset($delete_animal_disease))
            {
                if($delete_animal_disease->delete())
                {
                    $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Animal disease record deleted successfully...');
                    return json_encode($result);
                }
                else
                {
                    $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something went wrong...');
                    return json_encode($result);
                }

            }
        }
    }


    public function animalChartData(Request $request)
    {
        $id = base64_decode($request->animal_id);
        $sql_query = "SELECT ap.animal_id, ap.weight,ap.height, MONTH(ap.capture_date) AS progress_month FROM animal_progress AS ap WHERE ap.animal_id = '$id' AND YEAR(capture_date) = YEAR(CURDATE()) ORDER BY Month(capture_date)";
        $data = DB::select($sql_query);
        $weight_arr = [];
        $height_arr = [];
        $month_arr = [];
        if(count($data) > 0)
        {
            foreach($data as $d)
            {
                switch ($d->progress_month)
                {
                    case 1:
                        $month = 'January';
                        break;
                    case 2:
                        $month = 'Febuary';
                        break;
                    case 3:
                        $month = 'March';
                        break;
                    case 4:
                        $month = 'April';
                        break;
                    case 5:
                        $month = 'May';
                        break;
                    case 6:
                        $month = 'June';
                        break;
                    case 7:
                        $month = 'July';
                        break;
                    case 8:
                        $month = 'August';
                        break;
                    case 9:
                        $month = 'September';
                        break;
                    case 10:
                        $month = 'October';
                        break;
                    case 11:
                        $month = 'November';
                        break;
                    case 12:
                        $month = 'December';
                        break;
                }
                array_push($month_arr,$month);
                array_push($weight_arr,$d->weight);
                array_push($height_arr,$d->height);
            }
            $result = array('status'=> '0','month_arr' => $month_arr,'weight_arr' => $weight_arr,'height_arr' => $height_arr);
        }
        else
        {
            $result = array('status'=> '1','month_arr' => $month_arr,'weight_arr' => $weight_arr,'height_arr' => $height_arr);
        }
        return $result;
    }
}
