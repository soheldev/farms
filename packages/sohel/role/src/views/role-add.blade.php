@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Add Role</h2>
                </div>
                <div class="add-customer">
                    <form id="add_role_form" method="POST" action="" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="name" name="name" placeholder="Enter Role Name*" class="form-control">
                                    @error('name')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="slug" name="slug"placeholder="Enter Role Slug" class="form-control" readonly>
                                    @error('slug')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 form-group">
                                    <textarea class="form-control" id="description" name="description" placeholder="Enter Role Description" value="" rows="5"></textarea>
                                    @error('description')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li><button type="submit" class="btn cust-btn btn-save" id="role_submit_btn"><i id="role_submit_loder" style="font-size:15px"></i> Save</button></li>
                                        <li><button type="button" class="btn cust-btn btn-grey" id="reset_role_form">Cancel</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function () {
            $('#reset_role_form').click(function () {
                $('#add_role_form')[0].reset();
            });


            $('#add_role_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'name':{
                        required:true,
                    },
                    'description':{
                        required:true,
                    },
                },
                messages:{
                    'name':{
                        required:"Please enter role name."
                    },
                    'description':{
                        required:"Please enter role description.",
                    },
                },
                submitHandler:function (form) {
                    $('#role_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#role_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

            $('#name').keyup(function () {
               var value = $(this).val();
               $('#slug').val(value.replace(/\s+/g, '-').toLowerCase());
            });

        });


    </script>
@endsection