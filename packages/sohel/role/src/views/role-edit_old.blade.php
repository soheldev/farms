@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/setting/roles')}}">List Role&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Edit Role</p>
                        </div>
                        <br>
                        <form class="forms-sample" id="update_role_form" method="POST" action="" enctype="multipart/form-data">


                            <div class="form-group">
                                <label for="exampleInputName1">Role Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Role Name" value="{{$edit_role->name}}">
                                @error('name')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Role Slug<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="slug" name="slug" placeholder="Enter Slug" value="{{$edit_role->slug}}" readonly>
                                @error('slug')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Description</label>
                                <textarea class="form-control" id="description" name="description" placeholder="Enter Role Description" value="" rows="5">{{$edit_role->description}}</textarea>
                                @error('description')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary mr-2" id="role_update_btn"><i id="role_update_loder" style="font-size:15px"></i>  Update</button>
                            <button type="button" class="btn btn-light" id="reset_update_role_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function () {
            $('#reset_update_role_form').click(function () {
                $('#update_role_form')[0].reset();
            });

            $('#update_role_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'name':{
                        required:true,
                    },
                    'description':{
                        required:true,
                    },
                },
                messages:{
                    'name':{
                        required:"Please enter role name."
                    },
                    'description':{
                        required:"Please enter role description.",
                    },
                },
                submitHandler:function (form) {
                    $('#role_update_loder').addClass("fa fa-spinner fa-pulse");
                    $('#role_update_btn').attr('disabled',true);
                    form.submit();
                }
            });

            $('#name').keyup(function () {
                var value = $(this).val();
                $('#slug').val(value.replace(/\s+/g, '-').toLowerCase());
            });

        });

    </script>
@endsection