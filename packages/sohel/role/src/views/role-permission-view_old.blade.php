@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/setting/roles')}}">List Role&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">View Permission</p>
                        </div>
                        <br>
                        <form id="permission_form" method="post">
                            @foreach($permission_arr as $model => $permissions)
                                <h4># {{$model}}</h4>
                                <div class="table-responsive pt-3 ">
                                    <table class="table table-bordered ">
                                        <thead>
                                            <tr>
                                                @foreach($permissions as $key => $permission)
                                                    <th>
                                                        <input type="checkbox" @if(count($role_permission) > 0 && in_array($permission['id'], $role_permission)) checked @endif  name="permissions[]" value="{{$permission['id']}}"> {{$permission['name']}}
                                                    </th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <br>
                            @endforeach
                                <button type="button" class="btn btn-primary mr-2" id="role_permission_submit_btn"><i id="role_permission_submit_loder" style="font-size:15px"></i>  Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
  <script>
      $('#role_permission_submit_btn').click(function () {
          $('#role_permission_submit_loder').addClass("fa fa-spinner fa-pulse");
          $('#permission_form').submit();
      });
  </script>

@endsection