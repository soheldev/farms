@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="db-body">
        <form id="permission_form" method="post">
            @foreach($permission_arr as $model => $permissions)
                <div class="inner-forms">
                    <div class="box">
                        <div class="box-title">
                            <h2># {{$model}}</h2>
                        </div>
                        <div class="add-customer">
                            <div class="row">
                                @foreach($permissions as $key => $permission)
                                    <div class="col-sm-3 col-xs-12 form-group">
                                        <div class="cust-form relative">
                                            <div class="relative">
                                                <input type="checkbox" @if(count($role_permission) > 0 && in_array($permission['id'], $role_permission)) checked @endif name="permissions[]" value="{{$permission['id']}}">
                                                <label for="ch-1">{{$permission['name']}}</label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <div class="cust-form relative">
                            <ul class="list-inline">
                                <li><button type="button" class="btn cust-btn btn-save" id="role_permission_submit_btn"><i id="role_permission_submit_loder" style="font-size:15px"></i> Save</button></li>
                            </ul>
                        </div>
                    </div>
                </div>
        </form>
    </div>
@endsection

@section('jcontent')
  <script>
      $('#role_permission_submit_btn').click(function () {
          $('#role_permission_submit_loder').addClass("fa fa-spinner fa-pulse");
          $('#permission_form').submit();
      });
  </script>

@endsection