<?php

namespace Sohel\Role\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $table = 'roles';
    protected $fillable = ['name','slug','description','added_by'];
}
