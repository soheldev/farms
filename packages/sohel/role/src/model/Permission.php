<?php

namespace Sohel\Role\Model;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    //
    protected $table = 'permissions';
    protected $fillable = ['name','slug','description','model','status','added_by'];
}
