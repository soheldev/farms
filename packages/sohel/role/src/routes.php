<?php

Route::get('/{user_type}/setting/roles','Sohel\Role\RoleController@index')->middleware(['web','auth']);
Route::post('/fetch/role-data','Sohel\Role\RoleController@fetchRoleData')->middleware(['web','auth']);
Route::get('/{user_type}/setting/roles/add','Sohel\Role\RoleController@createRole')->middleware(['web','auth']);
Route::post('/{user_type}/setting/roles/add','Sohel\Role\RoleController@createRole')->middleware(['web','auth']);
Route::get('/{user_type}/setting/roles/view/{role_id}','Sohel\Role\RoleController@viewRole')->middleware(['web','auth']);
Route::get('/{user_type}/setting/roles/edit/{role_id}','Sohel\Role\RoleController@updateRole')->middleware(['web','auth']);
Route::post('/{user_type}/setting/roles/edit/{role_id}','Sohel\Role\RoleController@updateRole')->middleware(['web','auth']);
Route::post('/delete/roles','Sohel\Role\RoleController@deleteRole')->middleware(['web','auth']);

// Set Permission
Route::get('/{user_type}/setting/roles/set-permission/{role_id}','Sohel\Role\RoleController@setPermission')->middleware(['web','auth']);
Route::post('/{user_type}/setting/roles/set-permission/{role_id}','Sohel\Role\RoleController@setPermission')->middleware(['web','auth']);