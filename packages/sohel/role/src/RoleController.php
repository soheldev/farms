<?php

namespace Sohel\Role;

use App\Http\Helpers\CheckType;
use App\Http\Helpers\UniqueId;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sohel\Role\Model\Permission;
use Sohel\Role\Model\Role;
use Sohel\Role\Model\RolePermission;
use Sohel\Vaccine\Model\Vaccine;

class RoleController extends Controller
{
    //
    public function index()
    {
        return view('role::role-list');
    }

    public function fetchRoleData(Request $request)
    {
        $added_by = Auth::user()->id;
        $data = Role::where('added_by',$added_by)->skip($request->skip)->limit(5)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function createRole(Request $request)
    {
        if($request->method() == "GET")
        {
            return view('role::role-add');
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'name' => 'required',
                'description' => 'required',
            ],
                $messages = [
                    'name.required'    => 'Please enter role name.',
                    'description.required'    => 'Please enter description.'
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $role_arr = [];
                $role_arr['name'] = $request->name;
                $role_arr['slug'] = $request->slug;
                $role_arr['description'] = $request->description;
                $role_arr['added_by'] = Auth::user()->id;
                Role::create($role_arr);
                $type = CheckType::checkAuthType();
                return redirect($type.'/setting/roles')->with(['success' => 'Role Added Successfully...']);
            }
        }

    }


    public function viewRole($user, $role_id)
    {
        $id = base64_decode($role_id);
        $view_role = Role::find($id);
        return view('role::role-view',compact('view_role'));
    }


    public function updateRole($user, $role_id, Request $request)
    {
        $id = base64_decode($role_id);
        $edit_role = Role::find($id);
        if($request->method() == "GET")
        {
            return view('role::role-edit',compact('edit_role'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'name' => 'required',
                'description' => 'required',
            ],
                $messages = [
                    'name.required'    => 'Please enter role name.',
                    'description.required'    => 'Please enter description.'
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $edit_role->name = $request->name;
                $edit_role->slug = $request->slug;
                $edit_role->description = $request->description;
                $edit_role->save();
                $type = CheckType::checkAuthType();
                return redirect($type.'/setting/roles')->with(['success' => 'Role Updated Successfully...']);
            }
        }
    }

    public function deleteRole(Request $request)
    {
        $id = base64_decode($request->id);
        $delete_role = Role::find($id);
        if(isset($delete_role))
        {
            $role_id = $delete_role->id;
            if($delete_role->delete())
            {
                DB::table('role_permissions')->where('role_id',$role_id)->delete();
                $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Role Deleted Successfully...');
                return json_encode($result);
            }
            else
            {
                $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something Went Wrong...');
                return json_encode($result);
            }

        }
    }

    public function setPermission($user, $role_id, Request $request)
    {
        $id = base64_decode($role_id);
        if($request->method() == "GET")
        {
            $permissions = Permission::where('status','1')->orderBy('model')->get(['id','name','model'])->toArray();
            $role_permission = RolePermission::where('role_id',$id)->get()->pluck('permission_id')->toArray();
            $permission_arr = $this->arrayGroupBy($permissions,'model');
            return view('role::role-permission-view',compact('permission_arr','role_permission'));
        }
        else
        {
            $role_permission_arr = [];
            if(isset($request->permissions) && count($request->permissions) > 0)
            {
                foreach($request->permissions as $key => $permission)
                {
                    $role_permission_arr[$key]['role_id'] = $id;
                    $role_permission_arr[$key]['permission_id'] = $permission;
                    $role_permission_arr[$key]['added_by'] = Auth::user()->id;
                }
            }

            DB::table('role_permissions')->where('role_id',$id)->delete();
            DB::table('role_permissions')->insert($role_permission_arr);
            $type = CheckType::checkAuthType();
            return redirect($type.'/setting/roles')->with(['success' => 'Permission Set Successfully...']);

        }
    }

    public function arrayGroupBy($array, $key) {
        $return = array();
        foreach ($array as $val)
        {
            $val = (array) $val;
            $return[$val[$key]][] = $val;
        }
        return $return;
    }
}
