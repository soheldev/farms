<?php

namespace Sohel\Admin;

use App\Http\Helpers\ConnectDatabase;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    //

    public function __construct(Request $request)
    {
        $subdomains = explode('.', $request->getHost());
        $subdomain = $subdomains[0];
        ConnectDatabase::connectDB($subdomain);
    }


    public function viewAdminLogin()
    {
        return view('admin::admin-login');
    }

    public function viewAdminDashboard()
    {
        return view('admin::admin-dashboard');
    }

    public function adminLogout()
    {
        Auth::logout();
        return redirect('/login')->with(['error' => 'You have successfully logout']);
    }

    public function adminProfile(Request $request)
    {
        $admin_profile = Auth::user();
        if($request->method() == 'GET')
        {
            return view('admin::admin-profile',compact('admin_profile'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'email' => 'required',
                'mobile_number' => 'required',
                'name' => 'required',
            ],
            $messages = [
                'email.required'    => 'Please enter email id.',
                'name.required'    => 'Please enter name.',
                'mobile_number.required' => 'Please enter mobile number.',
            ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $admin_profile->email = $request->email;
                $admin_profile->name = $request->name;
                if($request->hasFile('profile_image'))
                {
                    if (! \File::exists(public_path()."/profile-pictures")) {
                        \File::makeDirectory(public_path()."/profile-pictures");
                    }
                    $imageName = time().'.'.request()->profile_image->getClientOriginalExtension();
                    request()->profile_image->move(public_path('/profile-pictures/'), $imageName);
                    $admin_profile->userInformation->profile_image = $imageName;
                }
                $admin_profile->userInformation->mobile_number = $request->mobile_number;
                $admin_profile->userInformation->save();
                $admin_profile->save();
                return redirect()->back()->with(['success' => 'Profile Updated Successfully...']);



            }

        }

    }

    public function checkAdminMailDuplication(Request $request)
    {
        $check_email = User::where('email',$request->email)->where('id','<>',$request->id)->first();
        if(isset($check_email))
        {
            return 'false';
        }
        else
        {
            return 'true';
        }

    }

    public function updateAdminProfile(Request $request)
    {
        $id = $request->id;
        $name = $request->name;
        $email = $request->email;
        $mobile = $request->mobile_number;
        $update_admin_profile = User::find($id);
        if(isset($update_admin_profile)) {
            dd($update_admin_profile->userInformation);
            $update_admin_profile->name = $name;
            $update_admin_profile->email = $email;
            $update_admin_profile->save();
            $update_admin_profile->userInformation->mobile_number = $mobile;
            $update_admin_profile->userInformation->save();
        }
    }
}
