<?php
//Route::get('/admin/login','Sohel\Admin\AdminController@viewAdminLogin')->middleware(['web']);



Route::get('/admin/dashboard','Sohel\Admin\AdminController@viewAdminDashboard')->middleware(['web','auth','check.connection']);
Route::post('/admin/logout','Sohel\Admin\AdminController@adminLogout')->middleware(['web','check.connection']);
Route::get('/admin/profile','Sohel\Admin\AdminController@adminProfile')->middleware(['web','auth','check.connection']);
Route::post('/admin/profile','Sohel\Admin\AdminController@adminProfile')->middleware(['web','auth','check.connection']);
Route::post('/admin-check-mail-duplication','Sohel\Admin\AdminController@checkAdminMailDuplication')->middleware(['web','auth','check.connection']);
Route::post('/admin/update/profile','Sohel\Admin\AdminController@updateAdminProfile')->middleware(['web','auth','check.connection']);
