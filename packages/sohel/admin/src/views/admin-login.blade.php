<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Admin Login</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{url('/public/media/backend')}}/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="{{url('/public/media/backend')}}/vendors/base/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{url('/public/media/backend')}}/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{url('/public/media/backend')}}/images/favicon.png"/>
</head>

<body>
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
            <div class="row flex-grow">
                <div class="col-lg-6 d-flex align-items-center justify-content-center">
                    <div class="auth-form-transparent text-left p-3">
                        <div class="brand-logo">
                            <img src="{{url('/public/media/backend')}}/images/logo.svg" alt="logo">
                        </div>
                        <h4>Welcome back!</h4>
                        <h6 class="font-weight-light">Happy to see you again!</h6>
                        <form class="pt-3" action="{{ url('/login') }}" method="POST" id="admin_login_form">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="exampleInputEmail">Email Id</label>
                                <div class="input-group">
                                    <div class="input-group-prepend bg-transparent">
                                        <span class="input-group-text bg-transparent border-right-0">
                                            <i class="mdi mdi-account-outline text-primary"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control form-control-lg border-left-0" id="email" name="email" placeholder="Email" value="{{ old('email') }}" autocomplete="email" autofocus>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                {{--@error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror--}}
                                <label id="email-error" class="text-danger" for="email"></label>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword">Password</label>
                                <div class="input-group">
                                    <div class="input-group-prepend bg-transparent">
                                        <span class="input-group-text bg-transparent border-right-0">
                                            <i class="mdi mdi-lock-outline text-primary"></i>
                                        </span>
                                    </div>
                                    <input type="password" class="form-control form-control-lg border-left-0" id="password" name="password" placeholder="Password">
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                {{--@error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror--}}
                                <label id="password-error" class="text-danger" for="password"></label>
                            </div>
                            <div class="my-2 d-flex justify-content-between align-items-center">
                                <div class="form-check">
                                    <label class="form-check-label text-muted">
                                        <input type="checkbox" class="form-check-input">
                                        Keep me signed in
                                    </label>
                                </div>
                                {{--<a href="javascript:void(0);" class="auth-link text-black">Forgot password?</a>--}}
                            </div>
                            <div class="my-3">
                                <input type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"
                                   href="javascript:void(0);" value="LOGIN">
                            </div>
                            <div class="mb-2 d-flex">
                                {{--<button type="button" class="btn btn-facebook auth-form-btn flex-grow mr-1">
                                    <i class="mdi mdi-facebook mr-2"></i>Facebook
                                </button>--}}
                                {{--<button type="button" class="btn btn-google auth-form-btn flex-grow ml-1">
                                    <i class="mdi mdi-google mr-2"></i>Google
                                </button>--}}
                            </div>
                            {{--<div class="text-center mt-4 font-weight-light">
                                Don't have an account? <a href="register-2.html" class="text-primary">Create</a>
                            </div>--}}
                        </form>
                    </div>
                </div>
                <div class="col-lg-6 login-half-bg d-flex flex-row">
                    <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy; 2018
                        All rights reserved.</p>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="{{url('/public/media/backend')}}/vendors/base/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="{{url('/public/media/backend')}}/js/jquery-3.4.1.js"></script>
<script src="{{url('/public/media/backend')}}/js/off-canvas.js"></script>
<script src="{{url('/public/media/backend')}}/js/hoverable-collapse.js"></script>
<script src="{{url('/public/media/backend')}}/js/template.js"></script>
<script src="{{url('/public/media/backend')}}/js/jquery.validate.js"></script>

<script>
    $(function () {
        $('#admin_login_form').validate({
            errorClass: 'text-danger',
            rules:{
                'email':{
                    required:true
                },
                'password':{
                    required:true
                }
            },
            messages:{
                'email':{
                    required:'Please enter your email id.'
                },
                'password':{
                    required:'Please enter your password.'
                }
            },
            submitHandler:function (form) {
                form.submit();
            }
        });
        
    })
</script>

<!-- endinject -->
</body>

</html>
