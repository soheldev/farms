<?php

Route::get('/{user_type}/deworming/agent/list','Sohel\Deworming\DewormingController@index')->middleware(['web','auth']);
Route::post('/fetch/deworming-agent-data','Sohel\Deworming\DewormingController@fetchDewormingAgentData')->middleware(['web','auth']);
Route::get('/{user_type}/deworming/agent/add','Sohel\Deworming\DewormingController@createDeworming')->middleware(['web','auth']);
Route::post('/{user_type}/deworming/agent/add','Sohel\Deworming\DewormingController@createDeworming')->middleware(['web','auth']);
Route::get('/{user_type}/deworming/agent/view/{deworming_id}','Sohel\Deworming\DewormingController@viewDeworming')->middleware(['web','auth']);
Route::get('/{user_type}/deworming/agent/edit/{deworming_id}','Sohel\Deworming\DewormingController@updateDeworming')->middleware(['web','auth']);
Route::post('/{user_type}/deworming/agent/edit/{deworming_id}','Sohel\Deworming\DewormingController@updateDeworming')->middleware(['web','auth']);
Route::post('/delete/deworming','Sohel\Deworming\DewormingController@deleteDeworming')->middleware(['web','auth']);