<?php

namespace Sohel\Deworming;

use App\Http\Helpers\CheckPermission;
use App\Http\Helpers\CheckType;
use App\Http\Helpers\UniqueId;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Sohel\Deworming\Model\Deworming;
use Sohel\Vaccine\Model\Vaccine;

class DewormingController extends Controller
{
    //
    public function index()
    {
        $add_deworming_permission = false;
        $update_deworming_permission = false;
        $delete_deworming_permission = false;
        $add_deworming_permission = CheckPermission::hasPermission('create.deworming');
        $update_deworming_permission = CheckPermission::hasPermission('update.deworming');
        $delete_deworming_permission = CheckPermission::hasPermission('delete.deworming');
        return view('deworming::deworming-list',compact('add_deworming_permission','update_deworming_permission','delete_deworming_permission'));
    }

    public function fetchDewormingAgentData(Request $request)
    {
        $added_by = Auth::user()->id;
        $data = Deworming::skip($request->skip)->limit(5)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function createDeworming(Request $request)
    {
        if($request->method() == "GET")
        {
            return view('deworming::deworming-add');
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'deworming_agent_name' => 'required',
                'body_weight' => 'required',
                'dosage' => 'required',
            ],
                $messages = [
                    'deworming_agent_name.required'    => 'Please enter deworming agent name.',
                    'body_weight.required'    => 'Please enter body weight.',
                    'dosage.required'    => 'Please enter dosage.',
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $unique_number = UniqueId::generateUniqueIdentification(10);
                $deworming_id = 'DW'.$unique_number;
                $deworming_arr = [];
                $deworming_arr['deworming_id'] = $deworming_id;
                $deworming_arr['deworming_agent_name'] = $request->deworming_agent_name;
                $deworming_arr['body_weight'] = $request->body_weight;
                $deworming_arr['dosage'] = $request->dosage;
                $deworming_arr['type'] = $request->type;
                $deworming_arr['notes'] = $request->deworming_notes;
                $deworming_arr['added_by'] = Auth::user()->id;
                Deworming::create($deworming_arr);
                $type = CheckType::checkAuthType();
                return redirect($type.'/deworming/agent/list')->with(['success' => 'Deworming Agent Added Successfully...']);
            }
        }

    }


    public function viewDeworming($user, $deworming_id)
    {
        $id = base64_decode($deworming_id);
        $view_deworming = Deworming::find($id);
        return view('deworming::deworming-view',compact('view_deworming'));
    }


    public function updateDeworming($user, $deworming_id, Request $request)
    {
        $id = base64_decode($deworming_id);
        $edit_deworming = Deworming::find($id);
        if($request->method() == "GET")
        {
            return view('deworming::deworming-edit',compact('edit_deworming'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'deworming_agent_name' => 'required',
                'body_weight' => 'required',
                'dosage' => 'required',
            ],
                $messages = [
                    'deworming_agent_name.required'    => 'Please enter deworming agent name.',
                    'body_weight.required'    => 'Please enter body weight.',
                    'dosage.required'    => 'Please enter dosage.',
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $edit_deworming->deworming_agent_name = $request->deworming_agent_name;
                $edit_deworming->body_weight = $request->body_weight;
                $edit_deworming->dosage = $request->dosage;
                $edit_deworming->notes = $request->deworming_notes;
                $edit_deworming->save();
                $type = CheckType::checkAuthType();
                return redirect($type.'/deworming/agent/list')->with(['success' => 'Deworming Agent Updated Successfully...']);
            }
        }
    }

    public function deleteDeworming(Request $request)
    {
        $id = base64_decode($request->id);
        $delete_deworming = Deworming::find($id);
        if(isset($delete_deworming))
        {
            if($delete_deworming->delete())
            {
                $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Deworming Agent Deleted Successfully...');
                return json_encode($result);
            }
            else
            {
                $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something Went Wrong...');
                return json_encode($result);
            }

        }
    }
}
