@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)
@section('content')
    <style>
        .my-custom-scrollbar {
            position: relative;
            height: 400px;
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>

    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">List Deworming Agent</p>
                        </div>
                        <br>
                        @if($add_deworming_permission == true)
                            <div class="pull-right">
                                <a type="button" class="btn btn-success btn-rounded" href="{{url($user.'/deworming/agent/add')}}">Add New</a>
                            </div>@endif
                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>Deworming Id</th>
                                    <th>Deworming Name</th>
                                    <th>Dosage (In ML)</th>
                                    <th>Deworming Type</th>
                                    @if($update_deworming_permission == true || $delete_deworming_permission == true)
                                        <th>Action</th>
                                    @endif
                                </tr>
                                </thead>

                                <tbody id="deworming_agent_table">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('jcontent')
    <script>
        var site_path = '{{url('/')}}';
        var user = '{{$user}}';
        var skip = 0;
        var i = 0;
        var update_deworming_permission = '{{$update_deworming_permission}}';
        var delete_deworming_permission = '{{$delete_deworming_permission}}';
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/deworming-agent-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td><a href="'+site_path+''+user+'/deworming/agent/view/'+btoa(this.id)+'">'+this.deworming_id+'</a></td>';
                            html_content += '<td>'+this.deworming_agent_name+'</td>';
                            html_content += '<td>'+this.dosage+' ML / '+this.body_weight+' KG</td>';
                            if(this.type == '1')
                            {
                                html_content += '<td>Internal</td>';
                            }
                            else
                            {
                                html_content += '<td>External</td>';
                            }
                            if(update_deworming_permission == true || delete_deworming_permission == true)
                            {
                                html_content += '<td><center>';
                                if(update_deworming_permission == true)
                                {
                                    html_content += '<a title="Edit" href="'+site_path+''+user+'/deworming/agent/edit/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; ';
                                }
                                if(delete_deworming_permission == true)
                                {
                                    html_content += '<a title="delete" href="javascript:void(0)" class="deworming_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
                                }
                                html_content += '</center></td>';
                            }
                            //html_content += '<td><center><a title="Edit" href="'+site_path+''+user+'/deworming/agent/edit/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; <a title="delete" href="javascript:void(0)" class="deworming_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></center></td>';
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="5"><center>No Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#deworming_agent_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 5;
                    fetchRecord(skip);
                }
            };
            $('body').on('click','.deworming_delete',function () {
                var id = btoa($(this).attr('data-attribute-id'));
                var _this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to delete this deworming agent.!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#71c016',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function(result){
                    if (result.value) {

                        $.ajax({
                            url: site_path + '/delete/deworming',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                id: id,
                            },
                            success: function (response) {
                                if(response.status == '1')
                                {
                                    _this.parent('center').parent('td').parent('tr').remove();
                                    if($('#deworming_agent_table tr').length == 0)
                                    {
                                        $('#deworming_agent_table').append('<tr><td colspan="5" class="no_record_found_row"><center>No Record Found...</center></td></tr>');
                                    }
                                }
                                Swal.fire({
                                    icon: response.icon,
                                    title: response.title,
                                    text: response.text,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            },
                            beforeSend: function () {

                            },
                            complete: function () {

                            },
                            error: function () {

                            }


                        });
                    }
            })
            });
        });
    </script>
@endsection