@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/deworming/agent/list')}}">List Deworming Agent&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">View Deworming Agent</p>
                        </div>
                        <br>
                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>
                                        Deworming Id : <span>#{{$view_deworming->deworming_id}}</span>
                                    </th>
                                    <th>
                                        Deworming Name : <span>{{$view_deworming->deworming_agent_name}}</span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Dosage : <span>{{$view_deworming->dosage}} ML / {{$view_deworming->body_weight}} KG</span>
                                    </th>
                                    <th>
                                        Notes : <span>{{isset($view_deworming->notes)?$view_deworming->notes:'N/A'}}</span>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')

@endsection