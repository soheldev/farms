@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Edit Deworming Agent</h2>
                </div>
                <div class="add-customer">
                    <form id="update_deworming_form" method="POST" action="" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="type" name="type">
                                        <option value="">Select Deworming Type*</option>
                                        <option value="1" @if($edit_deworming->type == '1') selected @endif>Internal</option>
                                        <option value="2" @if($edit_deworming->type == '2') selected @endif>External</option>
                                    </select>
                                    @error('type')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="deworming_agent_name" name="deworming_agent_name"
                                           placeholder="Enter Deworming Agent Name*" class="form-control" value="{{$edit_deworming->deworming_agent_name}}">
                                    @error('deworming_agent_name')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="body_weight" name="body_weight"
                                           placeholder="Body Weight (In KG)*" class="form-control" value="{{$edit_deworming->body_weight}}">
                                    @error('body_weight')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="dosage" name="dosage" placeholder="Dosage (In ML)*"
                                           class="form-control" value="{{$edit_deworming->dosage}}">
                                    @error('dosage')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12 form-group">
                                    <textarea class="form-control" id="deworming_notes" name="deworming_notes"
                                              placeholder="Enter Notes" value="" rows="5">{{$edit_deworming->notes}}</textarea>
                                @error('deworming_notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li>
                                            <button type="submit" class="btn cust-btn btn-save"
                                                    id="deworming_update_btn"><i id="deworming_update_loder"
                                                                                 style="font-size:15px"></i> Update
                                            </button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn cust-btn btn-grey"
                                                    id="reset_update_deworming_form">Cancel
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function () {
            $('#reset_update_deworming_form').click(function () {
                $('#update_deworming_form')[0].reset();
            });

            $('#update_deworming_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'type':{
                        required:true,
                    },
                    'deworming_agent_name':{
                        required:true,
                    },
                    'body_weight':{
                        required:true,
                    },
                    'dosage':{
                        required:true,
                    },
                },
                messages:{
                    'type':{
                        required:"Please select deworming type."
                    },
                    'deworming_agent_name':{
                        required:"Please enter deworming agent name."
                    },
                    'body_weight':{
                        required:"Please enter body weight."
                    },
                    'dosage':{
                        required:"Please enter dosage."
                    },
                },
                submitHandler:function (form) {
                    $('#deworming_update_loder').addClass("fa fa-spinner fa-pulse");
                    $('#deworming_update_btn').attr('disabled',true);
                    form.submit();
                }
            });

        })

    </script>
@endsection