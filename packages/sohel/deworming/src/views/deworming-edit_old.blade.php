@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/deworming/agent/list')}}">List Deworming Agent&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Edit Deworming Agent</p>
                        </div>
                        <br>
                        <form class="forms-sample" id="update_deworming_form" method="POST" action="" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="exampleInputName1">Deworming Type<sup style="color: red">*</sup></label>
                                <select class="form-control" id="type" name="type">
                                    <option value="">Select Deworming Type</option>
                                    <option value="1" @if($edit_deworming->type == '1') selected @endif>Internal</option>
                                    <option value="2" @if($edit_deworming->type == '2') selected @endif>External</option>
                                </select>
                                @error('type')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Deworming Agent Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="deworming_agent_name" name="deworming_agent_name" placeholder="Enter Deworming Agent Name" value="{{$edit_deworming->deworming_agent_name}}">
                                @error('deworming_agent_name')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Body Weight (In KG)<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="body_weight" name="body_weight" placeholder="Enter Body Weight" value="{{$edit_deworming->body_weight}}">
                                @error('body_weight')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Dosage (In ML)<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="dosage" name="dosage" placeholder="Enter Dosage" value="{{$edit_deworming->dosage}}">
                                @error('dosage')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Notes</label>
                                <textarea class="form-control" id="deworming_notes" name="deworming_notes" placeholder="Enter Notes" rows="5">{{$edit_deworming->notes}}</textarea>
                                @error('deworming_notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary mr-2" id="deworming_update_btn"><i id="deworming_update_loder" style="font-size:15px"></i>  Update</button>
                            <button type="button" class="btn btn-light" id="reset_update_deworming_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function () {
            $('#reset_update_deworming_form').click(function () {
                $('#update_deworming_form')[0].reset();
            });

            $('#update_deworming_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'type':{
                        required:true,
                    },
                    'deworming_agent_name':{
                        required:true,
                    },
                    'body_weight':{
                        required:true,
                    },
                    'dosage':{
                        required:true,
                    },
                },
                messages:{
                    'type':{
                        required:"Please select deworming type."
                    },
                    'deworming_agent_name':{
                        required:"Please enter deworming agent name."
                    },
                    'body_weight':{
                        required:"Please enter body weight."
                    },
                    'dosage':{
                        required:"Please enter dosage."
                    },
                },
                submitHandler:function (form) {
                    $('#deworming_update_loder').addClass("fa fa-spinner fa-pulse");
                    $('#deworming_update_btn').attr('disabled',true);
                    form.submit();
                }
            });

        })

    </script>
@endsection