<?php

namespace Sohel\Deworming\Model;

use Illuminate\Database\Eloquent\Model;

class Deworming extends Model
{
    //
    protected $table = 'dewormings';
    protected $fillable = ['deworming_id','deworming_agent_name','body_weight','dosage','notes','added_by','type'];
}
