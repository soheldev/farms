<?php

namespace Sohel\Deworming;

use Illuminate\Support\ServiceProvider;

class DewormingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->make('Sohel\Deworming\DewormingController');
        $this->loadViewsFrom(__DIR__.'/views', 'deworming');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__.'/routes.php';
    }
}
