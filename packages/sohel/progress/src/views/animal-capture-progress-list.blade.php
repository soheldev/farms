@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)
@section('content')
    <div class="db-body">
        <div class="inner-forms">
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif
            <div class="box ">
                <div class="box-title clearfix">
                    <h2 class="pull-left">Animal Capture Progress List</h2>
                </div>

                <div class="table-resposnive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Animal Id</th>
                            <th>Animal Name</th>
                            <th>Weight</th>
                            <th>Height</th>
                            <th>Due Date<br><span style="font-size: 8px">YYYY-MM-DD</span></th>
                            <th>Captured On<br><span style="font-size: 8px">YYYY-MM-DD</span></th>
                            @if($update_animal_progress_permission == true)
                                <th>Action</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody id="animal_capture_progress_table">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('jcontent')
    <script>
        var site_path = '{{url('/')}}';
        var user = '{{$user}}';
        var skip = 0;
        var i = 0;
        var search_value = '';
        var update_animal_progress_permission = '{{$update_animal_progress_permission}}';
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/animal-capture-progress-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip,
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td><a href="'+site_path+''+user+'/animal/view/'+btoa(this.animal_id)+'">'+this.animal_unique_id+'</a></td>';
                            html_content += '<td>'+this.animal_name+'</td>';
                            html_content += '<td>'+this.weight+'</td>';
                            html_content += '<td>'+this.height+'</td>';
                            html_content += '<td>'+this.due_date+'</td>';
                            html_content += '<td>'+this.capture_date+'</td>';
                            if(update_animal_progress_permission == true)
                            {
                                html_content += '<td>';
                                html_content += '<ul class="list-inline">';
                                html_content += '<li><a title="Edit" href="' + site_path + '' + user + '/capture/progress/edit/' + btoa(this.id) + '" class="action-btn btn-yellow edit" data-attribute-id="' + this.id + '"><i class="fa fa-pencil "></i></a></li>';
                                html_content += '</ul>';
                                html_content += '</td>';
                            }



                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="7"><center>No Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#animal_capture_progress_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 10;
                    fetchRecord(skip);
                }
            };

            $('body').on('click','.monthly_progress_btn',function () {
                var animal_id = $(this).attr('data-attribute-animal');
                var redirect_vacination_url = site_path+user+'/progress/animal/'+animal_id;
                window.location.href = redirect_vacination_url;
            });

            function searchRecord(skip,search_value)
            {
                $.ajax({
                    url: site_path+'/fetch/animal-capture-progress-data',
                    type: 'POST',
                    dataType: 'JSON',
                    data:{
                        skip:skip,
                        search_value:search_value
                    },
                    success:function (response) {
                        console.log(response);
                        var html_content = '';
                        if(response.status == "1")
                        {
                            $.each(response.data,function (index,value) {

                                html_content += '<tr>';
                                html_content += '<td><a href="'+site_path+''+user+'/animal/view/'+btoa(this.animal_id)+'">'+this.animal_unique_id+'</a></td>';
                                html_content += '<td>'+this.animal_name+'</td>';
                                html_content += '<td>'+this.weight+'</td>';
                                html_content += '<td>'+this.height+'</td>';
                                html_content += '<td>'+this.due_date+'</td>';
                                html_content += '<td>'+this.capture_date+'</td>';
                                if(update_animal_progress_permission == true)
                                {
                                    html_content += '<td><center><a title="Edit" href="'+site_path+''+user+'/capture/progress/edit/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></center></td>';
                                }
                                html_content += '</tr>';
                            });
                        }
                        else
                        {
                            html_content += '<tr><td colspan="7"><center>No Record Found...</center></td></tr>';
                        }
                        $('#animal_capture_progress_table').html(html_content);

                    },
                    beforeSend:function () {
                        $('#animal_capture_progress_table').html('<tr><td colspan="5"><center><i class="fa fa-spinner fa-pulse fa-2x"><i/></center></td></tr>');
                    },
                    complete:function(){

                    },
                    error:function(){

                    }
                });
            }

            $('#search_text').keyup(function () {
                search_value = $(this).val();
                skip = 0;
                searchRecord(skip,search_value);
            });


        });
    </script>
@endsection