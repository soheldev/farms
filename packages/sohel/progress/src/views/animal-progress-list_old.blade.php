@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)
@section('content')
    <style>
        .my-custom-scrollbar {
            position: relative;
            height: 400px;
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Pending Animal Progress List</p>
                        </div>
                        <br>
                        {{--<div class="pull-right">
                            <a type="button" class="btn btn-success btn-rounded" href="{{url($user.'/animal/add')}}">Add New</a>
                        </div>--}}

                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>Animal Id</th>
                                    <th>Animal Name</th>
                                    <th>Due Date<br><span style="font-size: 8px">YYYY-MM-DD</span></th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody id="animal_progress_table">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('jcontent')
    <script>
        var site_path = '{{url('/')}}';
        var user = '{{$user}}';
        var skip = 0;
        var i = 0;
        var search_value = '';
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/animal-pending-progress-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip,
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td><a href="'+site_path+''+user+'/animal/view/'+btoa(this.animal_id)+'">'+this.animal_unique_id+'</a></td>';
                            html_content += '<td>'+this.animal_name+'</td>';
                            html_content += '<td>'+this.due_date+'</td>';
                            html_content += '<td><button title="Enter Monthly Progress" data-attribute-animal="'+btoa(this.id)+'" class="btn btn-primary monthly_progress_btn">Add Monthly Progress Report</button></td>';
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="4"><center>No Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#animal_progress_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 5;
                    fetchRecord(skip);
                }
            };

            $('body').on('click','.monthly_progress_btn',function () {
                var animal_id = $(this).attr('data-attribute-animal');
                var redirect_vacination_url = site_path+user+'/progress/animal/'+animal_id;
                window.location.href = redirect_vacination_url;
            });

            function searchRecord(skip,search_value)
            {
                $.ajax({
                    url: site_path+'/fetch/animal-pending-progress-data',
                    type: 'POST',
                    dataType: 'JSON',
                    data:{
                        skip:skip,
                        search_value:search_value
                    },
                    success:function (response) {
                        console.log(response);
                        var html_content = '';
                        if(response.status == "1")
                        {
                            $.each(response.data,function (index,value) {

                                html_content += '<tr>';
                                html_content += '<td><a href="'+site_path+''+user+'/animal/view/'+btoa(this.animal_id)+'">'+this.animal_unique_id+'</a></td>';
                                html_content += '<td>'+this.animal_name+'</td>';
                                html_content += '<td>'+this.due_date+'</td>';
                                html_content += '<td><button title="Enter Monthly Progress" data-attribute-animal="'+btoa(this.id)+'" class="btn btn-primary monthly_progress_btn">Add Monthly Progress Report</button></td>';
                                html_content += '</tr>';
                            });
                        }
                        else
                        {
                            html_content += '<tr><td colspan="4"><center>No Record Found...</center></td></tr>';
                        }
                        $('#animal_progress_table').html(html_content);

                    },
                    beforeSend:function () {
                        $('#animal_progress_table').html('<tr><td colspan="4"><center><i class="fa fa-spinner fa-pulse fa-2x"><i/></center></td></tr>');
                    },
                    complete:function(){

                    },
                    error:function(){

                    }
                });
            }

            $('#search_text').keyup(function () {
                search_value = $(this).val();
                skip = 0;
                searchRecord(skip,search_value);
            });


        });
    </script>
@endsection