@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Add Monthly Progress For {{$animal_detail->prefix.$animal_detail->animal_id}}</h2>
                </div>
                <div class="add-customer">
                    <form id="add_animal_progress_form" method="POST" action="" enctype="multipart/form-data" autocomplete="off">
                        <input type="hidden" name="animal_id" id="animal_id" value="{{$animal_detail->id}}">


                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="weight" name="weight"
                                           placeholder="Enter Animal Weight (In Kgs)*" class="form-control">
                                    @error('weight')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="height" name="height" placeholder="Enter Animal Height (In Inch)*"
                                           class="form-control">
                                    @error('height')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="due_date" name="due_date"
                                           placeholder="Due Date" class="form-control"
                                           value="{{$pending_progress->due_date}}" readonly>
                                    @error('due_date')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="capture_date" name="capture_date" placeholder="Select Date Capture*"
                                           class="form-control">
                                    @error('capture_date')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12 col-xs-12 form-group">
                                    <textarea class="form-control" id="notes" name="notes"
                                              placeholder="Enter Notes" value="" rows="5"></textarea>
                                @error('notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li>
                                            <button type="submit" class="btn cust-btn btn-save"
                                                    id="animal_progress_submit_btn"><i id="animal_progress_submit_loder"
                                                                                        style="font-size:15px"></i> Save
                                            </button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn cust-btn btn-grey"
                                                    id="reset_animal_progress_form">Cancel
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function ()
        {
            $( "#capture_date" ).datepicker({
                dateFormat : 'yy-mm-d',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });

            $('#reset_animal_progress_form').click(function () {
                $('#add_animal_progress_form')[0].reset();
            });

            $("#weight").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#height").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });




            $('#add_animal_progress_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'weight':{
                        required:true,
                    },
                    'height':{
                        required:true,
                    },
                    'capture_date':{
                        required:true,
                    }
                },
                messages:{
                    'weight':{
                        required:"Please enter animal weight.",
                    },
                    'height':{
                        required:"Please enter animal height.",
                    },
                    'capture_date':{
                        required:"Please select date",
                    }
                },
                submitHandler:function (form) {
                    $('#animal_progress_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#animal_progress_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

        });


    </script>
@endsection