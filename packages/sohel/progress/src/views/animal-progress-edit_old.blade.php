@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Edit Monthly Progress Of {{$animal_detail->prefix.$animal_detail->animal_id}}</p>
                        </div>
                        <br>

                        <form class="forms-sample" id="edit_animal_progress_form" method="POST" action="" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="exampleInputName1">Animal Weight (In Kgs)<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="weight" name="weight" placeholder="Enter Aminal Weight" value="{{$edit_progress->weight}}">
                                @error('weight')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Animal Height (In Inch)<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="height" name="height" placeholder="Enter Aminal Height" value="{{$edit_progress->height}}">
                                @error('height')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Date Capture<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="capture_date" name="capture_date" placeholder="Select Date" value="{{$edit_progress->capture_date}}">
                                @error('capture_date')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Notes</label>
                                <textarea class="form-control" id="notes" name="notes" placeholder="Enter Notes" value="" rows="5">{{$edit_progress->notes}}</textarea>
                                @error('notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary mr-2" id="edit_animal_progress_submit_btn"><i id="edit_animal_progress_submit_loder" style="font-size:15px"></i>  Submit</button>
                            <button class="btn btn-light" id="reset_edit_animal_progress_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function ()
        {
            $( "#capture_date" ).datepicker({
                dateFormat : 'd/m/yy',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });

            $('#reset_edit_animal_progress_form').click(function () {
                $('#edit_animal_progress_form')[0].reset();
            });

            $("#weight").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#height").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });




            $('#edit_animal_progress_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'weight':{
                        required:true,
                    },
                    'height':{
                        required:true,
                    },
                    'capture_date':{
                        required:true,
                    }
                },
                messages:{
                    'weight':{
                        required:"Please enter animal weight.",
                    },
                    'height':{
                        required:"Please enter animal height.",
                    },
                    'capture_date':{
                        required:"Please select date",
                    }
                },
                submitHandler:function (form) {
                    $('#edit_animal_progress_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#edit_animal_progress_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

        });


    </script>
@endsection