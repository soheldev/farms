<?php

namespace Sohel\Progress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CheckPermission;
use App\Http\Helpers\CheckType;
use App\Http\Helpers\UniqueId;
use App\UserInformation;
use Faker\Provider\DateTime;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sohel\Animal\Model\Animal;
use Sohel\Animal\Model\AnimalDisease;
use Sohel\Animal\Model\AnimalVacination;
use Sohel\Animal\Model\CustomerAnimalExit;
use Sohel\Breed\Model\Breed;
use Sohel\City\Model\City;
use Sohel\Country\Model\Country;
use Sohel\Disease\Model\Disease;
use Sohel\Progress\Model\AnimalProgress;
use Sohel\Progress\Model\ProgressReminder;
use Sohel\State\Model\State;
use Sohel\User\Model\Customer;
use Sohel\Vaccine\Model\Vaccine;

class ProgressController extends Controller
{
    public function __construct()
    {
        //$this->checkType();
    }

    public function checkType()
    {
        $user_redirect = '';
        $auth_type = Auth::user()->userInformation->user_type;
        if($auth_type == "1")
        {
            $user_redirect = "/admin";
        }
        else
        {
            $user_redirect = "/vendor";
        }
        return $user_redirect;
    }
    //
    public function index()
    {
        return view('progress::animal-progress-list');
    }

    public function fetchAnimalPendingProgressData(Request $request)
    {
        $added_by = Auth::user()->id;
        $sql = "SELECT prog.id,prog.due_date,prog.status,a.id AS animal_id,CONCAT(a.prefix,a.animal_id) AS animal_unique_id,a.animal_name
                FROM animal_progress AS prog
                JOIN animals AS a
                ON prog.animal_id = a.id";
        $sql = $sql." WHERE prog.status = '0'";
        if(isset($request->search_value))
        {
            $sql = $sql." AND (CONCAT(a.prefix,a.animal_id) LIKE '%$request->search_value%' OR a.animal_name LIKE '%$request->search_value%')";
        }
        $sql = $sql." LIMIT $request->skip , 10";
        $data = DB::select($sql);
        if(count($data) > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function addAnimalProgress($user, $pending_progress_id, Request $request)
    {
        $id = base64_decode($pending_progress_id);
        $pending_progress = AnimalProgress::find($id);
        if($request->method() == "GET")
        {
            $animal_detail = Animal::select('id','prefix','animal_id')->find($pending_progress->animal_id);
            return view('progress::animal-progress-add',compact('animal_detail','pending_progress'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'weight' => 'required',
                'height' => 'required',
                'capture_date' => 'required',
            ],
                $messages = [
                    'weight.required'    => 'Please enter animal weight.',
                    'height.required'    => 'Please enter animal height.',
                    'capture_date.required'    => 'Please select date.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $animal_progress_arr = [];
                $animal_progress_arr['weight'] = $request->weight;
                $animal_progress_arr['height'] = $request->height;
                $animal_progress_arr['capture_date'] = $request->capture_date;
                $animal_progress_arr['notes'] = $request->notes;
                $animal_progress_arr['date'] = date('Y-m-d');
                $animal_progress_arr['status'] = '1';
                DB::table('animal_progress')->where('id',$pending_progress->id)->update($animal_progress_arr);
                $type = CheckType::checkAuthType();
                return redirect($type.'/progress/pending/list')->with(['success' => 'Animals Progress Added Successfully...']);

            }
        }
    }

    public function progressCaptureList()
    {
        $update_animal_progress_permission = CheckPermission::hasPermission('update.progress');
        return view('progress::animal-capture-progress-list',compact('update_animal_progress_permission'));
    }

    public function fetchAnimalCaptureProgressData(Request $request)
    {
        $today_date = date('Y-m-d');

        $sql = "SELECT ap.id,ap.animal_id,ap.weight,ap.height,ap.date,ap.due_date,ap.capture_date,CONCAT(a.prefix,a.animal_id) AS animal_unique_id,a.animal_name
                                                                FROM animal_progress AS ap
                                                                JOIN animals AS a
                                                                ON ap.animal_id = a.id";
        $sql = $sql." WHERE ap.date = '$today_date'";
        if(isset($request->search_value))
        {
            $sql = $sql." AND (a.animal_name LIKE '%$request->search_value%' OR CONCAT(a.prefix,a.animal_id) LIKE '%$request->search_value%')";
        }
        $sql = $sql." LIMIT $request->skip , 10";

        $fetch_todays_capture_progress_data = DB::select($sql);
        //dd($fetch_todays_capture_progress_data);
        if(count($fetch_todays_capture_progress_data) > 0)
        {
            $result = array('status'=>'1','data'=>$fetch_todays_capture_progress_data);
            return json_encode($result);
        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function editCaptureProgress($user, $id, Request $request)
    {
        $id = base64_decode($id);
        $edit_progress = AnimalProgress::find($id);
        if($request->method() == "GET")
        {
            $animal_detail = Animal::select('id','prefix','animal_id')->find($edit_progress->animal_id);
            return view('progress::animal-progress-edit',compact('edit_progress','animal_detail'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'weight' => 'required',
                'height' => 'required',
                'capture_date' => 'required',
            ],
                $messages = [
                    'weight.required'    => 'Please enter animal weight.',
                    'height.required'    => 'Please enter animal height.',
                    'capture_date.required'    => 'Please select date.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $edit_animal_progress_arr = [];
                $edit_animal_progress_arr['weight'] = $request->weight;
                $edit_animal_progress_arr['height'] = $request->height;
                $edit_animal_progress_arr['capture_date'] = $request->capture_date;
                $edit_animal_progress_arr['notes'] = $request->notes;
                DB::table('animal_progress')->where('id',$edit_progress->id)->update($edit_animal_progress_arr);
                $type = CheckType::checkAuthType();
                return redirect($type.'/progress/capture/list')->with(['success' => 'Animals Progress Updated Successfully...']);
            }

        }
    }

    public function progressList()
    {
        return view('progress::animal-view-progress-list');
    }

    public function fetchAnimalProgressData(Request $request)
    {
        $sql = "SELECT ap.id,ap.animal_id,ap.weight,ap.height,ap.date,ap.due_date,ap.capture_date,CONCAT(a.prefix,a.animal_id) AS animal_unique_id,a.animal_name
                                                                FROM animal_progress AS ap
                                                                JOIN animals AS a
                                                                ON ap.animal_id = a.id";
        $sql = $sql." WHERE ap.status = '1'";
        if(isset($request->search_value))
        {
            $sql = $sql." AND (a.animal_name LIKE '%$request->search_value%' OR CONCAT(a.prefix,a.animal_id) LIKE '%$request->search_value%')";
        }
        $sql = $sql." LIMIT $request->skip , 10";

        $fetch_progress_data = DB::select($sql);
        //dd($fetch_todays_capture_progress_data);
        if(count($fetch_progress_data) > 0)
        {
            $result = array('status'=>'1','data'=>$fetch_progress_data);
            return json_encode($result);
        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

}
