<?php

namespace Sohel\Progress\Model;

use Illuminate\Database\Eloquent\Model;

class AnimalProgress extends Model
{
    //
    protected $table = 'animal_progress';
    protected $fillable = ['animal_id','weight','height','capture_date','due_date','date','added_by','status'];
    //protected $with = ['animalBreed'];

    /*public function animalBreed()
    {
        return $this->belongsTo('Sohel\Breed\Model\Breed','breed_id','id');
    }*/

    /*public function animalOwner()
    {
        return $this->belongsTo('Sohel\User\Model\Customer','customer_id','id');
    }*/
}
