<?php

namespace Sohel\Progress\Model;

use Illuminate\Database\Eloquent\Model;

class ProgressReminder extends Model
{
    //
    protected $table = 'progress_reminders';
    protected $fillable = ['animal_id','progress_entry_date','progress_tracker','added_by'];
    //protected $with = ['animalBreed'];

    /*public function animalBreed()
    {
        return $this->belongsTo('Sohel\Breed\Model\Breed','breed_id','id');
    }*/

    /*public function animalOwner()
    {
        return $this->belongsTo('Sohel\User\Model\Customer','customer_id','id');
    }*/
}
