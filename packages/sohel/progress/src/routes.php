<?php
/*Route::get('/{user_type}/animal/list','Sohel\Animal\AnimalController@index')->middleware(['web','auth']);
Route::post('/fetch/animal-data','Sohel\Animal\AnimalController@fetchAnimalData')->middleware(['web','auth']);
Route::get('/{user_type}/animal/view/{animal_id}','Sohel\Animal\AnimalController@viewAnimal')->middleware(['web','auth']);
Route::get('/{user_type}/animal/add','Sohel\Animal\AnimalController@createAnimal')->middleware(['web','auth']);
Route::post('/{user_type}/animal/add','Sohel\Animal\AnimalController@createAnimal')->middleware(['web','auth']);
Route::get('/{user_type}/animal/edit/{animal_id}','Sohel\Animal\AnimalController@updateAnimal')->middleware(['web','auth']);
Route::post('/{user_type}/animal/edit/{animal_id}','Sohel\Animal\AnimalController@updateAnimal')->middleware(['web','auth']);
Route::post('/delete/animal','Sohel\Animal\AnimalController@deleteAnimal')->middleware(['web','auth']);
Route::get('/{user_type}/animal/exit/{animal_id}','Sohel\Animal\AnimalController@exitAnimal')->middleware(['web','auth']);
Route::post('/{user_type}/animal/exit/{animal_id}','Sohel\Animal\AnimalController@exitAnimal')->middleware(['web','auth']);*/


Route::get('/{user_type}/progress/pending/list','Sohel\Progress\ProgressController@index')->middleware(['web','auth']);
Route::post('/fetch/animal-pending-progress-data','Sohel\Progress\ProgressController@fetchAnimalPendingProgressData')->middleware(['web','auth']);
Route::get('{user_type}/progress/animal/{pending_progress_id}','Sohel\Progress\ProgressController@addAnimalProgress')->middleware(['web','auth']);
Route::post('{user_type}/progress/animal/{pending_progress_id}','Sohel\Progress\ProgressController@addAnimalProgress')->middleware(['web','auth']);

Route::get('/{user_type}/progress/capture/list','Sohel\Progress\ProgressController@progressCaptureList')->middleware(['web','auth']);
Route::post('/fetch/animal-capture-progress-data','Sohel\Progress\ProgressController@fetchAnimalCaptureProgressData')->middleware(['web','auth']);
Route::get('{user_type}/capture/progress/edit/{id}','Sohel\Progress\ProgressController@editCaptureProgress')->middleware(['web','auth']);
Route::post('{user_type}/capture/progress/edit/{id}','Sohel\Progress\ProgressController@editCaptureProgress')->middleware(['web','auth']);

Route::get('/{user_type}/progress/view/list','Sohel\Progress\ProgressController@progressList')->middleware(['web','auth']);
Route::post('/fetch/animal-progress-data','Sohel\Progress\ProgressController@fetchAnimalProgressData')->middleware(['web','auth']);