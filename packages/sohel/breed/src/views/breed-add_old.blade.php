@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-muted mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-primary mb-0 hover-cursor">Add Breed</p>
                        </div>
                        <br>
                        <h4 class="card-title">Add Breed</h4>
                        {{--<p class="card-description">
                            Basic form elements
                        </p>--}}
                        <form class="forms-sample" id="add_breed_form" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputName1">Breed Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="breed_name" name="breed_name" placeholder="Enter Breed Name" value="">
                                @error('breed_name')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Place Of Origin<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="place_of_origin" name="place_of_origin" placeholder="Enter Place Of Origin" value="">
                                @error('place_of_origin')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Notes</label>
                                <textarea class="form-control" id="notes" name="notes" placeholder="Enter Notes" value="" rows="5"></textarea>
                                @error('notes')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            {{--<div class="form-group">
                                <label for="exampleInputPassword4">Image<sup style="color: red">*</sup></label>
                                <input type="file" class="form-control" id="banner_image" name="banner_image" accept=".jpeg, .jpg, .png">
                                <br>
                                <label>Preview</label><br>
                                <img id="preview_banner_image" height="150px" width="150px" src="{{url('/public/media/backend/images/no-image.jpg')}}">
                                @error('banner_image')
                                --}}{{--<div class="alert alert-danger">{{ $message }}</div>--}}{{--
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>--}}
                            <button type="submit" class="btn btn-primary mr-2" id="breed_submit_btn"><i id="breed_submit_loder" style="font-size:15px"></i>  Submit</button>
                            <button class="btn btn-light" id="reset_breed_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function () {
            $('#reset_banner_image_form').click(function () {
                $('#add_banner_image_form')[0].reset();
            });

            $('#banner_image').change(function () {
                var uploaded_image = window.URL.createObjectURL(this.files[0]);
                $('#preview_banner_image').attr('src',uploaded_image);
            });


            $('#add_breed_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'breed_name':{
                        required:true,
                    },
                    'place_of_origin':{
                        required:true,
                    },
                    /*'value':{
                        required:true,
                    }*/
                },
                messages:{
                    'breed_name':{
                        required:"Please enter breed name."
                    },
                    'place_of_origin':{
                        required:"Please enter place of origin.",
                    },
                    /*'value':{
                        required:"Please enter vlaue."
                    }*/
                },
                submitHandler:function (form) {
                    $('#breed_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#breed_submit_btn').attr('disabled',true);
                    form.submit();
                    /*$('#admin_profile_submit_icon').addClass('fa fa-spinner fa-pulse');
                    var form = $('#update_admin_profile_form')[0];
                    console.log(1111);
                    var data = new FormData(form);
                    $.ajax({
                        url: javascript_site_path+'/admin/update/profile',
                        type: 'POST',
                        dataType: 'JSON',
                        processData: false,
                        contentType: false,
                        cache: false,
                        enctype: 'multipart/form-data',
                        data: data,
                        success:function(response){

                        },
                        beforeSend:function () {

                        },
                        complete:function () {

                        },
                        error:function () {

                        }
                    });*/

                }
            });

        });


    </script>
@endsection