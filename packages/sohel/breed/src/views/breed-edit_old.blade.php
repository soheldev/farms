@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/breed/list')}}">List Breed&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Edit Breed</p>
                        </div>
                        <br>
                        <form class="forms-sample" id="update_breed_form" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputName1">Breed Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="breed_name" name="breed_name" placeholder="Enter Breed Name" value="{{$edit_data->breed_name}}">
                                @error('breed_name')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Place Of Origin<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="place_of_origin" name="place_of_origin" placeholder="Enter Place Of Origin" value="{{$edit_data->place_of_origin}}">
                                @error('place_of_origin')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Notes</label>
                                <textarea class="form-control" id="notes" name="notes" placeholder="Enter Notes" value="" rows="5">{{$edit_data->notes}}</textarea>
                                @error('notes')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary mr-2" id="breed_update_btn"><i id="breed_update_loder" style="font-size:15px"></i>  Update</button>
                            <button type="button" class="btn btn-light" id="reset_breed_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function () {
            $('#reset_breed_form').click(function () {
                $('#update_breed_form')[0].reset();
            });

            $('#update_breed_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'breed_name':{
                        required:true,
                    },
                    'place_of_origin':{
                        required:true,
                    },
                },
                messages:{
                    'breed_name':{
                        required:"Please enter breed name."
                    },
                    'place_of_origin':{
                        required:"Please enter place of origin."
                    },
                },
                submitHandler:function (form) {
                    $('#breed_update_loder').addClass("fa fa-spinner fa-pulse");
                    $('#breed_update_btn').attr('disabled',true);
                    form.submit();
                    /*$('#admin_profile_submit_icon').addClass('fa fa-spinner fa-pulse');
                    var form = $('#update_admin_profile_form')[0];
                    console.log(1111);
                    var data = new FormData(form);
                    $.ajax({
                        url: javascript_site_path+'/admin/update/profile',
                        type: 'POST',
                        dataType: 'JSON',
                        processData: false,
                        contentType: false,
                        cache: false,
                        enctype: 'multipart/form-data',
                        data: data,
                        success:function(response){

                        },
                        beforeSend:function () {

                        },
                        complete:function () {

                        },
                        error:function () {

                        }
                    });*/

                }
            });

        })

    </script>
@endsection