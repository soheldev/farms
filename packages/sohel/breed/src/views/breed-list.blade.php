@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)
@section('content')
    <div class="db-body">
        <div class="inner-forms">
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif
            <div class="box ">
                <div class="box-title clearfix">
                    <h2 class="pull-left">List Breeds</h2>
                    @if($add_breed_permission == true)
                    <div class="top-btn pull-right">
                        <a class="btn cust-btn btn-blue-1" href="{{url($user.'/breed/add')}}"><i class="fa fa-plus"></i> Add New</a>
                    </div>
                    @endif
                </div>

                <div class="table-resposnive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Breed Id</th>
                            <th>Breed Name</th>
                            <th>Place Of Origin</th>
                            @if($update_breed_permission == true || $delete_breed_permission == true)
                                <th>Action</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody id="breed_table">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('jcontent')
    <script>
        var site_path = '{{url('/')}}';
        var user = '{{$user}}';
        var skip = 0;
        var i = 0;
        var update_breed_permission = '{{$update_breed_permission}}';
        var delete_breed_permission = '{{$delete_breed_permission}}';
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/breed-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td><a href="'+site_path+''+user+'/breed/view/'+btoa(this.id)+'">'+this.breed_id+'</a></td>';
                            html_content += '<td>'+this.breed_name+'</td>';
                            html_content += '<td>'+this.place_of_origin+'</td>';
                            if(update_breed_permission == true || delete_breed_permission == true)
                            {
                                html_content += '<td>';
                                html_content += '<ul class="list-inline">';
                                if(update_breed_permission == true)
                                {
                                    html_content += '<li><a title="Edit" href="' + site_path + '' + user + '/breed/edit/' + btoa(this.id) + '" class="action-btn btn-yellow edit" data-attribute-id="' + this.id + '"><i class="fa fa-pencil "></i></a></li>';
                                }
                                if(delete_breed_permission == true)
                                {
                                    html_content += '<li><a title="Delete" href="javascript:void(0)" class="action-btn btn-red delete" data-attribute-id="' + this.id + '"><i class="fa fa-trash"></i></a></li>';
                                }
                                html_content += '</ul>';
                                html_content += '</td>';
                            }
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="4"><center>No Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#breed_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 5;
                    fetchRecord(skip);
                }
            };
            $('body').on('click','.delete',function () {
                var id = btoa($(this).attr('data-attribute-id'));
                var _this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to delete this breed.!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#71c016',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function(result){
                    if (result.value) {

                        $.ajax({
                            url: site_path + '/delete/breed',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                id: id,
                            },
                            success: function (response) {
                                if(response.status == '1')
                                {
                                    _this.parent('center').parent('td').parent('tr').remove();
                                    if($('#breed_table tr').length == 0)
                                    {
                                        $('#breed_table').append('<tr><td colspan="5" class="no_record_found_row"><center>No Record Found...</center></td></tr>');
                                    }
                                }
                                Swal.fire({
                                    icon: response.icon,
                                    title: response.title,
                                    text: response.text,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            },
                            beforeSend: function () {

                            },
                            complete: function () {

                            },
                            error: function () {

                            }


                        });
                    }
            })
            });
        });
    </script>
@endsection