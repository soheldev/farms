@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Edit Breed</h2>
                </div>
                <div class="add-customer">
                    <form id="update_breed_form" method="POST" action="" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="breed_name" name="breed_name" placeholder="Enter Breed Name*" class="form-control" value="{{$edit_data->breed_name}}">
                                    @error('breed_name')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="place_of_origin" name="place_of_origin" placeholder="Enter Place Of Origin*" class="form-control" value="{{$edit_data->place_of_origin}}">
                                    @error('place_of_origin')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12 form-group">
                                <textarea class="form-control" id="notes" name="notes" placeholder="Enter Notes" value="" rows="5">{{$edit_data->notes}}</textarea>
                                @error('notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li><button type="submit" class="btn cust-btn btn-save" id="breed_update_btn"><i id="breed_update_loder" style="font-size:15px"></i> Update</button></li>
                                        <li><button type="button" class="btn cust-btn btn-grey" id="reset_breed_form">Cancel</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function () {
            $('#reset_breed_form').click(function () {
                $('#update_breed_form')[0].reset();
            });

            $('#update_breed_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'breed_name':{
                        required:true,
                    },
                    'place_of_origin':{
                        required:true,
                    },
                },
                messages:{
                    'breed_name':{
                        required:"Please enter breed name."
                    },
                    'place_of_origin':{
                        required:"Please enter place of origin."
                    },
                },
                submitHandler:function (form) {
                    $('#breed_update_loder').addClass("fa fa-spinner fa-pulse");
                    $('#breed_update_btn').attr('disabled',true);
                    form.submit();
                    /*$('#admin_profile_submit_icon').addClass('fa fa-spinner fa-pulse');
                    var form = $('#update_admin_profile_form')[0];
                    console.log(1111);
                    var data = new FormData(form);
                    $.ajax({
                        url: javascript_site_path+'/admin/update/profile',
                        type: 'POST',
                        dataType: 'JSON',
                        processData: false,
                        contentType: false,
                        cache: false,
                        enctype: 'multipart/form-data',
                        data: data,
                        success:function(response){

                        },
                        beforeSend:function () {

                        },
                        complete:function () {

                        },
                        error:function () {

                        }
                    });*/

                }
            });

        })

    </script>
@endsection