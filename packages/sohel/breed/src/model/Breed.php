<?php

namespace Sohel\Breed\Model;

use Illuminate\Database\Eloquent\Model;

class Breed extends Model
{
    //
    protected $table = 'breeds';
    protected $fillable = ['breed_id','breed_name','place_of_origin','notes','added_by'];
}
