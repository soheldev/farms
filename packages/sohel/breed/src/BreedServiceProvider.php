<?php

namespace Sohel\Breed;

use Illuminate\Support\ServiceProvider;

class BreedServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->make('Sohel\Breed\BreedController');
        $this->loadViewsFrom(__DIR__.'/views', 'breed');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__.'/routes.php';
    }
}
