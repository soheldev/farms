<?php

namespace Sohel\Breed;

use App\Http\Helpers\CheckPermission;
use App\Http\Helpers\CheckType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Sohel\Breed\Model\Breed;

class BreedController extends Controller
{
    //

    function generateCode($limit)
    {
        $code = '';
        for($i = 0; $i < $limit; $i++) { $code .= mt_rand(0, 9); }
        return $code;
    }

    public function index(Request $request)
    {
        $add_breed_permission = false;
        $update_breed_permission = false;
        $delete_breed_permission = false;
        $add_breed_permission = CheckPermission::hasPermission('create.breeds');
        $update_breed_permission = CheckPermission::hasPermission('update.breeds');
        $delete_breed_permission = CheckPermission::hasPermission('delete.breeds');
        return view('breed::breed-list',compact('add_breed_permission','update_breed_permission','delete_breed_permission'));
    }

    public function fetchBreedData(Request $request)
    {
        $added_by = Auth::user()->id;
        $data = Breed::skip($request->skip)->limit(5)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function createBreed(Request $request)
    {
        if($request->method() == "GET")
        {
            return view('breed::breed-add');
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'breed_name' => 'required',
                'place_of_origin' => 'required',
            ],
                $messages = [
                    'breed_name.required'    => 'Please enter breed name.',
                    'place_of_origin.required'    => 'Please enter place of origin.'
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $unique_number = $this->generateCode(10);
                $breed_id = 'B'.$unique_number;
                $breed_arr = [];
                $breed_arr['breed_id'] = $breed_id;
                $breed_arr['breed_name'] = $request->breed_name;
                $breed_arr['place_of_origin'] = $request->place_of_origin;
                $breed_arr['notes'] = $request->notes;
                $breed_arr['added_by'] = Auth::user()->id;
                Breed::create($breed_arr);
                $type = CheckType::checkAuthType();
                return redirect($type.'/breed/list')->with(['success' => 'Breed Added Successfully...']);

            }
        }
    }

    public function viewBreed($user, $breed_id)
    {
        $id = base64_decode($breed_id);
        $view_breed = Breed::find($id);
        return view('breed::breed-view',compact('view_breed'));
    }

    public function updateBreed($user,$breed_id, Request $request)
    {
        $id = base64_decode($breed_id);
        $edit_data = Breed::find($id);
        if($request->method() == "GET")
        {
            return view('breed::breed-edit',compact('edit_data'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'breed_name' => 'required',
                'place_of_origin' => 'required',
            ],
                $messages = [
                    'breed_name.required'    => 'Please enter breed name.',
                    'place_of_origin.required'    => 'Please enter place of origin.'
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $edit_data->breed_name = $request->breed_name;
                $edit_data->place_of_origin = $request->place_of_origin;
                $edit_data->notes = $request->notes;
                $edit_data->save();
                $type = CheckType::checkAuthType();
                return redirect($type.'/breed/list')->with(['success' => 'Breed Updated Successfully...']);
            }
        }
    }

    public function deleteBreed(Request $request)
    {
        $id = base64_decode($request->id);
        $delete_breed = Breed::find($id);
        if(isset($delete_breed))
        {
            if($delete_breed->delete())
            {
                $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Breed Deleted Successfully...');
                return json_encode($result);
            }
            else
            {
                $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something Went Wrong...');
                return json_encode($result);
            }

        }
    }
}
