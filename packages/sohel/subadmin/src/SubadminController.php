<?php

namespace Sohel\Subadmin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\UniqueId;
use App\UserInformation;
use App\UserRole;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sohel\Animal\Model\Animal;
use Sohel\City\Model\City;
use Sohel\Country\Model\Country;
use Sohel\Role\Model\Role;
use Sohel\State\Model\State;
use Sohel\User\Model\Customer;

class SubadminController extends Controller
{
    public function __construct()
    {
        //$this->checkType();
    }

    public function checkType()
    {
        $user_redirect = '';
        $auth_type = Auth::user()->userInformation->user_type;
        if ($auth_type == "1") {
            $user_redirect = "/admin";
        } else {
            $user_redirect = "/vendor";
        }
        return $user_redirect;
    }

    public function fetchcityAccordingToState(Request $request)
    {
        $cities_arr = [];
        $state_id = $request->state_id;
        $cities = City::where('state_id',$state_id)->get();
        if($cities->count() > 0)
        {
            foreach($cities as $key => $city)
            {
                $cities_arr[$key]['id'] = $city->id;
                $cities_arr[$key]['city_name'] = $city->city_name;
            }
            $result = array('status' => '1', 'data' => $cities_arr);
        }
        else
        {
            $result = array('status' => '0', 'data' => $cities_arr);
        }
        return json_encode($result);
    }

    //
    public function index()
    {
        return view('subadmin::subadmin-list');
    }

    public function fetchUserData(Request $request)
    {
        $added_by = Auth::user()->id;
        $sql = "SELECT u.id,u.name,u.email,ui.user_unique_id AS user_unique_id,ui.mobile_number
                FROM users AS u 
                JOIN user_informations AS ui
                ON u.id = ui.user_id";
        $sql = $sql . " WHERE u.user_type = '3'";
        if (isset($request->search_value)) {
            $sql = $sql . " AND (u.name LIKE '%$request->search_value%' OR ui.user_unique_id LIKE '%$request->search_value%' OR ui.mobile_number LIKE '%$request->search_value%')";
        }
        $sql = $sql . " LIMIT $request->skip , 5";
        $data = DB::select($sql);
        if (count($data) > 0) {
            $result = array('status' => '1', 'data' => $data);
            return json_encode($result);

        } else {
            $result = array('status' => '0');
            return json_encode($result);
        }
    }

    public function checkSubadminEmail(Request $request)
    {
        $email_id = $request->email;
        $flag = $request->flag;
        if ($flag == "1") {
            if (isset($email_id)) {
                $check_email = User::where('email', $email_id)->first();
                if (isset($check_email)) {
                    return 'false';
                } else {
                    return 'true';
                }
            }
        } else {
            if (isset($email_id)) {
                $id = $request->id;
                $check_email = User::where('email', $email_id)->where('id', '<>', $id)->first();
                if (isset($check_email)) {
                    return 'false';
                } else {
                    return 'true';
                }
            }
        }

    }

    public function checkSubadminContact(Request $request)
    {
        $contact = $request->contact_no;
        $flag = $request->flag;
        if ($flag == "1") {
            if (isset($contact)) {
                $check_contact = UserInformation::where('mobile_number', $contact)->first();
                if (isset($check_contact)) {
                    return 'false';
                } else {
                    return 'true';
                }
            }
        } else {
            if (isset($contact)) {
                $id = $request->id;
                $check_contact = UserInformation::where('mobile_number', $contact)->where('user_id', '<>', $id)->first();
                if (isset($check_contact)) {
                    return 'false';
                } else {
                    return 'true';
                }
            }
        }
    }

    public function createSubadmin(Request $request)
    {
        $added_by = Auth::user()->id;
        if ($request->method() == "GET") {
            $countries = Country::all('id', 'country_name');
            $roles = Role::where('added_by', $added_by)->get(['id', 'name']);
            return view('subadmin::subadmin-add', compact('countries', 'roles'));
        } else {

            $validator = \Validator::make($request->all(), [
                'role_id' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'contact_no' => 'required',
                'country_id' => 'required',
                'state_id' => 'required',
                'city_id' => 'required',
            ],
                $messages = [
                    'role_id.required' => 'Please select role for user.',
                    'first_name.required' => 'Please enter first name.',
                    'last_name.required' => 'Please enter last name.',
                    'contact_no.required' => 'Please enter contact number.',
                    'email.required' => 'Please enter email id.',
                    'country_id.required' => 'Please select country.',
                    'state_id.required' => 'Please select state.',
                    'city_id.required' => 'Please select city.',
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $user_name = $request->first_name . ' ' . $request->last_name;
                $create_user = [];
                $create_user['name'] = $user_name;
                $create_user['email'] = $request->email;
                $create_user['password'] = bcrypt('123456789');
                $create_user['user_type'] = '3';
                $created_user = \App\User::create($create_user);
                if (isset($created_user)) {
                    $create_userinfo_arr = [];
                    $unique_number = UniqueId::generateUniqueIdentification(10);
                    $customer_id = 'UR' . $unique_number;
                    $create_userinfo_arr['user_id'] = $created_user->id;
                    $create_userinfo_arr['user_unique_id'] = $customer_id;
                    $create_userinfo_arr['first_name'] = $request->first_name;
                    $create_userinfo_arr['last_name'] = $request->last_name;
                    $create_userinfo_arr['mobile_number'] = $request->contact_no;
                    $create_userinfo_arr['address_line_one'] = isset($request->address_line_one) ? $request->address_line_one : NULL;
                    $create_userinfo_arr['address_line_two'] = isset($request->address_line_two) ? $request->address_line_two : NULL;
                    $create_userinfo_arr['country_id'] = $request->country_id;
                    $create_userinfo_arr['state_id'] = $request->state_id;
                    $create_userinfo_arr['city_id'] = $request->city_id;
                    $create_userinfo_arr['pin_code'] = isset($request->pin_code) ? $request->pin_code : NULL;
                    $create_userinfo_arr['user_type'] = '3';
                    $create_userinfo_arr['user_status'] = '1';
                    if ($request->hasFile('profile_picture')) {
                        if (!\File::exists(public_path() . "/farm")) {
                            \File::makeDirectory(public_path() . "/farm");
                        }
                        $imageName = time() . '.' . request()->profile_picture->getClientOriginalExtension();
                        request()->profile_picture->move(public_path('/farm/'), $imageName);
                        $create_userinfo_arr['profile_image'] = $imageName;
                    }
                    UserInformation::create($create_userinfo_arr);
                    // Add user role
                    $user_role_arr = [];
                    $user_role_arr['user_id'] = $created_user->id;
                    $user_role_arr['role_id'] = $request->role_id;
                    UserRole::create($user_role_arr);
                    $type = $this->checkType();
                    return redirect($type . '/setting/users')->with(['success' => 'User Added Successfully...']);
                }

            }
        }
    }

    public function viewSubadmin($type, $user_id)
    {
        $id = base64_decode($user_id);
        $view_subadmin = UserInformation::where('user_id', $id)->first();
        return view('subadmin::subadmin-view', compact('view_subadmin'));
    }

    public function updateSubadmin($type, $user_id, Request $request)
    {
        $added_by = Auth::user()->id;
        $id = base64_decode($user_id);
        $update_subadmin = UserInformation::where('user_id', $id)->first();
        if ($request->method() == "GET") {
            $roles = Role::where('added_by', $added_by)->get(['id', 'name']);
            $countries = Country::all('id', 'country_name');
            $states = State::select('id', 'state_name')->where('country_id', $update_subadmin->country_id)->get();
            $cities = City::select('id', 'city_name')->where('state_id', $update_subadmin->state_id)->get();
            return view("subadmin::subadmin-edit", compact('update_subadmin', 'roles', 'countries', 'states', 'cities'));
        } else {
            $validator = \Validator::make($request->all(), [
                'role_id' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'contact_no' => 'required',
                'country_id' => 'required',
                'state_id' => 'required',
                'city_id' => 'required',
            ],
                $messages = [
                    'role_id.required' => 'Please select role for user.',
                    'first_name.required' => 'Please enter first name.',
                    'last_name.required' => 'Please enter last name.',
                    'contact_no.required' => 'Please enter contact number.',
                    'email.required' => 'Please enter email id.',
                    'country_id.required' => 'Please select country.',
                    'state_id.required' => 'Please select state.',
                    'city_id.required' => 'Please select city.',
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $update_user_arr = [];
                $user_name = $request->first_name . ' ' . $request->last_name;
                $update_user_arr['name'] = $user_name;
                $update_user_arr['email'] = $request->email;

                $update_user_info_arr = [];
                $update_user_info_arr['first_name'] = $request->first_name;
                $update_user_info_arr['last_name'] = $request->last_name;
                $update_user_info_arr['mobile_number'] = $request->contact_no;
                $update_user_info_arr['address_line_one'] = isset($request->address_line_one) ? $request->address_line_one : NULL;
                $update_user_info_arr['address_line_two'] = isset($request->address_line_two) ? $request->address_line_two : NULL;
                $update_user_info_arr['country_id'] = $request->country_id;
                $update_user_info_arr['state_id'] = $request->state_id;
                $update_user_info_arr['city_id'] = $request->city_id;
                $update_user_info_arr['pin_code'] = isset($request->pin_code) ? $request->pin_code : NULL;
                if ($request->hasFile('profile_picture')) {
                    $old_image = $update_subadmin->profile_image;
                    $imageName = time() . '.' . request()->profile_picture->getClientOriginalExtension();
                    request()->profile_picture->move(public_path('/farm/'), $imageName);
                    $update_user_info_arr['profile_image'] = $imageName;
                    if (isset($old_image)) {
                        unlink(public_path('/farm/' . $old_image));
                    }
                }
                DB::table('user_roles')->where('user_id', $update_subadmin->user_id)->update(['role_id' => $request->role_id]);
                DB::table('user_informations')->where('id', $update_subadmin->id)->update($update_user_info_arr);
                DB::table('users')->where('id', $update_subadmin->user_id)->update($update_user_arr);
                $type = $this->checkType();
                return redirect($type . '/setting/users')->with(['success' => 'User Updated Successfully...']);
            }
        }
    }

    public function deleteSubadmin(Request $request)
    {
        $id = base64_decode($request->id);
        if (isset($id)) {
            $delete_user = UserInformation::where('user_id', $id)->first();
            if (isset($delete_user)) {
                $delete_user_image = $delete_user->profile_image;
                if (isset($delete_user_image)) {
                    unlink(public_path('/farm/' . $delete_user_image));
                }
                DB::table('users')->where('id', $delete_user->user_id)->delete();
                DB::table('user_roles')->where('user_id', $delete_user->user_id)->delete();
                if ($delete_user->delete()) {
                    $result = array('status' => '1', 'icon' => 'success', 'title' => 'Deleted', 'text' => 'User deleted successfully...');
                    return json_encode($result);
                } else {
                    $result = array('status' => '0', 'icon' => 'error', 'title' => 'Opps', 'text' => 'Something went wrong...');
                    return json_encode($result);
                }

            }
        }
    }
}
