@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)
@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/setting/users')}}">List User&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Edit user</p>
                        </div>
                        <br>
                        <form class="forms-sample" id="update_subadmin_form" method="POST" action="" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="exampleInputName1">Role<sup style="color: red">*</sup></label>
                                <select class="form-control" id="role_id" name="role_id">
                                    <option value="">Select Role For User</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}" @if($role->id == $update_subadmin->users->userRole->role_id) selected @endif>{{$role->name}}</option>
                                    @endforeach
                                </select>
                                @error('country_id')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">First Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter First Name" value="{{$update_subadmin->first_name}}">
                                @error('first_name')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Last Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter Last Name" value="{{$update_subadmin->last_name}}">
                                @error('last_name')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Email Id<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Enter  Email Id" value="{{$update_subadmin->users->email}}">
                                @error('email')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Contact Number<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="contact_no" name="contact_no" placeholder="Enter Contact Number" value="{{$update_subadmin->mobile_number}}">
                                @error('contact_no')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Address Line One</label>
                                <textarea class="form-control" id="address_line_one" name="address_line_one" placeholder="Enter Address Line One" value="" rows="5">{{$update_subadmin->address_line_one}}</textarea>
                                @error('address_line_one')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Address Line Two</label>
                                <textarea class="form-control" id="address_line_two" name="address_line_two" placeholder="Enter Address Line Two" value="" rows="5">{{$update_subadmin->address_line_two}}</textarea>
                                @error('address_line_two')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Pin Code</label>
                                <input type="text" class="form-control" id="pin_code" name="pin_code" placeholder="Enter Pin Code" value="{{$update_subadmin->pin_code}}" autocomplete="off">
                                @error('pin_code')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>


                            <div class="form-group">
                                <label for="exampleInputName1">Country<sup style="color: red">*</sup></label>
                                <select class="form-control" id="country_id" name="country_id">
                                    <option value="">Select Country</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}" @if($country->id == $update_subadmin->country_id) selected @endif>{{$country->country_name}}</option>
                                    @endforeach
                                </select>
                                @error('country_id')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">State<sup style="color: red">*</sup></label>
                                <select class="form-control" id="state_id" name="state_id">
                                    <option value="">Select State</option>
                                    @foreach($states as $state)
                                        <option value="{{$state->id}}" @if($state->id == $update_subadmin->state_id) selected @endif>{{$state->state_name}}</option>
                                    @endforeach
                                </select>
                                @error('state_id')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">City<sup style="color: red">*</sup></label>
                                <select class="form-control" id="city_id" name="city_id">
                                    <option value="">Select City</option>
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}" @if($city->id == $update_subadmin->city_id) selected @endif>{{$city->city_name}}</option>
                                    @endforeach
                                </select>
                                @error('city_id')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            

                            <div class="form-group">
                                <label for="exampleInputPassword4">Profile Picture</label>
                                <input type="file" class="form-control" id="profile_picture" name="profile_picture" accept=".jpeg, .jpg, .png">
                                <br>
                                <label>Preview</label><br>
                                @if(isset($update_subadmin->profile_image))
                                    <img id="preview_profile_picture" height="150px" width="150px" src="{{url('/public/farm/'.$update_subadmin->profile_image)}}">
                                @else
                                    <img id="preview_profile_picture" height="150px" width="150px" src="{{url('/public/media/backend/images/no-image.jpg')}}">
                                @endif
                                @error('profile_picture')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary mr-2" id="subadmin_update_btn"><i id="subadmin_update_loder" style="font-size:15px"></i>  Update</button>
                            <button type="button" class="btn btn-light" id="reset_update_subadmin_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        var user_id = '{{$update_subadmin->user_id}}';
        var previous_state_id = '{{$update_subadmin->state_id}}';
        var previous_city_id = '{{$update_subadmin->city_id}}';
        $(function ()
        {
            $('#reset_update_subadmin_form').click(function () {
                $('#update_subadmin_form')[0].reset();
            });

            $('#profile_picture').change(function () {
                var uploaded_image = window.URL.createObjectURL(this.files[0]);
                $('#preview_profile_picture').attr('src',uploaded_image);
            });

            $("#first_name").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 65 || key > 90)) {
                    return false;
                }
            });

            $("#last_name").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 65 || key > 90)) {
                    return false;
                }
            });

            $("#contact_no").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105)) {
                    return false;
                }
            });

            $("#no_of_animals").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105)) {
                    return false;
                }
            });

            $('#update_subadmin_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'role_id':{
                        required:true,
                    },
                    'first_name':{
                        required:true,
                    },
                    'last_name':{
                        required:true,
                    },
                    'contact_no':{
                        required:true,
                        digits:true,
                        minlength:10,
                        maxlength:10,
                        remote: {
                            url: javascript_site_path+"/subadmin/check-contact",
                            type: "post",
                            data:{
                                flag:'0',
                                id:user_id
                            }
                        }
                    },
                    'email':{
                        required:true,
                        email:true,
                        remote: {
                            url: javascript_site_path+"/subadmin/check-email",
                            type: "post",
                            data:{
                                flag:'0',
                                id:user_id
                            }
                        }
                    },
                    'country_id':{
                        required:true,
                    },
                    'state_id':{
                        required:true,
                    },
                    'city_id':{
                        required:true,
                    },

                },
                messages:{
                    'role_id':{
                        required:"Please select role for user.",
                    },
                    'first_name':{
                        required:"Please enter first name.",
                    },
                    'last_name':{
                        required:"Please enter last name.",
                    },
                    'contact_no':{
                        required:"Please enter contact number.",
                        digits:"Please enter valid contact number.",
                        minlength:"Contact number should not be less than 10 digit.",
                        maxlength:"Contact number should not be greater than 10 digit.",
                        remote:"Contact number already exits."
                    },
                    'email':{
                        required:"Please enter email id.",
                        email:"Please enter valid email id.",
                        remote:"Email id already exits."
                    },
                    'country_id':{
                        required:"Please select country.",
                    },
                    'state_id':{
                        required:"Please select state.",
                    },
                    'city_id':{
                        required:"Please select city.",
                    },
                },
                submitHandler:function (form) {
                    $('#subadmin_update_loder').addClass("fa fa-spinner fa-pulse");
                    $('#subadmin_update_btn').attr('disabled',true);
                    form.submit();
                }
            });

            function fetchStates(country_id)
            {
                $.ajax({
                    url: javascript_site_path+'/admin/fetch/state-according-to-country',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        country_id: country_id
                    },
                    success:function(response)
                    {
                        var html_content = '';
                        if(response.status == "1")
                        {
                            html_content += '<option value="">Select State</option>';
                            $.each(response.data,function (key,value)
                            {
                                if(value.id == previous_state_id)
                                {
                                    fetchCity(previous_state_id);
                                    html_content += '<option value="'+value.id+'" selected>'+value.state_name+'</option>';
                                }
                                else
                                {
                                    html_content += '<option value="'+value.id+'">'+value.state_name+'</option>';
                                }
                            });
                            $('#state_id').html(html_content);
                        }
                        else
                        {
                            $('#state_id').html('<option value="">No States Found...</option>');
                        }
                    },
                    beforeSend:function () {
                        $('#state_id').html('<option value="">Fetching States...</option>');
                    },
                    complete:function () {

                    },
                    error:function () {

                    }
                });
            }

            function fetchCity(state_id)
            {
                $.ajax({
                    url: javascript_site_path+'/admin/fetch/city-according-to-state',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        state_id: state_id
                    },
                    success:function(response)
                    {
                        var html_content = '';
                        if(response.status == "1")
                        {
                            html_content += '<option value="">Select City</option>';
                            $.each(response.data,function (key,value)
                            {
                                if(value.id == previous_city_id)
                                {
                                    html_content += '<option value="'+value.id+'" selected>'+value.city_name+'</option>';
                                }
                                else
                                {
                                    html_content += '<option value="'+value.id+'">'+value.city_name+'</option>';
                                }
                            });
                            $('#city_id').html(html_content);
                        }
                        else
                        {
                            $('#city_id').html('<option value="">No Cities Found...</option>');
                        }
                    },
                    beforeSend:function () {
                        $('#city_id').html('<option value="">Fetching Cities...</option>');
                    },
                    complete:function () {

                    },
                    error:function () {

                    }
                });
            }

            $('#country_id').change(function () {
                $('#state_id').html('<option value="">Select State</option>');
                $('#city_id').html('<option value="">Select City</option>');
                var country_id = $(this).val();
                fetchStates(country_id)
            });

            $('#state_id').change(function () {
                var state_id = $(this).val();
                fetchCity(state_id);
            });

        })

    </script>
@endsection