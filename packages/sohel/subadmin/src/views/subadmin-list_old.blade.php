@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)
@section('content')
    <style>
        .my-custom-scrollbar {
            position: relative;
            height: 400px;
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">List Users</p>
                        </div>
                        <br>
                        <div class="pull-right">
                            <a type="button" class="btn btn-success btn-rounded" href="{{url($user.'/setting/users/add')}}">Add New</a>
                        </div>
                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>User Id</th>
                                    <th>Name</th>
                                    <th>Email Id</th>
                                    <th>Contact No</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody id="user_table">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('jcontent')
    <script>
        var site_path = '{{url('/')}}';
        var user = '{{$user}}';
        var skip = 0;
        var i = 0;
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/users-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td><a href="'+site_path+''+user+'/setting/users/view/'+btoa(this.id)+'">'+this.user_unique_id+'</a></td>';
                            html_content += '<td>'+this.name+'</td>';
                            html_content += '<td>'+this.email+'</td>';
                            html_content += '<td>'+this.mobile_number+'</td>';
                            html_content += '<td><center><a title="Edit" href="'+site_path+''+user+'/setting/users/edit/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; <a title="delete" href="javascript:void(0)" class="subadmin_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></center></td>';

                            //html_content += '<td><a type="button" href="'+site_path+'/admin/setting/banner/edit/'+this.id+'" class="btn btn-primary btn-rounded">Update</a></td>';
                            //html_content += '<td><a type="button" data-attribute-id="'+this.id+'" class="btn btn-danger btn-rounded delete">Delete</a></td>';
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="5"><center>No Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#user_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }

        function searchRecord(skip,search_value)
        {
            $.ajax({
                url: site_path+'/fetch/users-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip,
                    search_value:search_value
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td><a href="'+site_path+''+user+'/customer/view/'+btoa(this.id)+'">'+this.user_unique_id+'</a></td>';
                            html_content += '<td>'+this.name+'</td>';
                            html_content += '<td>'+this.email+'</td>';
                            html_content += '<td>'+this.mobile_number+'</td>';
                            html_content += '<td><center><a title="Edit" href="'+site_path+''+user+'/setting/users/edit/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; <a title="delete" href="javascript:void(0)" class="customer_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></center></td>';

                            //html_content += '<td><a type="button" href="'+site_path+'/admin/setting/banner/edit/'+this.id+'" class="btn btn-primary btn-rounded">Update</a></td>';
                            //html_content += '<td><a type="button" data-attribute-id="'+this.id+'" class="btn btn-danger btn-rounded delete">Delete</a></td>';
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        html_content += '<tr><td colspan="5"><center>No Record Found...</center></td></tr>';
                    }
                    $('#user_table').html(html_content);

                },
                beforeSend:function () {
                    $('#user_table').html('<tr><td colspan="5"><center><i class="fa fa-spinner fa-pulse fa-2x"><i/></center></td></tr>');
                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 5;
                    fetchRecord(skip);
                }
            };
            $('body').on('click','.subadmin_delete',function () {
                var id = btoa($(this).attr('data-attribute-id'));
                var _this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to delete this user.!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#71c016',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function(result){
                    if (result.value) {

                        $.ajax({
                            url: site_path + '/delete/user',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                id: id,
                            },
                            success: function (response) {
                                if(response.status == '1')
                                {
                                    _this.parent('center').parent('td').parent('tr').remove();
                                    if($('#user_table tr').length == 0)
                                    {
                                        $('#user_table').append('<tr><td colspan="5" class="no_record_found_row"><center>No Record Found...</center></td></tr>');
                                    }
                                }
                                Swal.fire({
                                    icon: response.icon,
                                    title: response.title,
                                    text: response.text,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            },
                            beforeSend: function () {

                            },
                            complete: function () {

                            },
                            error: function () {

                            }


                        });
                    }
                })
            });

            $('#search_text').keyup(function () {
                search_value = $(this).val();
                skip = 0;
                searchRecord(skip,search_value);
            });
        });
    </script>
@endsection