@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/setting/users')}}">List User&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">View User Information</p>
                        </div>

                        <br>

                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th rowspan="8">
                                        <div class="text-center">
                                            @if(isset($view_subadmin->profile_image))
                                                <img style="border-radius: 50%" height="150px" width="150px" src="{{url('/public/farm/'.$view_subadmin->profile_image)}}">
                                            @else
                                                <img style="border-radius: 50%" height="150px" width="150px" src="{{url('/public/media/backend/images/no-image.jpg')}}">
                                            @endif
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                    </th>
                                    <th>
                                        Customer Id : <span>#{{$view_subadmin->user_unique_id}}</span>
                                    </th>
                                    <th>
                                        Customer Name : <span>{{$view_subadmin->first_name}} {{$view_subadmin->last_name}}</span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Email Id : <span>{{$view_subadmin->users->email}}</span>
                                    </th>
                                    <th>
                                        Contact Number : <span>{{$view_subadmin->mobile_number}}</span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>Address Line 1 : <span>{{isset($view_subadmin->address_line_one)?$view_subadmin->address_line_one:'N/A'}}</span></th>
                                    <th>Address Line 2 : <span>{{isset($view_subadmin->address_line_two)?$view_subadmin->address_line_two:'N/A'}}</span></th>
                                </tr>
                                <tr>
                                    <th>City : <span>{{isset($view_subadmin->userCity->city_name)?$view_subadmin->userCity->city_name:'N/A'}}</span></th>
                                    <th>State : <span>{{isset($view_subadmin->userState->state_name)?$view_subadmin->userState->state_name:'N/A'}}</span></th>
                                </tr>

                                <tr>
                                    <th>Country : <span>{{isset($view_subadmin->userCountry->country_name)?$view_subadmin->userCountry->country_name:'N/A'}}</span></th>
                                    <th>Pin Code : <span>{{isset($view_subadmin->pin_code)?$view_subadmin->pin_code:'N/A'}}</span></th>
                                </tr>

                                <tr>
                                    <th>Role : <span>{{isset($view_subadmin->users->userRole->userRoleName->name)?$view_subadmin->users->userRole->userRoleName->name:'N/A'}}</span></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('jcontent')
<script>
    var site_path = '{{url('/')}}';

</script>

@endsection