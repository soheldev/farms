<?php

Route::get('/{user_type}/setting/users','Sohel\Subadmin\SubadminController@index')->middleware(['web','auth']);
Route::post('/fetch/users-data','Sohel\Subadmin\SubadminController@fetchUserData')->middleware(['web','auth']);
Route::get('/{user_type}/setting/users/add','Sohel\Subadmin\SubadminController@createSubadmin')->middleware(['web','auth']);
Route::post('/{user_type}/setting/users/add','Sohel\Subadmin\SubadminController@createSubadmin')->middleware(['web','auth']);
Route::post('/subadmin/check-email','Sohel\Subadmin\SubadminController@checkSubadminEmail');
Route::post('/subadmin/check-contact','Sohel\Subadmin\SubadminController@checkSubadminContact');
Route::get('/{user_type}/setting/users/view/{user_id}','Sohel\Subadmin\SubadminController@viewSubadmin')->middleware(['web','auth']);
Route::get('/{user_type}/setting/users/edit/{user_id}','Sohel\Subadmin\SubadminController@updateSubadmin')->middleware(['web','auth']);
Route::post('/{user_type}/setting/users/edit/{user_id}','Sohel\Subadmin\SubadminController@updateSubadmin')->middleware(['web','auth']);
Route::post('/delete/user','Sohel\Subadmin\SubadminController@deleteSubadmin')->middleware(['web','auth']);
Route::post('/admin/fetch/city-according-to-state','Sohel\Subadmin\SubadminController@fetchcityAccordingToState');

