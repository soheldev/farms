<?php

namespace Sohel\Subadmin;

use Illuminate\Support\ServiceProvider;

class SubadminServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->make('Sohel\Subadmin\SubadminController');
        $this->loadViewsFrom(__DIR__.'/views', 'subadmin');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__.'/routes.php';
    }
}
