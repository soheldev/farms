<?php

namespace Sohel\User\Model;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table = 'customers';
    protected $fillable = ['customer_id','customer_unique_id','first_name','last_name','profile_image','email','contact_no','address_line_one','address_line_two','pin_code','city_id','state_id','country_id','occupation','joining_date','channel','no_of_animals','added_by','notes'];

    public function customerCity()
    {
        return $this->belongsTo('Sohel\City\Model\City','city_id','id');
    }

    public function customerState()
    {
        return $this->belongsTo('Sohel\State\Model\State','state_id','id');
    }

    public function customerCountry()
    {
        return $this->belongsTo('Sohel\Country\Model\Country','country_id','id');
    }
}
