<?php
Route::get('/{user_type}/customer/list','Sohel\User\UserController@index')->middleware(['web','auth']);
Route::post('/fetch/customer-data','Sohel\User\UserController@fetchCustomerData')->middleware(['web','auth']);
Route::post('/user/check-email','Sohel\User\UserController@checkEmail');
Route::post('/user/check-contact','Sohel\User\UserController@checkContact');
Route::get('/{user_type}/customer/add','Sohel\User\UserController@createUser')->middleware(['web','auth']);
Route::post('/{user_type}/customer/add','Sohel\User\UserController@createUser')->middleware(['web','auth']);
Route::get('/{user_type}/customer/view/{customer_id}','Sohel\User\UserController@viewUser')->middleware(['web','auth']);
Route::get('/{user_type}/customer/edit/{customer_id}','Sohel\User\UserController@updateCustomer')->middleware(['web','auth']);
Route::post('/{user_type}/customer/edit/{customer_id}','Sohel\User\UserController@updateCustomer')->middleware(['web','auth']);
Route::post('/delete/customer','Sohel\User\UserController@deleteCustomer')->middleware(['web','auth']);

Route::post('/fetch/customer/animal','Sohel\User\UserController@fetchCustomerAnimal')->middleware(['web','auth']);


Route::get('/customer/dashboard','Sohel\User\UserController@viewCustomerDashboard')->middleware(['web','auth']);
Route::post('/customer/logout','Sohel\User\UserController@customerLogout')->middleware(['web']);
Route::get('/customer/my-animal','Sohel\User\UserController@customerAnimal')->middleware(['web','auth']);
Route::post('/fetch/customer/animal-data','Sohel\User\UserController@fetchCustomerAnimalData')->middleware(['web','auth']);
Route::get('/view/animal/{animal_id}','Sohel\User\UserController@viewCoustomerAnimalInfo')->middleware(['web','auth']);
