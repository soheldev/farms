@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)
@section('content')
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Edit Customer</h2>
                </div>
                <div class="add-customer">
                    <form id="update_customer_form" method="POST" action="" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="first_name" name="first_name" placeholder="Enter Customer First Name*" class="form-control" value="{{$update_customer->first_name}}">
                                    @error('first_name')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="last_name" name="last_name" placeholder="Enter Customer Last Name*" class="form-control" value="{{$update_customer->last_name}}">
                                    @error('last_name')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="email" name="email" placeholder="Enter Customer Email Id*" class="form-control" value="{{$update_customer->email}}">
                                    @error('email')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="contact_no" name="contact_no" placeholder="Enter Customer Contact Number*" class="form-control" value="{{$update_customer->contact_no}}">
                                    @error('contact_no')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <textarea class="form-control" id="address_line_one" name="address_line_one" placeholder="Enter Address Line One" value="" rows="5">{{$update_customer->address_line_one}}</textarea>
                                @error('address_line_one')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <textarea class="form-control" id="address_line_two" name="address_line_two" placeholder="Enter Address Line Two" value="" rows="5">{{$update_customer->address_line_two}}</textarea>
                                @error('address_line_two')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="pin_code" name="pin_code" placeholder="Enter Pincode" class="form-control" value="{{$update_customer->pin_code}}" autocomplete="off">
                                    @error('pin_code')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative ">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="country_id" name="country_id">
                                        <option value="">Select Country*</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" @if($country->id == $update_customer->country_id) selected @endif>{{$country->country_name}}</option>
                                        @endforeach
                                    </select>
                                    @error('country_id')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative ">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="state_id" name="state_id">
                                        <option value="">Select State*</option>
                                        @foreach($states as $state)
                                            <option value="{{$state->id}}" @if($state->id == $update_customer->state_id) selected @endif>{{$state->state_name}}</option>
                                        @endforeach
                                    </select>
                                    @error('state_id')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative ">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="city_id" name="city_id">
                                        <option value="">Select City*</option>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}" @if($city->id == $update_customer->city_id) selected @endif>{{$city->city_name}}</option>
                                        @endforeach
                                    </select>
                                    @error('city_id')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="occupation" name="occupation" placeholder="Enter Customer Occupation*" class="form-control" value="{{$update_customer->occupation}}">
                                    @error('occupation')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative ">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="channel" name="channel">
                                        <option value="">Select Customer Channel</option>
                                        <option @if($update_customer->channel == '0') selected @endif value="0">Youtube</option>
                                        <option @if($update_customer->channel == '1') selected @endif value="1">Facebook</option>
                                        <option @if($update_customer->channel == '2') selected @endif value="2">Refered By Someone</option>
                                        <option @if($update_customer->channel == '3') selected @endif value="3">Other</option>
                                    </select>
                                    @error('channel')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="no_of_animals" name="no_of_animals" placeholder="Enter Number Of Animals*" class="form-control" value="{{$update_customer->no_of_animals}}">
                                    @error('no_of_animals')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <textarea class="form-control" id="notes" name="notes" placeholder="Enter Notes" value="" rows="5">{{$update_customer->notes}}</textarea>
                                @error('notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="file" id="profile_picture" name="profile_picture" accept=".jpeg, .jpg, .png" class="form-control">
                                    <br>
                                    <label>Preview</label><br>
                                    @if(isset($update_customer->profile_image))
                                        <img id="preview_profile_picture" height="150px" width="150px" src="{{url('/public/customer/'.$update_customer->profile_image)}}">
                                    @else
                                        <img id="preview_profile_picture" height="150px" width="150px" src="{{url('/public/media/backend/images/no-image.jpg')}}">
                                    @endif
                                    @error('profile_picture')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li><button type="submit" class="btn cust-btn btn-save" id="customer_update_btn"><i id="customer_update_loder" style="font-size:15px"></i> Save</button></li>
                                        <li><button type="button" class="btn cust-btn btn-grey" id="reset_update_customer_form">Cancel</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        var user_id = '{{$update_customer->customer_id}}';
        var previous_state_id = '{{$update_customer->state_id}}';
        var previous_city_id = '{{$update_customer->city_id}}';
        $(function ()
        {
            $('#reset_update_customer_form').click(function () {
                $('#update_customer_form')[0].reset();
            });

            $('#profile_picture').change(function () {
                var uploaded_image = window.URL.createObjectURL(this.files[0]);
                $('#preview_profile_picture').attr('src',uploaded_image);
            });

            $("#first_name").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 65 || key > 90)) {
                    return false;
                }
            });

            $("#last_name").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 65 || key > 90)) {
                    return false;
                }
            });

            $("#contact_no").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105)) {
                    return false;
                }
            });

            $("#no_of_animals").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105)) {
                    return false;
                }
            });

            $('#update_customer_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'first_name':{
                        required:true,
                    },
                    'last_name':{
                        required:true,
                    },
                    'contact_no':{
                        required:true,
                        digits:true,
                        minlength:10,
                        maxlength:10,
                        remote: {
                            url: javascript_site_path+"/user/check-contact",
                            type: "post",
                            data:{
                                flag:'0',
                                id:user_id
                            }
                        }
                    },
                    'email':{
                        required:true,
                        email:true,
                        remote: {
                            url: javascript_site_path+"/user/check-email",
                            type: "post",
                            data:{
                                flag:'0',
                                id:user_id
                            }
                        }
                    },
                    'country_id':{
                        required:true,
                    },
                    'state_id':{
                        required:true,
                    },
                    'city_id':{
                        required:true,
                    },
                    'occupation':{
                        required:true,
                    },
                    'channel':{
                        required:true,
                    },
                    'no_of_animals':{
                        required:true,
                        digits:true
                    },


                },
                messages:{
                    'first_name':{
                        required:"Please enter customer first name.",
                    },
                    'last_name':{
                        required:"Please enter customer last name.",
                    },
                    'contact_no':{
                        required:"Please enter contact number.",
                        digits:"Please enter valid contact number.",
                        minlength:"Contact number should not be less than 10 digit.",
                        maxlength:"Contact number should not be greater than 10 digit.",
                        remote:"Contact number already exits."
                    },
                    'email':{
                        required:"Please enter email id.",
                        email:"Please enter valid email id.",
                        remote:"Email id already exits."
                    },
                    'country_id':{
                        required:"Please select country.",
                    },
                    'state_id':{
                        required:"Please select state.",
                    },
                    'city_id':{
                        required:"Please select city.",
                    },
                    'occupation':{
                        required:"Please enter customer occupation.",
                    },
                    'channel':{
                        required:"Please select customer channel. ",
                    },
                    'no_of_animals':{
                        required:"Please enter number of animal.",
                        digits:"Please enter valid number.",
                    },
                },
                submitHandler:function (form) {
                    $('#customer_update_loder').addClass("fa fa-spinner fa-pulse");
                    $('#customer_update_btn').attr('disabled',true);
                    form.submit();
                }
            });

            function fetchStates(country_id)
            {
                $.ajax({
                    url: javascript_site_path+'/admin/fetch/state-according-to-country',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        country_id: country_id
                    },
                    success:function(response)
                    {
                        var html_content = '';
                        if(response.status == "1")
                        {
                            html_content += '<option value="">Select State</option>';
                            $.each(response.data,function (key,value)
                            {
                                if(value.id == previous_state_id)
                                {
                                    fetchCity(previous_state_id);
                                    html_content += '<option value="'+value.id+'" selected>'+value.state_name+'</option>';
                                }
                                else
                                {
                                    html_content += '<option value="'+value.id+'">'+value.state_name+'</option>';
                                }
                            });
                            $('#state_id').html(html_content);
                        }
                        else
                        {
                            $('#state_id').html('<option value="">No States Found...</option>');
                        }
                    },
                    beforeSend:function () {
                        $('#state_id').html('<option value="">Fetching States...</option>');
                    },
                    complete:function () {

                    },
                    error:function () {

                    }
                });
            }

            function fetchCity(state_id)
            {
                $.ajax({
                    url: javascript_site_path+'/admin/fetch/city-according-to-state',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        state_id: state_id
                    },
                    success:function(response)
                    {
                        var html_content = '';
                        if(response.status == "1")
                        {
                            html_content += '<option value="">Select City</option>';
                            $.each(response.data,function (key,value)
                            {
                                if(value.id == previous_city_id)
                                {
                                    html_content += '<option value="'+value.id+'" selected>'+value.city_name+'</option>';
                                }
                                else
                                {
                                    html_content += '<option value="'+value.id+'">'+value.city_name+'</option>';
                                }
                            });
                            $('#city_id').html(html_content);
                        }
                        else
                        {
                            $('#city_id').html('<option value="">No Cities Found...</option>');
                        }
                    },
                    beforeSend:function () {
                        $('#city_id').html('<option value="">Fetching Cities...</option>');
                    },
                    complete:function () {

                    },
                    error:function () {

                    }
                });
            }

            $('#country_id').change(function () {
                $('#state_id').html('<option value="">Select State</option>');
                $('#city_id').html('<option value="">Select City</option>');
                var country_id = $(this).val();
                fetchStates(country_id)
            });

            $('#state_id').change(function () {
                var state_id = $(this).val();
                fetchCity(state_id);
            });

        })

    </script>
@endsection