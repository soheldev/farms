@extends('layouts.user.user')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="d-flex justify-content-between flex-wrap">
                    <div class="d-flex align-items-end flex-wrap">
                        <div class="mr-md-3 mr-xl-5">
                            <h2>Welcome {{Auth::user()->name}},</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body dashboard-tabs p-0">
                        <ul class="nav nav-tabs px-4" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="information-tab" data-toggle="tab" href="#information" role="tab" aria-controls="overview" aria-selected="true">Information</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" id="pending-information-tab" data-toggle="tab" href="#pending-information" role="tab" aria-controls="purchases" aria-selected="false">Pending Information</a>
                            </li>

                            <li class="nav-item">
                            </li>

                        </ul>
                        <div class="tab-content py-0 px-0">
                            <div class="tab-pane fade show active" id="information" role="tabpanel" aria-labelledby="overview-tab">
                                <div class="d-flex flex-wrap justify-content-xl-between">
                                    <div class="d-flex border-md-right flex-grow-1 align-items-center justify-content-center p-3 item">
                                        <i class="mdi mdi-cow mr-3 icon-lg"></i>
                                        <div class="d-flex flex-column justify-content-around">
                                            <small class="mb-1 text-muted">Total Animals</small>
                                            <h5 class="mr-2 mb-0">0</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            {{--<div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <p class="card-title">#Animal Overview</p>
                        --}}{{--<div id="animal-overview-chart-legend" class="d-flex justify-content-center pt-3"></div>--}}{{--
                        <canvas id="animal-overview-chart" style="width: 100px;"></canvas>
                        <br><br>
                    </div>
                </div>
            </div>--}}
            {{--<div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div id="animal-overview-chart-legend" class="d-flex justify-content-center pt-3"></div>
                        <canvas id="animal-overview-chart" style="width: 100px;"></canvas>
                        <br><br>
                    </div>
                </div>
            </div>--}}
            {{--<div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div id="animal-breed-chart-legend" class="d-flex justify-content-center pt-3" style="width:80%"></div>
                        <canvas id="animal-breed-chart"></canvas>
                    </div>
                </div>
            </div>--}}
        </div>
        {{--<div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <p class="card-title">Recent Purchases</p>
                        <div class="table-responsive">
                            <table id="recent-purchases-listing" class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Status report</th>
                                    <th>Office</th>
                                    <th>Price</th>
                                    <th>Date</th>
                                    <th>Gross amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Jeremy Ortega</td>
                                    <td>Levelled up</td>
                                    <td>Catalinaborough</td>
                                    <td>$790</td>
                                    <td>06 Jan 2018</td>
                                    <td>$2274253</td>
                                </tr>
                                <tr>
                                    <td>Alvin Fisher</td>
                                    <td>Ui design completed</td>
                                    <td>East Mayra</td>
                                    <td>$23230</td>
                                    <td>18 Jul 2018</td>
                                    <td>$83127</td>
                                </tr>
                                <tr>
                                    <td>Emily Cunningham</td>
                                    <td>support</td>
                                    <td>Makennaton</td>
                                    <td>$939</td>
                                    <td>16 Jul 2018</td>
                                    <td>$29177</td>
                                </tr>
                                <tr>
                                    <td>Minnie Farmer</td>
                                    <td>support</td>
                                    <td>Agustinaborough</td>
                                    <td>$30</td>
                                    <td>30 Apr 2018</td>
                                    <td>$44617</td>
                                </tr>
                                <tr>
                                    <td>Betty Hunt</td>
                                    <td>Ui design not completed</td>
                                    <td>Lake Sandrafort</td>
                                    <td>$571</td>
                                    <td>25 Jun 2018</td>
                                    <td>$78952</td>
                                </tr>
                                <tr>
                                    <td>Myrtie Lambert</td>
                                    <td>Ui design completed</td>
                                    <td>Cassinbury</td>
                                    <td>$36</td>
                                    <td>05 Nov 2018</td>
                                    <td>$36422</td>
                                </tr>
                                <tr>
                                    <td>Jacob Kennedy</td>
                                    <td>New project</td>
                                    <td>Cletaborough</td>
                                    <td>$314</td>
                                    <td>12 Jul 2018</td>
                                    <td>$34167</td>
                                </tr>
                                <tr>
                                    <td>Ernest Wade</td>
                                    <td>Levelled up</td>
                                    <td>West Fidelmouth</td>
                                    <td>$484</td>
                                    <td>08 Sep 2018</td>
                                    <td>$50862</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>--}}
    </div>
@endsection

@section('jcontent')
    <script>
        var javascript_site_path = '{{url('/')}}';
        $(function () {
            //animalOverviewChart();
            //animalBreedChart();
        });

        function getRandomColor() {
            var letters = '0123456789ABCDEF'.split('');
            var color = '#';
            for (var i = 0; i < 6; i++ ) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        function animalBreedChart()
        {
            $.ajax({
                url: javascript_site_path+'/fetch/animal/breed/data',
                type: 'POST',
                //dataType: 'JSON',
                success:function (response) {
                    console.log(response.breed);
                    if(response.status == "0")
                    {
                        $("#animal-breed-chart-legend").html('');
                        var animalBreedCanvas = $("#animal-breed-chart").get(0).getContext("2d");
                        var data = {
                            datasets: [{
                                data: response.breed_animal,
                                backgroundColor:[
                                    "rgb(255, 99, 132)",
                                    "rgb(54, 162, 235)",
                                    "rgb(255,255,0)"
                                ],
                                borderWidth: 3,
                                fullWidth:true,
                            }],
                            labels: response.breed,
                        };
                        var options = {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                            },
                            animation: {
                                animation:true,
                                animateRotate: true,
                                rotation: -0.01 * Math.PI,
                                cutoutPercentage: 50,
                            },
                        };
                        var animalBreed = new Chart(animalBreedCanvas, {
                            type: 'doughnut',
                            data: data,
                            options: options
                        });
                    }
                    else
                    {
                        $("#animal-breed-chart-legend").html('<h3>No Data Found...</h3>');
                    }
                },
                beforeSend:function () {
                    $("#animal-breed-chart-legend").html('<i class="fa fa-spinner fa-pulse fa-2x"></i>&nbsp;<h3> Fetching Animal Data</h3>');
                }
            });
        }

        function animalOverviewChart()
        {
            $.ajax({
               url: javascript_site_path+'/fetch/animal/overall/data',
               type: 'POST',
               //dataType: 'JSON',
               success:function (response) {
                    if(response.status == "0")
                    {
                        $("#animal-overview-chart-legend").html('');
                        var animalOverviewCanvas = $("#animal-overview-chart").get(0).getContext("2d");
                        var data = {
                            datasets: [{
                                data: [response.infected_animal,response.fit_animal],
                                backgroundColor:["rgb(255, 99, 132)","rgb(54, 162, 235)"],
                                borderWidth: 3,
                                fullWidth:true,
                            }],
                            labels: ['Animal With Disease','Animal With No Disease'],
                        };
                        var options = {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                            },
                            animation: {
                                animation:true,
                                animateRotate: true,
                                rotation: -0.01 * Math.PI,
                                cutoutPercentage: 50,
                            },
                        };
                        var animalOverview = new Chart(animalOverviewCanvas, {
                            type: 'doughnut',
                            data: data,
                            options: options
                        });
                    }
                    else
                    {
                        $("#animal-overview-chart-legend").html('<h3>No Data Found...</h3>');
                    }
               },
               beforeSend:function () {
                   $("#animal-overview-chart-legend").html('<i class="fa fa-spinner fa-pulse fa-2x"></i>&nbsp;<h3> Fetching Animal Data</h3>');
               }
            });
        }
    </script>
    @endsection