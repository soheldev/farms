@extends('layouts.user.user')
@section('content')
    <style>
        .my-custom-scrollbar {
            position: relative;
            height: 400px;
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url('customer/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">List Animal</p>
                        </div>
                        <br>
                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>Animal Id</th>
                                    <th>Animal Type</th>
                                    <th>Animal Name</th>
                                    <th>Animal Breed</th>
                                    <th>Animal Gender</th>
                                </tr>
                                </thead>

                                <tbody id="customer_animal_table">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('jcontent')
    <script>

        var site_path = '{{url('/')}}';
        var skip = 0;
        var i = 0;
        var search_value = '';
        $(function (){
            fetchCustomerAnimal(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 5;
                    fetchCustomerAnimal(skip);
                }
            };
        });

        function fetchCustomerAnimal(skip)
        {
            $.ajax({
                url: site_path+'/fetch/customer/animal-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip,
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td><a href="'+site_path+'/view/animal/'+btoa(this.id)+'">'+this.animal_unique_id+'</a></td>';
                            if(this.animal_type == '0')
                            {
                                html_content += '<td>Goat</td>';
                            }
                            else
                            {
                                html_content += '<td>Sheep</td>';
                            }
                            html_content += '<td>'+this.animal_name+'</td>';
                            html_content += '<td>'+this.breed_name+'</td>';
                            if(this.gender == '0')
                            {
                                html_content += '<td>Andul</td>';
                            }
                            else if(this.gender == '1')
                            {
                                html_content += '<td>Khassi</td>';
                            }
                            else
                            {
                                html_content += '<td>Female</td>';
                            }
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="5"><center>No Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#customer_animal_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });

            function searchRecord(skip,search_value)
            {
                $.ajax({
                    url: site_path+'/fetch/customer/animal-data',
                    type: 'POST',
                    dataType: 'JSON',
                    data:{
                        skip:skip,
                        search_value:search_value
                    },
                    success:function (response) {
                        console.log(response);
                        var html_content = '';
                        if(response.status == "1")
                        {
                            $.each(response.data,function (index,value) {

                                html_content += '<tr>';
                                html_content += '<td><a href="'+site_path+'/customer/animal/view/'+btoa(this.id)+'">'+this.animal_unique_id+'</a></td>';
                                if(this.animal_type == '0')
                                {
                                    html_content += '<td>Goat</td>';
                                }
                                else
                                {
                                    html_content += '<td>Sheep</td>';
                                }
                                html_content += '<td>'+this.animal_name+'</td>';
                                html_content += '<td>'+this.breed_name+'</td>';
                                if(this.gender == '0')
                                {
                                    html_content += '<td>Andul</td>';
                                }
                                else if(this.gender == '1')
                                {
                                    html_content += '<td>Khassi</td>';
                                }
                                else
                                {
                                    html_content += '<td>Female</td>';
                                }
                                html_content += '</tr>';
                            });
                        }
                        else
                        {
                            html_content += '<tr><td colspan="5"><center>No Record Found...</center></td></tr>';
                        }
                        $('#customer_animal_table').html(html_content);

                    },
                    beforeSend:function () {
                        $('#customer_animal_table').html('<tr><td colspan="5"><center><i class="fa fa-spinner fa-pulse fa-2x"><i/></center></td></tr>');
                    },
                    complete:function(){

                    },
                    error:function(){

                    }
                });
            }

            $('#search_text').keyup(function () {
                search_value = $(this).val();
                skip = 0;
                searchRecord(skip,search_value);
            });
        }
    </script>
@endsection