<?php

namespace Sohel\User;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CheckPermission;
use App\Http\Helpers\UniqueId;
use App\UserInformation;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sohel\Animal\Model\Animal;
use Sohel\City\Model\City;
use Sohel\Country\Model\Country;
use Sohel\State\Model\State;
use Sohel\User\Model\Customer;

class UserController extends Controller
{
    public function __construct()
    {
        //$this->checkType();
    }

    public function checkType()
    {
        $user_redirect = '';
        $auth_type = Auth::user()->userInformation->user_type;
        if($auth_type == "1")
        {
            $user_redirect = "/admin";
        }
        else
        {
            $user_redirect = "/vendor";
        }
        return $user_redirect;
    }
    //
    public function index()
    {
        $add_customer_permission = false;
        $update_customer_permission = false;
        $delete_customer_permission = false;
        $add_customer_permission = CheckPermission::hasPermission('create.customer');
        $update_customer_permission = CheckPermission::hasPermission('update.customer');
        $delete_customer_permission = CheckPermission::hasPermission('delete.customer');
        return view('user::user-list',compact('add_customer_permission','update_customer_permission','delete_customer_permission'));
    }

    public function fetchCustomerData(Request $request)
    {
        //dd($request->all());
        $added_by = Auth::user()->id;
        $data = Customer::query();
        //$data = $data->where('added_by',$added_by);
        if(isset($search_value) && $search_value != '')
        {
            $data = $data->where('first_name', 'LIKE', '%' .$search_value. '%');
        }
        $data = $data->skip($request->skip)->limit(5);
        $data = $data->get();
        //$data = Customer::skip($request->skip)->limit(5)->where('added_by',$added_by)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function checkEmail(Request $request)
    {
        $email_id = $request->email;
        $flag = $request->flag;
        if($flag == "1")
        {
            if(isset($email_id))
            {
                $check_email = User::where('email',$email_id)->first();
                if(isset($check_email))
                {
                    return 'false';
                }
                else
                {
                    return 'true';
                }
            }
        }
        else
        {
            if(isset($email_id))
            {
                $id = $request->id;
                $check_email = User::where('email',$email_id)->where('id','<>',$id)->first();
                if(isset($check_email))
                {
                    return 'false';
                }
                else
                {
                    return 'true';
                }
            }
        }

    }

    public function checkContact(Request $request)
    {
        $contact = $request->contact_no;
        $flag = $request->flag;
        if($flag == "1")
        {
            if(isset($contact))
            {
                $check_contact = UserInformation::where('mobile_number',$contact)->first();
                if(isset($check_contact))
                {
                    return 'false';
                }
                else
                {
                    return 'true';
                }
            }
        }
        else
        {
            if(isset($contact))
            {
                $id = $request->id;
                $check_contact = UserInformation::where('mobile_number',$contact)->where('user_id','<>',$id)->first();
                if(isset($check_contact))
                {
                    return 'false';
                }
                else
                {
                    return 'true';
                }
            }
        }
    }

    public function createUser(Request $request)
    {
        if($request->method() == "GET")
        {
            $countries = Country::all('id','country_name');
            return view('user::user-add',compact('countries'));
        }
        else
        {

            $validator = \Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'contact_no' => 'required',
                'country_id' => 'required',
                'state_id' => 'required',
                'city_id' => 'required',
                'occupation' => 'required',
                'channel' => 'required',
                'no_of_animals' => 'required',
            ],
                $messages = [
                    'first_name.required'    => 'Please enter customer first name.',
                    'last_name.required'    => 'Please enter customer last name.',
                    'contact_no.required'    => 'Please enter customer contact number.',
                    'email.required'    => 'Please enter customer email id.',
                    'country_id.required'    => 'Please select country.',
                    'state_id.required'    => 'Please select state.',
                    'city_id.required'    => 'Please select city.',
                    'occupation.required'    => 'Please enter customer occupation.',
                    'channel.required'    => 'Please select channel.',
                    'no_of_animals.required'    => 'Please enter number of animal..',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $customer_name = $request->first_name.' '.$request->last_name;
                $create_customer_user = [];
                $create_customer_user['name'] = $customer_name;
                $create_customer_user['email'] = $request->email;
                $create_customer_user['password'] = bcrypt('123456789');
                $create_customer_user['user_type'] = '4';
                $created_user = \App\User::create($create_customer_user);
                if(isset($created_user))
                {
                    $create_customer_arr = [];
                    $unique_number = UniqueId::generateUniqueIdentification(10);
                    $customer_id = 'C'.$unique_number;
                    $create_customer_arr['customer_id'] = $created_user->id;
                    $create_customer_arr['customer_unique_id'] = $customer_id;
                    $create_customer_arr['first_name'] = $request->first_name;
                    $create_customer_arr['last_name'] = $request->last_name;
                    $create_customer_arr['email'] = $request->email;
                    $create_customer_arr['contact_no'] = $request->contact_no;
                    $create_customer_arr['address_line_one'] = isset($request->address_line_one)?$request->address_line_one:NULL;
                    $create_customer_arr['address_line_two'] = isset($request->address_line_two)?$request->address_line_two:NULL;
                    $create_customer_arr['country_id'] = $request->country_id;
                    $create_customer_arr['state_id'] = $request->state_id;
                    $create_customer_arr['city_id'] = $request->city_id;
                    $create_customer_arr['pin_code'] = isset($request->pin_code)?$request->pin_code:NULL;
                    $create_customer_arr['occupation'] = $request->occupation;
                    $create_customer_arr['channel'] = $request->channel;
                    $create_customer_arr['notes'] = $request->notes;
                    $create_customer_arr['no_of_animals'] = $request->no_of_animals;
                    $create_customer_arr['added_by'] = Auth::user()->id;
                    $create_customer_arr['joining_date'] = date('Y-m-d');
                    if($request->hasFile('profile_picture'))
                    {
                        if (! \File::exists(public_path()."/customer")) {
                            \File::makeDirectory(public_path()."/customer");
                        }
                        $imageName = time().'.'.request()->profile_picture->getClientOriginalExtension();
                        request()->profile_picture->move(public_path('/customer/'), $imageName);
                        $create_customer_arr['profile_image'] = $imageName;
                    }
                    Customer::create($create_customer_arr);
                    $type = $this->checkType();
                    return redirect($type.'/customer/list')->with(['success' => 'Customer Added Successfully...']);
                }

            }
        }
    }

    public function viewUser($type,$customer_id)
    {
        $id = base64_decode($customer_id);
        $view_customer = Customer::find($id);
        return view('user::user-view',compact('view_customer'));
    }

    public function updateCustomer($type, $customer_id, Request $request)
    {
        $id = base64_decode($customer_id);
        $update_customer = Customer::find($id);
        if($request->method() == "GET")
        {
            $countries = Country::all('id','country_name');
            $states = State::select('id','state_name')->where('country_id',$update_customer->country_id)->get();
            $cities = City::select('id','city_name')->where('state_id',$update_customer->state_id)->get();
            return view("user::user-edit",compact('update_customer','countries','states','cities'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'contact_no' => 'required',
                'country_id' => 'required',
                'state_id' => 'required',
                'city_id' => 'required',
                'occupation' => 'required',
                'channel' => 'required',
                'no_of_animals' => 'required',
            ],
                $messages = [
                    'first_name.required'    => 'Please enter customer first name.',
                    'last_name.required'    => 'Please enter customer last name.',
                    'contact_no.required'    => 'Please enter customer contact number.',
                    'email.required'    => 'Please enter customer email id.',
                    'country_id.required'    => 'Please select country.',
                    'state_id.required'    => 'Please select state.',
                    'city_id.required'    => 'Please select city.',
                    'occupation.required'    => 'Please enter customer occupation.',
                    'channel.required'    => 'Please select channel.',
                    'no_of_animals.required'    => 'Please enter number of animal..',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $update_user_arr = [];
                $customer_name = $request->first_name.' '.$request->last_name;
                $update_user_arr['name'] = $customer_name;
                $update_user_arr['email'] = $request->email;

                $update_customer_arr = [];
                $update_customer_arr['first_name'] = $request->first_name;
                $update_customer_arr['last_name'] = $request->last_name;
                $update_customer_arr['email'] = $request->email;
                $update_customer_arr['contact_no'] = $request->contact_no;
                $update_customer_arr['address_line_one'] = isset($request->address_line_one)?$request->address_line_one:NULL;
                $update_customer_arr['address_line_two'] = isset($request->address_line_two)?$request->address_line_two:NULL;
                $update_customer_arr['country_id'] = $request->country_id;
                $update_customer_arr['state_id'] = $request->state_id;
                $update_customer_arr['city_id'] = $request->city_id;
                $update_customer_arr['pin_code'] = isset($request->pin_code)?$request->pin_code:NULL;
                $update_customer_arr['occupation'] = $request->occupation;
                $update_customer_arr['channel'] = $request->channel;
                $update_customer_arr['notes'] = $request->notes;
                $update_customer_arr['no_of_animals'] = $request->no_of_animals;
                if($request->hasFile('profile_picture'))
                {
                    $old_image = $update_customer->profile_image;
                    $imageName = time().'.'.request()->profile_picture->getClientOriginalExtension();
                    request()->profile_picture->move(public_path('/customer/'), $imageName);
                    $update_customer_arr['profile_image'] = $imageName;
                    if(isset($old_image))
                    {
                        unlink(public_path('/customer/'.$old_image));
                    }
                }
                DB::table('customers')->where('id',$update_customer->id)->update($update_customer_arr);
                DB::table('users')->where('id',$update_customer->customer_id)->update($update_user_arr);
                $type = $this->checkType();
                return redirect($type.'/customer/list')->with(['success' => 'Customer Updated Successfully...']);
            }
        }
    }

    public function deleteCustomer(Request $request)
    {
        $id = base64_decode($request->id);
        if(isset($id))
        {
            $delete_customer = Customer::find($id);
            if(isset($delete_customer))
            {
                $delete_customer_image = $delete_customer->profile_image;
                if(isset($delete_customer_image))
                {
                    unlink(public_path('/customer/'.$delete_customer_image));
                }
                DB::table('users')->where('id',$delete_customer->customer_id)->delete();
                if($delete_customer->delete())
                {
                    $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Customer deleted successfully...');
                    return json_encode($result);
                }
                else
                {
                    $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something went wrong...');
                    return json_encode($result);
                }

            }
        }
    }

    public function fetchCustomerAnimal(Request $request)
    {
        $customer_id = $request->customer_id;
        $animals = Animal::select('id','prefix','animal_id','animal_type','animal_name','breed_id','gender')->where('customer_id',$customer_id)->get();
        if($animals->count() > 0)
        {
            $result = array('status'=>'1','data'=>$animals);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function viewCustomerDashboard()
    {
        return view('user::user-dashboard');
    }

    public function customerLogout()
    {
        Auth::logout();
        return redirect('/login')->with(['error' => 'You have successfully logout']);
    }

    public function customerAnimal()
    {
        return view('user::user-animal-list');
    }

    public function fetchCustomerAnimalData(Request $request)
    {
        $customer_id = Auth::user()->id;
        $search_value = $request->search_value;

        $sql = "SELECT a.id,CONCAT(a.prefix,a.animal_id) AS animal_unique_id, a.animal_type,a.animal_name,a.breed_id,a.gender,ab.breed_name
                FROM animals AS a 
                JOIN breeds AS ab 
                ON a.breed_id = ab.id";
        $sql = $sql." WHERE a.customer_id = '$customer_id'  AND a.status = '1'";
        if(isset($request->search_value))
        {
            $sql = $sql." AND (a.animal_name LIKE '%$request->search_value%' OR CONCAT(a.prefix,a.animal_id) LIKE '%$request->search_value%' OR ab.breed_name LIKE '%$request->search_value%')";
        }
        $sql = $sql." LIMIT $request->skip , 5";
        $data = DB::select($sql);
        if(count($data) > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function viewCoustomerAnimalInfo($animal_id)
    {
        $animal_id =  base64_decode($animal_id);
        $view_animal_info = Animal::find($animal_id);
        return view('user::user-animal-view',compact('view_animal_info'));
    }
}
