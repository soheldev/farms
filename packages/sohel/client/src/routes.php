<?php

Route::get('/master/client/list','Sohel\Client\ClientController@index')->middleware(['web','auth']);
Route::post('/fetch/client-data','Sohel\Client\ClientController@fetchClientData')->middleware(['web','auth']);
Route::get('/master/client/add','Sohel\Client\ClientController@createClient')->middleware(['web','auth']);
Route::post('/master/client/add','Sohel\Client\ClientController@createClient')->middleware(['web','auth']);
Route::get('/master/client/view/{client_id}','Sohel\Client\ClientController@viewClient')->middleware(['web','auth']);
Route::get('/master/client/edit/{client_id}','Sohel\Client\ClientController@updateClient')->middleware(['web','auth']);
Route::post('/master/client/edit/{client_id}','Sohel\Client\ClientController@updateClient')->middleware(['web','auth']);
Route::post('/delete/client','Sohel\Client\ClientController@deleteClient')->middleware(['web','auth']);


