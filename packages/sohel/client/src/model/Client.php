<?php

namespace Sohel\Client\Model;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $table = 'clients';
    protected $fillable = ['client_id','client_name','farm_name','email','contact_no','sub_domain','database_name','database_username','database_password','notes'];
    protected $with = ['stateName'];

    /*public function countryName()
    {
        return $this->belongsTo('Sohel\Country\Model\Country','country_id','id');
    }*/

    public function stateName()
    {
        return $this->belongsTo('Sohel\State\Model\State','state_id','id');
    }
}
