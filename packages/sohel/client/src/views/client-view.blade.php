@extends('layouts.master.master')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url('/master/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url('/master/client/list')}}">List Clients&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">View Client Information</p>
                        </div>

                        <br>

                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>
                                        Client Id : <span>#{{$view_client->client_id}}</span>
                                    </th>
                                    <th>
                                        Client Name : <span>{{$view_client->client_name}}</span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Email Id : <span>{{$view_client->email}}</span>
                                    </th>
                                    <th>
                                        Contact Number : <span>{{$view_client->contact_no}}</span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Farm Name : <span>{{$view_client->farm_name}}</span>
                                    </th>
                                    <th>
                                        Sub Domain : <span>{{$view_client->sub_domain}}</span>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('jcontent')
<script>
    var site_path = '{{url('/')}}';

</script>

@endsection