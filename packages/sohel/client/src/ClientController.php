<?php

namespace Sohel\Client;

use App\Http\Controllers\Controller;
use App\Http\Helpers\UniqueId;
use App\UserInformation;
use App\UserRole;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sohel\Animal\Model\Animal;
use Sohel\City\Model\City;
use Sohel\Client\Model\Client;
use Sohel\Country\Model\Country;
use Sohel\Role\Model\Role;
use Sohel\State\Model\State;
use Sohel\User\Model\Customer;

class ClientController extends Controller
{
    public function __construct()
    {
        //$this->checkType();
    }

    public function checkType()
    {
        $user_redirect = '';
        $auth_type = Auth::user()->userInformation->user_type;
        if ($auth_type == "1") {
            $user_redirect = "/admin";
        } else {
            $user_redirect = "/vendor";
        }
        return $user_redirect;
    }

    public function fetchcityAccordingToState(Request $request)
    {
        $cities_arr = [];
        $state_id = $request->state_id;
        $cities = City::where('state_id',$state_id)->get();
        if($cities->count() > 0)
        {
            foreach($cities as $key => $city)
            {
                $cities_arr[$key]['id'] = $city->id;
                $cities_arr[$key]['city_name'] = $city->city_name;
            }
            $result = array('status' => '1', 'data' => $cities_arr);
        }
        else
        {
            $result = array('status' => '0', 'data' => $cities_arr);
        }
        return json_encode($result);
    }

    //
    public function index()
    {
        return view('client::client-list');
    }

    public function fetchClientData(Request $request)
    {
        $sql = "SELECT c.id,c.client_id,c.client_name,c.farm_name,c.email,c.contact_no,c.sub_domain,c.status
                FROM clients AS c";
        if (isset($request->search_value))
        {
            $sql = $sql . " WHERE (c.client_id LIKE '%$request->search_value%' OR c.client_name LIKE '%$request->search_value%' OR c.farm_name LIKE '%$request->search_value%' OR c.email LIKE '%$request->search_value%' OR c.contact_no LIKE '%$request->search_value%' OR c.sub_domain LIKE '%$request->search_value%' )";
        }
        $sql = $sql . " LIMIT $request->skip , 5";
        $data = DB::select($sql);
        if (count($data) > 0)
        {
            $result = array('status' => '1', 'data' => $data);
            return json_encode($result);

        }
        else
        {
            $result = array('status' => '0');
            return json_encode($result);
        }
    }

    public function checkSubadminEmail(Request $request)
    {
        $email_id = $request->email;
        $flag = $request->flag;
        if ($flag == "1") {
            if (isset($email_id)) {
                $check_email = User::where('email', $email_id)->first();
                if (isset($check_email)) {
                    return 'false';
                } else {
                    return 'true';
                }
            }
        } else {
            if (isset($email_id)) {
                $id = $request->id;
                $check_email = User::where('email', $email_id)->where('id', '<>', $id)->first();
                if (isset($check_email)) {
                    return 'false';
                } else {
                    return 'true';
                }
            }
        }

    }

    public function checkSubadminContact(Request $request)
    {
        $contact = $request->contact_no;
        $flag = $request->flag;
        if ($flag == "1") {
            if (isset($contact)) {
                $check_contact = UserInformation::where('mobile_number', $contact)->first();
                if (isset($check_contact)) {
                    return 'false';
                } else {
                    return 'true';
                }
            }
        } else {
            if (isset($contact)) {
                $id = $request->id;
                $check_contact = UserInformation::where('mobile_number', $contact)->where('user_id', '<>', $id)->first();
                if (isset($check_contact)) {
                    return 'false';
                } else {
                    return 'true';
                }
            }
        }
    }

    public function createClient(Request $request)
    {
        if ($request->method() == "GET")
        {
            return view('client::client-add');
        }
        else
        {

            $validator = \Validator::make($request->all(), [
                'client_name' => 'required',
                'farm_name' => 'required',
                'email' => 'required',
                'contact_no' => 'required',
                'sub_domain' => 'required',
                'database_name' => 'required',
                'database_username' => 'required',
                //'database_password' => 'required',
            ],
                $messages = [
                    'client_name.required' => 'Please enter client name.',
                    'farm_name.required' => 'Please enter client farm name.',
                    'email.required' => 'Please enter email id.',
                    'contact_no.required' => 'Please enter contact number.',
                    'sub_domain.required' => 'Please enter sub domain.',
                    'database_name.required' => 'Please enter database name.',
                    'database_username.required' => 'Please enter database username.',
                    //'database_password.required' => 'Please enter database password.',
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            } else {

                $create_client = [];
                $unique_number = UniqueId::generateUniqueIdentification(10);
                $client_id = 'CL' . $unique_number;
                $create_client['client_id'] = $client_id;
                $create_client['client_name'] = $request->client_name;
                $create_client['farm_name'] = $request->farm_name;
                $create_client['email'] = $request->email;
                $create_client['contact_no'] = $request->contact_no;
                $create_client['sub_domain'] = $request->sub_domain;
                $create_client['database_name'] = $request->database_name;
                $create_client['database_username'] = $request->database_username;
                $create_client['database_password'] = $request->database_password;
                $create_client['notes'] = $request->notes;
                Client::create($create_client);
                return redirect('/master/client/list')->with(['success' => 'Client Added Successfully...']);


            }
        }
    }

    public function viewClient($client_id)
    {
        $id = base64_decode($client_id);
        $view_client = Client::find($id);
        return view('client::client-view', compact('view_client'));
    }

    public function updateClient($client_id, Request $request)
    {
        $id = base64_decode($client_id);
        $update_client = Client::where('id', $id)->first();
        if ($request->method() == "GET")
        {
            return view("client::client-edit", compact('update_client'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'client_name' => 'required',
                'farm_name' => 'required',
                'email' => 'required',
                'contact_no' => 'required',
                'sub_domain' => 'required',
                'database_name' => 'required',
                'database_username' => 'required',
                //'database_password' => 'required',
            ],
                $messages = [
                    'client_name.required' => 'Please enter client name.',
                    'farm_name.required' => 'Please enter client farm name.',
                    'email.required' => 'Please enter email id.',
                    'contact_no.required' => 'Please enter contact number.',
                    'sub_domain.required' => 'Please enter sub domain.',
                    'database_name.required' => 'Please enter database name.',
                    'database_username.required' => 'Please enter database username.',
                    //'database_password.required' => 'Please enter database password.',
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            } else {

                $client_id = $update_client->id;
                $update_client_arr = [];
                $update_client_arr['client_name'] = $request->client_name;
                $update_client_arr['farm_name'] = $request->farm_name;
                $update_client_arr['email'] = $request->email;
                $update_client_arr['contact_no'] = $request->contact_no;
                $update_client_arr['sub_domain'] = $request->sub_domain;
                $update_client_arr['database_name'] = $request->database_name;
                $update_client_arr['database_username'] = $request->database_username;
                $update_client_arr['database_password'] = $request->database_password;
                $update_client_arr['notes'] = $request->notes;
                DB::table('clients')->where('id', $client_id)->update($update_client_arr);
                return redirect('/master/client/list')->with(['success' => 'Client Updated Successfully...']);
            }
        }
    }

    public function deleteClient(Request $request)
    {
        $id = base64_decode($request->id);
        if (isset($id)) {
            $delete_client = Client::where('id', $id)->first();
            if (isset($delete_client)) {
                if ($delete_client->delete()) {
                    $result = array('status' => '1', 'icon' => 'success', 'title' => 'Deleted', 'text' => 'Client deleted successfully...');
                    return json_encode($result);
                } else {
                    $result = array('status' => '0', 'icon' => 'error', 'title' => 'Opps', 'text' => 'Something went wrong...');
                    return json_encode($result);
                }

            }
        }
    }
}
