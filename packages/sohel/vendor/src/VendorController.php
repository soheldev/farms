<?php

namespace Sohel\Vendor;

use App\Http\Helpers\ConnectDatabase;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Sohel\Animal\Model\Animal;
use Sohel\Animal\Model\AnimalDisease;
use Sohel\Breed\Model\Breed;
use Sohel\Payment\Model\Payment;
use Sohel\Payment\Model\PaymentReminder;
use Sohel\Progress\Model\AnimalProgress;
use Sohel\Progress\Model\ProgressReminder;
use Sohel\User\Model\Customer;

class VendorController extends Controller
{
    //

    public function __construct(Request $request)
    {
        $subdomains = explode('.', $request->getHost());
        $subdomain = $subdomains[0];
        ConnectDatabase::connectDB($subdomain);
    }

    public function viewVendorLogin()
    {
        return view('vendor::vendor-login');
    }

    public function viewVendorDashboard()
    {
        $added_by = Auth::user()->id;
        $pending_payments = PaymentReminder::where('payment_tracker','0')->where('payment_due_date','<=',date('Y-m-d'))->get();
        if($pending_payments->count() > 0)
        {
            $this->generatePendingCustomerPaymentLine($pending_payments);
        }
        $pending_progress = ProgressReminder::where('progress_tracker','0')->where('progress_entry_date','<=',date('Y-m-d'))->get();
        if($pending_progress->count() > 0)
        {
            $this->generatePendingAnimalProgressLine($pending_progress);
        }
        $animal_count = Animal::where('status','1')->count();
        $customer_count = Customer::count();
        $breed_count = Breed::count();
        $progress_reminder_count = AnimalProgress::where('status','0')->count();
        $pending_payment_count = Payment::where('payment_status','0')->count();
        $current_month_earning = DB::select("SELECT sum(amount_paid) AS earning FROM payments
                                                    WHERE MONTH(payment_date) = MONTH(CURRENT_DATE())
                                                    AND YEAR(payment_date) = YEAR(CURRENT_DATE())
                                                    AND payment_status = '1'");
        $current_month_earning = $current_month_earning[0];
        $current_month_pending_amount = DB::select("SELECT sum(amount_due) AS pending FROM payments
                                                    WHERE MONTH(due_date) = MONTH(CURRENT_DATE())
                                                    AND YEAR(due_date) = YEAR(CURRENT_DATE())
                                                    AND payment_status = '0'");
        $current_month_pending_amount = $current_month_pending_amount[0];
        return view('vendor::vendor-dashboard',compact('progress_reminder_count','animal_count','customer_count','breed_count','pending_payment_count','current_month_earning','current_month_pending_amount'));
    }

    public function generatePendingAnimalProgressLine($pending_progress)
    {
        $pending_progress_lines_arr = [];
        $update_progress_reminder_arr = [];
        foreach($pending_progress as $index => $progress)
        {
            $pending_progress_lines_arr[$index]['animal_id'] = $progress->animal_id;
            $pending_progress_lines_arr[$index]['weight'] = 0;
            $pending_progress_lines_arr[$index]['height'] = 0;
            $pending_progress_lines_arr[$index]['due_date'] = $progress->progress_entry_date;
            $pending_progress_lines_arr[$index]['added_by'] = Auth::user()->id;

            $progress_reminder = new \DateTime($progress->progress_entry_date);
            $progress_reminder->modify('+1 months');
            $new_progress_reminder_date = $progress_reminder->format("Y-m-d");
            $update_progress_reminder_arr['progress_entry_date'] = $new_progress_reminder_date;
            DB::table('progress_reminders')->where('id',$progress->id)->update($update_progress_reminder_arr);
        }
        DB::table('animal_progress')->insert($pending_progress_lines_arr);
    }

    public function generatePendingCustomerPaymentLine($pending_payments)
    {
        $pending_payment_lines_arr = [];
        $update_payment_reminder_arr = [];
        foreach($pending_payments as $key => $pending_payment)
        {
            $pending_payment_lines_arr[$key]['animal_id'] = $pending_payment->animal_id;
            $pending_payment_lines_arr[$key]['customer_id'] = $pending_payment->paymentReminderAnimalInfo->customer_id;
            $pending_payment_lines_arr[$key]['amount_due'] = $pending_payment->paymentReminderAnimalInfo->monthly_rent;
            $pending_payment_lines_arr[$key]['amount_paid'] = 0;
            $pending_payment_lines_arr[$key]['due_date'] = $pending_payment->payment_due_date;
            $pending_payment_lines_arr[$key]['payment_status'] = '0';
            $pending_payment_lines_arr[$key]['added_by'] = Auth::user()->id;
            $pending_payment_lines_arr[$key]['created_at'] = date('Y-m-d h:m:i');

            $payment_reminder = new \DateTime($pending_payment->payment_due_date);
            $payment_reminder->modify('+1 months');
            $new_payment_due_date = $payment_reminder->format("Y-m-d");
            $update_payment_reminder_arr['payment_due_date'] = $new_payment_due_date;
            DB::table('payments_reminders')->where('id',$pending_payment->id)->update($update_payment_reminder_arr);
        }
        DB::table('payments')->insert($pending_payment_lines_arr);
    }

    public function vendorLogout()
    {
        Auth::logout();
        return redirect('/login')->with(['error' => 'You have successfully logout']);
    }

    public function vendorProfile(Request $request)
    {
        $vendor_profile = Auth::user();
        if($request->method() == 'GET')
        {
            return view('vendor::vendor-profile',compact('vendor_profile'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'email' => 'required',
                'mobile_number' => 'required',
                'name' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
            ],
            $messages = [
                'email.required'    => 'Please enter email id.',
                'name.required'    => 'Please enter farm name.',
                'first_name.required'    => 'Please enter first name.',
                'last_name.required'    => 'Please enter last name.',
                'mobile_number.required' => 'Please enter mobile number.',
            ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $vendor_profile->email = $request->email;
                $vendor_profile->name = $request->name;
                if($request->hasFile('profile_image'))
                {
                    if (! \File::exists(public_path()."/farm")) {
                        \File::makeDirectory(public_path()."/farm");
                    }
                    $imageName = time().'.'.request()->profile_image->getClientOriginalExtension();
                    request()->profile_image->move(public_path('/farm/'), $imageName);
                    $vendor_profile->userInformation->profile_image = $imageName;
                }
                $vendor_profile->userInformation->first_name = $request->first_name;
                $vendor_profile->userInformation->last_name = $request->last_name;
                $vendor_profile->userInformation->mobile_number = $request->mobile_number;
                $vendor_profile->userInformation->save();
                $vendor_profile->save();
                return redirect()->back()->with(['success' => 'Profile Updated Successfully...']);



            }

        }

    }

    public function checkVendorMailDuplication(Request $request)
    {
        $check_email = User::where('email',$request->email)->where('id','<>',$request->id)->first();
        if(isset($check_email))
        {
            return 'false';
        }
        else
        {
            return 'true';
        }

    }

    public function updateVendorProfile(Request $request)
    {
        $id = $request->id;
        $name = $request->name;
        $email = $request->email;
        $mobile = $request->mobile_number;
        $update_admin_profile = User::find($id);
        if(isset($update_admin_profile)) {
            dd($update_admin_profile->userInformation);
            $update_admin_profile->name = $name;
            $update_admin_profile->email = $email;
            $update_admin_profile->save();
            $update_admin_profile->userInformation->mobile_number = $mobile;
            $update_admin_profile->userInformation->save();
        }
    }

    public function animalOverallData(Request $request)
    {
        $current_logged_in_farm_id = Auth::user()->id;
        $animals = Animal::where('added_by',$current_logged_in_farm_id)->where('status','1')->get();
        $fit_animal = $animals->reject(function ($value, $key) {
            $infected_animal = AnimalDisease::where('animal_id',$value->id)->where('recovered_date',NULL)->first();
            if(isset($infected_animal))
            {
                return $value;
            }
        });
        $infected_animal = $animals->filter(function ($value, $key) {
            $infected_animal = AnimalDisease::where('animal_id',$value->id)->where('recovered_date',NULL)->first();
            if(isset($infected_animal))
            {
                return $value;
            }
        });
        $fit_animal_count = $fit_animal->count();
        $infected_animal_count = $infected_animal->count();
        if($fit_animal_count > 0 || $infected_animal_count > 0)
        {
            $result = array('status' => '0','fit_animal'=>$fit_animal_count,'infected_animal' =>$infected_animal_count);
        }
        else
        {
            $result = array('status' => '1','fit_animal'=>$fit_animal_count,'infected_animal' =>$infected_animal_count);
        }
        return $result;
    }

    public function animalBreedData()
    {
        $current_logged_in_farm_id = Auth::user()->id;
        $breed_arr = [];
        $breed_count_arr = [];
        $breeds = Breed::where('added_by',$current_logged_in_farm_id)->get();
        foreach($breeds as $breed)
        {
            array_push($breed_arr,$breed->breed_name);
            $animal_count_breed_wise = Animal::where('added_by',$current_logged_in_farm_id)->where('status','1')->where('breed_id',$breed->id)->count();
            array_push($breed_count_arr,$animal_count_breed_wise);
        }
        if(count($breed_arr) > 0 && count($breed_count_arr) > 0)
        {
            $result = array('status' => '0','breed' => $breed_arr, 'breed_animal' => $breed_count_arr);
        }
        else
        {
            $result = array('status' => '1','breed' => $breed_arr, 'breed_animal' => $breed_count_arr);
        }
        return $result;

    }
}
