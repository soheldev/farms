<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Vendor Login</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{url('/public/media/backend')}}/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="{{url('/public/media/backend')}}/vendors/base/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{url('/public/media/backend')}}/css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{url('/public/media/backend')}}/images/favicon.png"/>
</head>

<body>
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth lock-full-bg">
            <div class="row w-100">
                <div class="col-lg-4 mx-auto">
                    <div class="auth-form-transparent text-left p-5 text-center">
                        @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                        @endif
                        <img src="{{url('/public/media/backend')}}/images/faces/face13.jpg" class="lock-profile-img" alt="img">
                        <form class="pt-5" action="{{ url('/login') }}" method="POST" id="vendor_login_form">
                            <div class="form-group">
                                <input type="text" class="form-control text-center" id="email" name="email" placeholder="Email" value="{{ old('email') }}" autocomplete="off" autofocus>
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-control text-center" id="password" name="password" placeholder="Password" autocomplete="off">
                            </div>
                            <div class="mt-5">
                                <button type="submit" id="vendor_login_button" class="btn btn-block btn-success btn-lg font-weight-medium">LOGIN</button>
                            </div>
                            {{--<div class="mt-3 text-center">
                                <a href="#" class="auth-link text-white">Sign in using a different account</a>
                            </div>--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="{{url('/public/media/backend')}}/vendors/base/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="{{url('/public/media/backend')}}/js/jquery-3.4.1.js"></script>
<script src="{{url('/public/media/backend')}}/js/off-canvas.js"></script>
<script src="{{url('/public/media/backend')}}/js/hoverable-collapse.js"></script>
<script src="{{url('/public/media/backend')}}/js/template.js"></script>
<script src="{{url('/public/media/backend')}}/js/jquery.validate.js"></script>

<script>
    $(function () {
        $('#vendor_login_form').validate({
            errorClass: 'text-danger',
            rules:{
                'email':{
                    required:true
                },
                'password':{
                    required:true
                }
            },
            messages:{
                'email':{
                    required:'Please enter your email id.'
                },
                'password':{
                    required:'Please enter your password.'
                }
            },
            submitHandler:function (form) {
                $('#vendor_login_button').attr('disabled',true);
                form.submit();
            }
        });
        
    })
</script>

<!-- endinject -->
</body>

</html>
