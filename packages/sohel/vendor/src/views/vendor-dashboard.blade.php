@extends('layouts.vendor.vendor')
@section('content')
    <div class="db-body">
        <div class="count-list">
            <ul class="list-inline">
                <li>
                    <p><i class="fa fa-user-circle-o"></i> Total Customer</p>
                    <h3>{{$customer_count}}</h3>
                    <p>Click Here</p>
                </li>
                <li>
                    <p><i class="fa fa-users"></i> Total Animals</p>
                    <h3>{{$animal_count}}</h3>
                    <p>Click Here</p>
                </li>
                <li>
                    <p><i class="fa fa-users"></i> Total Breed</p>
                    <h3>{{$breed_count}}</h3>
                    <p>Click Here</p>
                </li>
                <li>
                    <p><i class="fa fa-money"></i> Monthly Revenue Received</p>
                    <h3>{{isset($current_month_earning->earning)?$current_month_earning->earning:'0'}}/-</h3>
                    <p>Click Here</p>
                </li>
                <li>
                    <p><i class="fa fa-money"></i> Monthly Revenue Pending</p>
                    <h3>{{isset($current_month_pending_amount->pending)?$current_month_pending_amount->pending:'0'}}/-</h3>
                    <p>Click Here</p>
                </li>
                <li>
                    <p><i class="fa fa-money"></i> Pending Animal Progress</p>
                    <h3>{{isset($progress_reminder_count)?$progress_reminder_count:'0'}}</h3>
                    <p>Click Here</p>
                </li>
                <li>
                    <p><i class="fa fa-money"></i> Pending Animal Payment</p>
                    <h3>{{isset($pending_payment_count)?$pending_payment_count:'0'}}</h3>
                    <p>Click Here</p>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="box">
                    <div class="box-title">
                        <h2>App Versions</h2>
                    </div>
                    <div class="grap-img">
                        <img src="{{url('/public/media/backend')}}/assets/images/graph.png">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <div class="box">
                    <div class="box-title">
                        <h2>App Versions</h2>
                    </div>
                    <div class="grap-img">
                        <img src="{{url('/public/media/backend')}}/assets/images/graph.png">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="box">
                    <div class="box-title">
                        <h2>App Versions</h2>
                    </div>
                    <div class="grap-img">
                        <img src="{{url('/public/media/backend')}}/assets/images/graph.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>
        var javascript_site_path = '{{url('/')}}';
        $(function () {
            animalOverviewChart();
            animalBreedChart();
        });

        function getRandomColor() {
            var letters = '0123456789ABCDEF'.split('');
            var color = '#';
            for (var i = 0; i < 6; i++ ) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        function animalBreedChart()
        {
            $.ajax({
                url: javascript_site_path+'/fetch/animal/breed/data',
                type: 'POST',
                //dataType: 'JSON',
                success:function (response) {
                    console.log(response.breed);
                    if(response.status == "0")
                    {
                        $("#animal-breed-chart-legend").html('');
                        var animalBreedCanvas = $("#animal-breed-chart").get(0).getContext("2d");
                        var data = {
                            datasets: [{
                                data: response.breed_animal,
                                backgroundColor:[
                                    "rgb(255, 99, 132)",
                                    "rgb(54, 162, 235)",
                                    "rgb(255,255,0)"
                                ],
                                borderWidth: 3,
                                fullWidth:true,
                            }],
                            labels: response.breed,
                        };
                        var options = {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                            },
                            animation: {
                                animation:true,
                                animateRotate: true,
                                rotation: -0.01 * Math.PI,
                                cutoutPercentage: 50,
                            },
                        };
                        var animalBreed = new Chart(animalBreedCanvas, {
                            type: 'doughnut',
                            data: data,
                            options: options
                        });
                    }
                    else
                    {
                        $("#animal-breed-chart-legend").html('<h3>No Data Found...</h3>');
                    }
                },
                beforeSend:function () {
                    $("#animal-breed-chart-legend").html('<i class="fa fa-spinner fa-pulse fa-2x"></i>&nbsp;<h3> Fetching Animal Data</h3>');
                }
            });
        }

        function animalOverviewChart()
        {
            $.ajax({
               url: javascript_site_path+'/fetch/animal/overall/data',
               type: 'POST',
               //dataType: 'JSON',
               success:function (response) {
                    if(response.status == "0")
                    {
                        $("#animal-overview-chart-legend").html('');
                        var animalOverviewCanvas = $("#animal-overview-chart").get(0).getContext("2d");
                        var data = {
                            datasets: [{
                                data: [response.infected_animal,response.fit_animal],
                                backgroundColor:["rgb(255, 99, 132)","rgb(54, 162, 235)"],
                                borderWidth: 3,
                                fullWidth:true,
                            }],
                            labels: ['Animal With Disease','Animal With No Disease'],
                        };
                        var options = {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                            },
                            animation: {
                                animation:true,
                                animateRotate: true,
                                rotation: -0.01 * Math.PI,
                                cutoutPercentage: 50,
                            },
                        };
                        var animalOverview = new Chart(animalOverviewCanvas, {
                            type: 'doughnut',
                            data: data,
                            options: options
                        });
                    }
                    else
                    {
                        $("#animal-overview-chart-legend").html('<h3>No Data Found...</h3>');
                    }
               },
               beforeSend:function () {
                   $("#animal-overview-chart-legend").html('<i class="fa fa-spinner fa-pulse fa-2x"></i>&nbsp;<h3> Fetching Animal Data</h3>');
               }
            });
        }
    </script>
    @endsection