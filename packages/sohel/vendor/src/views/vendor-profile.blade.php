@extends('layouts.vendor.vendor')

@section('content')
    <div class="db-body">
        <div class="inner-forms">
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif
            <div class="box">
                <div class="box-title">
                    <h2>Update Profile</h2>
                </div>
                <div class="add-customer">
                    <form id="update_vendor_profile_form" method="POST" action="" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="name" name="name" placeholder="Farm Name*" class="form-control" value="{{$vendor_profile->name}}">
                                    @error('name')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                                <input type="hidden" name="id" id="id" value="{{$vendor_profile->id}}">
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="first_name" name="first_name" placeholder="First Name*" class="form-control" value="{{$vendor_profile->userInformation->first_name}}">
                                    @error('first_name')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="last_name" name="last_name" placeholder="Last Name*" class="form-control" value="{{$vendor_profile->userInformation->last_name}}">
                                    @error('last_name')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="email" name="email" placeholder="Email Id*" class="form-control" value="{{$vendor_profile->email}}">
                                    @error('email')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="mobile_number" name="mobile_number" placeholder="Mobile Number*" class="form-control" value="{{$vendor_profile->userInformation->mobile_number}}">
                                    @error('mobile_number')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="file" id="profile_image" name="profile_image" accept=".jpeg, .jpg, .png" class="form-control">
                                    <br>
                                    <img id="preview_profile_image" height="50px" width="50px" @if(isset($vendor_profile->userInformation->profile_image)) src="{{url('/public/farm/'.$vendor_profile->userInformation->profile_image)}}" @else src="{{url('/public/media/backend/images/no-image.jpg')}}" @endif>
                                    @error('profile_image')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        {{--<div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="animal_type" name="animal_type">
                                        <option value="">Select Animal Type</option>
                                        <option value="0">Goat</option>
                                        <option value="1">Sheep</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative ">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control">
                                        <option value="">Select Owner</option>
                                        <option value="">Wasim Momin</option>
                                        <option value="">Imran Popatiya</option>
                                        <option value="">Sohail Patel</option>
                                    </select>
                                </div>
                            </div>
                        </div>--}}
                        {{--<div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" name="" placeholder="Animal Name" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative ">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="gender" name="gender">
                                        <option value="">Select Animal Gender</option>
                                        <option value="0">Andul</option>
                                        <option value="1">Khassi</option>
                                        <option value="2">Female</option>
                                    </select>
                                </div>
                            </div>
                        </div>--}}
                        {{--<div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" name="" placeholder="Animal Age In Months*" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="date" name="" placeholder="Animal Date Of Birth" class="form-control">
                                </div>
                            </div>
                        </div>--}}
                        {{--<div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" name="" placeholder="Animal Weight (In Kgs)*" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" name="" placeholder="Animal Height (In Inch)*" class="form-control">
                                </div>
                            </div>
                        </div>--}}
                        {{--<div class="row">
                            <div class="col-sm-12 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <div class="relative">
                                        <input type="checkbox" name="" class="">
                                        <label for="ch-1">Is payement done</label>
                                    </div>
                                </div>
                            </div>
                        </div>--}}
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li><button type="submit" class="btn cust-btn btn-save" id="vendor_profile_submit_btn"><i id="vendor_profile_submit_icon" style="font-size:15px"></i> Save</button></li>
                                        <li><button type="button" class="btn cust-btn btn-grey" id="reset_vendor_profile_form">Cancel</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $('#update_vendor_profile_form').validate({
            errorClass: 'text-danger',
            rules:{
                'name':{
                    required:true,
                },
                'first_name':{
                    required:true,
                },
                'last_name':{
                    required:true,
                },
                'email':{
                    required:true,
                    remote:{
                        url: javascript_site_path+"/vendor-check-mail-duplication",
                        type: "post",
                        data:{
                            id: $('#id').val(),
                        }
                    },
                },
                'mobile_number':{
                    required:true,
                }
            },
            messages:{
                'name':{
                    required:"Please enter your farm name."
                },
                'first_name':{
                    required:"Please enter your first name."
                },
                'last_name':{
                    required:"Please enter your last name."
                },
                'email':{
                    required:"Please enter your email id.",
                    remote:"Email already exist."
                },
                'mobile_number':{
                    required:"Please enter your mobile number."
                }
            },
            submitHandler:function (form) {
                $('#vendor_profile_submit_icon').addClass("fa fa-spinner fa-pulse");
                $('#vendor_profile_submit_btn').attr('disabled',true);
                form.submit();
            }
        });

        $(function () {

            $('#profile_image').change(function () {
                var uploaded_image = window.URL.createObjectURL(this.files[0]);
                $('#preview_profile_image').attr('src',uploaded_image);
            });

            $('#reset_vendor_profile_form').click(function () {
                $('#update_vendor_profile_form')[0].reset();
            })
        })

    </script>
@endsection