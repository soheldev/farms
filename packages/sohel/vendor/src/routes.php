<?php
//Route::get('/vendor/login','Sohel\Vendor\VendorController@viewVendorLogin')->middleware(['web']);
Route::get('/vendor/dashboard','Sohel\Vendor\VendorController@viewVendorDashboard')->middleware(['web','auth','check.connection']);
Route::get('/vendor/logout','Sohel\Vendor\VendorController@vendorLogout')->middleware(['web','check.connection']);
Route::get('/vendor/profile','Sohel\Vendor\VendorController@vendorProfile')->middleware(['web','auth','check.connection']);
Route::post('/vendor/profile','Sohel\Vendor\VendorController@vendorProfile')->middleware(['web','auth','check.connection']);
Route::post('/vendor-check-mail-duplication','Sohel\Vendor\VendorController@checkVendorMailDuplication')->middleware(['web','auth','check.connection']);
Route::post('/vendor/update/profile','Sohel\Vendor\VendorController@updateVendorProfile')->middleware(['web','auth','check.connection']);


Route::post('/fetch/animal/overall/data','Sohel\Vendor\VendorController@animalOverallData')->middleware(['web','auth','check.connection']);
Route::post('/fetch/animal/breed/data','Sohel\Vendor\VendorController@animalBreedData')->middleware(['web','auth','check.connection']);


