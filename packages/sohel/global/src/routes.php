<?php
Route::get('/admin/setting/global','Sohel\Globall\GlobalController@index')->middleware(['web','auth']);
Route::get('/admin/setting/global/add','Sohel\Globall\GlobalController@addGlobalValues')->middleware(['web','auth']);
Route::post('/admin/setting/global/add','Sohel\Globall\GlobalController@addGlobalValues')->middleware(['web','auth']);
Route::post('/fetch/global-value-data','Sohel\Globall\GlobalController@fetchData')->middleware(['web','auth']);
Route::get('/admin/setting/global/edit/{id}','Sohel\Globall\GlobalController@editGlobalValues')->middleware(['web','auth']);
Route::post('/admin/setting/global/edit/{id}','Sohel\Globall\GlobalController@editGlobalValues')->middleware(['web','auth']);
