@extends('layouts.admin.admin')

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-muted mb-0 hover-cursor" href="{{url('/admin/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-primary mb-0 hover-cursor">Update Global Values</p>
                        </div>
                        <br>
                        <h4 class="card-title">Update Global Values</h4>
                        {{--<p class="card-description">
                            Basic form elements
                        </p>--}}
                        <form class="forms-sample" id="update_global_value_form" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputName1">{{$edit_global_value->title}}<sup style="color: red">*</sup></label>
                                @if($edit_global_value->is_image == '0')
                                    <input type="text" class="form-control" id="value" name="value" placeholder="Enter Title" value="{{$edit_global_value->value}}">
                                @else
                                    <input type="file" class="form-control browse_image" id="value" name="value" accept=".jpeg, .jpg, .png">
                                    <br>
                                    <label>Preview</label><br>
                                    <img id="preview_image" height="100px" width="100px" src="{{url('/public/global/'.$edit_global_value->value)}}">
                                @endif
                                @error('value')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary mr-2" id="global_update_btn"><i id="global_update_loder" style="font-size:15px"></i>  Submit</button>
                            <button class="btn btn-light" id="reset_global_value_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        var flag = '{{$edit_global_value->is_image}}';
        console.log(flag);
        $(function () {

            $('.browse_image').change(function () {
                var uploaded_image = window.URL.createObjectURL(this.files[0]);
                $('#preview_image').attr('src',uploaded_image);

            });

            $('#update_global_value_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'value':{
                        required:true,
                    }
                },
                messages:{
                    'value':{
                        required:function () {
                            if(flag == 0)
                            {
                                return "Please enter vlaue."
                            }
                            else
                            {
                                return "Please select image."
                            }
                        }
                    },
                },
                submitHandler:function (form) {
                    $('#global_update_loder').addClass("fa fa-spinner fa-pulse");
                    $('#global_update_btn').attr('disabled',true);
                    form.submit();
                }
            });

        })

    </script>
@endsection