@extends('layouts.admin.admin')
@section('content')
    <style>
        .my-custom-scrollbar {
            position: relative;
            height: 400px;
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>

    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-muted mb-0 hover-cursor" href="{{url('/admin/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-primary mb-0 hover-cursor">List Global Values</p>
                        </div>
                        <br>
                        <h4 class="card-title">List Global Values</h4>
                        {{--<p class="card-description">
                            Add class <code>.table-bordered</code>
                        </p>--}}
                        <div class="pull-right">
                            <a type="button" class="btn btn-success btn-rounded" href="{{url('/admin/setting/global/add')}}">Add New</a>
                        </div>
                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Value</th>
                                    <th>Update</th>
                                </tr>
                                </thead>

                                <tbody id="global_value_table">
                                </tbody>

                                                                    {{--<td>1</td>
                                    <td>Herman Beck</td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 25%"
                                                 aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-rounded">Primary</button>
                                    </td>--}}

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('jcontent')
    <script>
        var site_path = '{{url('/')}}';
        var skip = 0;
        var i = 0;
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/global-value-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {
                            html_content += '<tr>';
                            html_content += '<td>'+this.id+'</td>';
                            html_content += '<td>'+this.title+'</td>';
                            if(this.value.includes('.png') || this.value.includes('.jpg'))
                            {
                                html_content += '<td><img src="'+site_path+'/public/global/'+this.value+'"></td>';
                            }
                            else
                            {
                                html_content += '<td>'+this.value+'</td>';
                            }
                            html_content += '<td><a type="button" href="'+site_path+'/admin/setting/global/edit/'+this.id+'" class="btn btn-primary btn-rounded">Update</a></td>';
                            html_content += '</tr>';
                        });

                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="4"><center>No Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#global_value_table').append(html_content);
                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 5;
                    fetchRecord(skip);
                }
            };
        });
    </script>
@endsection