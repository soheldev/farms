@extends('layouts.admin.admin')

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-muted mb-0 hover-cursor" href="{{url('/admin/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-primary mb-0 hover-cursor">Add Global Values</p>
                        </div>
                        <br>
                        <h4 class="card-title">Add Global Values</h4>
                        {{--<p class="card-description">
                            Basic form elements
                        </p>--}}
                        <form class="forms-sample" id="add_global_value_form" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputName1">Title<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="">
                                @error('title')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail3">Slug<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug" value="" readonly>
                                @error('slug')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword4">Value<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="value" name="value" placeholder="Enter Value" value="">
                                @error('value')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword4">Image<sup style="color: red">*</sup></label>
                                <input type="file" class="form-control" id="image" name="image">
                                @error('image')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            {{--<div class="form-group">
                                <label for="exampleInputPassword4">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword4" placeholder="Password">
                            </div>--}}
                            {{--<div class="form-group">
                                <label for="exampleSelectGender">Gender</label>
                                <select class="form-control" id="exampleSelectGender">
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>
                            </div>--}}
                            {{--<div class="form-group">
                                <label>File upload</label>
                                <input type="file" name="img[]" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                                    <span class="input-group-append">
                          <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                                </div>
                            </div>--}}
                            {{--<div class="form-group">
                                <label for="exampleInputCity1">City</label>
                                <input type="text" class="form-control" id="exampleInputCity1" placeholder="Location">
                            </div>--}}
                            {{--<div class="form-group">
                                <label for="exampleTextarea1">Textarea</label>
                                <textarea class="form-control" id="exampleTextarea1" rows="4"></textarea>
                            </div>--}}
                            <button type="submit" class="btn btn-primary mr-2" id="global_submit_btn"><i id="global_submit_loder" style="font-size:15px"></i>  Submit</button>
                            <button class="btn btn-light" id="reset_global_value_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function () {
            $('#reset_global_value_form').click(function () {
                $('#add_global_value_form')[0].reset();
            });

            $('#title').keyup(function () {
                var str = $(this).val();
                $('#slug').val(str.toLowerCase().replace(/\s/g, "-"));
            });

            $('#add_global_value_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'title':{
                        required:true,
                    },
                    'slug':{
                        required:true,
                    },
                    /*'value':{
                        required:true,
                    }*/
                },
                messages:{
                    'title':{
                        required:"Please enter title."
                    },
                    'slug':{
                        required:"Please enter slug.",
                    },
                    /*'value':{
                        required:"Please enter vlaue."
                    }*/
                },
                submitHandler:function (form) {
                    $('#global_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#global_submit_btn').attr('disabled',true);
                    form.submit();
                    /*$('#admin_profile_submit_icon').addClass('fa fa-spinner fa-pulse');
                    var form = $('#update_admin_profile_form')[0];
                    console.log(1111);
                    var data = new FormData(form);
                    $.ajax({
                        url: javascript_site_path+'/admin/update/profile',
                        type: 'POST',
                        dataType: 'JSON',
                        processData: false,
                        contentType: false,
                        cache: false,
                        enctype: 'multipart/form-data',
                        data: data,
                        success:function(response){

                        },
                        beforeSend:function () {

                        },
                        complete:function () {

                        },
                        error:function () {

                        }
                    });*/

                }
            });

        })

    </script>
@endsection