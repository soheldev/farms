<?php

namespace Sohel\Globall;

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Sohel\Globall\Model\GlobalValue;

class GlobalController extends Controller
{
    //
    public function index()
    {
        return view('globall::global-list');
    }

    public function addGlobalValues(Request $request)
    {
        if($request->method() == "GET")
        {
            return view('globall::global-add');
        }
        else
        {
            $create_global_value = new GlobalValue();
            if($request->hasFile('image'))
            {
                if (! \File::exists(public_path()."/global")) {
                    \File::makeDirectory(public_path()."/global");
                }
                $imageName = time().'.'.request()->image->getClientOriginalExtension();
                request()->image->move(public_path('/global/'), $imageName);
                $create_global_value->value = $imageName;

            }
            else
            {
                $create_global_value->value = $request->value;
            }
            $create_global_value->title = $request->title;
            $create_global_value->slug = $request->slug;
            $create_global_value->save();
            return redirect('/admin/setting/global');
        }
    }

    public function fetchData(Request $request)
    {
        $data = GlobalValue::skip($request->skip)->limit(5)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function editGlobalValues(Request $request,$id)
    {
        $edit_global_value = GlobalValue::find($id);
        if($request->method() == 'GET')
        {
            return view('globall::global-edit',compact('edit_global_value'));
        }
        else
        {
            if($request->hasFile('value'))
            {
                $old_image = $edit_global_value->value;
                $imageName = time().'.'.request()->value->getClientOriginalExtension();
                request()->value->move(public_path('/global/'), $imageName);
                $edit_global_value->value = $imageName;
                unlink(public_path('/global/'.$old_image));
            }
            else
            {
                $edit_global_value->value = $request->value;
            }
            $edit_global_value->save();
            return redirect('/admin/setting/global')->with('success','Record updated successfully');
        }
    }

}
