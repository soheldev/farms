<?php

namespace Sohel\Globall\Model;

use Illuminate\Database\Eloquent\Model;

class GlobalValue extends Model
{
    //
    protected $table = 'global_values';
}
