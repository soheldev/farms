<?php

namespace Sohel\Animal\Model;

use Illuminate\Database\Eloquent\Model;

class CustomerAnimalExit extends Model
{
    //
    protected $table = 'customer_animal_exists';
    protected $fillable = ['animal_id','customer_id','animal_entry_date','animal_entry_weight','animal_entry_height','animal_exit_date','animal_exit_weight','animal_exit_height','exit_reason','animal_weight_gain','animal_height_gain','notes'];
    //protected $with = ['animalvacination'];

    /*public function animalvacination()
    {
        return $this->belongsTo('Sohel\Vaccine\Model\Vaccine','vaccine_id','id');
    }*/

    /*public function animalOwner()
    {
        return $this->belongsTo('Sohel\User\Model\Customer','customer_id','id');
    }*/
}
