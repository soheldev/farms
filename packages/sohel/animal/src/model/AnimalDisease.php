<?php

namespace Sohel\Animal\Model;

use Illuminate\Database\Eloquent\Model;

class AnimalDisease extends Model
{
    //
    protected $table = 'animal_diseases';
    protected $fillable = ['animal_id','disease_id','diagnosed_date','treatment','recovered_date','notes'];
    protected $with = ['animaldisease'];

    public function animaldisease()
    {
        return $this->belongsTo('Sohel\Disease\Model\Disease','disease_id','id');
    }

    /*public function animalOwner()
    {
        return $this->belongsTo('Sohel\User\Model\Customer','customer_id','id');
    }*/
}
