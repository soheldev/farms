<?php

namespace Sohel\Animal\Model;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    //
    protected $table = 'animals';
    protected $fillable = ['prefix','animal_id','animal_type','animal_name','breed_id','gender','customer_id','age_in_month','date_of_birth','is_owned_by_farm','notes','entry_date','entry_weight','entry_height','added_by','monthly_rent'];
    protected $with = ['animalBreed'];

    public function animalBreed()
    {
        return $this->belongsTo('Sohel\Breed\Model\Breed','breed_id','id');
    }

    public function animalOwner()
    {
        return $this->belongsTo('Sohel\User\Model\Customer','customer_id','customer_id');
    }
}
