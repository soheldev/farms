<?php

namespace Sohel\Animal\Model;

use Illuminate\Database\Eloquent\Model;

class AnimalVacination extends Model
{
    //
    protected $table = 'animal_vacinations';
    protected $fillable = ['animal_id','vaccine_id','dose_number','vacination_date','weight_on_vacination_day','quantity','notes'];
    protected $with = ['animalvacination'];

    public function animalvacination()
    {
        return $this->belongsTo('Sohel\Vaccine\Model\Vaccine','vaccine_id','id');
    }

    /*public function animalOwner()
    {
        return $this->belongsTo('Sohel\User\Model\Customer','customer_id','id');
    }*/
}
