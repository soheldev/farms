<?php

namespace Sohel\Animal\Model;

use Illuminate\Database\Eloquent\Model;

class AnimalDeworming extends Model
{
    //
    protected $table = 'animal_dewormings';
    protected $fillable = ['animal_id','deworming_id','predefine_body_weight','predefine_dosage','actual_body_weight','actual_dosage','due_date','deworming_date','deworming_status','notes','added_by'];
    protected $with = ['animalDeworming'];

    public function animalDeworming()
    {
        return $this->belongsTo('Sohel\Deworming\Model\Deworming','deworming_id','id');
    }

    /*public function animalOwner()
    {
        return $this->belongsTo('Sohel\User\Model\Customer','customer_id','id');
    }*/
}
