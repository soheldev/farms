@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Edit Animal Vaccination</h2>
                </div>
                <div class="add-customer">
                    <form id="edit_animal_vacination_form" method="POST" action="" enctype="multipart/form-data" autocomplete="off">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="vaccine_id" name="vaccine_id">
                                        <option value="">Select Vaccination*</option>
                                        @foreach($vacinations as $vacine)
                                            <option value="{{$vacine->id}}" @if($vacine->id == $edit_animal_vacination->vaccine_id) selected @endif>{{$vacine->vaccine_name}}</option>
                                        @endforeach
                                    </select>
                                    @error('vaccine_id')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="dose_number" name="dose_number"
                                           placeholder="Enter Dose Number*" class="form-control" value="{{$edit_animal_vacination->dose_number}}">
                                    @error('dose_number')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="vacination_date" name="vacination_date"
                                           placeholder="Select Vaccination Date*" class="form-control" value="{{$edit_animal_vacination->vacination_date}}">
                                    @error('vacination_date')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="weight_on_vacination_day" name="weight_on_vacination_day" placeholder="Enter Weight On Vaccination Day*"
                                           class="form-control" value="{{$edit_animal_vacination->weight_on_vacination_day}}">
                                    @error('weight_on_vacination_day')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="quantity" name="quantity"
                                           placeholder="Enter Quantity In ML" class="form-control" value="{{$edit_animal_vacination->quantity}}">
                                    @error('quantity')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <textarea class="form-control" id="notes" name="notes"
                                          placeholder="Enter Notes" value="" rows="5">{{$edit_animal_vacination->notes}}</textarea>
                                @error('notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li>
                                            <button type="submit" class="btn cust-btn btn-save"
                                                    id="edit_animal_vacination_submit_btn"><i id="edit_animal_vacination_submit_loder"
                                                                                         style="font-size:15px"></i> Save
                                            </button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn cust-btn btn-grey"
                                                    id="reset_edit_animal_vacination_form">Cancel
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function ()
        {
            $( "#vacination_date" ).datepicker({
                dateFormat : 'd/m/yy',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });

            $('#reset_edit_animal_vacination_form').click(function () {
                $('#edit_animal_vacination_form')[0].reset();
            });

            $("#dose_number").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#weight_on_vacination_day").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#quantity").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });




            $('#edit_animal_vacination_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'vaccine_id':{
                        required:true,
                    },
                    'dose_number':{
                        required:true,
                    },
                    'vacination_date':{
                        required:true,
                    },
                    'weight_on_vacination_day':{
                        required:true,
                    },
                    'quantity':{
                        required:true,
                    }
                },
                messages:{
                    'vaccine_id':{
                        required:"Please select vacination.",
                    },
                    'dose_number':{
                        required:"Please enter dose number.",
                    },
                    'vacination_date':{
                        required:"Please select vacination date.",
                    },
                    'weight_on_vacination_day':{
                        required:"Please enter weight of animal on vacination day",
                    },
                    'quantity':{
                        required:"Please enter quantity in ml.",
                    },
                },
                submitHandler:function (form) {
                    $('#edit_animal_vacination_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#edit_animal_vacination_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

        });


    </script>
@endsection