@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Edit Animal Disease</h2>
                </div>
                <div class="add-customer">
                    <form id="edit_animal_disease_form" method="POST" action="" enctype="multipart/form-data" autocomplete="off">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="disease_id" name="disease_id">
                                        <option value="">Select Disease*</option>
                                        @foreach($diseases as $disease)
                                            <option value="{{$disease->id}}" @if($disease->id == $edit_animal_disease->disease_id) selected @endif>{{$disease->disease_name}}</option>
                                        @endforeach
                                    </select>
                                    @error('disease_id')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="diagnosed_date" name="diagnosed_date"
                                           placeholder="Select Diagnosed Date*" class="form-control" value="{{$edit_animal_disease->diagnosed_date}}">
                                    @error('diagnosed_date')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                    <textarea class="form-control" id="treatment" name="treatment"
                                              placeholder="Enter Treatment" value="" rows="5">{{$edit_animal_disease->treatment}}</textarea>
                                @error('treatment')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="recovered_date" name="recovered_date"
                                           placeholder="Select Recovered Date*" class="form-control" value="{{$edit_animal_disease->recovered_date}}">
                                    @error('recovered_date')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                    <textarea class="form-control" id="notes" name="notes"
                                              placeholder="Enter Notes" value="" rows="5">{{$edit_animal_disease->notes}}</textarea>
                                @error('notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li>
                                            <button type="submit" class="btn cust-btn btn-save"
                                                    id="edit_animal_disease_submit_btn"><i id="edit_animal_disease_submit_loder"
                                                                                      style="font-size:15px"></i> Save
                                            </button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn cust-btn btn-grey"
                                                    id="reset_edit_animal_disease_form">Cancel
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function ()
        {
            $( "#diagnosed_date" ).datepicker({
                dateFormat : 'd/m/yy',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });

            $( "#recovered_date" ).datepicker({
                dateFormat : 'd/m/yy',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });

            $('#reset_edit_animal_disease_form').click(function () {
                $('#edit_animal_disease_form')[0].reset();
            });


            $('#edit_animal_disease_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'disease_id':{
                        required:true,
                    },
                    'diagnosed_date':{
                        required:true,
                    },
                    'treatment':{
                        required:true,
                    },
                    /*'recovered_date':{
                        required:true,
                    },*/
                },
                messages:{
                    'disease_id':{
                        required:"Please select disease.",
                    },
                    'diagnosed_date':{
                        required:"Please select diagnosed date.",
                    },
                    'treatment':{
                        required:"Please enter treatment.",
                    },
                    /*'recovered_date':{
                        required:"Please select recovered date.",
                    },*/
                },
                submitHandler:function (form) {
                    $('#edit_animal_disease_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#edit_animal_disease_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

        });


    </script>
@endsection