@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/animal/list')}}">&nbsp;Animal List&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/animal/vacination/'.Request::segment(5))}}">&nbsp;Animal Vaccination List&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Add Animal Vaccination</p>
                        </div>
                        <br>

                        <form class="forms-sample" id="add_animal_vacination_form" method="POST" action="" enctype="multipart/form-data">

                            <input type="hidden" name="animal_id" id="animal_id" value="{{$id}}">
                            <div class="form-group">
                                <label for="exampleInputName1">Vaccination<sup style="color: red">*</sup></label>
                                <select class="form-control" id="vaccine_id" name="vaccine_id">
                                    <option value="">Select Vaccination</option>
                                    @foreach($vacinations as $vacine)
                                        <option value="{{$vacine->id}}">{{$vacine->vaccine_name}}</option>
                                    @endforeach
                                </select>
                                @error('vaccine_id')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Dose Number<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="dose_number" name="dose_number" placeholder="Enter Dose Number" value="">
                                @error('dose_number')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Vaccination Date<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="vacination_date" name="vacination_date" placeholder="Select Vaccination Date" value="">
                                @error('vacination_date')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Weight Vaccination Day<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="weight_on_vacination_day" name="weight_on_vacination_day" placeholder="Enter Weight On Vaccination Day" value="">
                                @error('weight_on_vacination_day')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Quantity (In ML)<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Enter Quantity In ML" value="">
                                @error('quantity')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Notes</label>
                                <textarea class="form-control" id="notes" name="notes" placeholder="Enter Notes" value="" rows="5"></textarea>
                                @error('notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>


                            <button type="submit" class="btn btn-primary mr-2" id="animal_vacination_submit_btn"><i id="animal_vacination_submit_loder" style="font-size:15px"></i>  Submit</button>
                            <button class="btn btn-light" id="reset_animal_vacination_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function ()
        {
            $( "#vacination_date" ).datepicker({
                dateFormat : 'd/m/yy',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });

            $('#reset_animal_vacination_form').click(function () {
                $('#add_animal_vacination_form')[0].reset();
            });

            $("#dose_number").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#weight_on_vacination_day").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#quantity").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });




            $('#add_animal_vacination_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'vaccine_id':{
                        required:true,
                    },
                    'dose_number':{
                        required:true,
                    },
                    'vacination_date':{
                        required:true,
                    },
                    'weight_on_vacination_day':{
                        required:true,
                    },
                    'quantity':{
                        required:true,
                    }
                },
                messages:{
                    'vaccine_id':{
                        required:"Please select vacination.",
                    },
                    'dose_number':{
                        required:"Please enter dose number.",
                    },
                    'vacination_date':{
                        required:"Please select vacination date.",
                    },
                    'weight_on_vacination_day':{
                        required:"Please enter weight of animal on vacination day",
                    },
                    'quantity':{
                        required:"Please enter quantity in ml.",
                    },
                },
                submitHandler:function (form) {
                    $('#animal_vacination_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#animal_vacination_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

        });


    </script>
@endsection