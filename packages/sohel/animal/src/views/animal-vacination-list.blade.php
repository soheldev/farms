@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)
@section('content')
    <div class="db-body">
        <div class="inner-forms">
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif
            <div class="box ">
                <div class="box-title clearfix">
                    <h2 class="pull-left">List Vaccination Detail Of #{{$animal_detail->prefix.$animal_detail->animal_id}}</h2>
                    @if($animal_detail->status == '1')
                        @if($add_animal_vaccination_permission == true)
                            <div class="top-btn pull-right">
                                <a class="btn cust-btn btn-blue-1" href="{{url($user.'/animal/vacination/add/'.Request::segment(4))}}"><i class="fa fa-plus"></i> Add New Vaccination</a>
                            </div>
                        @endif
                    @endif
                </div>

                <div class="table-resposnive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Vaccine Id</th>
                            <th>Vaccine Name</th>
                            <th>Dose Number</th>
                            <th>Vaccination Date</th>
                            <th>Weight On Vaccination Day</th>
                            <th>Quantity</th>
                            @if($animal_detail->status == '1')
                                @if($update_animal_vaccination_permission == true || $delete_animal_vaccination_permission == true)
                                    <th>Action</th>
                                @endif
                            @endif
                        </tr>
                        </thead>
                        <tbody id="animal_vacination_table">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('jcontent')
    <script>
        var site_path = '{{url('/')}}';
        var user = '{{$user}}';
        var animal_id = '{{Request::segment(4)}}';
        var animal_status = '{{$animal_detail->status}}';
        var skip = 0;
        var i = 0;
        var update_animal_vaccination_permission = '{{$update_animal_vaccination_permission}}';
        var delete_animal_vaccination_permission = '{{$delete_animal_vaccination_permission}}';
        var search_value = '';
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/animal-vaccine-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip,
                    animal_id:animal_id
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td>'+this.animalvacination.vaccine_id+'</td>';
                            html_content += '<td>'+this.animalvacination.vaccine_name+'</td>';
                            if(this.dose_number != null && this.dose_number != '')
                            {
                                html_content += '<td>'+this.dose_number+'</td>';
                            }
                            else
                            {
                                html_content += '<td>N/A</td>';
                            }

                            if(this.vacination_date != null && this.vacination_date != '')
                            {
                                html_content += '<td>'+this.vacination_date+'</td>';
                            }
                            else
                            {
                                html_content += '<td>N/A</td>';
                            }

                            if(this.weight_on_vacination_day != null && this.weight_on_vacination_day != '')
                            {
                                html_content += '<td>'+this.weight_on_vacination_day+'</td>';
                            }
                            else
                            {
                                html_content += '<td>N/A</td>';
                            }

                            if(this.quantity != null && this.quantity != '')
                            {
                                html_content += '<td>'+this.quantity+'</td>';
                            }
                            else
                            {
                                html_content += '<td>N/A</td>';
                            }
                            if(animal_status == '1')
                            {
                                if(update_animal_vaccination_permission == true || delete_animal_vaccination_permission == true)
                                {
                                    html_content += '<td>';
                                    html_content += '<ul class="list-inline">';
                                    if(update_animal_vaccination_permission == true)
                                    {
                                        html_content += '<li><a title="Edit" href="'+site_path+''+user+'/animal/vacination/edit/'+animal_id+'/'+btoa(this.id)+'" class="action-btn btn-yellow edit" data-attribute-id="' + this.id + '"><i class="fa fa-pencil "></i></a></li>';
                                    }
                                    if(delete_animal_vaccination_permission == true)
                                    {
                                        html_content += '<li><a title="Delete" href="javascript:void(0)" class="action-btn btn-red animal_vacination_delete" data-attribute-id="' + this.id + '"><i class="fa fa-trash"></i></a></li>';
                                    }
                                    html_content += '</ul>';
                                    html_content += '</td>';
                                }

                            }

                            //html_content += '<td><center><a title="Edit" href="'+site_path+''+user+'/animal/vacination/edit/'+animal_id+'/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; <a title="delete" href="javascript:void(0)" class="animal_vacination_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></center></td>';
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="7"><center>No Vaccination Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    console.log(html_content);
                    $('#animal_vacination_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 5;
                    fetchRecord(skip);
                }
            };


            $('body').on('click','.animal_vacination_delete',function () {
                var id = btoa($(this).attr('data-attribute-id'));
                var _this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to delete this animal vacination record.!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#71c016',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function(result){
                    if (result.value) {

                        $.ajax({
                            url: site_path + '/delete/animal/vacination',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                id: id,
                            },
                            success: function (response) {
                                if(response.status == '1')
                                {
                                    _this.parent('li').parent('ul').parent('td').parent('tr').remove();
                                    if($('#animal_vacination_table tr').length == 0)
                                    {
                                        $('#animal_vacination_table').append('<tr><td colspan="7" class="no_record_found_row"><center>No Record Found...</center></td></tr>');
                                    }
                                }
                                Swal.fire({
                                    icon: response.icon,
                                    title: response.title,
                                    text: response.text,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            },
                            beforeSend: function () {

                            },
                            complete: function () {

                            },
                            error: function () {

                            }
                        });
                    }
                })
            });

            function searchRecord(skip,search_value)
            {
                $.ajax({
                    url: site_path+'/fetch/animal-data',
                    type: 'POST',
                    dataType: 'JSON',
                    data:{
                        skip:skip,
                        search_value:search_value
                    },
                    success:function (response) {
                        console.log(response);
                        var html_content = '';
                        if(response.status == "1")
                        {
                            $.each(response.data,function (index,value) {

                                html_content += '<tr>';
                                html_content += '<td>'+this.vaccine_id+'</td>';
                                html_content += '<td>'+this.vaccine_id+'</td>';
                                html_content += '<td>'+this.dose_number+'</td>';
                                html_content += '<td>'+this.vacination_date+'</td>';
                                html_content += '<td>'+this.weight_on_vacination_day+'</td>';
                                html_content += '<td>'+this.quantity+'</td>';
                                html_content += '<td><center><a title="Edit" href="'+site_path+''+user+'/animal/vacination/edit/'+animal_id+'/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; <a title="delete" href="javascript:void(0)" class="animal_vacination_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></center></td>';
                                html_content += '</tr>';
                            });
                        }
                        else
                        {
                            html_content += '<tr><td colspan="7"><center>No Vaccination Record Found...</center></td></tr>';
                        }
                        $('#animal_vacination_table').html(html_content);

                    },
                    beforeSend:function () {
                        $('#animal_vacination_table').html('<tr><td colspan="6"><center><i class="fa fa-spinner fa-pulse fa-2x"><i/></center></td></tr>');
                    },
                    complete:function(){

                    },
                    error:function(){

                    }
                });
            }

            /*$('#search_text').keyup(function () {
                search_value = $(this).val();
                skip = 0;
                searchRecord(skip,search_value);
            });*/


        });
    </script>
@endsection