@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/animal/list')}}">&nbsp;Animal List&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/animal/disease/'.Request::segment(5))}}">&nbsp;Animal Disease List&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Add Animal Disease</p>
                        </div>
                        <br>

                        <form class="forms-sample" id="add_animal_disease_form" method="POST" action="" enctype="multipart/form-data">

                            <input type="hidden" name="animal_id" id="animal_id" value="{{$id}}">
                            <div class="form-group">
                                <label for="exampleInputName1">Disease<sup style="color: red">*</sup></label>
                                <select class="form-control" id="disease_id" name="disease_id">
                                    <option value="">Select Disease</option>
                                    @foreach($diseases as $disease)
                                        <option value="{{$disease->id}}">{{$disease->disease_name}}</option>
                                    @endforeach
                                </select>
                                @error('disease_id')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Diagnosed Date<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="diagnosed_date" name="diagnosed_date" placeholder="Select Diagnosed Date" value="">
                                @error('diagnosed_date')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Treatment</label>
                                <textarea class="form-control" id="treatment" name="treatment" placeholder="Enter Treatment" value="" rows="5"></textarea>
                                @error('treatment')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Recovered Date<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="recovered_date" name="recovered_date" placeholder="Select Recovered Date" value="">
                                @error('recovered_date')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Notes</label>
                                <textarea class="form-control" id="notes" name="notes" placeholder="Enter Notes" value="" rows="5"></textarea>
                                @error('notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>


                            <button type="submit" class="btn btn-primary mr-2" id="animal_disease_submit_btn"><i id="animal_disease_submit_loder" style="font-size:15px"></i>  Submit</button>
                            <button class="btn btn-light" id="reset_animal_disease_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function ()
        {
            $( "#diagnosed_date" ).datepicker({
                dateFormat : 'd/m/yy',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });

            $( "#recovered_date" ).datepicker({
                dateFormat : 'd/m/yy',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });

            $('#reset_animal_disease_form').click(function () {
                $('#add_animal_disease_form')[0].reset();
            });

            $('#add_animal_disease_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'disease_id':{
                        required:true,
                    },
                    'diagnosed_date':{
                        required:true,
                    },
                    'treatment':{
                        required:true,
                    },
                    /*'recovered_date':{
                        required:true,
                    },*/
                },
                messages:{
                    'disease_id':{
                        required:"Please select disease.",
                    },
                    'diagnosed_date':{
                        required:"Please select diagnosed date.",
                    },
                    'treatment':{
                        required:"Please enter treatment.",
                    },
                    /*'recovered_date':{
                        required:"Please select recovered date.",
                    },*/
                },
                submitHandler:function (form) {
                    $('#animal_disease_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#animal_disease_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

        });


    </script>
@endsection