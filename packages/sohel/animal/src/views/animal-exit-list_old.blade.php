@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)
@section('content')
    <style>
        .my-custom-scrollbar {
            position: relative;
            height: 400px;
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">List Exited Animal</p>
                        </div>
                        <br>
                        {{--<div class="pull-right">
                            <a type="button" class="btn btn-success btn-rounded" href="{{url($user.'/animal/add')}}">Add New</a>
                        </div>--}}

                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>Animal Id</th>
                                    <th>Animal Type</th>
                                    <th>Animal Name</th>
                                    <th>Animal Breed</th>
                                    <th>Animal Gender</th>
                                    <th>Vaccination</th>
                                    <th>Disease</th>
                                </tr>
                                </thead>

                                <tbody id="animal_exit_table">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('jcontent')
    <script>
        var site_path = '{{url('/')}}';
        var user = '{{$user}}';
        var skip = 0;
        var i = 0;
        var search_value = '';
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/exit-animal-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip,
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td><a href="'+site_path+''+user+'/animal/view/'+btoa(this.id)+'">'+this.animal_unique_id+'</a></td>';
                            if(this.animal_type == '0')
                            {
                                html_content += '<td>Goat</td>';
                            }
                            else
                            {
                                html_content += '<td>Sheep</td>';
                            }
                            html_content += '<td>'+this.animal_name+'</td>';
                            html_content += '<td>'+this.breed_name+'</td>';
                            if(this.gender == '0')
                            {
                                html_content += '<td>Andul</td>';
                            }
                            else if(this.gender == '1')
                            {
                                html_content += '<td>Khassi</td>';
                            }
                            else
                            {
                                html_content += '<td>Female</td>';
                            }
                            html_content += '<td><a href="javascript:void(0);" title="Vaccination History" data-attribute-animal="'+btoa(this.id)+'" class="vacination_btn">View Vaccination</a></td>';
                            html_content += '<td><a href="javascript:void(0);" title="Disease History" data-attribute-animal="'+btoa(this.id)+'" class="disease_btn">View Disease</a></td>';
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="7"><center>No Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#animal_exit_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 5;
                    fetchRecord(skip);
                }
            };

            $('body').on('click','.vacination_btn',function () {
                var animal_id = $(this).attr('data-attribute-animal');
                var redirect_vacination_url = site_path+user+'/animal/vacination/'+animal_id;
                window.location.href = redirect_vacination_url;
            });

            $('body').on('click','.disease_btn',function () {
                var animal_id = $(this).attr('data-attribute-animal');
                var redirect_disease_url = site_path+user+'/animal/disease/'+animal_id;
                window.location.href = redirect_disease_url;
            });

            $('body').on('click','.exit_animal',function () {
                var id = btoa($(this).attr('data-attribute-id'));
                var _this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to exit this animal.!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#71c016',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, exit it!'
                }).then(function(result){
                    if (result.value) {
                        var redirect_exit_url = site_path+user+'/animal/exit/'+id;
                        window.location.href = redirect_exit_url;
                    }
                })
            });

            $('body').on('click','.animal_delete',function () {
                var id = btoa($(this).attr('data-attribute-id'));
                var _this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to delete this animal.!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#71c016',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function(result){
                    if (result.value) {

                        $.ajax({
                            url: site_path + '/delete/animal',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                id: id,
                            },
                            success: function (response) {
                                if(response.status == '1')
                                {
                                    _this.parent('center').parent('td').parent('tr').remove();
                                    if($('#animal_exit_table tr').length == 0)
                                    {
                                        $('#animal_exit_table').append('<tr><td colspan="8" class="no_record_found_row"><center>No Record Found...</center></td></tr>');
                                    }
                                }
                                Swal.fire({
                                    icon: response.icon,
                                    title: response.title,
                                    text: response.text,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            },
                            beforeSend: function () {

                            },
                            complete: function () {

                            },
                            error: function () {

                            }
                        });
                    }
                })
            });

            function searchRecord(skip,search_value)
            {
                $.ajax({
                    url: site_path+'/fetch/animal-data',
                    type: 'POST',
                    dataType: 'JSON',
                    data:{
                        skip:skip,
                        search_value:search_value
                    },
                    success:function (response) {
                        console.log(response);
                        var html_content = '';
                        if(response.status == "1")
                        {
                            $.each(response.data,function (index,value) {

                                html_content += '<tr>';
                                html_content += '<td><a href="'+site_path+''+user+'/animal/view/'+btoa(this.id)+'">'+this.animal_unique_id+'</a></td>';
                                if(this.animal_type == '0')
                                {
                                    html_content += '<td>Goat</td>';
                                }
                                else
                                {
                                    html_content += '<td>Sheep</td>';
                                }
                                html_content += '<td>'+this.animal_name+'</td>';
                                html_content += '<td>'+this.breed_name+'</td>';
                                if(this.gender == '0')
                                {
                                    html_content += '<td>Andul</td>';
                                }
                                else if(this.gender == '1')
                                {
                                    html_content += '<td>Khassi</td>';
                                }
                                else
                                {
                                    html_content += '<td>Female</td>';
                                }
                                html_content += '<td><button title="Manage Vaccination" data-attribute-animal="'+btoa(this.id)+'" class="btn btn-primary vacination_btn">Vaccination</button></td>';
                                html_content += '<td><button title="Manage Disease" data-attribute-animal="'+btoa(this.id)+'" class="btn btn-danger disease_btn">Disease</button></td>';
                                html_content += '<td><center><a title="Edit" href="'+site_path+''+user+'/animal/edit/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; <a title="delete" href="javascript:void(0)" class="animal_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a> &nbsp; <a href="javascript:void(0);" title="Exit" class="exit_animal" data-attribute-id="'+this.id+'"><i class="fa fa-sign-out" aria-hidden="true"></i></a></center></td>';
                                html_content += '</tr>';
                            });
                        }
                        else
                        {
                            html_content += '<tr><td colspan="8"><center>No Record Found...</center></td></tr>';
                        }
                        $('#animal_exit_table').html(html_content);

                    },
                    beforeSend:function () {
                        $('#animal_exit_table').html('<tr><td colspan="8"><center><i class="fa fa-spinner fa-pulse fa-2x"><i/></center></td></tr>');
                    },
                    complete:function(){

                    },
                    error:function(){

                    }
                });
            }

            $('#search_text').keyup(function () {
                search_value = $(this).val();
                skip = 0;
                searchRecord(skip,search_value);
            });


        });
    </script>
@endsection