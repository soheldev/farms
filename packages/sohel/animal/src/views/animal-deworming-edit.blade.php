@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Edit Animal Deworming</h2>
                </div>
                <div class="add-customer">
                    <form id="edit_animal_deworming_form" method="POST" action="" enctype="multipart/form-data" autocomplete="off">

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="deworming_type" name="deworming_type">
                                        <option value="">Select Deworming Type*</option>
                                        <option value="1" @if($current_deworming->type == '1') selected @endif>Internal</option>
                                        <option value="2" @if($current_deworming->type == '2') selected @endif>External</option>
                                    </select>
                                    @error('deworming_type')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="deworming_agent" name="deworming_agent">
                                        <option value="">Select Deworming Agent*</option>
                                        @foreach($deworming_agents as $deworming_agent)
                                            <option value="{{$deworming_agent->id}}" @if($deworming_agent->id == $update_animal_deworming->deworming_id) selected @endif>{{$deworming_agent->deworming_agent_name}}</option>
                                        @endforeach
                                    </select>
                                    @error('deworming_agent')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="deworming_dosage" name="deworming_dosage"
                                           placeholder="Deworming Dosage" readonly class="form-control"
                                           value="{{$update_animal_deworming->predefine_body_weight.' KG / '.$update_animal_deworming->predefine_dosage.' ML'}}">
                                    @error('deworming_dosage')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="actual_body_weight" name="actual_body_weight" placeholder="Enter Animal Weight*"
                                           class="form-control" value="{{$update_animal_deworming->actual_body_weight}}">
                                    @error('actual_body_weight')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="actual_dosage" name="actual_dosage"
                                           placeholder="Actual Dosage" readonly class="form-control"
                                           @if(isset($update_animal_deworming->actual_dosage)) value="{{$update_animal_deworming->actual_dosage}}" @endif>
                                    @error('actual_dosage')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="deworming_date" name="deworming_date" placeholder="Select Deworming Date*"
                                           class="form-control" @if(isset($update_animal_deworming->deworming_date)) value="{{$update_animal_deworming->deworming_date}}" @endif>
                                    @error('deworming_date')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12 form-group">
                                    <textarea class="form-control" id="notes" name="notes"
                                              placeholder="Enter Notes" value="" rows="5">@if(isset($update_animal_deworming->notes)){{$update_animal_deworming->notes}}@endif</textarea>
                                @error('notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li>
                                            <button type="submit" class="btn cust-btn btn-save"
                                                    id="edit_animal_deworming_submit_btn"><i id="edit_animal_deworming_submit_loder"
                                                                                        style="font-size:15px"></i> Save
                                            </button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn cust-btn btn-grey"
                                                    id="reset_edit_animal_deworming_form">Cancel
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        var deworming_agent_id = '{{$update_animal_deworming->deworming_id}}';
        var predefine_body_weight = '{{$update_animal_deworming->predefine_body_weight}}';
        var predefine_dosage = '{{$update_animal_deworming->predefine_dosage}}';
        $(function ()
        {
            $( "#deworming_date" ).datepicker({
                dateFormat : 'yy-mm-d',
                changeYear: true,
                changeMonth: true,
                //maxDate: 0,
                yearRange: '2000:+0',
            });

            $('#deworming_type').change(function () {
                var deworming_type = $(this).val();
                $.ajax({
                    url: javascript_site_path+'/get-deworming-agents',
                    type:'POST',
                    dataType:'JSON',
                    data:{
                        type: deworming_type
                    },
                    success:function (response)
                    {
                        var html_content = '';
                        if(response.status == '0')
                        {
                            html_content = '<option value="">Select Deworming Agent</option>';
                            $.each(response.data,function (key,value) {
                                if(this.id == deworming_agent_id)
                                {
                                    html_content += '<option value="'+this.id+'" selected>'+this.deworming_agent_name+'</option>';
                                }
                                else
                                {
                                    html_content += '<option value="'+this.id+'">'+this.deworming_agent_name+'</option>';
                                }
                            });
                        }
                        else
                        {
                            html_content = '<option value="">No Deworming Agent Found</option>';
                        }
                        $('#deworming_agent').html(html_content);
                    }
                });
            });

            $('#actual_body_weight').keyup(function () {
                var animal_body_weight = $(this).val();
                var predefine_dosage_divide_by_predefine_body_weight = predefine_dosage / predefine_body_weight;
                var actual_dosage = animal_body_weight * predefine_dosage_divide_by_predefine_body_weight;
                $('#actual_dosage').val(actual_dosage+' ML');
            });

            $('#deworming_agent').change(function () {
                var deworming_agent_id = $(this).val();
                $.ajax({
                    url: javascript_site_path+'/get-deworming-dosage',
                    type:'POST',
                    dataType:'JSON',
                    data:{
                        id: deworming_agent_id
                    },
                    success:function (response)
                    {
                        predefine_body_weight = response.data.body_weight;
                        predefine_dosage = response.data.dosage;
                        $('#deworming_dosage').val(predefine_body_weight+' KG / '+predefine_dosage+' ML');
                        if($('#actual_body_weight').val() != '')
                        {
                            var animal_body_weight = $('#actual_body_weight').val();
                            var predefine_dosage_divide_by_predefine_body_weight = predefine_dosage / predefine_body_weight;
                            var actual_dosage = animal_body_weight * predefine_dosage_divide_by_predefine_body_weight;
                            $('#actual_dosage').val(actual_dosage+' ML');
                        }
                    }
                });
            });


            $('#reset_edit_animal_deworming_form').click(function () {
                $('#edit_animal_deworming_form')[0].reset();
            });


            $('#edit_animal_deworming_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'deworming_agent':{
                        required:true,
                    },
                    'actual_body_weight':{
                        required:true,
                    },
                    'deworming_date':{
                        required:true,
                    },
                    /*'recovered_date':{
                        required:true,
                    },*/
                },
                messages:{
                    'deworming_agent':{
                        required:"Please select deworming agent name.",
                    },
                    'actual_body_weight':{
                        required:"Please enter animal weight",
                    },
                    'deworming_date':{
                        required:"Please select deworming date.",
                    },
                    /*'recovered_date':{
                        required:"Please select recovered date.",
                    },*/
                },
                submitHandler:function (form) {
                    $('#edit_animal_deworming_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#edit_animal_deworming_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

        });


    </script>
@endsection