@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)
@section('content')
    <div class="db-body">
        <div class="inner-forms">
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif
            <div class="box ">
                <div class="box-title clearfix">
                    <h2 class="pull-left">List Animal</h2>
                    @if($add_animal_permission == true)
                        <div class="top-btn pull-right">
                            <a class="btn cust-btn btn-blue-1" href="{{url($user.'/animal/add')}}"><i class="fa fa-plus"></i> Add New</a>
                        </div>
                    @endif
                </div>

                <div class="table-resposnive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Animal Id</th>
                            <th>Animal Type</th>
                            <th>Animal Name</th>
                            <th>Animal Breed</th>
                            <th>Animal Gender</th>
                            {{--@if($view_animal_deworming == true)
                                <th>Deworming</th>
                            @endif--}}
                            {{--@if($view_animal_vaccination == true)
                                <th>Vaccination</th>
                            @endif--}}
                            {{--@if($view_animal_disease == true)
                                <th>Disease</th>
                            @endif--}}
                            @if($view_animal_deworming == true || $view_animal_vaccination == true|| $view_animal_disease == true || $update_animal_permission == true || $delete_animal_permission == true || $exit_animal_permission == true)
                                <th>Action</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody id="animal_table">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('jcontent')
    <script>
        var site_path = '{{url('/')}}';
        var user = '{{$user}}';
        var skip = 0;
        var i = 0;
        var search_value = '';
        var update_animal_permission = '{{$update_animal_permission}}';
        var delete_animal_permission = '{{$delete_animal_permission}}';
        var exit_animal_permission = '{{$exit_animal_permission}}';
        var view_animal_deworming_permission = '{{$view_animal_deworming}}';
        var view_animal_vaccination_permission = '{{$view_animal_vaccination}}';
        var view_animal_disease_permission = '{{$view_animal_disease}}';
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/animal-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip,
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td><a href="'+site_path+''+user+'/animal/view/'+btoa(this.id)+'">'+this.animal_unique_id+'</a></td>';
                            if(this.animal_type == '0')
                            {
                                html_content += '<td>Goat</td>';
                            }
                            else
                            {
                                html_content += '<td>Sheep</td>';
                            }
                            html_content += '<td>'+this.animal_name+'</td>';
                            html_content += '<td>'+this.breed_name+'</td>';
                            if(this.gender == '0')
                            {
                                html_content += '<td>Andul</td>';
                            }
                            else if(this.gender == '1')
                            {
                                html_content += '<td>Khassi</td>';
                            }
                            else
                            {
                                html_content += '<td>Female</td>';
                            }

                            if(view_animal_deworming_permission == true || view_animal_vaccination_permission == true || view_animal_disease_permission == true || update_animal_permission == true || delete_animal_permission == true || exit_animal_permission == true)
                            {
                                html_content += '<td>';
                                html_content += '<ul class="list-inline">';
                                if(view_animal_deworming_permission == true)
                                {
                                    html_content += '<li><button title="Manage Deworming" data-attribute-animal="'+btoa(this.id)+'" class="action-btn btn-red btn deworming_btn">D</button></li>';
                                }
                                if(view_animal_vaccination_permission == true)
                                {
                                    html_content += '<li><button title="Manage Vaccination" data-attribute-animal="'+btoa(this.id)+'" class="action-btn btn-red btn vacination_btn">V</button></li>';
                                }
                                if(view_animal_disease_permission == true)
                                {
                                    html_content += '<li><button title="Manage Disease" data-attribute-animal="'+btoa(this.id)+'" class="action-btn btn-red btn disease_btn">D</button></li>';
                                }
                                if(update_animal_permission == true)
                                {
                                    html_content += '<li><a title="Edit" href="' + site_path + '' + user + '/animal/edit/' + btoa(this.id) + '" class="action-btn btn-yellow edit" data-attribute-id="' + this.id + '"><i class="fa fa-pencil "></i></a></li>';
                                }
                                if(delete_animal_permission == true)
                                {
                                    html_content += '<li><a title="Delete" href="javascript:void(0)" class="action-btn btn-red animal_delete" data-attribute-id="' + this.id + '"><i class="fa fa-trash"></i></a></li>';
                                }
                                if(exit_animal_permission == true)
                                {
                                    html_content += '<li><a href="javascript:void(0);" title="Exit" class="action-btn btn-purple exit_animal" data-attribute-id="'+this.id+'"><i class="fa fa-sign-out "></i></a></li>';
                                }
                                html_content += '</ul>';
                                html_content += '</td>';
                            }
                            //html_content += '<td><center><a title="Edit" href="'+site_path+''+user+'/animal/edit/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; <a title="delete" href="javascript:void(0)" class="animal_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a> &nbsp; <a href="javascript:void(0);" title="Exit" class="exit_animal" data-attribute-id="'+this.id+'"><i class="fa fa-sign-out" aria-hidden="true"></i></a></center></td>';
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="6"><center>No Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#animal_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 5;
                    fetchRecord(skip);
                }
            };

            $('body').on('click','.deworming_btn',function () {
                var animal_id = $(this).attr('data-attribute-animal');
                var redirect_deworming_url = site_path+user+'/animal/deworming/'+animal_id;
                window.location.href = redirect_deworming_url;
            });

            $('body').on('click','.vacination_btn',function () {
                var animal_id = $(this).attr('data-attribute-animal');
                var redirect_vacination_url = site_path+user+'/animal/vacination/'+animal_id;
                window.location.href = redirect_vacination_url;
            });

            $('body').on('click','.disease_btn',function () {
                var animal_id = $(this).attr('data-attribute-animal');
                var redirect_disease_url = site_path+user+'/animal/disease/'+animal_id;
                window.location.href = redirect_disease_url;
            });

            $('body').on('click','.exit_animal',function () {
                var id = btoa($(this).attr('data-attribute-id'));
                var _this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to exit this animal.!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#71c016',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, exit it!'
                }).then(function(result){
                    if (result.value) {
                        var redirect_exit_url = site_path+user+'/animal/exit/'+id;
                        window.location.href = redirect_exit_url;
                    }
                })
            });

            $('body').on('click','.animal_delete',function () {
                var id = btoa($(this).attr('data-attribute-id'));
                var _this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to delete this animal.!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#71c016',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function(result){
                    if (result.value) {

                        $.ajax({
                            url: site_path + '/delete/animal',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                id: id,
                            },
                            success: function (response) {
                                if(response.status == '1')
                                {
                                    _this.parent('center').parent('td').parent('tr').remove();
                                    if($('#animal_table tr').length == 0)
                                    {
                                        $('#animal_table').append('<tr><td colspan="9" class="no_record_found_row"><center>No Record Found...</center></td></tr>');
                                    }
                                }
                                Swal.fire({
                                    icon: response.icon,
                                    title: response.title,
                                    text: response.text,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            },
                            beforeSend: function () {

                            },
                            complete: function () {

                            },
                            error: function () {

                            }
                        });
                    }
                })
            });

            function searchRecord(skip,search_value)
            {
                $.ajax({
                    url: site_path+'/fetch/animal-data',
                    type: 'POST',
                    dataType: 'JSON',
                    data:{
                        skip:skip,
                        search_value:search_value
                    },
                    success:function (response) {
                        console.log(response);
                        var html_content = '';
                        if(response.status == "1")
                        {
                            $.each(response.data,function (index,value) {

                                html_content += '<tr>';
                                html_content += '<td><a href="'+site_path+''+user+'/animal/view/'+btoa(this.id)+'">'+this.animal_unique_id+'</a></td>';
                                if(this.animal_type == '0')
                                {
                                    html_content += '<td>Goat</td>';
                                }
                                else
                                {
                                    html_content += '<td>Sheep</td>';
                                }
                                html_content += '<td>'+this.animal_name+'</td>';
                                html_content += '<td>'+this.breed_name+'</td>';
                                if(this.gender == '0')
                                {
                                    html_content += '<td>Andul</td>';
                                }
                                else if(this.gender == '1')
                                {
                                    html_content += '<td>Khassi</td>';
                                }
                                else
                                {
                                    html_content += '<td>Female</td>';
                                }
                                if(view_animal_deworming_permission == true)
                                {
                                    html_content += '<td><button title="Manage Deworming" data-attribute-animal="'+btoa(this.id)+'" class="btn btn-warning deworming_btn">Deworming</button></td>';
                                }
                                if(view_animal_vaccination_permission == true)
                                {
                                    html_content += '<td><button title="Manage Vaccination" data-attribute-animal="'+btoa(this.id)+'" class="btn btn-primary vacination_btn">Vaccination</button></td>';
                                }
                                if(view_animal_disease_permission == true)
                                {
                                    html_content += '<td><button title="Manage Disease" data-attribute-animal="'+btoa(this.id)+'" class="btn btn-danger disease_btn">Disease</button></td>';
                                }
                                if(update_animal_permission == true || delete_animal_permission == true || exit_animal_permission == true)
                                {
                                    html_content += '<td><center>';
                                    if(update_animal_permission == true)
                                    {
                                        html_content += '<a title="Edit" href="'+site_path+''+user+'/animal/edit/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; ';
                                    }
                                    if(delete_animal_permission == true)
                                    {
                                        html_content += '<a title="delete" href="javascript:void(0)" class="animal_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a> &nbsp; ';
                                    }
                                    if(exit_animal_permission == true)
                                    {
                                        html_content += '<a href="javascript:void(0);" title="Exit" class="exit_animal" data-attribute-id="'+this.id+'"><i class="fa fa-sign-out" aria-hidden="true"></i></a>';
                                    }
                                    html_content += '</center></td>';
                                }
                                //html_content += '<td><center><a title="Edit" href="'+site_path+''+user+'/animal/edit/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; <a title="delete" href="javascript:void(0)" class="animal_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a> &nbsp; <a href="javascript:void(0);" title="Exit" class="exit_animal" data-attribute-id="'+this.id+'"><i class="fa fa-sign-out" aria-hidden="true"></i></a></center></td>';
                                html_content += '</tr>';
                            });
                        }
                        else
                        {
                            html_content += '<tr><td colspan="9"><center>No Record Found...</center></td></tr>';
                        }
                        $('#animal_table').html(html_content);

                    },
                    beforeSend:function () {
                        $('#animal_table').html('<tr><td colspan="8"><center><i class="fa fa-spinner fa-pulse fa-2x"><i/></center></td></tr>');
                    },
                    complete:function(){

                    },
                    error:function(){

                    }
                });
            }

            $('#search_text').keyup(function () {
                search_value = $(this).val();
                skip = 0;
                searchRecord(skip,search_value);
            });


        });
    </script>
@endsection