@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)
@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/animal/list')}}">List Animals&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Edit Animal</p>
                        </div>
                        <br>
                        <form class="forms-sample" id="update_animal_form" method="POST" action="" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="exampleInputName1">Animal Type<sup style="color: red">*</sup></label>
                                <select class="form-control" id="animal_type" name="animal_type">
                                    <option value="">Select Animal Type</option>
                                    <option value="0" @if($update_animal->animal_type == "0") selected @endif>Goat</option>
                                    <option value="1" @if($update_animal->animal_type == "1") selected @endif>Sheep</option>
                                </select>
                                @error('animal_type')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Is Owned By Farm</label>&nbsp;
                                <input type="checkbox" id="is_owned_by_farm" name="is_owned_by_farm" @if($update_animal->is_owned_by_farm == "1") checked @endif value="1">
                                @error('animal_name')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group" id="animal_owner_div">
                                <label for="exampleInputName1">Animal Owner<sup style="color: red">*</sup></label>
                                <select class="form-control" id="customer_id" name="customer_id">
                                    <option value="">Select Animal Owner</option>
                                    @foreach($customers as $customer)
                                        <option value="{{$customer->customer_id}}" @if($update_animal->customer_id == $customer->customer_id) selected @endif>{{$customer->first_name}}{{' '.$customer->last_name}}{{' ('.$customer->customer_unique_id.')'}}</option>
                                    @endforeach
                                </select>
                                @error('customer_id')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Animal Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="animal_name" name="animal_name" placeholder="Enter Animal Name" value="{{$update_animal->animal_name}}">
                                @error('animal_name')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Animal Breed<sup style="color: red">*</sup></label>
                                <select class="form-control" id="breed_id" name="breed_id">
                                    <option value="">Select Animal Breed</option>
                                    @foreach($breeds as $breed)
                                        <option value="{{$breed->id}}" @if($update_animal->breed_id == $breed->id) selected @endif>{{$breed->breed_name}}</option>
                                    @endforeach
                                </select>
                                @error('breed_id')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Animal Gender<sup style="color: red">*</sup></label>
                                <select class="form-control" id="gender" name="gender">
                                    <option value="">Select Animal Gender</option>
                                    <option value="0" @if($update_animal->gender == "0") selected @endif>Andul</option>
                                    <option value="1" @if($update_animal->gender == "1") selected @endif>Khassi</option>
                                    <option value="2" @if($update_animal->gender == "2") selected @endif>Female</option>
                                </select>
                                @error('gender')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Animal Age In Months<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="age_in_month" name="age_in_month" placeholder="Enter Aminal Age In Months" value="{{$update_animal->age_in_month}}">
                                @error('age_in_month')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Animal Date Of Birth</label>
                                <input type="text" class="form-control" id="date_of_birth" name="date_of_birth" placeholder="Select Animal Date Of Birth" value="{{$update_animal->date_of_birth}}">
                                @error('date_of_birth')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Animal Weight (In Kgs)<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="entry_weight" name="entry_weight" placeholder="Enter Aminal Weight" value="{{$update_animal->entry_weight}}">
                                @error('entry_weight')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Animal Height (In Inch)<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="entry_height" name="entry_height" placeholder="Enter Aminal Height" value="{{$update_animal->entry_height}}">
                                @error('entry_height')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Animal Registration Date</label>
                                <input type="text" class="form-control" id="entry_date" name="entry_date" placeholder="Select Animal Registration Date" value="{{$update_animal->entry_date}}" readonly>
                                @error('entry_date')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Notes</label>
                                <textarea class="form-control" id="notes" name="notes" placeholder="Enter Notes" value="" rows="5">{{$update_animal->notes}}</textarea>
                                @error('notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary mr-2" id="animal_update_btn"><i id="animal_update_loder" style="font-size:15px"></i>  Update</button>
                            <button type="button" class="btn btn-light" id="reset_update_animal_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function ()
        {
            $('#reset_update_animal_form').click(function () {
                $('#update_animal_form')[0].reset();
            });

            /*$( "#entry_date" ).datepicker({
                dateFormat : 'yy-mm-d',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });*/

            $( "#date_of_birth" ).datepicker({
                dateFormat : 'd/m/yy',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });

            $('#is_owned_by_farm').click(function () {
                if($(this).is(":checked")){
                    $('#animal_owner_div').hide();
                }
                else if($(this).is(":not(:checked)")){
                    $('#animal_owner_div').show();
                }
            });

            $("#age_in_month").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#entry_weight").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#entry_height").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });




            $('#update_animal_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'animal_type':{
                        required:true,
                    },
                    'animal_name':{
                        required:true,
                    },
                    'gender':{
                        required:true,
                    },
                    'breed_id':{
                        required:true,
                    },
                    'customer_id':{
                        required:function () {
                            if($('#is_owned_by_farm').is(":checked"))
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        },
                    },
                    'age_in_month':{
                        required:true,
                    },
                    'entry_weight':{
                        required:true,
                    },
                    'entry_height':{
                        required:true,
                    },
                    'entry_date':{
                        required:true,
                    },
                },
                messages:{
                    'animal_type':{
                        required:"Please select animal type.",
                    },
                    'animal_name':{
                        required:"Please enter animal name.",
                    },
                    'gender':{
                        required:"Please select animal gender.",
                    },
                    'breed_id':{
                        required:"Please select animal breed.",
                    },
                    'customer_id':{
                        required:"Please select animal owner name.",
                    },
                    'age_in_month':{
                        required:"Please enter animal age in months.",
                    },
                    'entry_weight':{
                        required:"Please enter animal weight.",
                    },
                    'entry_height':{
                        required:"Please enter animal height.",
                    },
                    'entry_date':{
                        required:"Please select registration date.",
                    },
                },
                submitHandler:function (form) {
                    $('#animal_update_loder').addClass("fa fa-spinner fa-pulse");
                    $('#animal_update_btn').attr('disabled',true);
                    form.submit();
                }
            });

        })

    </script>
@endsection