@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Exit Animal</h2>
                </div>
                <div class="add-customer">
                    <form id="aminal_exit_form" method="POST" action="" enctype="multipart/form-data" autocomplete="off">

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="animal_entry_date" name="animal_entry_date"
                                           placeholder="Select Animal Entry Date*" class="form-control"
                                           value="{{date("d/m/Y", strtotime($exit_animal->entry_date))}}" readonly>
                                    @error('animal_entry_date')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="animal_entry_weight" name="animal_entry_weight"
                                           placeholder="Enter Aminal Entry Weight*" class="form-control"
                                           value="{{$exit_animal->entry_weight}}" readonly>
                                    @error('animal_entry_weight')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="animal_entry_height" name="animal_entry_height"
                                           placeholder="Enter Aminal Entry Height*" class="form-control"
                                           value="{{$exit_animal->entry_height}}" readonly>
                                    @error('animal_entry_height')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="animal_exit_date" name="animal_exit_date"
                                           placeholder="Select Animal Exit Date*" class="form-control">
                                    @error('animal_exit_date')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="animal_exit_weight" name="animal_exit_weight"
                                           placeholder="Enter Animal Exit Weight (In Kgs)*" class="form-control">
                                    @error('animal_exit_weight')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="animal_exit_height" name="animal_exit_height"
                                           placeholder="Enter Animal Exit Height (In Inch)*" class="form-control">
                                    @error('animal_exit_height')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12 col-xs-12 form-group">
                                    <textarea class="form-control" id="exit_reason" name="exit_reason"
                                              placeholder="Enter Exit Reason*" value="" rows="5"></textarea>
                                @error('exit_reason')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="animal_weight_gain" name="animal_weight_gain"
                                           placeholder="Animal Weight Gain" class="form-control" readonly>
                                    @error('animal_weight_gain')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="animal_height_gain" name="animal_height_gain"
                                           placeholder="Animal Height Gain" class="form-control" readonly>
                                    @error('animal_height_gain')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12 col-xs-12 form-group">
                                    <textarea class="form-control" id="notes" name="notes"
                                              placeholder="Enter Notes" value="" rows="5"></textarea>
                                @error('notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li>
                                            <button type="submit" class="btn cust-btn btn-save"
                                                    id="animal_exit_submit_btn"><i id="animal_exit_submit_loder"
                                                                                      style="font-size:15px"></i> Save
                                            </button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn cust-btn btn-grey"
                                                    id="reset_animal_exit_form">Cancel
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function ()
        {
            $( "#animal_exit_date" ).datepicker({
                dateFormat : 'd/m/yy',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });

            $('#reset_animal_exit_form').click(function () {
                $('#aminal_exit_form')[0].reset();
            });


            $("#age_in_month").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#animal_exit_weight").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#animal_exit_height").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });




            $('#aminal_exit_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'animal_exit_date':{
                        required:true,
                    },
                    'animal_exit_weight':{
                        required:true,
                    },
                    'animal_exit_height':{
                        required:true,
                    },
                    'exit_reason':{
                        required:true,
                    },
                },
                messages:{
                    'animal_exit_date':{
                        required:"Please select animal exit date.",
                    },
                    'animal_exit_weight':{
                        required:"Please enter animal weight.",
                    },
                    'animal_exit_height':{
                        required:"Please enter animal height.",
                    },
                    'exit_reason':{
                        required:"Please enter animal exit reason.",
                    },
                },
                submitHandler:function (form) {
                    $('#animal_exit_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#animal_exit_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

            $('#animal_exit_weight').keyup(function () {
                var animal_entry_weight = $('#animal_entry_weight').val();
                var animal_exit_weight = $(this).val();
                var animal_weight = Math.round(animal_exit_weight - animal_entry_weight);
                $('#animal_weight_gain').val(animal_weight);
            });

            $('#animal_exit_height').keyup(function () {
                var animal_entry_height = $('#animal_entry_height').val();
                var animal_exit_height = $(this).val();
                var animal_height = Math.round(animal_exit_height - animal_entry_height);
                $('#animal_height_gain').val(animal_height);
            });

        });


    </script>
@endsection