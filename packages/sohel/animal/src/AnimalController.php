<?php

namespace Sohel\Animal;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CheckPermission;
use App\Http\Helpers\CheckType;
use App\Http\Helpers\GeneratePaymentLines;
use App\Http\Helpers\GenerateProgressLines;
use App\Http\Helpers\UniqueId;
use App\UserInformation;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sohel\Animal\Model\Animal;
use Sohel\Animal\Model\AnimalDeworming;
use Sohel\Animal\Model\AnimalDisease;
use Sohel\Animal\Model\AnimalVacination;
use Sohel\Animal\Model\CustomerAnimalExit;
use Sohel\Breed\Model\Breed;
use Sohel\City\Model\City;
use Sohel\Country\Model\Country;
use Sohel\Deworming\Model\Deworming;
use Sohel\Disease\Model\Disease;
use Sohel\Payment\Model\Payment;
use Sohel\Payment\Model\PaymentReminder;
use Sohel\Progress\Model\AnimalProgress;
use Sohel\Progress\Model\ProgressReminder;
use Sohel\State\Model\State;
use Sohel\User\Model\Customer;
use Sohel\Vaccine\Model\Vaccine;

class AnimalController extends Controller
{
    public function __construct()
    {
        //$this->checkType();
    }

    public function checkType()
    {
        $user_redirect = '';
        $auth_type = Auth::user()->userInformation->user_type;
        if($auth_type == "1")
        {
            $user_redirect = "/admin";
        }
        else
        {
            $user_redirect = "/vendor";
        }
        return $user_redirect;
    }
    //
    public function index()
    {
        $add_animal_permission = false;
        $update_animal_permission = false;
        $delete_animal_permission = false;
        $exit_animal_permission = false;
        $add_animal_permission = CheckPermission::hasPermission('create.animal');
        $update_animal_permission = CheckPermission::hasPermission('update.animal');
        $delete_animal_permission = CheckPermission::hasPermission('delete.animal');
        $exit_animal_permission = CheckPermission::hasPermission('exit.animal');
        $view_animal_deworming = CheckPermission::hasPermission('view.animaldeworming');
        $view_animal_vaccination = CheckPermission::hasPermission('view.animalvaccination');
        $view_animal_disease = CheckPermission::hasPermission('view.animaldisease');
        return view('animal::animal-list',compact('add_animal_permission','update_animal_permission','delete_animal_permission','exit_animal_permission','view_animal_deworming','view_animal_vaccination','view_animal_disease'));
    }

    public function exiterAnimalList()
    {
        return view('animal::animal-exit-list');
    }

    public function fetchAnimalData(Request $request)
    {
        $added_by = Auth::user()->id;
        $search_value = $request->search_value;
        $sql = "SELECT a.id,CONCAT(a.prefix,a.animal_id) AS animal_unique_id, a.animal_type,a.animal_name,a.breed_id,a.gender,ab.breed_name
                FROM animals AS a 
                JOIN breeds AS ab 
                ON a.breed_id = ab.id";
        $sql = $sql." WHERE a.status = '1'";
        if(isset($request->search_value))
        {
            $sql = $sql." AND (a.animal_name LIKE '%$request->search_value%' OR CONCAT(a.prefix,a.animal_id) LIKE '%$request->search_value%' OR ab.breed_name LIKE '%$request->search_value%')";
        }
        $sql = $sql." LIMIT $request->skip , 5";
        $data = DB::select($sql);


        /*$data = Animal::query();
        $data = $data->where('added_by',$added_by);
        if(isset($search_value) && $search_value != '')
        {
            $data = $data->where('animal_name', 'LIKE', '%' .$search_value. '%');
        }
        $data = $data->where('status','=','1');
        $data = $data->skip($request->skip)->limit(5);
        $data = $data->get();*/

        //$data = Animal::skip($request->skip)->limit(5)->where('added_by',$added_by)->get();
        if(count($data) > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function fetchExitAnimalData(Request $request)
    {
        $added_by = Auth::user()->id;
        $search_value = $request->search_value;

        $sql = "SELECT a.id,CONCAT(a.prefix,a.animal_id) AS animal_unique_id, a.animal_type,a.animal_name,a.breed_id,a.gender,ab.breed_name
                FROM animals AS a 
                JOIN breeds AS ab 
                ON a.breed_id = ab.id";
        $sql = $sql." WHERE a.status = '0'";
        if(isset($request->search_value))
        {
            $sql = $sql." AND (a.animal_name LIKE '%$request->search_value%' OR CONCAT(a.prefix,a.animal_id) LIKE '%$request->search_value%' OR ab.breed_name LIKE '%$request->search_value%')";
        }
        $sql = $sql." LIMIT $request->skip , 5";
        $data = DB::select($sql);


        /*$data = Animal::query();
        $data = $data->where('added_by',$added_by);
        if(isset($search_value) && $search_value != '')
        {
            $data = $data->where('animal_name', 'LIKE', '%' .$search_value. '%');
        }
        $data = $data->where('status','=','1');
        $data = $data->skip($request->skip)->limit(5);
        $data = $data->get();*/

        //$data = Animal::skip($request->skip)->limit(5)->where('added_by',$added_by)->get();
        if(count($data) > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }


    public function viewAnimal($type, $animal_id)
    {
        $id = base64_decode($animal_id);
        $view_animal = Animal::find($id);
        return view('animal::animal-view',compact('view_animal'));
    }

    public function createAnimal(Request $request)
    {
        if($request->method() == "GET")
        {
            $added_by = Auth::user()->id;
            $breeds = Breed::all(['id','breed_name']);
            $customers = Customer::all(['id','customer_id','customer_unique_id','first_name','last_name']);
            $deworming_agents = Deworming::all(['id','deworming_agent_name']);
            return view("animal::animal-add",compact('breeds','customers','deworming_agents'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'animal_type' => 'required',
                'animal_name' => 'required',
                'breed_id' => 'required',
                'gender' => 'required',
                'age_in_month' => 'required',
                'entry_weight' => 'required',
                'entry_height' => 'required',
                'entry_date' => 'required',
                'monthly_rent' => 'required',
            ],
                $messages = [
                    'animal_type.required'    => 'Please select animal type.',
                    'animal_name.required'    => 'Please enter animal name.',
                    'breed_id.required'    => 'Please select animal breed.',
                    'gender.required'    => 'Please select animal gender.',
                    'age_in_month.required'    => 'Please enter animal age in months.',
                    'entry_weight.required'    => 'Please enter animal weight.',
                    'entry_height.required'    => 'Please enter animal height.',
                    'entry_date.required'    => 'Please select registration date.',
                    'monthly_rent.required'    => 'Please enter animal monthly rent.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $prefix = $this->generateAnimalPrefix($request->animal_type,$request->gender);
                $unique_number = UniqueId::generateUniqueIdentification(10);
                $animal_id = $unique_number;
                $insert_animal_arr = [];
                if($request->has('is_owned_by_farm'))
                {
                    $insert_animal_arr['customer_id'] = Auth::user()->id;
                    $insert_animal_arr['is_owned_by_farm'] = '1';
                }
                else
                {
                    $insert_animal_arr['customer_id'] = $request->customer_id;
                    $insert_animal_arr['is_owned_by_farm'] = '0';
                }
                $insert_animal_arr['prefix'] = $prefix;
                $insert_animal_arr['animal_id'] = $animal_id;
                $insert_animal_arr['animal_type'] = $request->animal_type;
                $insert_animal_arr['animal_name'] = $request->animal_name;
                $insert_animal_arr['breed_id'] = $request->breed_id;
                $insert_animal_arr['gender'] = $request->gender;
                $insert_animal_arr['age_in_month'] = $request->age_in_month;
                $insert_animal_arr['date_of_birth'] = $request->date_of_birth;
                $insert_animal_arr['entry_date'] = $request->entry_date;
                $insert_animal_arr['entry_weight'] = $request->entry_weight;
                $insert_animal_arr['entry_height'] = $request->entry_height;
                $insert_animal_arr['monthly_rent'] = $request->monthly_rent;
                $insert_animal_arr['notes'] = $request->notes;
                $insert_animal_arr['added_by'] = Auth::user()->id;
                $created_animal = Animal::create($insert_animal_arr);
                if($created_animal)
                {
                    $this->generatePaymentLine($created_animal, $request);
                    $this->generateAnimalInitialProgress($created_animal, $request);
                    $this->generateAnimalProgressReminder($created_animal, $request);
                    /*if(isset($request->deworming_date) && isset($request->deworming_agent))
                    {
                        $this->scheduleAnimalDeworming($created_animal, $request);
                    }*/
                }
                $type = CheckType::checkAuthType();
                return redirect($type.'/animal/list')->with(['success' => 'Animals Added Successfully...']);
            }
        }
    }

    public function getDateDifference($date1, $date2)
    {
        $date1 = strtotime($date1);
        $date2 = strtotime($date2);

        $diff = abs($date2 - $date1);

        $years = floor($diff / (365*60*60*24));

        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));

        return $months;
    }

    public function generatePaymentLine($created_animal, $request)
    {
        $payment_reminder_arr = [];
        $generate_payment_line = [];
        if($request->has('is_payment_done'))
        {
            $payment_reminder = new \DateTime($request->entry_date);
            $payment_reminder->modify('+1 months');
            $payment_due_date = $payment_reminder->format("Y-m-d");
            $payment_reminder_arr['animal_id'] = $created_animal->id;
            $payment_reminder_arr['payment_due_date'] = $payment_due_date;
            $payment_reminder_arr['added_by'] = Auth::user()->id;

            $generate_payment_line['animal_id'] = $created_animal->id;
            $generate_payment_line['customer_id'] = $request->customer_id;
            $generate_payment_line['amount_due'] = $request->monthly_rent;
            $generate_payment_line['amount_paid'] = $request->monthly_rent;
            $generate_payment_line['due_date'] = $request->entry_date;
            $generate_payment_line['payment_date'] = $request->entry_date;
            $generate_payment_line['mode_of_payment'] = '0';
            $generate_payment_line['payment_status'] = '1';
            $generate_payment_line['added_by'] = Auth::user()->id;
            $generate_payment_line['created_at'] = date('Y-m-d h:m:i');
        }
        else
        {
            $generate_payment_line['animal_id'] = $created_animal->id;
            $generate_payment_line['customer_id'] = $request->customer_id;
            $generate_payment_line['amount_due'] = $request->monthly_rent;
            $generate_payment_line['amount_paid'] = 0;
            $generate_payment_line['due_date'] = $request->entry_date;
            $generate_payment_line['payment_status'] = '0';
            $generate_payment_line['added_by'] = Auth::user()->id;
            $generate_payment_line['created_at'] = date('Y-m-d h:m:i');

            $payment_reminder = new \DateTime($request->entry_date);
            $payment_reminder->modify('+1 months');
            $payment_due_date = $payment_reminder->format("Y-m-d");
            $payment_reminder_arr['animal_id'] = $created_animal->id;
            $payment_reminder_arr['payment_due_date'] = $payment_due_date;
            $payment_reminder_arr['added_by'] = Auth::user()->id;
        }
        PaymentReminder::create($payment_reminder_arr);
        Payment::create($generate_payment_line);
        $current_date = date("Y-m-d");
        if($current_date > $payment_due_date)
        {
            $loop = $this->getDateDifference($payment_due_date, $current_date);
            for ($i = 0; $i <= $loop; $i++)
            {
                GeneratePaymentLines::generateParticularCustomerPaymentLine($created_animal->id);
            }

        }
    }

    public function generateAnimalInitialProgress($created_animal, $request)
    {
        $insert_progress_arr = [];
        $insert_progress_arr['animal_id'] = $created_animal->id;
        $insert_progress_arr['weight'] = $request->entry_weight;
        $insert_progress_arr['height'] = $request->entry_height;
        $insert_progress_arr['capture_date'] = $request->entry_date;
        $insert_progress_arr['due_date'] = $request->entry_date;
        $insert_progress_arr['date'] = 'Initial Progress';
        $insert_progress_arr['date'] = date('Y-m-d');
        $insert_progress_arr['status'] = '1';
        $insert_progress_arr['added_by'] = Auth::user()->id;
        AnimalProgress::create($insert_progress_arr);
    }

    public function generateAnimalProgressReminder($created_animal, $request)
    {
        $progress_reminder = new \DateTime($request->entry_date);
        $progress_reminder->modify('+1 months');
        $progress_entry_date = $progress_reminder->format("Y-m-d");

        $progress_reminder_arr = [];
        $progress_reminder_arr['animal_id'] = $created_animal->id;
        $progress_reminder_arr['progress_entry_date'] = $progress_entry_date;
        $progress_reminder_arr['progress_tracker'] = '0';
        $progress_reminder_arr['added_by'] = Auth::user()->id;
        ProgressReminder::create($progress_reminder_arr);
        $current_date = date("Y-m-d");
        if($current_date > $progress_entry_date)
        {
            $loop = $this->getDateDifference($progress_entry_date, $current_date);
            for ($i = 0; $i <= $loop; $i++)
            {
                GenerateProgressLines::generateParticularAnimalProgressLine($created_animal->id);
            }

        }
    }

    public function scheduleAnimalDeworming($created_animal, $request)
    {
        $schedule_deworming_arr = [];
        $deworming_agent_detail = Deworming::find($request->deworming_agent);
        if(isset($deworming_agent_detail))
        {
            $schedule_deworming_arr['animal_id'] = $created_animal->id;
            $schedule_deworming_arr['deworming_id'] = $deworming_agent_detail->id;
            $schedule_deworming_arr['predefine_body_weight'] = $deworming_agent_detail->body_weight;
            $schedule_deworming_arr['predefine_dosage'] = $deworming_agent_detail->dosage;
            $schedule_deworming_arr['due_date'] = $request->deworming_date;
            $schedule_deworming_arr['added_by'] = Auth::user()->id;
            AnimalDeworming::create($schedule_deworming_arr);
        }
    }


    public function updateAnimal($type, $animal_id,Request $request)
    {

        $id = base64_decode($animal_id);
        $update_animal = Animal::find($id);
        if($request->method() == "GET")
        {
            $added_by = Auth::user()->id;
            $breeds = Breed::all(['id','breed_name']);
            $customers = Customer::all(['id','customer_id','customer_unique_id','first_name','last_name']);
            return view('animal::animal-edit',compact('update_animal','breeds','customers'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'animal_type' => 'required',
                'animal_name' => 'required',
                'breed_id' => 'required',
                'gender' => 'required',
                'age_in_month' => 'required',
                'entry_weight' => 'required',
                'entry_height' => 'required',
            ],
                $messages = [
                    'animal_type.required'    => 'Please select animal type.',
                    'animal_name.required'    => 'Please enter animal name.',
                    'breed_id.required'    => 'Please select animal breed.',
                    'gender.required'    => 'Please select animal gender.',
                    'age_in_month.required'    => 'Please enter animal age in months.',
                    'entry_weight.required'    => 'Please enter animal weight.',
                    'entry_height.required'    => 'Please enter animal height.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $update_animal_arr = [];
                if($request->animal_type != $update_animal->animal_type || $request->gender != $update_animal->gender)
                {
                    $prefix = $this->generateAnimalPrefix($request->animal_type,$request->gender);
                    $update_animal_arr['prefix'] = $prefix;
                }
                if($request->has('is_owned_by_farm'))
                {
                    $update_animal_arr['customer_id'] = Auth::user()->id;
                    $update_animal_arr['is_owned_by_farm'] = '1';
                }
                else
                {
                    $update_animal_arr['customer_id'] = $request->customer_id;
                    $update_animal_arr['is_owned_by_farm'] = '0';
                }
                $update_animal_arr['animal_type'] = $request->animal_type;
                $update_animal_arr['animal_name'] = $request->animal_name;
                $update_animal_arr['breed_id'] = $request->breed_id;
                $update_animal_arr['gender'] = $request->gender;
                $update_animal_arr['age_in_month'] = $request->age_in_month;
                $update_animal_arr['date_of_birth'] = $request->date_of_birth;
                $update_animal_arr['entry_weight'] = $request->entry_weight;
                $update_animal_arr['entry_height'] = $request->entry_height;
                $update_animal_arr['notes'] = $request->notes;
                DB::table('animals')->where('id',$update_animal->id)->update($update_animal_arr);
                DB::table('animal_progress')->where('animal_id',$update_animal->id)->where('capture_date',$update_animal->entry_date)->update(['weight' => $request->entry_weight, 'height' => $request->entry_height]);
                $type = CheckType::checkAuthType();
                return redirect($type.'/animal/list')->with(['success' => 'Animal Updated Successfully...']);
            }
        }
    }

    public function deleteAnimal(Request $request)
    {
        $id = base64_decode($request->id);
        if(isset($id))
        {
            $delete_animal = Animal::find($id);
            if(isset($delete_animal))
            {
                if($delete_animal->delete())
                {
                    DB::table('animal_progress')->where('animal_id',$id)->delete();
                    DB::table('progress_reminders')->where('animal_id',$id)->delete();
                    $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Animal deleted successfully...');
                    return json_encode($result);
                }
                else
                {
                    $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something went wrong...');
                    return json_encode($result);
                }

            }
        }
    }

    public function exitAnimal($type, $animal_id,Request $request)
    {

        $id = base64_decode($animal_id);
        $exit_animal = Animal::find($id);
        if($request->method() == "GET")
        {
            return view('animal::animal-exit',compact('exit_animal'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'animal_exit_date' => 'required',
                'animal_exit_weight' => 'required',
                'animal_exit_height' => 'required',
                'exit_reason' => 'required',
            ],
                $messages = [
                    'animal_exit_date.required'    => 'Please select animal exit date.',
                    'animal_exit_weight.required'    => 'Please enter animal weight.',
                    'animal_exit_height.required'    => 'Please select animal height.',
                    'exit_reason.required'    => 'Please select animal exit reason.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $exit_animal_arr = [];
                $exit_animal_arr['animal_id'] = $exit_animal->id;
                $exit_animal_arr['customer_id'] = $exit_animal->customer_id;
                $exit_animal_arr['animal_entry_date'] = $request->animal_entry_date;
                $exit_animal_arr['animal_entry_weight'] = $request->animal_entry_weight;
                $exit_animal_arr['animal_entry_height'] = $request->animal_entry_height;
                $exit_animal_arr['animal_exit_date'] = $request->animal_exit_date;
                $exit_animal_arr['animal_exit_weight'] = $request->animal_exit_weight;
                $exit_animal_arr['animal_exit_height'] = $request->animal_exit_height;
                $exit_animal_arr['exit_reason'] = $request->exit_reason;
                $exit_animal_arr['animal_weight_gain'] = $request->animal_weight_gain;
                $exit_animal_arr['animal_height_gain'] = $request->animal_height_gain;
                $exit_animal_arr['notes'] = $request->notes;
                CustomerAnimalExit::create($exit_animal_arr);
                $exit_animal->status = '0';
                $exit_animal->save();
                DB::table('progress_reminders')->where('animal_id',$exit_animal->id)->update(['progress_tracker' => '1']);
                $type = CheckType::checkAuthType();
                return redirect($type.'/animal/list')->with(['success' => 'Animal Exist Successfully...']);
            }
        }
    }

    public function generateAnimalPrefix($animal_type, $animal_gender)
    {
        $prefix = '';
        if($animal_type == "0" && $animal_gender == "0")
        {
            $prefix = "GA";
        }
        elseif ($animal_type == "0" && $animal_gender == "1")
        {
            $prefix = "GK";
        }
        elseif ($animal_type == "0" && $animal_gender == "2")
        {
            $prefix = "GF";
        }
        elseif ($animal_type == "1" && $animal_gender == "0")
        {
            $prefix = "SA";
        }
        elseif ($animal_type == "1" && $animal_gender == "1")
        {
            $prefix = "SK";
        }
        elseif ($animal_type == "1" && $animal_gender == "2")
        {
            $prefix = "SF";
        }
        return $prefix;
    }

   public function listAnimalVacination($user, $animal_id)
   {
       $add_animal_vaccination_permission = false;
       $update_animal_vaccination_permission = false;
       $delete_animal_vaccination_permission = false;
       $add_animal_vaccination_permission = CheckPermission::hasPermission('create.animalvaccination');
       $update_animal_vaccination_permission = CheckPermission::hasPermission('update.animalvaccination');
       $delete_animal_vaccination_permission = CheckPermission::hasPermission('delete.animalvaccination');
       $id = base64_decode($animal_id);
       $animal_detail = Animal::select('prefix','animal_id','status')->find($id);
       return view('animal::animal-vacination-list',compact('animal_detail','add_animal_vaccination_permission','update_animal_vaccination_permission','delete_animal_vaccination_permission'));
   }

   public function fetchAnimalVaccineData(Request $request)
   {
       $animal_id = base64_decode($request->animal_id);
       $added_by = Auth::user()->id;
       //$search_value = $request->search_value;
       $data = AnimalVacination::query();
       //$data = $data->where('added_by',$added_by);
       /*if(isset($search_value) && $search_value != '')
       {
           $data = $data->where('animal_name', 'LIKE', '%' .$search_value. '%');
       }*/
       $data = $data->where('animal_id',$animal_id);
       $data = $data->skip($request->skip)->limit(5);
       $data = $data->get();

       //$data = Animal::skip($request->skip)->limit(5)->where('added_by',$added_by)->get();
       if($data->count() > 0)
       {
           $result = array('status'=>'1','data'=>$data);
           return json_encode($result);

       }
       else
       {
           $result = array('status'=>'0');
           return json_encode($result);
       }
   }

   public function addAnimalVacination($user,$animal_id,Request $request)
   {
       $id = base64_decode($animal_id);
       $added_by = Auth::user()->id;
       $vacinations = Vaccine::all(['id','vaccine_name']);
       if($request->method() == "GET")
       {
            return view('animal::animal-vacination-add',compact('id','vacinations'));
       }
       else
       {
           $validator = \Validator::make($request->all(), [
               'vaccine_id' => 'required',
               'dose_number' => 'required',
               'vacination_date' => 'required',
               'weight_on_vacination_day' => 'required',
               'quantity' => 'required',
           ],
               $messages = [
                   'vaccine_id.required'    => 'Please select vacination.',
                   'dose_number.required'    => 'Please enter dose number.',
                   'vacination_date.required'    => 'Please select vacination date.',
                   'weight_on_vacination_day.required'    => 'Please enter weight of animal on vacination day.',
                   'quantity.required'    => 'Please enter quantity in ml.',
               ]);
           if ($validator->fails())
           {
               return redirect()->back()
                   ->withErrors($validator)
                   ->withInput();
           }
           else
           {
               $insert_vacination_arr = [];
               $insert_vacination_arr['animal_id'] = $request->animal_id;
               $insert_vacination_arr['vaccine_id'] = $request->vaccine_id;
               $insert_vacination_arr['dose_number'] = $request->dose_number;
               $insert_vacination_arr['vacination_date'] = $request->vacination_date;
               $insert_vacination_arr['weight_on_vacination_day'] = $request->weight_on_vacination_day;
               $insert_vacination_arr['quantity'] = $request->quantity;
               $insert_vacination_arr['notes'] = $request->notes;
               AnimalVacination::create($insert_vacination_arr);
               $type = CheckType::checkAuthType();
               return redirect($type.'/animal/vacination/'.$animal_id)->with(['success' => 'Animal Vaccine Added Successfully...']);
           }
       }
   }

   public function editAnimalVacination($user, $animal_id, $id, Request $request)
   {
       $id = base64_decode($id);
       $added_by = Auth::user()->id;
       $edit_animal_vacination = AnimalVacination::find($id);
       $vacinations = Vaccine::all(['id','vaccine_name']);
       if($request->method() == "GET")
       {
            return view('animal::animal-vacination-edit',compact('edit_animal_vacination','vacinations'));
       }
       else
       {
           $validator = \Validator::make($request->all(), [
               'vaccine_id' => 'required',
               'dose_number' => 'required',
               'vacination_date' => 'required',
               'weight_on_vacination_day' => 'required',
               'quantity' => 'required',
           ],
               $messages = [
                   'vaccine_id.required'    => 'Please select vacination.',
                   'dose_number.required'    => 'Please enter dose number.',
                   'vacination_date.required'    => 'Please select vacination date.',
                   'weight_on_vacination_day.required'    => 'Please enter weight of animal on vacination day.',
                   'quantity.required'    => 'Please enter quantity in ml.',
               ]);
           if ($validator->fails())
           {
               return redirect()->back()
                   ->withErrors($validator)
                   ->withInput();
           }
           else
           {
               $update_vacination_arr = [];
               $update_vacination_arr['vaccine_id'] = $request->vaccine_id;
               $update_vacination_arr['dose_number'] = $request->dose_number;
               $update_vacination_arr['vacination_date'] = $request->vacination_date;
               $update_vacination_arr['weight_on_vacination_day'] = $request->weight_on_vacination_day;
               $update_vacination_arr['quantity'] = $request->quantity;
               $update_vacination_arr['notes'] = $request->notes;
               DB::table('animal_vacinations')->where('id',$edit_animal_vacination->id)->update($update_vacination_arr);
               $type = CheckType::checkAuthType();
               return redirect($type.'/animal/vacination/'.$animal_id)->with(['success' => 'Animal Vaccine Updated Successfully...']);
           }
       }
   }

    public function deleteAnimalVacination(Request $request)
    {
        $id = base64_decode($request->id);
        if(isset($id))
        {
            $delete_animal_vacination = AnimalVacination::find($id);
            if(isset($delete_animal_vacination))
            {
                if($delete_animal_vacination->delete())
                {
                    $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Animal vacination record deleted successfully...');
                    return json_encode($result);
                }
                else
                {
                    $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something went wrong...');
                    return json_encode($result);
                }

            }
        }
    }

    public function listAnimalDisease($user, $animal_id)
    {
        $add_animal_disease_permission = false;
        $update_animal_disease_permission = false;
        $delete_animal_disease_permission = false;
        $add_animal_disease_permission = CheckPermission::hasPermission('create.animaldisease');
        $update_animal_disease_permission = CheckPermission::hasPermission('update.animaldisease');
        $delete_animal_disease_permission = CheckPermission::hasPermission('delete.animaldisease');
        $id = base64_decode($animal_id);
        $animal_detail = Animal::select('prefix','animal_id','status')->find($id);
        return view('animal::animal-disease-list',compact('animal_detail','add_animal_disease_permission','update_animal_disease_permission','delete_animal_disease_permission'));
    }

    public function fetchAnimalDiseaseData(Request $request)
    {
        $animal_id = base64_decode($request->animal_id);
        $added_by = Auth::user()->id;
        //$search_value = $request->search_value;
        $data = AnimalDisease::query();
        //$data = $data->where('added_by',$added_by);
        /*if(isset($search_value) && $search_value != '')
        {
            $data = $data->where('animal_name', 'LIKE', '%' .$search_value. '%');
        }*/
        $data = $data->where('animal_id',$animal_id);
        $data = $data->skip($request->skip)->limit(5);
        $data = $data->get();

        //$data = Animal::skip($request->skip)->limit(5)->where('added_by',$added_by)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function addAnimalDisease($user,$animal_id,Request $request)
    {
        $id = base64_decode($animal_id);
        $added_by = Auth::user()->id;
        $diseases = Disease::all(['id','disease_name']);
        if($request->method() == "GET")
        {
            return view('animal::animal-disease-add',compact('id','diseases'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'disease_id' => 'required',
                'diagnosed_date' => 'required',
                'treatment' => 'required',
            ],
                $messages = [
                    'disease_id.required'    => 'Please select disease.',
                    'diagnosed_date.required'    => 'Please select diagnosed date.',
                    'treatment.required'    => 'Please enter treatment.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $insert_disease_arr = [];
                $insert_disease_arr['animal_id'] = $request->animal_id;
                $insert_disease_arr['disease_id'] = $request->disease_id;
                $insert_disease_arr['diagnosed_date'] = $request->diagnosed_date;
                $insert_disease_arr['treatment'] = $request->treatment;
                $insert_disease_arr['recovered_date'] = isset($request->recovered_date)?$request->recovered_date:NULL;
                $insert_disease_arr['notes'] = isset($request->notes)?$request->notes:NULL;
                AnimalDisease::create($insert_disease_arr);
                $type = CheckType::checkAuthType();
                return redirect($type.'/animal/disease/'.$animal_id)->with(['success' => 'Animal Disease Added Successfully...']);
            }
        }
    }

    public function editAnimalDisease($user, $animal_id, $id, Request $request)
    {
        $id = base64_decode($id);
        $added_by = Auth::user()->id;
        $edit_animal_disease = AnimalDisease::find($id);
        $diseases = Disease::all(['id','disease_name']);
        if($request->method() == "GET")
        {
            return view('animal::animal-disease-edit',compact('edit_animal_disease','diseases'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'disease_id' => 'required',
                'diagnosed_date' => 'required',
                'treatment' => 'required',
            ],
                $messages = [
                    'disease_id.required'    => 'Please select disease.',
                    'diagnosed_date.required'    => 'Please select diagnosed date.',
                    'treatment.required'    => 'Please enter treatment.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $update_disease_arr = [];
                $update_disease_arr['disease_id'] = $request->disease_id;
                $update_disease_arr['diagnosed_date'] = $request->diagnosed_date;
                $update_disease_arr['treatment'] = $request->treatment;
                $update_disease_arr['recovered_date'] = isset($request->recovered_date)?$request->recovered_date:NULL;
                $update_disease_arr['notes'] = isset($request->notes)?$request->notes:NULL;
                DB::table('animal_diseases')->where('id',$edit_animal_disease->id)->update($update_disease_arr);
                $type = CheckType::checkAuthType();
                return redirect($type.'/animal/disease/'.$animal_id)->with(['success' => 'Animal Disease Updated Successfully...']);
            }
        }
    }

    public function deleteAnimalDisease(Request $request)
    {
        $id = base64_decode($request->id);
        if(isset($id))
        {
            $delete_animal_disease = AnimalDisease::find($id);
            if(isset($delete_animal_disease))
            {
                if($delete_animal_disease->delete())
                {
                    $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Animal disease record deleted successfully...');
                    return json_encode($result);
                }
                else
                {
                    $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something went wrong...');
                    return json_encode($result);
                }

            }
        }
    }

    public function listAnimalDeworming($user, $animal_id)
    {
        $add_animal_deworming_permission = false;
        $update_animal_deworming_permission = false;
        $delete_animal_deworming_permission = false;
        $add_animal_deworming_permission = CheckPermission::hasPermission('create.animaldeworming');
        $update_animal_deworming_permission = CheckPermission::hasPermission('update.animaldeworming');
        $delete_animal_deworming_permission = CheckPermission::hasPermission('delete.animaldeworming');
        $id = base64_decode($animal_id);
        $animal_detail = Animal::select('prefix','animal_id','status')->find($id);
        return view('animal::animal-deworming-list',compact('animal_detail','add_animal_deworming_permission','update_animal_deworming_permission','delete_animal_deworming_permission'));
    }

    public function fetchAnimalDewormingData(Request $request)
    {
        $animal_id = base64_decode($request->animal_id);
        $added_by = Auth::user()->id;
        //$search_value = $request->search_value;
        $data = AnimalDeworming::query();
        //$data = $data->where('added_by',$added_by);
        /*if(isset($search_value) && $search_value != '')
        {
            $data = $data->where('animal_name', 'LIKE', '%' .$search_value. '%');
        }*/
        $data = $data->where('animal_id',$animal_id);
        $data = $data->skip($request->skip)->limit(5);
        $data = $data->get();

        //$data = Animal::skip($request->skip)->limit(5)->where('added_by',$added_by)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function addAnimalDeworming($user, $animal_id, Request $request)
    {
        $id = base64_decode($animal_id);
        $added_by = Auth::user()->id;
        $deworming_agents = Deworming::all();
        if($request->method() == "GET")
        {
            return view('animal::animal-deworming-add',compact('id','deworming_agents'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'deworming_agent' => 'required',
                'actual_body_weight' => 'required',
                'deworming_date' => 'required',
            ],
                $messages = [
                    'deworming_agent.required'    => 'Please select deworming agent.',
                    'actual_body_weight.required'    => 'Please enter animal weight.',
                    'deworming_date.required'    => 'Please select deworming date.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $schedule_deworming_arr = [];
                $deworming_agent_detail = Deworming::find($request->deworming_agent);
                if(isset($deworming_agent_detail))
                {
                    $schedule_deworming_arr['animal_id'] = $request->animal_id;
                    $schedule_deworming_arr['deworming_id'] = $deworming_agent_detail->id;
                    $schedule_deworming_arr['predefine_body_weight'] = $deworming_agent_detail->body_weight;
                    $schedule_deworming_arr['predefine_dosage'] = $deworming_agent_detail->dosage;
                    $schedule_deworming_arr['actual_body_weight'] = $request->actual_body_weight;
                    $schedule_deworming_arr['actual_dosage'] = $request->actual_dosage;
                    $schedule_deworming_arr['deworming_date'] = $request->deworming_date;
                    $schedule_deworming_arr['notes'] = $request->notes;
                    $schedule_deworming_arr['deworming_status'] = '1';
                    $schedule_deworming_arr['added_by'] = Auth::user()->id;
                    AnimalDeworming::create($schedule_deworming_arr);
                    $type = CheckType::checkAuthType();
                    return redirect($type.'/animal/deworming/'.$animal_id)->with(['success' => 'Animal Deworming Added Successfully...']);
                }
            }
        }
    }

    public function getDewormingAgents(Request $request)
    {
        $type = $request->type;
        $deworning_agents = Deworming::where('type',$type)->get(['id','deworming_agent_name']);
        if($deworning_agents->count() > 0)
        {
            $result = array('status' => '0', 'data' => $deworning_agents);
            return json_encode($result);
        }
        else
        {
            $result = array('status' => '1', 'data' => $deworning_agents);
            return json_encode($result);
        }
    }

    public function getDewormingDosage(Request $request)
    {
        $id = $request->id;
        $deworming_dosage = Deworming::find($id);
        $result = array('status' => '0', 'data' => $deworming_dosage);
        return json_encode($result);
    }

    public function editAnimalDeworming($user, $animal_id, $id, Request $request)
    {
        $id = base64_decode($id);
        $update_animal_deworming = AnimalDeworming::find($id);
        $current_deworming = Deworming::find($update_animal_deworming->deworming_id);
        $deworming_agents = Deworming::all();
        if($request->method() == "GET")
        {
            return view('animal::animal-deworming-edit',compact('update_animal_deworming','deworming_agents','current_deworming'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'deworming_agent' => 'required',
                'actual_body_weight' => 'required',
                'deworming_date' => 'required',
            ],
                $messages = [
                    'deworming_agent.required'    => 'Please select deworming agent.',
                    'actual_body_weight.required'    => 'Please enter animal weight.',
                    'deworming_date.required'    => 'Please select deworming date.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $update_deworming_arr = [];
                $deworming_agent_detail = Deworming::find($request->deworming_agent);
                if(isset($deworming_agent_detail))
                {
                    $update_deworming_arr['deworming_id'] = $deworming_agent_detail->id;
                    $update_deworming_arr['predefine_body_weight'] = $deworming_agent_detail->body_weight;
                    $update_deworming_arr['predefine_dosage'] = $deworming_agent_detail->dosage;
                    $update_deworming_arr['actual_body_weight'] = $request->actual_body_weight;
                    $update_deworming_arr['actual_dosage'] = $request->actual_dosage;
                    $update_deworming_arr['deworming_date'] = $request->deworming_date;
                    $update_deworming_arr['notes'] = $request->notes;
                    DB::table('animal_dewormings')->where('id',$update_animal_deworming->id)->update($update_deworming_arr);
                    $type = CheckType::checkAuthType();
                    return redirect($type.'/animal/deworming/'.$animal_id)->with(['success' => 'Animal Deworming Updated Successfully...']);
                }
            }
        }
    }

    public function deleteAnimalDeworming(Request $request)
    {
        $id = base64_decode($request->id);
        if(isset($id))
        {
            $delete_animal_deworming = AnimalDeworming::find($id);
            if(isset($delete_animal_deworming))
            {
                if($delete_animal_deworming->delete())
                {
                    $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Animal deworming record deleted successfully...');
                    return json_encode($result);
                }
                else
                {
                    $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something went wrong...');
                    return json_encode($result);
                }

            }
        }
    }


    public function animalChartData(Request $request)
    {
        $id = base64_decode($request->animal_id);
        $sql_query = "SELECT ap.animal_id, ap.weight,ap.height, MONTH(ap.due_date) AS progress_month FROM animal_progress AS ap WHERE ap.animal_id = '$id' AND YEAR(due_date) = YEAR(CURDATE()) ORDER BY Month(due_date)";
        $data = DB::select($sql_query);
        $weight_arr = [];
        $height_arr = [];
        $month_arr = [];
        if(count($data) > 0)
        {
            foreach($data as $d)
            {
                switch ($d->progress_month)
                {
                    case 1:
                        $month = 'January';
                        break;
                    case 2:
                        $month = 'Febuary';
                        break;
                    case 3:
                        $month = 'March';
                        break;
                    case 4:
                        $month = 'April';
                        break;
                    case 5:
                        $month = 'May';
                        break;
                    case 6:
                        $month = 'June';
                        break;
                    case 7:
                        $month = 'July';
                        break;
                    case 8:
                        $month = 'August';
                        break;
                    case 9:
                        $month = 'September';
                        break;
                    case 10:
                        $month = 'October';
                        break;
                    case 11:
                        $month = 'November';
                        break;
                    case 12:
                        $month = 'December';
                        break;
                }
                array_push($month_arr,$month);
                array_push($weight_arr,$d->weight);
                array_push($height_arr,$d->height);
            }
            $result = array('status'=> '0','month_arr' => $month_arr,'weight_arr' => $weight_arr,'height_arr' => $height_arr);
        }
        else
        {
            $result = array('status'=> '1','month_arr' => $month_arr,'weight_arr' => $weight_arr,'height_arr' => $height_arr);
        }
        return $result;
    }
}
