<?php
Route::get('/{user_type}/animal/list','Sohel\Animal\AnimalController@index')->middleware(['web','auth']);
Route::post('/fetch/animal-data','Sohel\Animal\AnimalController@fetchAnimalData')->middleware(['web','auth']);
Route::get('/{user_type}/animal/view/{animal_id}','Sohel\Animal\AnimalController@viewAnimal')->middleware(['web','auth']);
Route::get('/{user_type}/animal/add','Sohel\Animal\AnimalController@createAnimal')->middleware(['web','auth']);
Route::post('/{user_type}/animal/add','Sohel\Animal\AnimalController@createAnimal')->middleware(['web','auth']);
Route::get('/{user_type}/animal/edit/{animal_id}','Sohel\Animal\AnimalController@updateAnimal')->middleware(['web','auth']);
Route::post('/{user_type}/animal/edit/{animal_id}','Sohel\Animal\AnimalController@updateAnimal')->middleware(['web','auth']);
Route::post('/delete/animal','Sohel\Animal\AnimalController@deleteAnimal')->middleware(['web','auth']);
Route::get('/{user_type}/animal/exit/{animal_id}','Sohel\Animal\AnimalController@exitAnimal')->middleware(['web','auth']);
Route::post('/{user_type}/animal/exit/{animal_id}','Sohel\Animal\AnimalController@exitAnimal')->middleware(['web','auth']);

Route::get('/{user_type}/animal/exited','Sohel\Animal\AnimalController@exiterAnimalList')->middleware(['web','auth']);
Route::post('/fetch/exit-animal-data','Sohel\Animal\AnimalController@fetchExitAnimalData')->middleware(['web','auth']);

Route::post('/get/animal-chart-data','Sohel\Animal\AnimalController@animalChartData')->middleware(['web','auth']);

// Animal vacination routes here
Route::get('{user_type}/animal/vacination/{animal_id}','Sohel\Animal\AnimalController@listAnimalVacination')->middleware(['web','auth']);
Route::post('/fetch/animal-vaccine-data','Sohel\Animal\AnimalController@fetchAnimalVaccineData')->middleware(['web','auth']);
Route::get('{user_type}/animal/vacination/add/{animal_id}','Sohel\Animal\AnimalController@addAnimalVacination')->middleware(['web','auth']);
Route::post('{user_type}/animal/vacination/add/{animal_id}','Sohel\Animal\AnimalController@addAnimalVacination')->middleware(['web','auth']);
Route::get('{user_type}/animal/vacination/edit/{animal_id}/{id}','Sohel\Animal\AnimalController@editAnimalVacination')->middleware(['web','auth']);
Route::post('{user_type}/animal/vacination/edit/{animal_id}/{id}','Sohel\Animal\AnimalController@editAnimalVacination')->middleware(['web','auth']);
Route::post('/delete/animal/vacination','Sohel\Animal\AnimalController@deleteAnimalVacination')->middleware(['web','auth']);

// Animal disease route here
Route::get('{user_type}/animal/disease/{animal_id}','Sohel\Animal\AnimalController@listAnimalDisease')->middleware(['web','auth']);
Route::post('/fetch/animal-disease-data','Sohel\Animal\AnimalController@fetchAnimalDiseaseData')->middleware(['web','auth']);
Route::get('{user_type}/animal/disease/add/{animal_id}','Sohel\Animal\AnimalController@addAnimalDisease')->middleware(['web','auth']);
Route::post('{user_type}/animal/disease/add/{animal_id}','Sohel\Animal\AnimalController@addAnimalDisease')->middleware(['web','auth']);
Route::get('{user_type}/animal/disease/edit/{animal_id}/{id}','Sohel\Animal\AnimalController@editAnimalDisease')->middleware(['web','auth']);
Route::post('{user_type}/animal/disease/edit/{animal_id}/{id}','Sohel\Animal\AnimalController@editAnimalDisease')->middleware(['web','auth']);
Route::post('/delete/animal/disease','Sohel\Animal\AnimalController@deleteAnimalDisease')->middleware(['web','auth']);

// Animal deworming route here
Route::get('{user_type}/animal/deworming/{animal_id}','Sohel\Animal\AnimalController@listAnimalDeworming')->middleware(['web','auth']);
Route::post('/fetch/animal-deworming-data','Sohel\Animal\AnimalController@fetchAnimalDewormingData')->middleware(['web','auth']);
Route::get('{user_type}/animal/deworming/add/{animal_id}','Sohel\Animal\AnimalController@addAnimalDeworming')->middleware(['web','auth']);
Route::post('{user_type}/animal/deworming/add/{animal_id}','Sohel\Animal\AnimalController@addAnimalDeworming')->middleware(['web','auth']);
Route::get('{user_type}/animal/deworming/edit/{animal_id}/{id}','Sohel\Animal\AnimalController@editAnimalDeworming')->middleware(['web','auth']);
Route::post('{user_type}/animal/deworming/edit/{animal_id}/{id}','Sohel\Animal\AnimalController@editAnimalDeworming')->middleware(['web','auth']);
Route::post('/delete/animal/deworming','Sohel\Animal\AnimalController@deleteAnimalDeworming')->middleware(['web','auth']);

Route::post('/get-deworming-dosage','Sohel\Animal\AnimalController@getDewormingDosage')->middleware(['web','auth']);
Route::post('/get-deworming-agents','Sohel\Animal\AnimalController@getDewormingAgents')->middleware(['web','auth']);