<?php

namespace Sohel\Country;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Sohel\Country\Model\Country;

class CountryController extends Controller
{
    //
    public function index()
    {
        return view('country::country-list');
    }

    public function fetchCountryData(Request $request)
    {
        $data = Country::skip($request->skip)->limit(5)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function createCountry(Request $request)
    {
        if($request->method() == "GET")
        {
            return view('country::country-add');
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'country_name' => 'required',
            ],
                $messages = [
                    'country_name.required'    => 'Please enter country name.',
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $insert_country = new Country();
                $insert_country->country_name = $request->country_name;
                $insert_country->save();
                return redirect('/admin/setting/country')->with(['success' => 'Country Added Successfully...']);
            }
        }
    }

    public function updateCountry($country_id, Request $request)
    {
        $id = base64_decode($country_id);
        $edit_country = Country::find($id);
        if($request->method() == "GET")
        {
            return view('country::country-edit',compact('edit_country'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'country_name' => 'required',
            ],
                $messages = [
                    'country_name.required'    => 'Please enter country name.',
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $edit_country->country_name = $request->country_name;
                $edit_country->save();
                return redirect('/admin/setting/country')->with(['success' => 'Country Updated Successfully...']);
            }
        }
    }

    public function deleteCountry(Request $request)
    {
        $id = base64_decode($request->id);
        $delete_country = Country::find($id);
        if(isset($delete_country))
        {
            if($delete_country->delete())
            {
                $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Country Deleted Successfully...');
                return json_encode($result);
            }
            else
            {
                $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something Went Wrong...');
                return json_encode($result);
            }

        }
    }
}
