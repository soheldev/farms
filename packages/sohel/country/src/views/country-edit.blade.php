@extends('layouts.admin.admin')

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url('/admin/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url('/admin/setting/country')}}">List Country&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Edit Country</p>
                        </div>
                        <br>
                        <form class="forms-sample" id="update_country_form" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputName1">Country Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="country_name" name="country_name" placeholder="Enter Country Name" value="{{$edit_country->country_name}}" autocomplete="off">
                                @error('country_name')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary mr-2" id="country_update_btn"><i id="country_update_loder" style="font-size:15px"></i>  Update</button>
                            <button type="button" class="btn btn-light" id="reset_country_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function () {
            $('#reset_country_form').click(function () {
                $('#update_country_form')[0].reset();
            });

            $('#update_country_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'country_name':{
                        required:true,
                    },
                },
                messages:{
                    'country_name':{
                        required:"Please enter country name."
                    },
                },
                submitHandler:function (form) {
                    $('#country_update_loder').addClass("fa fa-spinner fa-pulse");
                    $('#country_update_btn').attr('disabled',true);
                    form.submit();
                    /*$('#admin_profile_submit_icon').addClass('fa fa-spinner fa-pulse');
                    var form = $('#update_admin_profile_form')[0];
                    console.log(1111);
                    var data = new FormData(form);
                    $.ajax({
                        url: javascript_site_path+'/admin/update/profile',
                        type: 'POST',
                        dataType: 'JSON',
                        processData: false,
                        contentType: false,
                        cache: false,
                        enctype: 'multipart/form-data',
                        data: data,
                        success:function(response){

                        },
                        beforeSend:function () {

                        },
                        complete:function () {

                        },
                        error:function () {

                        }
                    });*/

                }
            });

        })

    </script>
@endsection