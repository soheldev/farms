<?php

namespace Sohel\Country\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $table = 'countries';
    protected $fillable = ['country_name'];
}
