<?php

namespace Sohel\Vaccine;

use Illuminate\Support\ServiceProvider;

class VaccineServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->make('Sohel\Vaccine\VaccineController');
        $this->loadViewsFrom(__DIR__.'/views', 'vaccine');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__.'/routes.php';
    }
}
