@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url($user.'/vaccine/list')}}">List Vaccine&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Edit Vaccine</p>
                        </div>
                        <br>
                        <form class="forms-sample" id="update_vaccine_form" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputName1">Vaccine Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="vaccine_name" name="vaccine_name" placeholder="Enter Vaccine Name" value="{{$edit_vaccine->vaccine_name}}">
                                @error('vaccine_name')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Disease<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="disease" name="disease" placeholder="Enter Disease" value="{{$edit_vaccine->disease}}">
                                @error('disease')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName1">Notes</label>
                                <textarea class="form-control" id="vaccine_notes" name="vaccine_notes" placeholder="Enter Notes" value="" rows="5">{{$edit_vaccine->notes}}</textarea>
                                @error('vaccine_notes')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary mr-2" id="vaccine_update_btn"><i id="vaccine_update_loder" style="font-size:15px"></i>  Update</button>
                            <button type="button" class="btn btn-light" id="reset_update_vaccine_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function () {
            $('#reset_update_vaccine_form').click(function () {
                $('#update_vaccine_form')[0].reset();
            });

            $('#update_vaccine_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'vaccine_name':{
                        required:true,
                    },
                    'disease':{
                        required:true,
                    },
                },
                messages:{
                    'vaccine_name':{
                        required:"Please enter vaccine name."
                    },
                    'disease':{
                        required:"Please enter disease."
                    },
                },
                submitHandler:function (form) {
                    $('#vaccine_update_loder').addClass("fa fa-spinner fa-pulse");
                    $('#vaccine_update_btn').attr('disabled',true);
                    form.submit();
                    /*$('#admin_profile_submit_icon').addClass('fa fa-spinner fa-pulse');
                    var form = $('#update_admin_profile_form')[0];
                    console.log(1111);
                    var data = new FormData(form);
                    $.ajax({
                        url: javascript_site_path+'/admin/update/profile',
                        type: 'POST',
                        dataType: 'JSON',
                        processData: false,
                        contentType: false,
                        cache: false,
                        enctype: 'multipart/form-data',
                        data: data,
                        success:function(response){

                        },
                        beforeSend:function () {

                        },
                        complete:function () {

                        },
                        error:function () {

                        }
                    });*/

                }
            });

        })

    </script>
@endsection