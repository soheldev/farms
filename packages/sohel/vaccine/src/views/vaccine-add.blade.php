@php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
@endphp
@extends($extends)

@section('content')
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Add Vaccine</h2>
                </div>
                <div class="add-customer">
                    <form id="add_vaccine_form" method="POST" action="" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="vaccine_name" name="vaccine_name" placeholder="Enter Vaccine Name*" class="form-control">
                                    @error('vaccine_name')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="disease" name="disease" placeholder="Enter Disease*" class="form-control">
                                    @error('disease')
                                    <p style="color: red">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12 form-group">
                                <textarea class="form-control" id="vaccine_notes" name="vaccine_notes" placeholder="Enter Notes" value="" rows="5"></textarea>
                                @error('vaccine_notes')
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li><button type="submit" class="btn cust-btn btn-save" id="vaccine_submit_btn"><i id="vaccine_submit_loder" style="font-size:15px"></i> Save</button></li>
                                        <li><button type="button" class="btn cust-btn btn-grey" id="reset_vaccine_form">Cancel</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function () {
            $('#reset_vaccine_form').click(function () {
                $('#add_vaccine_form')[0].reset();
            });

            $('#banner_image').change(function () {
                var uploaded_image = window.URL.createObjectURL(this.files[0]);
                $('#preview_banner_image').attr('src',uploaded_image);
            });


            $('#add_vaccine_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'vaccine_name':{
                        required:true,
                    },
                    'disease':{
                        required:true,
                    },
                    /*'value':{
                        required:true,
                    }*/
                },
                messages:{
                    'vaccine_name':{
                        required:"Please enter vaccine name."
                    },
                    'disease':{
                        required:"Please enter disease.",
                    },
                    /*'value':{
                        required:"Please enter vlaue."
                    }*/
                },
                submitHandler:function (form) {
                    $('#vaccine_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#vaccine_submit_btn').attr('disabled',true);
                    form.submit();
                    /*$('#admin_profile_submit_icon').addClass('fa fa-spinner fa-pulse');
                    var form = $('#update_admin_profile_form')[0];
                    console.log(1111);
                    var data = new FormData(form);
                    $.ajax({
                        url: javascript_site_path+'/admin/update/profile',
                        type: 'POST',
                        dataType: 'JSON',
                        processData: false,
                        contentType: false,
                        cache: false,
                        enctype: 'multipart/form-data',
                        data: data,
                        success:function(response){

                        },
                        beforeSend:function () {

                        },
                        complete:function () {

                        },
                        error:function () {

                        }
                    });*/

                }
            });

        });


    </script>
@endsection