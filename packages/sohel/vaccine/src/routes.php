<?php
Route::get('/{user_type}/vaccine/list','Sohel\Vaccine\VaccineController@index')->middleware(['web','auth']);
Route::post('/fetch/vaccine-data','Sohel\Vaccine\VaccineController@fetchVaccineData')->middleware(['web','auth']);
Route::get('/{user_type}/vaccine/add','Sohel\Vaccine\VaccineController@createVaccine')->middleware(['web','auth']);
Route::post('/{user_type}/vaccine/add','Sohel\Vaccine\VaccineController@createVaccine')->middleware(['web','auth']);
Route::get('/{user_type}/vaccine/view/{vaccine_id}','Sohel\Vaccine\VaccineController@viewVaccine')->middleware(['web','auth']);
Route::get('/{user_type}/vaccine/edit/{vaccine_id}','Sohel\Vaccine\VaccineController@updateVaccine')->middleware(['web','auth']);
Route::post('/{user_type}/vaccine/edit/{vaccine_id}','Sohel\Vaccine\VaccineController@updateVaccine')->middleware(['web','auth']);
Route::post('/delete/vaccine','Sohel\Vaccine\VaccineController@deleteVaccine')->middleware(['web','auth']);

// vaccine utility route
Route::get('/vendor/utility/vaccine','Sohel\Vaccine\VaccineController@vaccineUtility')->middleware(['web','auth']);
Route::post('/vendor/utility/vaccine','Sohel\Vaccine\VaccineController@vaccineUtility')->middleware(['web','auth']);