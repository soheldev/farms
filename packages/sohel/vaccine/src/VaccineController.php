<?php

namespace Sohel\Vaccine;

use App\Http\Helpers\CheckPermission;
use App\Http\Helpers\CheckType;
use App\Http\Helpers\UniqueId;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sohel\Animal\Model\Animal;
use Sohel\Animal\Model\AnimalVacination;
use Sohel\Vaccine\Model\Vaccine;

class VaccineController extends Controller
{
    //
    public function index()
    {
        $add_vaccine_permission = false;
        $update_vaccine_permission = false;
        $delete_vaccine_permission = false;
        $add_vaccine_permission = CheckPermission::hasPermission('create.vaccine');
        $update_vaccine_permission = CheckPermission::hasPermission('update.vaccine');
        $delete_vaccine_permission = CheckPermission::hasPermission('delete.vaccine');
        return view('vaccine::vaccine-list',compact('add_vaccine_permission','update_vaccine_permission','delete_vaccine_permission'));
    }

    public function fetchVaccineData(Request $request)
    {
        $added_by = Auth::user()->id;
        $data = Vaccine::skip($request->skip)->limit(5)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function createVaccine(Request $request)
    {
        if($request->method() == "GET")
        {
            return view('vaccine::vaccine-add');
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'vaccine_name' => 'required',
                'disease' => 'required',
            ],
                $messages = [
                    'vaccine_name.required'    => 'Please enter vaccine name.',
                    'disease.required'    => 'Please enter disease.'
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $unique_number = UniqueId::generateUniqueIdentification(10);
                $vaccine_id = 'VC'.$unique_number;
                $vaccine_arr = [];
                $vaccine_arr['vaccine_id'] = $vaccine_id;
                $vaccine_arr['vaccine_name'] = $request->vaccine_name;
                $vaccine_arr['disease'] = $request->disease;
                $vaccine_arr['notes'] = $request->vaccine_notes;
                $vaccine_arr['added_by'] = Auth::user()->id;
                Vaccine::create($vaccine_arr);
                $type = CheckType::checkAuthType();
                return redirect($type.'/vaccine/list')->with(['success' => 'Vaccine Added Successfully...']);
            }
        }

    }


    public function viewVaccine($user, $vaccine_id)
    {
        $id = base64_decode($vaccine_id);
        $view_vaccine = Vaccine::find($id);
        return view('vaccine::vaccine-view',compact('view_vaccine'));
    }


    public function updateVaccine($user, $vaccine_id, Request $request)
    {
        $id = base64_decode($vaccine_id);
        $edit_vaccine = Vaccine::find($id);
        if($request->method() == "GET")
        {
            return view('vaccine::vaccine-edit',compact('edit_vaccine'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'vaccine_name' => 'required',
                'disease' => 'required',
            ],
                $messages = [
                    'vaccine_name.required'    => 'Please enter vaccine name.',
                    'disease.required'    => 'Please enter disease.'
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $edit_vaccine->vaccine_name = $request->vaccine_name;
                $edit_vaccine->disease = $request->disease;
                $edit_vaccine->notes = $request->vaccine_notes;
                $edit_vaccine->save();
                $type = CheckType::checkAuthType();
                return redirect($type.'/vaccine/list')->with(['success' => 'Vaccine Updated Successfully...']);
            }
        }
    }

    public function deleteVaccine(Request $request)
    {
        $id = base64_decode($request->id);
        $delete_vaccine = Vaccine::find($id);
        if(isset($delete_vaccine))
        {
            if($delete_vaccine->delete())
            {
                $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Vaccine Deleted Successfully...');
                return json_encode($result);
            }
            else
            {
                $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something Went Wrong...');
                return json_encode($result);
            }

        }
    }

    public function vaccineUtility(Request $request)
    {
        if($request->method() == "GET")
        {
            $animals = Animal::where('status','1')->get(['id','prefix','animal_id','animal_name']);
            $vaccines = Vaccine::all('id','vaccine_id','vaccine_name');
            return view('vaccine::utility-vaccine-add',compact('animals','vaccines'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'animal_ids' => 'required',
                'vaccine_id' => 'required',
            ],
                $messages = [
                    'animal_ids.required'    => 'Please select animals.',
                    'vaccine_id.required'    => 'Please select vaccine.',
                ]);
            if ($validator->fails())
            {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $insert_bulk_vacination_arr = [];
                foreach($request->animal_ids as $index => $animal_id)
                {
                    $insert_bulk_vacination_arr[$index]['animal_id'] = $animal_id;
                    $insert_bulk_vacination_arr[$index]['vaccine_id'] = $request->vaccine_id;
                }
                DB::table('animal_vacinations')->insert($insert_bulk_vacination_arr);
                return redirect()->back()->with(['success' => 'Vaccination Record Added Successfully...']);
            }
        }
    }
}
