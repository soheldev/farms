<?php

namespace Sohel\State;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Sohel\Country\Model\Country;
use Sohel\State\Model\State;

class StateController extends Controller
{
    //
    public function index()
    {
        return view('state::state-list');
    }

    public function fetchStateData(Request $request)
    {
        $data = State::skip($request->skip)->limit(5)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function createState(Request $request)
    {
        if($request->method() == "GET")
        {
            $countries = Country::all('id','country_name');
            return view('state::state-add',compact('countries'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'country_id' => 'required',
                'state_name' => 'required',
            ],
                $messages = [
                    'country_id.required'    => 'Please select country.',
                    'state_name.required'    => 'Please enter state name.',
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $insert_state = new State();
                $insert_state->country_id = $request->country_id;
                $insert_state->state_name = $request->state_name;
                $insert_state->save();
                return redirect('/admin/setting/state')->with(['success' => 'State Added Successfully...']);
            }
        }
    }

    public function updateState($state_id, Request $request)
    {
        $id = base64_decode($state_id);
        $edit_state = State::find($id);
        if($request->method() == "GET")
        {
            $countries = Country::all('id','country_name');
            return view('state::state-edit',compact('edit_state','countries'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'country_id' => 'required',
                'state_name' => 'required',
            ],
                $messages = [
                    'country_id.required'    => 'Please select country.',
                    'state_name.required'    => 'Please enter state name.',
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $edit_state->country_id = $request->country_id;
                $edit_state->state_name = $request->state_name;
                $edit_state->save();
                return redirect('/admin/setting/state')->with(['success' => 'State Updated Successfully...']);
            }
        }
    }

    public function deleteState(Request $request)
    {
        $id = base64_decode($request->id);
        $delete_state = State::find($id);
        if(isset($delete_state))
        {
            if($delete_state->delete())
            {
                $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'State Deleted Successfully...');
                return json_encode($result);
            }
            else
            {
                $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something Went Wrong...');
                return json_encode($result);
            }

        }
    }
}
