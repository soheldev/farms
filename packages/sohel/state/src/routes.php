<?php
Route::get('/admin/setting/state','Sohel\State\StateController@index')->middleware(['web','auth']);
Route::post('/fetch/state-data','Sohel\State\StateController@fetchStateData')->middleware(['web','auth']);
Route::get('/admin/setting/state/add','Sohel\State\StateController@createState')->middleware(['web','auth']);
Route::post('/admin/setting/state/add','Sohel\State\StateController@createState')->middleware(['web','auth']);
Route::get('/admin/setting/state/edit/{state_id}','Sohel\State\StateController@updateState')->middleware(['web','auth']);
Route::post('/admin/setting/state/edit/{state_id}','Sohel\State\StateController@updateState')->middleware(['web','auth']);
Route::post('/delete/state','Sohel\State\StateController@deleteState')->middleware(['web','auth']);