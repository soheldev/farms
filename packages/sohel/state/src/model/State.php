<?php

namespace Sohel\State\Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    //
    protected $table = 'states';
    protected $fillable = ['state_name'];
    protected $with = ['countryName'];

    public function countryName()
    {
        return $this->belongsTo('Sohel\Country\Model\Country','country_id','id');
    }
}
