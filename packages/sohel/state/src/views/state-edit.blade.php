@extends('layouts.admin.admin')

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url('/admin/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url('/admin/setting/state')}}">List State&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Edit State</p>
                        </div>
                        <br>
                        <form class="forms-sample" id="update_state_form" method="POST" action="" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="exampleInputName1">Country Name<sup style="color: red">*</sup></label>
                                <select class="form-control" name="country_id" id="country_id">
                                    <option value="">Select Country</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}" @if($edit_state->country_id == $country->id) selected @endif>{{$country->country_name}}</option>
                                    @endforeach
                                </select>
                                @error('country_id')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">State Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="state_name" name="state_name" placeholder="Enter State Name" value="{{$edit_state->state_name}}" autocomplete="off">
                                @error('state_name')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary mr-2" id="state_update_btn"><i id="state_update_loder" style="font-size:15px"></i>  Update</button>
                            <button type="button" class="btn btn-light" id="reset_state_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function () {
            $('#reset_state_form').click(function () {
                $('#update_state_form')[0].reset();
            });

            $('#update_state_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'country_id':{
                        required:true,
                    },
                    'state_name':{
                        required:true,
                    },
                },
                messages:{
                    'country_id':{
                        required:"Please select country."
                    },
                    'state_name':{
                        required:"Please enter state name."
                    },
                },
                submitHandler:function (form) {
                    $('#state_update_loder').addClass("fa fa-spinner fa-pulse");
                    $('#state_update_btn').attr('disabled',true);
                    form.submit();
                    /*$('#admin_profile_submit_icon').addClass('fa fa-spinner fa-pulse');
                    var form = $('#update_admin_profile_form')[0];
                    console.log(1111);
                    var data = new FormData(form);
                    $.ajax({
                        url: javascript_site_path+'/admin/update/profile',
                        type: 'POST',
                        dataType: 'JSON',
                        processData: false,
                        contentType: false,
                        cache: false,
                        enctype: 'multipart/form-data',
                        data: data,
                        success:function(response){

                        },
                        beforeSend:function () {

                        },
                        complete:function () {

                        },
                        error:function () {

                        }
                    });*/

                }
            });

        })

    </script>
@endsection