@extends('layouts.admin.admin')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="{{url('/admin/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="{{url('/admin/breed/list')}}">List Breed&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">View Breed</p>
                        </div>
                        <br>
                        <div class="row col-12">
                            <div class="col-6">
                                <p><h3>Breed Id :- <span>#{{$view_breed->breed_id}}</span></h3></p>
                            </div>
                            <div class="col-6">
                                <p><h3>Breed Name :- <span>{{$view_breed->breed_name}}</span></h3></p>
                            </div>
                        </div>
                        <br>
                        <div class="row col-12">
                            <div class="col-6">
                                <p><h3>Place Of Origin :- <span>{{$view_breed->place_of_origin}}</span></h3></p>
                            </div>
                            <div class="col-6">
                                <p><h3>Notes :- <span>{{isset($view_breed->notes)?$view_breed->notes:'N/A'}}</span></h3></p>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')

@endsection