@extends('layouts.master.master')

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-muted mb-0 hover-cursor" href="{{url('/vendor/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-primary mb-0 hover-cursor">Update Profile</p>
                        </div>
                        <br>
                        <h4 class="card-title">Update Profile</h4>
                        {{--<p class="card-description">
                            Basic form elements
                        </p>--}}
                        <form class="forms-sample" id="update_vendor_profile_form" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputName1">Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{$master_profile->name}}">
                                @error('name')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                                <input type="hidden" name="id" id="id" value="{{$master_profile->id}}">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">First Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{$master_profile->userInformation->first_name}}">
                                @error('name')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Last Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{$master_profile->userInformation->last_name}}">
                                @error('name')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail3">Email address<sup style="color: red">*</sup></label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$master_profile->email}}">
                                @error('email')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword4">Mobile Number<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Mobile Number" value="{{$master_profile->userInformation->mobile_number}}">
                                @error('mobile_number')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword4">Image<sup style="color: red">*</sup></label>
                                <input type="file" class="form-control" id="profile_image" name="profile_image" accept=".jpeg, .jpg, .png">
                                <br>
                                <label>Preview</label><br>
                                <img id="preview_profile_image" height="150px" width="150px" @if(isset($master_profile->userInformation->profile_image)) src="{{url('/public/master/'.$master_profile->userInformation->profile_image)}}" @else src="{{url('/public/media/backend/images/no-image.jpg')}}" @endif>
                                @error('profile_image')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>
                            {{--<div class="form-group">
                                <label for="exampleInputPassword4">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword4" placeholder="Password">
                            </div>--}}
                            {{--<div class="form-group">
                                <label for="exampleSelectGender">Gender</label>
                                <select class="form-control" id="exampleSelectGender">
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>
                            </div>--}}
                            {{--<div class="form-group">
                                <label>File upload</label>
                                <input type="file" name="img[]" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                                    <span class="input-group-append">
                          <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                                </div>
                            </div>--}}
                            {{--<div class="form-group">
                                <label for="exampleInputCity1">City</label>
                                <input type="text" class="form-control" id="exampleInputCity1" placeholder="Location">
                            </div>--}}
                            {{--<div class="form-group">
                                <label for="exampleTextarea1">Textarea</label>
                                <textarea class="form-control" id="exampleTextarea1" rows="4"></textarea>
                            </div>--}}
                            <button type="submit" class="btn btn-primary mr-2" id="vendor_profile_submit_btn"><i id="vendor_profile_submit_icon" style="font-size:15px"></i> Submit</button>
                            <button type="button" class="btn btn-light" id="reset_vendor_profile_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $('#update_vendor_profile_form').validate({
            errorClass: 'text-danger',
            rules:{
                'name':{
                    required:true,
                },
                'first_name':{
                    required:true,
                },
                'last_name':{
                    required:true,
                },
                'email':{
                    required:true,
                    remote:{
                        url: javascript_site_path+"/vendor-check-mail-duplication",
                        type: "post",
                        data:{
                            id: $('#id').val(),
                        }
                    },
                },
                'mobile_number':{
                    required:true,
                }
            },
            messages:{
                'name':{
                    required:"Please enter your farm name."
                },
                'first_name':{
                    required:"Please enter your first name."
                },
                'last_name':{
                    required:"Please enter your last name."
                },
                'email':{
                    required:"Please enter your email id.",
                    remote:"Email already exist."
                },
                'mobile_number':{
                    required:"Please enter your mobile number."
                }
            },
            submitHandler:function (form) {
                $('#vendor_profile_submit_icon').addClass("fa fa-spinner fa-pulse");
                $('#vendor_profile_submit_btn').attr('disabled',true);
                form.submit();
                /*$('#admin_profile_submit_icon').addClass('fa fa-spinner fa-pulse');
                var form = $('#update_admin_profile_form')[0];
                console.log(1111);
                var data = new FormData(form);
                $.ajax({
                    url: javascript_site_path+'/admin/update/profile',
                    type: 'POST',
                    dataType: 'JSON',
                    processData: false,
                    contentType: false,
                    cache: false,
                    enctype: 'multipart/form-data',
                    data: data,
                    success:function(response){

                    },
                    beforeSend:function () {

                    },
                    complete:function () {

                    },
                    error:function () {

                    }
                });*/

            }
        });

        $(function () {

            $('#profile_image').change(function () {
                var uploaded_image = window.URL.createObjectURL(this.files[0]);
                $('#preview_profile_image').attr('src',uploaded_image);
            });

            $('#reset_vendor_profile_form').click(function () {
                $('#update_vendor_profile_form')[0].reset();
            })
        })

    </script>
@endsection