<?php

namespace Sohel\Master;

use App\Http\Helpers\ConnectDatabase;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sohel\Animal\Model\Animal;
use Sohel\Animal\Model\AnimalDisease;
use Sohel\Breed\Model\Breed;
use Sohel\Payment\Model\Payment;
use Sohel\Payment\Model\PaymentReminder;
use Sohel\Progress\Model\AnimalProgress;
use Sohel\Progress\Model\ProgressReminder;
use Sohel\User\Model\Customer;

class MasterController extends Controller
{
    //

    /*public function __construct(Request $request)
    {
        $subdomains = explode('.', $request->getHost());
        $subdomain = $subdomains[0];
        ConnectDatabase::connectDB($subdomain);
    }*/

    public function viewMaster()
    {
        dd(111);
    }

    public function viewVendorLogin()
    {
        return view('vendor::vendor-login');
    }

    public function viewMasterDashboard()
    {
        return view('master::master-dashboard');
    }

    public function generatePendingAnimalProgressLine($pending_progress)
    {
        $pending_progress_lines_arr = [];
        $update_progress_reminder_arr = [];
        foreach($pending_progress as $index => $progress)
        {
            $pending_progress_lines_arr[$index]['animal_id'] = $progress->animal_id;
            $pending_progress_lines_arr[$index]['weight'] = 0;
            $pending_progress_lines_arr[$index]['height'] = 0;
            $pending_progress_lines_arr[$index]['due_date'] = $progress->progress_entry_date;
            $pending_progress_lines_arr[$index]['added_by'] = Auth::user()->id;

            $progress_reminder = new \DateTime($progress->progress_entry_date);
            $progress_reminder->modify('+1 months');
            $new_progress_reminder_date = $progress_reminder->format("Y-m-d");
            $update_progress_reminder_arr['progress_entry_date'] = $new_progress_reminder_date;
            DB::table('progress_reminders')->where('id',$progress->id)->update($update_progress_reminder_arr);
        }
        DB::table('animal_progress')->insert($pending_progress_lines_arr);
    }

    public function generatePendingCustomerPaymentLine($pending_payments)
    {
        $pending_payment_lines_arr = [];
        $update_payment_reminder_arr = [];
        foreach($pending_payments as $key => $pending_payment)
        {
            $pending_payment_lines_arr[$key]['animal_id'] = $pending_payment->animal_id;
            $pending_payment_lines_arr[$key]['customer_id'] = $pending_payment->paymentReminderAnimalInfo->customer_id;
            $pending_payment_lines_arr[$key]['amount_due'] = $pending_payment->paymentReminderAnimalInfo->monthly_rent;
            $pending_payment_lines_arr[$key]['amount_paid'] = 0;
            $pending_payment_lines_arr[$key]['due_date'] = $pending_payment->payment_due_date;
            $pending_payment_lines_arr[$key]['payment_status'] = '0';
            $pending_payment_lines_arr[$key]['added_by'] = Auth::user()->id;
            $pending_payment_lines_arr[$key]['created_at'] = date('Y-m-d h:m:i');

            $payment_reminder = new \DateTime($pending_payment->payment_due_date);
            $payment_reminder->modify('+1 months');
            $new_payment_due_date = $payment_reminder->format("Y-m-d");
            $update_payment_reminder_arr['payment_due_date'] = $new_payment_due_date;
            DB::table('payments_reminders')->where('id',$pending_payment->id)->update($update_payment_reminder_arr);
        }
        DB::table('payments')->insert($pending_payment_lines_arr);
    }

    public function masterLogout()
    {
        Auth::logout();
        return redirect('/login')->with(['error' => 'You have successfully logout']);
    }

    public function masterProfile(Request $request)
    {
        $master_profile = Auth::user();
        if($request->method() == 'GET')
        {
            return view('master::master-profile',compact('master_profile'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'email' => 'required',
                'mobile_number' => 'required',
                'name' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
            ],
            $messages = [
                'email.required'    => 'Please enter email id.',
                'name.required'    => 'Please enter farm name.',
                'first_name.required'    => 'Please enter first name.',
                'last_name.required'    => 'Please enter last name.',
                'mobile_number.required' => 'Please enter mobile number.',
            ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $master_profile->email = $request->email;
                $master_profile->name = $request->name;
                if($request->hasFile('profile_image'))
                {
                    if (! \File::exists(public_path()."/master")) {
                        \File::makeDirectory(public_path()."/master");
                    }
                    $imageName = time().'.'.request()->profile_image->getClientOriginalExtension();
                    request()->profile_image->move(public_path('/master/'), $imageName);
                    $master_profile->userInformation->profile_image = $imageName;
                }
                $master_profile->userInformation->first_name = $request->first_name;
                $master_profile->userInformation->last_name = $request->last_name;
                $master_profile->userInformation->mobile_number = $request->mobile_number;
                $master_profile->userInformation->save();
                $master_profile->save();
                return redirect()->back()->with(['success' => 'Profile Updated Successfully...']);



            }

        }

    }

    public function checkMasterMailDuplication(Request $request)
    {
        $check_email = User::where('email',$request->email)->where('id','<>',$request->id)->first();
        if(isset($check_email))
        {
            return 'false';
        }
        else
        {
            return 'true';
        }

    }

    public function updateMasterProfile(Request $request)
    {
        $id = $request->id;
        $name = $request->name;
        $email = $request->email;
        $mobile = $request->mobile_number;
        $update_admin_profile = User::find($id);
        if(isset($update_admin_profile)) {
            dd($update_admin_profile->userInformation);
            $update_admin_profile->name = $name;
            $update_admin_profile->email = $email;
            $update_admin_profile->save();
            $update_admin_profile->userInformation->mobile_number = $mobile;
            $update_admin_profile->userInformation->save();
        }
    }

}
