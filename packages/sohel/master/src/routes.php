<?php
//Route::get('/vendor/login','Sohel\Vendor\VendorController@viewVendorLogin')->middleware(['web']);

Route::get('/master/dashboard','Sohel\Master\MasterController@viewMasterDashboard')->middleware(['web','auth']);
Route::post('/master/logout','Sohel\Master\MasterController@masterLogout')->middleware(['web']);
Route::get('/master/profile','Sohel\Master\MasterController@masterProfile')->middleware(['web','auth']);
Route::post('/master/profile','Sohel\Master\MasterController@masterProfile')->middleware(['web','auth']);
Route::post('/master-check-mail-duplication','Sohel\Master\MasterController@checkMasterMailDuplication')->middleware(['web','auth']);
Route::post('/master/update/profile','Sohel\Master\MasterController@updateMasterProfile')->middleware(['web','auth']);



