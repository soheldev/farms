<?php

namespace Sohel\Disease;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CheckPermission;
use App\Http\Helpers\CheckType;
use App\Http\Helpers\UniqueId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Sohel\Disease\Model\Disease;


class DiseaseController extends Controller
{
    //
    public function index()
    {
        $add_disease_permission = false;
        $update_disease_permission = false;
        $delete_disease_permission = false;
        $add_disease_permission = CheckPermission::hasPermission('create.disease');
        $update_disease_permission = CheckPermission::hasPermission('update.disease');
        $delete_disease_permission = CheckPermission::hasPermission('delete.disease');
        return view('disease::disease-list',compact('add_disease_permission','update_disease_permission','delete_disease_permission'));
    }

    public function fetchDiseaseData(Request $request)
    {
        $added_by = Auth::user()->id;
        $data = Disease::skip($request->skip)->limit(5)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function createDisease(Request $request)
    {
        if($request->method() == "GET")
        {
            return view('disease::disease-add');
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'disease_name' => 'required',
            ],
                $messages = [
                    'disease_name.required'    => 'Please enter disease name.',
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $unique_number = UniqueId::generateUniqueIdentification(10);
                $disease_id = 'DS'.$unique_number;
                $disease_arr = [];
                $disease_arr['disease_id'] = $disease_id;
                $disease_arr['disease_name'] = $request->disease_name;
                $disease_arr['description'] = $request->description;
                $disease_arr['added_by'] = Auth::user()->id;
                Disease::create($disease_arr);
                $type = CheckType::checkAuthType();
                return redirect($type.'/disease/list')->with(['success' => 'Disease Added Successfully...']);
            }
        }
    }

    public function viewDisease($user, $disease_id)
    {
        $id = base64_decode($disease_id);
        $view_disease = Disease::find($id);
        return view('disease::disease-view',compact('view_disease'));
    }

    public function updateDisease($user, $disease_id, Request $request)
    {
        $id = base64_decode($disease_id);
        $edit_disease = Disease::find($id);
        if($request->method() == "GET")
        {
            return view('disease::disease-edit',compact('edit_disease'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'disease_name' => 'required',
            ],
                $messages = [
                    'disease_name.required'    => 'Please enter disease name.',
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $edit_disease->disease_name = $request->disease_name;
                $edit_disease->description = $request->description;
                $edit_disease->save();
                $type = CheckType::checkAuthType();
                return redirect($type.'/disease/list')->with(['success' => 'Disease Updated Successfully...']);

            }
        }
    }

    public function deleteDisease(Request $request)
    {
        $id = base64_decode($request->id);
        $delete_disease = Disease::find($id);
        if(isset($delete_disease))
        {
            if($delete_disease->delete())
            {
                $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'Disease Deleted Successfully...');
                return json_encode($result);
            }
            else
            {
                $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something Went Wrong...');
                return json_encode($result);
            }

        }
    }
}
