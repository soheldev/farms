<?php

namespace Sohel\Disease\Model;

use Illuminate\Database\Eloquent\Model;

class Disease extends Model
{
    //
    protected $table = 'diseases';
    protected $fillable = ['disease_id','disease_name','description','added_by'];
}
