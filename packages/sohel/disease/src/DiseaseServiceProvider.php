<?php

namespace Sohel\Disease;

use Illuminate\Support\ServiceProvider;

class DiseaseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->make('Sohel\Disease\DiseaseController');
        $this->loadViewsFrom(__DIR__.'/views', 'disease');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__.'/routes.php';
    }
}
