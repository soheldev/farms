<?php

namespace Sohel\City;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Sohel\City\Model\City;
use Sohel\Country\Model\Country;
use Sohel\State\Model\State;

class CityController extends Controller
{
    //
    public function index()
    {
        return view('city::city-list');
    }

    public function fetchCityData(Request $request)
    {

        $data = City::skip($request->skip)->limit(5)->get();
        if($data->count() > 0)
        {
            $result = array('status'=>'1','data'=>$data);
            return json_encode($result);

        }
        else
        {
            $result = array('status'=>'0');
            return json_encode($result);
        }
    }

    public function fetchStateAccordingToCountry(Request $request)
    {
        $states_arr = [];
        $country_id = $request->country_id;
        $states = State::where('country_id',$country_id)->get();
        if($states->count() > 0)
        {
            foreach($states as $key => $state)
            {
                $states_arr[$key]['id'] = $state->id;
                $states_arr[$key]['state_name'] = $state->state_name;
            }
            $result = array('status' => '1', 'data' => $states_arr);
        }
        else
        {
            $result = array('status' => '0', 'data' => $states_arr);
        }
        return json_encode($result);
    }

    public function createCity(Request $request)
    {
        if($request->method() == "GET")
        {
            $countries = Country::all('id','country_name');
            return view('city::city-add',compact('countries'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'country_id' => 'required',
                'state_id' => 'required',
                'city_name' => 'required',
            ],
                $messages = [
                    'country_id.required'    => 'Please select country.',
                    'state_id.required'    => 'Please select state.',
                    'city_name.required'    => 'Please enter city name.',
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $insert_city = new City();
                $insert_city->country_id = $request->country_id;
                $insert_city->state_id = $request->state_id;
                $insert_city->city_name = $request->city_name;
                $insert_city->save();
                return redirect('/admin/setting/city')->with(['success' => 'City Added Successfully...']);
            }
        }
    }

    public function updateCity($city_id, Request $request)
    {
        $id = base64_decode($city_id);
        $edit_city = City::find($id);
        if($request->method() == "GET")
        {
            $countries = Country::all('id','country_name');
            $states = State::all('id','state_name');
            return view('city::city-edit',compact('edit_city','countries','states'));
        }
        else
        {
            $validator = \Validator::make($request->all(), [
                'country_id' => 'required',
                'state_id' => 'required',
                'city_name' => 'required',
            ],
                $messages = [
                    'country_id.required'    => 'Please select country.',
                    'state_id.required'    => 'Please select state.',
                    'city_name.required'    => 'Please enter city name.',
                ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else
            {
                $edit_city->country_id = $request->country_id;
                $edit_city->state_id = $request->state_id;
                $edit_city->city_name = $request->city_name;
                $edit_city->save();
                return redirect('/admin/setting/city')->with(['success' => 'City Updated Successfully...']);
            }
        }
    }

    public function deleteCity(Request $request)
    {
        $id = base64_decode($request->id);
        $delete_city = City::find($id);
        if(isset($delete_city))
        {
            if($delete_city->delete())
            {
                $result = array('status'=>'1','icon' => 'success','title' => 'Deleted','text'=>'City Deleted Successfully...');
                return json_encode($result);
            }
            else
            {
                $result = array('status'=>'0','icon' => 'error','title' => 'Opps','text'=>'Something Went Wrong...');
                return json_encode($result);
            }

        }
    }
}
