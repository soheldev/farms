@extends('layouts.admin.admin')

@section('content')
    <div class="content-wrapper">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-muted mb-0 hover-cursor" href="{{url('/admin/dashboard')}}">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-primary mb-0 hover-cursor">Add City</p>
                        </div>
                        <br>
                        <form class="forms-sample" id="add_city_form" method="POST" action="" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="exampleInputName1">Country Name<sup style="color: red">*</sup></label>
                                <select class="form-control" name="country_id" id="country_id">
                                    <option value="">Select Country</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}">{{$country->country_name}}</option>
                                    @endforeach
                                </select>
                                @error('country_id')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">State Name<sup style="color: red">*</sup></label>
                                <select class="form-control" name="state_id" id="state_id">
                                    <option value="">Select State</option>
                                </select>
                                @error('country_id')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">City Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="city_name" name="city_name" placeholder="Enter City Name" value="" autocomplete="off">
                                @error('city_name')
                                {{--<div class="alert alert-danger">{{ $message }}</div>--}}
                                <p style="color: red">{{ $message }}</p>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary mr-2" id="city_submit_btn"><i id="city_submit_loder" style="font-size:15px"></i>  Submit</button>
                            <button class="btn btn-light" id="reset_city_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jcontent')
    <script>

        var javascript_site_path = '{{url('/')}}';
        $(function ()
        {
            $('#reset_city_form').click(function () {
                $('#add_city_form')[0].reset();
            });

            $('#country_id').change(function () {
               var country_id = $(this).val();
                $.ajax({
                    url: javascript_site_path+'/admin/fetch/state-according-to-country',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        country_id: country_id
                    },
                    success:function(response)
                    {
                        var html_content = '';
                        if(response.status == "1")
                        {
                            html_content += '<option value="">Select State</option>';
                            $.each(response.data,function (key,value) {
                                html_content += '<option value="'+value.id+'">'+value.state_name+'</option>';
                            });
                            $('#state_id').html(html_content);
                        }
                        else
                        {
                            $('#state_id').html('<option value="">No States Found...</option>');
                        }
                    },
                    beforeSend:function () {
                        $('#state_id').html('<option value="">Fetching States...</option>');
                    },
                    complete:function () {

                    },
                    error:function () {

                    }
                });

            });

            $('#add_city_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'country_id':{
                        required:true,
                    },
                    'state_id':{
                        required:true,
                    },
                    'city_name':{
                        required:true,
                    },
                },
                messages:{
                    'country_id':{
                        required:"Please select country."
                    },
                    'state_id':{
                        required:"Please select state."
                    },
                    'city_name':{
                        required:"Please enter city name."
                    },
                },
                submitHandler:function (form) {
                    $('#city_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#city_submit_btn').attr('disabled',true);
                    form.submit();
                    /*$('#admin_profile_submit_icon').addClass('fa fa-spinner fa-pulse');
                    var form = $('#update_admin_profile_form')[0];
                    console.log(1111);
                    var data = new FormData(form);
                    $.ajax({
                        url: javascript_site_path+'/admin/update/profile',
                        type: 'POST',
                        dataType: 'JSON',
                        processData: false,
                        contentType: false,
                        cache: false,
                        enctype: 'multipart/form-data',
                        data: data,
                        success:function(response){

                        },
                        beforeSend:function () {

                        },
                        complete:function () {

                        },
                        error:function () {

                        }
                    });*/

                }
            });

        });


    </script>
@endsection