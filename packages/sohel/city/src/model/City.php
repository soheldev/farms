<?php

namespace Sohel\City\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    protected $table = 'cities';
    protected $fillable = ['city_name'];
    protected $with = ['stateName'];

    /*public function countryName()
    {
        return $this->belongsTo('Sohel\Country\Model\Country','country_id','id');
    }*/

    public function stateName()
    {
        return $this->belongsTo('Sohel\State\Model\State','state_id','id');
    }
}
