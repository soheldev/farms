-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2020 at 11:36 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mustafa_farms`
--

-- --------------------------------------------------------

--
-- Table structure for table `animals`
--

CREATE TABLE `animals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `prefix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `animal_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `animal_type` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '''0'' => ''Goat'',''1'' => ''Sheep''',
  `animal_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `breed_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '''0'' => ''Andul'',''1'' => ''Khassi'',''2'' => ''Female''',
  `customer_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age_in_month` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_owned_by_farm` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '''0'' => ''no'',''1'' => ''yes''',
  `notes` text COLLATE utf8mb4_unicode_ci,
  `entry_date` date DEFAULT NULL,
  `entry_weight` decimal(14,2) NOT NULL DEFAULT '0.00',
  `entry_height` decimal(14,2) NOT NULL DEFAULT '0.00',
  `monthly_rent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '''0'' =>''Exit'',''1'' => ''Not Exit''',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `animals`
--

INSERT INTO `animals` (`id`, `prefix`, `animal_id`, `animal_type`, `animal_name`, `breed_id`, `gender`, `customer_id`, `age_in_month`, `date_of_birth`, `is_owned_by_farm`, `notes`, `entry_date`, `entry_weight`, `entry_height`, `monthly_rent`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 'GA', '8299822554', '0', 'Jugoonu', '1', '0', '9', '5', '18/9/2019', '0', 'Nice animal', '2020-01-20', '12.00', '1.80', '2300', '6', '1', '2020-05-24 05:12:52', '2020-05-24 05:12:52'),
(4, 'GA', '0187789684', '0', 'Moracco', '2', '0', '9', '11', '17/7/2019', '0', 'Nice Animal', '2020-06-01', '18.00', '3.80', '2550', '6', '1', '2020-05-31 21:46:22', '2020-05-31 21:46:22');

-- --------------------------------------------------------

--
-- Table structure for table `animal_dewormings`
--

CREATE TABLE `animal_dewormings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `animal_id` bigint(20) DEFAULT NULL,
  `deworming_id` bigint(20) DEFAULT NULL,
  `predefine_body_weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `predefine_dosage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actual_body_weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actual_dosage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `deworming_date` date DEFAULT NULL,
  `deworming_status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '''0'' => ''pending'',''1'' => ''completed'',''2'' => ''missed''',
  `notes` text COLLATE utf8mb4_unicode_ci,
  `added_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `animal_dewormings`
--

INSERT INTO `animal_dewormings` (`id`, `animal_id`, `deworming_id`, `predefine_body_weight`, `predefine_dosage`, `actual_body_weight`, `actual_dosage`, `due_date`, `deworming_date`, `deworming_status`, `notes`, `added_by`, `created_at`, `updated_at`) VALUES
(2, 4, 1, '3', '1', NULL, NULL, '2020-06-04', NULL, '0', NULL, '6', '2020-05-31 21:46:22', '2020-05-31 21:46:22');

-- --------------------------------------------------------

--
-- Table structure for table `animal_diseases`
--

CREATE TABLE `animal_diseases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `animal_id` bigint(20) NOT NULL,
  `disease_id` bigint(20) NOT NULL,
  `diagnosed_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `treatment` text COLLATE utf8mb4_unicode_ci,
  `recovered_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `animal_progress`
--

CREATE TABLE `animal_progress` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `animal_id` bigint(20) NOT NULL,
  `weight` decimal(8,2) NOT NULL DEFAULT '0.00',
  `height` decimal(8,2) NOT NULL DEFAULT '0.00',
  `capture_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'YYYY-MM-DD',
  `due_date` date DEFAULT NULL COMMENT 'YYYY-MM-DD',
  `notes` text COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '''0'' => ''pending'',''1'' => ''completed''',
  `added_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `animal_progress`
--

INSERT INTO `animal_progress` (`id`, `animal_id`, `weight`, `height`, `capture_date`, `due_date`, `notes`, `date`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, '12.00', '1.80', '2020-01-20', '2020-01-20', NULL, '2020-05-24', '1', '6', '2020-05-24 05:12:52', '2020-05-24 05:12:52'),
(2, 1, '18.00', '6.80', '2020-02-21', '2020-02-20', 'Nice progress', '2020-05-24', '1', '6', '2020-05-24 10:42:52', '2020-05-24 10:42:52'),
(3, 1, '25.00', '10.50', '2020-03-21', '2020-03-20', 'Nice progress', '2020-05-24', '1', '6', '2020-05-24 10:42:53', '2020-05-24 10:42:53'),
(4, 1, '35.00', '18.80', '2020-04-21', '2020-04-20', 'Nice progress', '2020-05-24', '1', '6', '2020-05-24 10:42:53', '2020-05-24 10:42:53'),
(5, 1, '43.00', '21.50', '2020-05-24', '2020-05-20', 'NIce progress', '2020-05-24', '1', '6', '2020-05-24 10:42:53', '2020-05-24 10:42:53'),
(8, 4, '18.00', '3.80', '2020-06-1', '2020-06-01', NULL, '2020-06-01', '1', '6', '2020-05-31 21:46:22', '2020-05-31 21:46:22');

-- --------------------------------------------------------

--
-- Table structure for table `animal_vacinations`
--

CREATE TABLE `animal_vacinations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `animal_id` bigint(20) DEFAULT NULL,
  `vaccine_id` bigint(20) DEFAULT NULL,
  `dose_number` int(11) DEFAULT NULL,
  `vacination_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight_on_vacination_day` decimal(8,2) DEFAULT NULL,
  `quantity` decimal(8,2) DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `breeds`
--

CREATE TABLE `breeds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `breed_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `breed_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_of_origin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `breeds`
--

INSERT INTO `breeds` (`id`, `breed_id`, `breed_name`, `place_of_origin`, `notes`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'B2496577962', 'Breed 1', 'Pune', NULL, 6, '2020-05-16 03:48:11', '2020-05-16 03:48:11'),
(2, 'B1853110005', 'Breed 2', 'Mumbai', NULL, 6, '2020-05-16 03:48:27', '2020-05-16 03:48:27'),
(3, 'B7656846465', 'Breed 3', 'Nasik', NULL, 6, '2020-05-16 03:48:41', '2020-05-16 03:48:41'),
(4, 'B4587910941', 'Breed 5', 'Nagpur', NULL, 6, '2020-05-16 03:48:57', '2020-05-16 03:48:57'),
(5, 'B6868599571', 'Breed 4', 'Rajasthan', NULL, 6, '2020-05-16 03:49:21', '2020-05-16 03:49:21');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_id` bigint(20) UNSIGNED DEFAULT NULL,
  `state_id` bigint(20) UNSIGNED DEFAULT NULL,
  `city_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `country_id`, `state_id`, `city_name`, `created_at`, `updated_at`) VALUES
(3, 2, 4, 'Pune', '2020-05-03 09:23:06', '2020-05-03 09:23:06'),
(4, 2, 4, 'Mumbai', '2020-05-03 09:23:18', '2020-05-03 09:23:18'),
(5, 2, 6, 'Abc', '2020-05-09 05:52:28', '2020-05-09 05:52:28');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'This column holds country name',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_name`, `created_at`, `updated_at`) VALUES
(2, 'India', '2020-05-03 04:45:23', '2020-05-03 04:45:23'),
(3, 'UAE', '2020-05-03 04:46:23', '2020-05-03 04:46:33');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `customer_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_line_one` text COLLATE utf8mb4_unicode_ci,
  `address_line_two` text COLLATE utf8mb4_unicode_ci,
  `pin_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `joining_date` date DEFAULT NULL,
  `channel` enum('0','1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3' COMMENT '''0'' => ''youtube'',''1'' => ''facebook'',''2'' => ''refered_by_someone'',''3'' => ''others''',
  `notes` text COLLATE utf8mb4_unicode_ci,
  `no_of_animals` int(11) DEFAULT NULL,
  `status` enum('0','1','2','') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '''0'' => ''Inactive'',''1'' => ''Active'',''2'' => ''blocked''',
  `added_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `customer_id`, `customer_unique_id`, `profile_image`, `first_name`, `last_name`, `email`, `contact_no`, `address_line_one`, `address_line_two`, `pin_code`, `city_id`, `state_id`, `country_id`, `occupation`, `joining_date`, `channel`, `notes`, `no_of_animals`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(2, 9, 'C3022829899', '1590316613.jpg', 'Sohel', 'Patel', 'sohelp@gmail.com', '9585748596', 'Kondwa Pune 48', NULL, '411048', '3', '4', '2', 'Employee', '2020-05-24', '2', 'Refered by Mr.khan', 2, '1', '6', '2020-05-24 05:06:53', '2020-05-24 05:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `customer_animal_exists`
--

CREATE TABLE `customer_animal_exists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `animal_id` bigint(20) NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `animal_entry_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `animal_entry_weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `animal_entry_height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `animal_exit_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `animal_exit_weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `animal_exit_height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exit_reason` text COLLATE utf8mb4_unicode_ci,
  `animal_weight_gain` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `animal_height_gain` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dewormings`
--

CREATE TABLE `dewormings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deworming_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'This column hold unique deworming id',
  `deworming_agent_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'This column hold name of deworming agent',
  `body_weight` bigint(20) DEFAULT NULL,
  `dosage` bigint(20) DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dewormings`
--

INSERT INTO `dewormings` (`id`, `deworming_id`, `deworming_agent_name`, `body_weight`, `dosage`, `notes`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'DW8565915885', 'Fentas Plus', 3, 1, NULL, 6, '2020-05-28 13:02:27', '2020-05-28 14:01:56'),
(2, 'DW3751835717', 'Neozide Plus', 4, 1, NULL, 6, '2020-05-28 13:43:02', '2020-05-28 13:43:02');

-- --------------------------------------------------------

--
-- Table structure for table `diseases`
--

CREATE TABLE `diseases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `disease_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'This column hold unique disease id',
  `disease_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'This column hold name of disease',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `diseases`
--

INSERT INTO `diseases` (`id`, `disease_id`, `disease_name`, `description`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'DS6272761228', 'Muscle Pain', 'Here there will be very huge muscle pain', 6, '2020-05-16 03:59:01', '2020-05-16 03:59:01'),
(2, 'DS5677535024', 'Fever', 'Here there will be huge fever', 6, '2020-05-16 03:59:18', '2020-05-16 03:59:18'),
(3, 'DS3609251502', 'Cough', 'Here there will be huge cough', 6, '2020-05-16 03:59:42', '2020-05-16 03:59:42'),
(4, 'DS8510096594', 'Ommitting And Loose Motion', 'Here there will be huge ommitting and loose motion', 6, '2020-05-16 04:04:43', '2020-05-16 04:04:43');

-- --------------------------------------------------------

--
-- Table structure for table `global_values`
--

CREATE TABLE `global_values` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_image` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `global_values`
--

INSERT INTO `global_values` (`id`, `title`, `slug`, `is_image`, `value`, `created_at`, `updated_at`) VALUES
(1, 'Site Title', 'site-title', '0', 'Zapper', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_12_27_194314_create_user_information_table', 1),
(4, '2020_01_07_184725_create_global_values_table', 1),
(5, '2020_05_01_143148_banner_images', 2),
(6, '2020_05_01_201452_create_breed', 3),
(7, '2020_05_03_022933_create_vaccine', 4),
(9, '2020_05_03_044555_create_diseases', 5),
(10, '2020_05_03_094336_create_countries', 6),
(11, '2020_05_03_102713_create_states', 7),
(12, '2020_05_03_131808_create_cities', 8),
(14, '2020_05_05_045018_create_customers', 9),
(15, '2020_05_06_003207_create_animals', 10),
(16, '2020_05_09_013915_create_animal_vacinations', 11),
(17, '2020_05_11_004053_create_animal_diseases', 12),
(18, '2020_05_11_040905_create_customer_animal_exists', 13),
(19, '2020_05_12_021430_create_progress_reminders', 14),
(20, '2020_05_12_051535_create_animal_progress', 15),
(21, '2020_05_16_125427_create_payments', 16),
(22, '2020_05_16_131005_create_payments_reminders', 16),
(23, '2020_05_28_174422_create_dewormings', 17),
(24, '2020_05_31_175641_create_animal_dewormings', 18);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `animal_id` bigint(20) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `amount_due` bigint(20) DEFAULT NULL,
  `amount_paid` bigint(20) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `mode_of_payment` enum('0','1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '''0'' => ''cash'',''1'' => ''bank_transfer'',''2'' => ''google_pay'',''3'' => ''phone_pay''',
  `payment_status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '''0'' => ''pending'',''1'' => ''completed'',''2'' => ''cancelled''',
  `notes` text COLLATE utf8mb4_unicode_ci,
  `added_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `animal_id`, `customer_id`, `amount_due`, `amount_paid`, `due_date`, `payment_date`, `mode_of_payment`, `payment_status`, `notes`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 9, 2300, 2300, '2020-01-20', '2020-01-21', '0', '1', 'Paid in cash', '6', '2020-05-24 04:35:42', '2020-05-24 05:12:52'),
(2, 1, 9, 2300, 0, '2020-02-20', NULL, '0', '0', NULL, '6', '2020-05-24 04:35:42', NULL),
(3, 1, 9, 2300, 0, '2020-03-20', NULL, '0', '0', NULL, '6', '2020-05-24 04:35:42', NULL),
(4, 1, 9, 2300, 0, '2020-04-20', NULL, '0', '0', NULL, '6', '2020-05-24 04:35:42', NULL),
(5, 1, 9, 2300, 0, '2020-05-20', NULL, '0', '0', NULL, '6', '2020-05-24 04:35:42', NULL),
(8, 4, 9, 2550, 2550, '2020-06-01', '2020-06-01', '0', '1', NULL, '6', '2020-05-31 21:36:16', '2020-05-31 21:46:22');

-- --------------------------------------------------------

--
-- Table structure for table `payments_reminders`
--

CREATE TABLE `payments_reminders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `animal_id` bigint(20) DEFAULT NULL,
  `payment_due_date` date DEFAULT NULL COMMENT '(YYYY-MM-DD)',
  `payment_tracker` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '''0'' => ''payment_tracker_on'',''1'' => ''payment_tracker_off''',
  `added_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments_reminders`
--

INSERT INTO `payments_reminders` (`id`, `animal_id`, `payment_due_date`, `payment_tracker`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-06-20', '0', '6', '2020-05-24 05:12:52', '2020-05-24 05:12:52'),
(4, 4, '2020-07-01', '0', '6', '2020-05-31 21:46:22', '2020-05-31 21:46:22');

-- --------------------------------------------------------

--
-- Table structure for table `progress_reminders`
--

CREATE TABLE `progress_reminders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `animal_id` bigint(20) DEFAULT NULL,
  `progress_entry_date` date DEFAULT NULL COMMENT 'YYYY-MM-DD',
  `progress_tracker` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '''0'' => ''progress_reminder_on'',''1'' => ''progress_reminder_off''',
  `added_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `progress_reminders`
--

INSERT INTO `progress_reminders` (`id`, `animal_id`, `progress_entry_date`, `progress_tracker`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-06-20', '0', '6', '2020-05-24 05:12:52', '2020-05-24 05:12:52'),
(4, 4, '2020-07-01', '0', '6', '2020-05-31 21:46:22', '2020-05-31 21:46:22');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_id` bigint(20) UNSIGNED DEFAULT NULL,
  `state_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `country_id`, `state_name`, `created_at`, `updated_at`) VALUES
(4, 2, 'Maharashtra', '2020-05-03 07:40:00', '2020-05-03 07:40:00'),
(5, 2, 'Mumbai', '2020-05-04 12:08:26', '2020-05-04 12:08:26'),
(6, 2, 'Karnatak', '2020-05-09 05:52:11', '2020-05-09 05:52:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` enum('0','1','2','3','4') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '''0''=>''Super_admin'',''1'' => ''admin'',''2'' => ''sub_admin'',''3'' => ''vendor'',''4'' => ''Customer''',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'superadmin@gmail.com', NULL, '$2y$10$F01AWM1YjcFr8MFTaL5PpuNkWGkjgCJJgRvn16H6hqM84NaemlLmC', NULL, '1', '2020-05-01 08:57:06', '2020-05-01 08:57:06'),
(6, 'Bakshi Farms', 'bakshifarms@gmail.com', NULL, '$2y$10$CZPnx.oFTJNx.9KskX.SmOBrxnItraIerWgfmQRVTDPYlkm03H1BK', NULL, '3', '2020-05-04 12:53:28', '2020-05-04 12:53:28'),
(7, 'Ahefaz Farms', 'ahefazfarms@gmail.com', NULL, '$2y$10$f4sHPbonbYzFxmUVZ9Knve9imJuqSEw9K5oNyDiGtGpyQYlxIPgdG', NULL, '3', '2020-05-04 12:55:08', '2020-05-04 12:55:08'),
(9, 'Sohel Patel', 'sohelp@gmail.com', NULL, '$2y$10$NTPm3P4U9kNyb0cS.vEe.uqte4FIXynvibGW5fDLdVIi7GDqvT25O', NULL, '4', '2020-05-24 05:06:53', '2020-05-24 05:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `user_informations`
--

CREATE TABLE `user_informations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_unique_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monthly_rent` bigint(20) DEFAULT NULL,
  `country_id` bigint(20) UNSIGNED DEFAULT NULL,
  `state_id` bigint(20) UNSIGNED DEFAULT NULL,
  `city_id` bigint(20) UNSIGNED DEFAULT NULL,
  `address_line_one` text COLLATE utf8mb4_unicode_ci,
  `address_line_two` text COLLATE utf8mb4_unicode_ci,
  `pin_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` enum('0','1','2','3','4') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '''0''=>''Super_admin'',''1'' => ''admin'',''2'' => ''sub_admin'',''3'' => ''vendor'',''4'' => ''Customer''',
  `user_status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '''0''=>''inactive'',''1''=>''active'',''2''=>''blocked''',
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_informations`
--

INSERT INTO `user_informations` (`id`, `user_id`, `user_unique_id`, `profile_image`, `first_name`, `last_name`, `monthly_rent`, `country_id`, `state_id`, `city_id`, `address_line_one`, `address_line_two`, `pin_code`, `user_type`, `user_status`, `mobile_number`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, '1588343379.jpg', 'Sohel', 'Patel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '8766409349', '2020-05-01 08:57:06', '2020-05-01 08:59:39'),
(4, 6, 'FR0962546135', '1588616608.jpg', 'Javed', 'Khan', 15, 2, 4, 3, 'Shivneri nagar lane no 3, Kondwa', NULL, '411048', '3', '1', '9545951720', '2020-05-04 12:53:28', '2020-05-05 07:11:21'),
(5, 7, 'FR2465650823', '1588616708.png', 'Ahefaz', 'Shaikh', 12, 2, 4, 3, 'Shivneri nagar lane no 4, Kondwa', NULL, '411048', '3', '1', '9545951730', '2020-05-04 12:55:08', '2020-05-04 12:55:08');

-- --------------------------------------------------------

--
-- Table structure for table `vaccines`
--

CREATE TABLE `vaccines` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vaccine_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'This column hold unique vaccine id',
  `vaccine_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'This column hold name of vaccine',
  `disease` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vaccines`
--

INSERT INTO `vaccines` (`id`, `vaccine_id`, `vaccine_name`, `disease`, `notes`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'VC3561817168', 'Dolo Vaccine', 'Muscle Pain', 'This vaccine is used for muscle pain', 6, '2020-05-16 03:50:17', '2020-05-16 03:55:26'),
(2, 'VC2321050448', 'Crosin Vaccine', 'Fever', 'This vaccine is used for fever', 6, '2020-05-16 03:50:56', '2020-05-16 03:55:56'),
(3, 'VC5539702348', 'Benedryl Vaccine', 'Cough', 'This vaccine is used for coughing', 6, '2020-05-16 03:51:28', '2020-05-16 03:56:16'),
(4, 'VC0088296585', 'Tramsed Vaccine', 'Ommiting and Losse Motion', 'This vaccine is used for ommitting and loose motion', 6, '2020-05-16 03:52:33', '2020-05-16 03:56:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `animals`
--
ALTER TABLE `animals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `animal_dewormings`
--
ALTER TABLE `animal_dewormings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `animal_diseases`
--
ALTER TABLE `animal_diseases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `animal_progress`
--
ALTER TABLE `animal_progress`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `animal_vacinations`
--
ALTER TABLE `animal_vacinations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `breeds`
--
ALTER TABLE `breeds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `state_id` (`state_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_animal_exists`
--
ALTER TABLE `customer_animal_exists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dewormings`
--
ALTER TABLE `dewormings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diseases`
--
ALTER TABLE `diseases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `global_values`
--
ALTER TABLE `global_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments_reminders`
--
ALTER TABLE `payments_reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `progress_reminders`
--
ALTER TABLE `progress_reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_informations`
--
ALTER TABLE `user_informations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `vaccines`
--
ALTER TABLE `vaccines`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `animals`
--
ALTER TABLE `animals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `animal_dewormings`
--
ALTER TABLE `animal_dewormings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `animal_diseases`
--
ALTER TABLE `animal_diseases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `animal_progress`
--
ALTER TABLE `animal_progress`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `animal_vacinations`
--
ALTER TABLE `animal_vacinations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `breeds`
--
ALTER TABLE `breeds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer_animal_exists`
--
ALTER TABLE `customer_animal_exists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dewormings`
--
ALTER TABLE `dewormings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `diseases`
--
ALTER TABLE `diseases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `global_values`
--
ALTER TABLE `global_values`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `payments_reminders`
--
ALTER TABLE `payments_reminders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `progress_reminders`
--
ALTER TABLE `progress_reminders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_informations`
--
ALTER TABLE `user_informations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vaccines`
--
ALTER TABLE `vaccines`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `states`
--
ALTER TABLE `states`
  ADD CONSTRAINT `states_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_informations`
--
ALTER TABLE `user_informations`
  ADD CONSTRAINT `user_informations_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
