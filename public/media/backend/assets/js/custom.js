jQuery(document).ready(function(){
//Sticky header
jQuery(window).scroll(function(){
	var sticky = jQuery('body'),
	    scroll = jQuery(window).scrollTop();
		
	if(scroll >= 200) sticky.addClass('sticky-header');
	else sticky.removeClass('sticky-header');	
});

jQuery('.cust-inp label').click(function(){
	(this).toggleClass('chng-label')
});
jQuery(".mobile-menu").click(function(){
  jQuery("body").toggleClass("open-main-menu"); 	
});
jQuery(".left-humberger").click(function(){
  jQuery("body").toggleClass("open-left-menu"); 	
});


jQuery('#our-team-slider').owlCarousel({
	    loop:true,
		items:3,
		nav:true,
		dots:false,
		autoplay:true,
		stopOnHover:true,
		autoplayTimeout:5000,
		smartSpeed:2500,
		navText: [
		           "<i class='fa fa-angle-left'></i>",
				   "<i class='fa fa-angle-right'></i>"
		         ],
        responsive:{
			320:{
				items:1
			},
			480:{
				items:2
			},
			600:{
				items:2
			},
			767:{
				items:3
			},
			992:{
				items:3
			}
		}
});
jQuery('.pop-events').owlCarousel({
	    loop:true,
		items:1,
		nav:false,
		dots:true,
		autoplay:true,
		stopOnHover:true,
		autoplayTimeout:5000,
		smartSpeed:2500,
		margin:10,
		 responsive:{
			320:{
				items:1
			},
			480:{
				items:2
			},
			600:{
				items:2
			},
			767:{
				items:2
			},
			992:{
				items:3
			}
		}
});
jQuery('.simil-cour').owlCarousel({
	    loop:true,
		items:1,
		nav:false,
		dots:true,
		autoplay:true,
		stopOnHover:true,
		autoplayTimeout:5000,
		smartSpeed:2500,
		margin:10,
});
	
});

$(function() {
			var selectedClass = "";
			$(".fil-cat").click(function(){ 
			selectedClass = $(this).attr("data-rel"); 
		 $("#portfolio").fadeTo(100, 0.1);
			$("#portfolio div").not("."+selectedClass).fadeOut().removeClass('scale-anm');
		setTimeout(function() {
		  $("."+selectedClass).fadeIn().addClass('scale-anm');
		  $("#portfolio").fadeTo(300, 1);
		}, 300); 
			
		});
	});
