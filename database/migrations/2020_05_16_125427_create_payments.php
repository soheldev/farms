<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('animal_id')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->bigInteger('amount_due')->nullable();
            $table->bigInteger('amount_paid')->nullable();
            $table->date('due_date')->nullable();
            $table->date('payment_date')->nullable();
            $table->enum('mode_of_payment',['0','1','2','3'])->default('0')->comment("'0' => 'cash','1' => 'bank_transfer','2' => 'google_pay','3' => 'phone_pay'");
            $table->enum('payment_status',['0','1','2'])->default('0')->comment("'0' => 'pending','1' => 'completed','2' => 'cancelled'");
            $table->text('notes')->nullable();
            $table->string('added_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
