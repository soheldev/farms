<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgressReminders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('progress_reminders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('animal_id')->nullable();
            $table->date('progress_entry_date')->nullable()->comment("YYYY-MM-DD");
            $table->enum('progress_tracker',['0','1'])->default('0')->comment("'0' => 'progress_reminder_on','1' => 'progress_reminder_off'");
            $table->string('added_by')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('progress_reminders');
    }
}
