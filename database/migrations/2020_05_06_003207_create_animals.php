<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('prefix')->nullable();
            $table->string('animal_id')->nullable();
            $table->enum('animal_type',['0','1'])->default('0')->comment("'0' => 'Goat','1' => 'Sheep'");
            $table->string('animal_name')->nullable();
            $table->string('breed_id')->nullable();
            $table->enum('gender',['0','1','2'])->default('0')->comment("'0' => 'Andul','1' => 'Khassi','2' => 'Female'");
            $table->string('customer_id')->nullable();
            $table->string('age_in_month')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->enum('is_owned_by_farm',['0','1'])->default('0')->comment("'0' => 'no','1' => 'yes'");
            $table->text('notes')->nullable();
            $table->date('entry_date')->nullable();
            $table->decimal('entry_weight')->default(0.00);
            $table->decimal('entry_height')->default(0.00);
            $table->string('monthly_rent')->nullable();
            $table->string('added_by')->nullable();
            $table->enum('status',['0','1'])->default('1')->comment("'0' => 'Exit','1' => 'Not Exit'");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}
