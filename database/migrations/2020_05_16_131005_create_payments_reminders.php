<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsReminders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments_reminders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('animal_id')->nullable();
            $table->date('payment_due_date')->comment("(YYYY-MM-DD)")->nullable();
            $table->enum('payment_tracker',['0','1'])->default('0')->comment("'0' => 'payment_tracker_on','1' => 'payment_tracker_off'");
            $table->string('added_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments_reminders');
    }
}
