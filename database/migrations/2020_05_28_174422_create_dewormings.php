<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDewormings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dewormings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('deworming_id')->nullable()->comment('This column hold unique deworming id');
            $table->string('deworming_agent_name')->nullable()->comment('This column hold name of deworming agent');
            $table->bigInteger('body_weight')->nullable();
            $table->bigInteger('dosage')->nullable();
            $table->string('notes')->nullable();
            $table->integer('added_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dewormings');
    }
}
