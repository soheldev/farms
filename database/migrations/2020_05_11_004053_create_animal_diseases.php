<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalDiseases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animal_diseases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('animal_id');
            $table->bigInteger('disease_id');
            $table->string('diagnosed_date')->nullable();
            $table->text('treatment')->nullable();
            $table->string('recovered_date')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_diseases');
    }
}
