<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalDewormings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animal_dewormings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('animal_id')->nullable();
            $table->bigInteger('deworming_id')->nullable();
            $table->string('predefine_body_weight')->nullable();
            $table->string('predefine_dosage')->nullable();
            $table->string('actual_body_weight')->nullable();
            $table->string('actual_dosage')->nullable();
            $table->date('due_date')->nullable();
            $table->date('deworming_date')->nullable();
            $table->enum('deworming_status',['0','1','2'])->default('0')->comment("'0' => 'pending','1' => 'completed','2' => 'missed'");
            $table->text('notes')->nullable();
            $table->string('added_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_dewormings');
    }
}
