<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalVacinations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animal_vacinations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('animal_id');
            $table->bigInteger('vaccine_id');
            $table->integer('dose_number');
            $table->string('vacination_date');
            $table->decimal('weight_on_vacination_day');
            $table->decimal('quantity');
            $table->text('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_vacinations');
    }
}
