<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalProgress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animal_progress', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('animal_id');
            $table->decimal('weight')->default(0.00);
            $table->decimal('height')->default(0.00);
            $table->string('capture_date')->nullable()->comment("YYYY-MM-DD");
            $table->date('due_date')->nullable()->comment("YYYY-MM-DD");
            $table->text('notes')->nullable();
            $table->date('date')->nullable();
            $table->string('added_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_progress');
    }
}
