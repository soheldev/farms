<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerAnimalExists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_animal_exists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('animal_id');
            $table->bigInteger('customer_id');
            $table->string('animal_entry_date')->nullable();
            $table->string('animal_entry_weight')->nullable();
            $table->string('animal_entry_height')->nullable();
            $table->string('animal_exit_date')->nullable();
            $table->string('animal_exit_weight')->nullable();
            $table->string('animal_exit_height')->nullable();
            $table->text('exit_reason')->nullable();
            $table->string('animal_weight_gain')->nullable();
            $table->string('animal_height_gain')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_animal_exists');
    }
}
