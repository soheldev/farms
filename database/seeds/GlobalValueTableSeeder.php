<?php

use Illuminate\Database\Seeder;

class GlobalValueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \Illuminate\Support\Facades\DB::table('global_values')->delete();

        \Illuminate\Support\Facades\DB::table('global_values')->insert(['title' => 'Site Title','slug' => 'site-title','value' => 'Zapper']);
    }
}
