<?php

use Illuminate\Database\Seeder;

class UserInformationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \Illuminate\Support\Facades\DB::table('user_informations')->delete();

        \App\UserInformation::create(['first_name' => 'Sohel','last_name' => 'Patel','user_type' => '1','user_status' => '1','mobile_number' => '8766409349']);
    }
}
