<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \Illuminate\Support\Facades\DB::table('users')->delete();

        \App\User::create(['name' => 'Super Admin','email' => 'superadmin@gmail.com','password' => bcrypt('123456789')]);
    }
}
