<?php

use Illuminate\Database\Seeder;

class VaccineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \Illuminate\Support\Facades\DB::table('vaccines')->delete();

        \Illuminate\Support\Facades\DB::table('vaccines')->insert(['vaccine_id' => 'V1234567891','vaccine_name' => 'Vaccine One','disease' => 'Disease One']);
    }
}
