<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Master Dashboard</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?php echo e(url('/public/media/backend')); ?>/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?php echo e(url('/public/media/backend')); ?>/vendors/base/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="<?php echo e(url('/public/media/backend')); ?>/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?php echo e(url('/public/media/backend')); ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo e(url('/public/media/backend')); ?>/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo e(url('/public/media/backend')); ?>/css/select2.min.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="<?php echo e(url('/public/media/backend')); ?>/images/favicon.png" />
</head>
<body>

<style>
    .btn {
        line-height: 0 !important;
    }
</style>

<div class="container-scroller">
    <?php echo $__env->make('layouts.master.master_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:partials/_sidebar.html -->
            <?php echo $__env->make('layouts.master.master_side_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- partial -->
            
            <div class="main-panel">
                <?php echo $__env->yieldContent('content'); ?>
                <?php echo $__env->make('layouts.master.master_footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->



<!-- plugins:js -->
<script src="<?php echo e(url('/public/media/backend')); ?>/vendors/base/vendor.bundle.base.js"></script>
<!-- endinject -->
<script src="<?php echo e(url('/public/media/backend')); ?>/js/jquery-3.4.1.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/js/jquery-ui.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/js/select2.min.js"></script>
<!-- Plugin js for this page-->
<script src="<?php echo e(url('/public/media/backend')); ?>/vendors/chart.js/Chart.min.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/vendors/datatables.net/jquery.dataTables.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="<?php echo e(url('/public/media/backend')); ?>/js/off-canvas.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/js/hoverable-collapse.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/js/template.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="<?php echo e(url('/public/media/backend')); ?>/js/dashboard.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/js/data-table.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/js/jquery.dataTables.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/js/dataTables.bootstrap4.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/js/jquery.validate.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/js/sweet-alert.js"></script>
<!-- End custom js for this page-->
<?php echo $__env->yieldContent('jcontent'); ?>
</body>

</html><?php /**PATH C:\xampp\htdocs\farms\resources\views/layouts/master/master.blade.php ENDPATH**/ ?>