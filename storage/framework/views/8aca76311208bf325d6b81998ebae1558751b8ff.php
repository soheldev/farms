<?php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
?>

<?php $__env->startSection('content'); ?>
    <div class="db-body">
        <div class="inner-forms">
            <?php if(session('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo e(session('success')); ?>

                </div>
            <?php endif; ?>
            <div class="box ">
                <div class="box-title clearfix">
                    <h2 class="pull-left">List Disease</h2>
                    <?php if($add_disease_permission == true): ?>
                        <div class="top-btn pull-right">
                            <a class="btn cust-btn btn-blue-1" href="<?php echo e(url($user.'/disease/add')); ?>"><i class="fa fa-plus"></i> Add New</a>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="table-resposnive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Disease Id</th>
                            <th>Disease Name</th>
                            <?php if($update_disease_permission == true || $delete_disease_permission == true): ?>
                                <th>Action</th>
                            <?php endif; ?>
                        </tr>
                        </thead>
                        <tbody id="disease_table">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('jcontent'); ?>
    <script>
        var site_path = '<?php echo e(url('/')); ?>';
        var user = '<?php echo e($user); ?>';
        var skip = 0;
        var i = 0;
        var update_disease_permission = '<?php echo e($update_disease_permission); ?>';
        var delete_disease_permission = '<?php echo e($delete_disease_permission); ?>';
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/disease-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td><a href="'+site_path+''+user+'/disease/view/'+btoa(this.id)+'">'+this.disease_id+'</a></td>';
                            html_content += '<td>'+this.disease_name+'</td>';
                            //html_content += '<td><center><a title="Edit" href="'+site_path+''+user+'/disease/edit/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; <a title="delete" href="javascript:void(0)" class="disease_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></center></td>';
                            if(update_disease_permission == true || delete_disease_permission == true)
                            {
                                html_content += '<td>';
                                html_content += '<ul class="list-inline">';
                                if(update_disease_permission == true)
                                {
                                    html_content += '<li><a title="Edit" href="' + site_path + '' + user + '/disease/edit/' + btoa(this.id) + '" class="action-btn btn-yellow edit" data-attribute-id="' + this.id + '"><i class="fa fa-pencil "></i></a></li>';
                                }
                                if(delete_disease_permission == true)
                                {
                                    html_content += '<li><a title="Delete" href="javascript:void(0)" class="action-btn btn-red disease_delete" data-attribute-id="' + this.id + '"><i class="fa fa-trash"></i></a></li>';
                                }
                                html_content += '</ul>';
                                html_content += '</td>';
                            }
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="3" class="no_record_found_row"><center>No Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#disease_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 5;
                    fetchRecord(skip);
                }
            };
            $('body').on('click','.disease_delete',function () {
                var id = btoa($(this).attr('data-attribute-id'));
                var _this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to delete this disease.!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#71c016',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function(result){
                    if (result.value) {

                        $.ajax({
                            url: site_path + '/delete/disease',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                id: id,
                            },
                            success: function (response) {
                                if(response.status == '1')
                                {
                                    _this.parent('li').parent('ul').parent('td').parent('tr').remove();
                                    if($('#disease_table tr').length == 0)
                                    {
                                        $('#disease_table').append('<tr><td colspan="5" class="no_record_found_row"><center>No Record Found...</center></td></tr>');
                                    }
                                }
                                Swal.fire({
                                    icon: response.icon,
                                    title: response.title,
                                    text: response.text,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            },
                            beforeSend: function () {

                            },
                            complete: function () {

                            },
                            error: function () {

                            }


                        });
                    }
            })
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($extends, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\disease\src/views/disease-list.blade.php ENDPATH**/ ?>