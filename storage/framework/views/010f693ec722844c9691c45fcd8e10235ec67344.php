<?php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
?>


<?php $__env->startSection('content'); ?>
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Add Animal</h2>
                </div>
                <div class="add-customer">
                    <form id="add_animal_form" method="POST" action="" enctype="multipart/form-data" autocomplete="off" >

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative ">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="animal_type" name="animal_type">
                                        <option value="">Select Animal Type*</option>
                                        <option value="0">Goat</option>
                                        <option value="1">Sheep</option>
                                    </select>
                                    <?php if ($errors->has('animal_type')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('animal_type'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <div class="relative">
                                        <input type="checkbox" name="is_owned_by_farm" id="is_owned_by_farm" value="1">
                                        <label for="ch-1">Is Owned By Farm</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="animal_owner_div">
                            <div class="col-sm-12 col-xs-12 form-group">
                                <select class="form-control" id="customer_id" name="customer_id">
                                    <option></option>
                                    <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($customer->customer_id); ?>"><?php echo e($customer->first_name); ?><?php echo e(' '.$customer->last_name); ?><?php echo e(' ('.$customer->customer_unique_id.')'); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <label id="customer_id-error" class="text-danger" for="customer_id"></label>
                                <?php if ($errors->has('customer_id')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('customer_id'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative ">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="gender" name="gender">
                                        <option value="">Select Animal Gender</option>
                                        <option value="0">Andul</option>
                                        <option value="1">Khassi</option>
                                        <option value="2">Female</option>
                                    </select>
                                    <?php if ($errors->has('gender')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('gender'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative ">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="breed_id" name="breed_id">
                                        <option value="">Select Animal Breed</option>
                                        <?php $__currentLoopData = $breeds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $breed): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($breed->id); ?>"><?php echo e($breed->breed_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <?php if ($errors->has('breed_id')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('breed_id'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="animal_name" name="animal_name" placeholder="Enter Animal Name*" class="form-control">
                                    <?php if ($errors->has('animal_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('animal_name'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="age_in_month" name="age_in_month" placeholder="Enter Aminal Age In Months*" class="form-control">
                                    <?php if ($errors->has('age_in_month')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('age_in_month'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="date_of_birth" name="date_of_birth" placeholder="Select Animal Date Of Birth*" class="form-control">
                                    <?php if ($errors->has('date_of_birth')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('date_of_birth'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="entry_weight" name="entry_weight" placeholder="Animal Weight (In Kgs)*" class="form-control">
                                    <?php if ($errors->has('entry_weight')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('entry_weight'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="entry_height" name="entry_height" placeholder="Animal Height (In Inch)*" class="form-control">
                                    <?php if ($errors->has('entry_height')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('entry_height'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="entry_date" name="entry_date" placeholder="Select Animal Registration Date*" class="form-control">
                                    <?php if ($errors->has('entry_date')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('entry_date'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="monthly_rent" name="monthly_rent" placeholder="Enter Aminal Monthly Rent*" class="form-control">
                                    <?php if ($errors->has('monthly_rent')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('monthly_rent'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <div class="relative">
                                        <input type="checkbox" name="is_payment_done" id="is_payment_done" value="1">
                                        <label for="ch-1">Is Payment Done</label>
                                        <?php if ($errors->has('is_payment_done')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('is_payment_done'); ?>
                                        <p style="color: red"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12 form-group">
                                <textarea class="form-control" id="notes" name="notes" placeholder="Enter Notes" value="" rows="5"></textarea>
                                <?php if ($errors->has('notes')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('notes'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li><button type="submit" class="btn cust-btn btn-save" id="animal_submit_btn"><i id="animal_submit_loder" style="font-size:15px"></i> Save</button></li>
                                        <li><button type="button" class="btn cust-btn btn-grey" id="reset_animal_form">Cancel</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jcontent'); ?>
    <script>

        var javascript_site_path = '<?php echo e(url('/')); ?>';
        $(function ()
        {
            $( "#entry_date" ).datepicker({
                dateFormat : 'yy-mm-d',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });

            $( "#date_of_birth" ).datepicker({
                dateFormat : 'd/m/yy',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });

            /*$( "#deworming_date" ).datepicker({
                dateFormat : 'yy-mm-d',
                changeYear: true,
                changeMonth: true,
                //maxDate: 0,
                yearRange: '2000:+0',
            });*/

            $('#customer_id').select2({
                placeholder: "Select Customer",
                allowClear: true
            });


            $('#reset_animal_form').click(function () {
                $('#add_animal_form')[0].reset();
            });

            $('#is_owned_by_farm').click(function () {
                if($(this).is(":checked")){
                    $('#animal_owner_div').hide();
                }
                else if($(this).is(":not(:checked)")){
                    $('#animal_owner_div').show();
                }
            });

            $("#age_in_month").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#entry_weight").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#entry_height").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#monthly_rent").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });




            $('#add_animal_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'animal_type':{
                        required:true,
                    },
                    'animal_name':{
                        required:true,
                    },
                    'gender':{
                        required:true,
                    },
                    'breed_id':{
                        required:true,
                    },
                    'customer_id':{
                        required:function () {
                            if($('#is_owned_by_farm').is(":checked"))
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        },
                    },
                    'age_in_month':{
                        required:true,
                    },
                    'entry_weight':{
                        required:true,
                    },
                    'entry_height':{
                        required:true,
                    },
                    'entry_date':{
                        required:true,
                    },
                    'monthly_rent':{
                        required:true,
                    },

                },
                messages:{
                    'animal_type':{
                        required:"Please select animal type.",
                    },
                    'animal_name':{
                        required:"Please enter animal name.",
                    },
                    'gender':{
                        required:"Please select animal gender.",
                    },
                    'breed_id':{
                        required:"Please select animal breed.",
                    },
                    'customer_id':{
                        required:"Please select animal owner name.",
                    },
                    'age_in_month':{
                        required:"Please enter animal age in months.",
                    },
                    'entry_weight':{
                        required:"Please enter animal weight.",
                    },
                    'entry_height':{
                        required:"Please enter animal height.",
                    },
                    'entry_date':{
                        required:"Please select registration date.",
                    },
                    'monthly_rent':{
                        required:"Please enter animal monthly rent.",
                    },
                },
                submitHandler:function (form) {
                    $('#animal_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#animal_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

        });


    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($extends, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\animal\src/views/animal-add.blade.php ENDPATH**/ ?>