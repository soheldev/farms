<?php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
?>


<?php $__env->startSection('content'); ?>
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Edit Disease</h2>
                </div>
                <div class="add-customer">
                    <form id="update_disease_form" method="POST" action="" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="disease_name" name="disease_name" placeholder="Enter Disease Name*" class="form-control" value="<?php echo e($edit_disease->disease_name); ?>">
                                    <?php if ($errors->has('disease_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('disease_name'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <textarea class="form-control" id="description" name="description" placeholder="Enter Description" value="" rows="5"><?php echo e($edit_disease->description); ?></textarea>
                                <?php if ($errors->has('description')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('description'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li><button type="submit" class="btn cust-btn btn-save" id="disease_update_btn"><i id="disease_update_loder" style="font-size:15px"></i> Update</button></li>
                                        <li><button type="button" class="btn cust-btn btn-grey" id="reset_update_disease_form">Cancel</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jcontent'); ?>
    <script>

        var javascript_site_path = '<?php echo e(url('/')); ?>';
        $(function () {
            $('#reset_update_disease_form').click(function () {
                $('#update_disease_form')[0].reset();
            });

            $('#update_disease_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'disease_name':{
                        required:true,
                    },
                },
                messages:{
                    'disease_name':{
                        required:"Please enter disease name."
                    },
                },
                submitHandler:function (form) {
                    $('#disease_update_loder').addClass("fa fa-spinner fa-pulse");
                    $('#disease_update_btn').attr('disabled',true);
                    form.submit();
                    /*$('#admin_profile_submit_icon').addClass('fa fa-spinner fa-pulse');
                    var form = $('#update_admin_profile_form')[0];
                    console.log(1111);
                    var data = new FormData(form);
                    $.ajax({
                        url: javascript_site_path+'/admin/update/profile',
                        type: 'POST',
                        dataType: 'JSON',
                        processData: false,
                        contentType: false,
                        cache: false,
                        enctype: 'multipart/form-data',
                        data: data,
                        success:function(response){

                        },
                        beforeSend:function () {

                        },
                        complete:function () {

                        },
                        error:function () {

                        }
                    });*/

                }
            });

        })

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($extends, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\disease\src/views/disease-edit.blade.php ENDPATH**/ ?>