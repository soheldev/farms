<?php
    $segment = Request::segment(2);
    $value = Request::segment(3);

?>
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item <?php if($segment == "dashboard"): ?>active <?php endif; ?>">
            <a class="nav-link <?php if($segment == "dashboard"): ?> hover-cursor <?php endif; ?>" href="<?php echo e(url('/admin/dashboard')); ?>">
                <i class="mdi mdi-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link <?php if($segment == "setting"): ?> hover-cursor <?php endif; ?>" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="mdi mdi-settings menu-icon"></i>
                <span class="menu-title">Manage Setting</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse <?php if($segment == "setting"): ?> show <?php endif; ?>" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link <?php if($value == "global"): ?> active <?php endif; ?>" href="<?php echo e(url('/admin/setting/global')); ?>">Manage Global Values</a></li>
                    <li class="nav-item"> <a class="nav-link <?php if($value == "banner"): ?> active <?php endif; ?>" href="<?php echo e(url('/admin/setting/banner')); ?>">Manage Banner Images</a></li>
                    <li class="nav-item"> <a class="nav-link <?php if($value == "country"): ?> active <?php endif; ?>" href="<?php echo e(url('/admin/setting/country')); ?>">Manage Countries</a></li>
                    <li class="nav-item"> <a class="nav-link <?php if($value == "state"): ?> active <?php endif; ?>" href="<?php echo e(url('/admin/setting/state')); ?>">Manage States</a></li>
                    <li class="nav-item"> <a class="nav-link <?php if($value == "city"): ?> active <?php endif; ?>" href="<?php echo e(url('/admin/setting/city')); ?>">Manage Cities</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item <?php if($segment == "farm"): ?>active <?php endif; ?>">
            <a class="nav-link" href="<?php echo e(url('/admin/farm/list')); ?>">
                <i class="mdi mdi-flower menu-icon"></i>
                <span class="menu-title">Manage Farms</span>
            </a>
        </li>

        <li class="nav-item <?php if($segment == "breed"): ?>active <?php endif; ?>">
            <a class="nav-link" href="<?php echo e(url('/admin/breed/list')); ?>">
                <i class="mdi mdi-dna menu-icon"></i>
                <span class="menu-title">Manage Breed</span>
            </a>
        </li>

        <li class="nav-item <?php if($segment == "vaccine"): ?> active <?php endif; ?>">
            <a class="nav-link" href="<?php echo e(url('/admin/vaccine/list')); ?>">
                <i class="mdi mdi-medical-bag menu-icon"></i>
                <span class="menu-title">Manage Vaccine</span>
            </a>
        </li>

        <li class="nav-item <?php if($segment == "disease"): ?> active <?php endif; ?>">
            <a class="nav-link" href="<?php echo e(url('/admin/disease/list')); ?>">
                <i class="mdi mdi-medical-bag menu-icon"></i>
                <span class="menu-title">Manage Disease</span>
            </a>
        </li>

        

        

        
        
        
        
        
        
        
        
    </ul>
</nav><?php /**PATH C:\xampp\htdocs\farms\resources\views/layouts/admin/admin_side_nav.blade.php ENDPATH**/ ?>