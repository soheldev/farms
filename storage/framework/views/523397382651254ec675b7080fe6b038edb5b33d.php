<?php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="<?php echo e(url($user.'/dashboard')); ?>">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="<?php echo e(url($user.'/customer/list')); ?>">List Customer&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">View Customer Information</p>
                        </div>

                        <br>

                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th rowspan="8">
                                        <div class="text-center">
                                            <?php if(isset($view_customer->profile_image)): ?>
                                                <img style="border-radius: 50%" height="150px" width="150px" src="<?php echo e(url('/public/customer/'.$view_customer->profile_image)); ?>">
                                            <?php else: ?>
                                                <img style="border-radius: 50%" height="150px" width="150px" src="<?php echo e(url('/public/media/backend/images/no-image.jpg')); ?>">
                                            <?php endif; ?>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                    </th>
                                    <th>
                                        Customer Id : <span>#<?php echo e($view_customer->customer_unique_id); ?></span>
                                    </th>
                                    <th>
                                        Customer Name : <span><?php echo e($view_customer->first_name); ?> <?php echo e($view_customer->last_name); ?></span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Email Id : <span><?php echo e($view_customer->email); ?></span>
                                    </th>
                                    <th>
                                        Contact Number : <span><?php echo e($view_customer->contact_no); ?></span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>Address Line 1 : <span><?php echo e(isset($view_customer->address_line_one)?$view_customer->address_line_one:'N/A'); ?></span></th>
                                    <th>Address Line 2 : <span><?php echo e(isset($view_customer->address_line_two)?$view_customer->address_line_two:'N/A'); ?></span></th>
                                </tr>
                                <tr>
                                    <th>City : <span><?php echo e(isset($view_customer->customerCity->city_name)?$view_customer->customerCity->city_name:'N/A'); ?></span></th>
                                    <th>State : <span><?php echo e(isset($view_customer->customerState->state_name)?$view_customer->customerState->state_name:'N/A'); ?></span></th>
                                </tr>

                                <tr>
                                    <th>Country : <span><?php echo e(isset($view_customer->customerCountry->country_name)?$view_customer->customerCountry->country_name:'N/A'); ?></span></th>
                                    <th>Pin Code : <span><?php echo e(isset($view_customer->pin_code)?$view_customer->pin_code:'N/A'); ?></span></th>
                                </tr>

                                <tr>
                                    <th>Occupation : <span><?php echo e(isset($view_customer->occupation)?$view_customer->occupation:'N/A'); ?></span></th>
                                    <th>Joining Date : <span><?php echo e(isset($view_customer->joining_date)?date("d-m-Y", strtotime($view_customer->joining_date)):'N/A'); ?></span></th>
                                </tr>

                                <tr>
                                    <th>
                                        Channel :
                                        <?php switch($view_customer->channel):
                                            case ('0'): ?>
                                            <span>Youtube</span>
                                            <?php break; ?>

                                            <?php case ('1'): ?>
                                            <span>Facebook</span>
                                            <?php break; ?>

                                            <?php case ('2'): ?>
                                            <span>Refered By Someone</span>
                                            <?php break; ?>

                                            <?php case ('3'): ?>
                                            <span>Others</span>
                                            <?php break; ?>

                                            <?php default: ?>
                                            <span>N/A</span>
                                        <?php endswitch; ?>
                                    </th>
                                    <th>
                                        Number Of Animals : <span><?php echo e(isset($view_customer->no_of_animals)?$view_customer->no_of_animals:'N/A'); ?></span>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="<?php echo e(url($user.'/dashboard')); ?>">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="<?php echo e(url($user.'/customer/list')); ?>">List Customer&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">View Customer Animal Information</p>
                        </div>
                        <br>
                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>Animal Id</th>
                                    <th>Animal Type</th>
                                    <th>Animal Name</th>
                                    <th>Animal Breed</th>
                                    <th>Animal Gender</th>
                                </tr>
                                </thead>
                                <tbody id="customer_animal_table">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jcontent'); ?>
<script>
    var site_path = '<?php echo e(url('/')); ?>';
    var user = '<?php echo e($user); ?>';
    var customer_id = '<?php echo e($view_customer->customer_id); ?>';
    $(function () {
        fetchCustomerAnimal();
    });

    function fetchCustomerAnimal()
    {
        $.ajax({
            url: site_path+'/fetch/customer/animal',
            type: 'POST',
            dataType: 'JSON',
            data:{
                customer_id:customer_id,
            },
            success:function (response) {
                var html_content = '';
                if(response.status == "1")
                {
                    $.each(response.data,function (index,value) {

                        html_content += '<tr>';
                        html_content += '<td><a href="'+site_path+''+user+'/animal/view/'+btoa(this.id)+'">'+this.prefix+''+this.animal_id+'</a></td>';
                        if(this.animal_type == '0')
                        {
                            html_content += '<td>Goat</td>';
                        }
                        else
                        {
                            html_content += '<td>Sheep</td>';
                        }
                        html_content += '<td>'+this.animal_name+'</td>';
                        html_content += '<td>'+this.animal_breed.breed_name+'</td>';
                        if(this.gender == '0')
                        {
                            html_content += '<td>Andul</td>';
                        }
                        else if(this.gender == '1')
                        {
                            html_content += '<td>Khassi</td>';
                        }
                        else
                        {
                            html_content += '<td>Female</td>';
                        }
                        html_content += '</tr>';
                    });
                }
                else
                {
                    html_content += '<tr><td colspan="5"><center>No Record Found...</center></td></tr>';
                }
                $('#customer_animal_table').append(html_content);

            },
            beforeSend:function () {

            },
            complete:function(){

            },
            error:function(){

            }
        });
    }
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make($extends, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\user\src/views/user-view.blade.php ENDPATH**/ ?>