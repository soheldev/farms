<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <?php if(session('success')): ?>
            <div class="alert alert-success" role="alert">
                <?php echo e(session('success')); ?>

            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="<?php echo e(url('/admin/dashboard')); ?>">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Add Farm</p>
                        </div>
                        <br>

                        <form class="forms-sample" id="add_farm_form" method="POST" action="" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputName1">Farm Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="farm_name" name="farm_name" placeholder="Enter Farm Name" value="" autocomplete="off">
                                <?php if ($errors->has('farm_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('farm_name'); ?>
                                
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Farm Owner First Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="farm_owner_first_name" name="farm_owner_first_name" placeholder="Enter Farm Owner First Name" value="" autocomplete="off">
                                <?php if ($errors->has('farm_owner_first_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('farm_owner_first_name'); ?>
                                
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Farm Owner Last Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="farm_owner_last_name" name="farm_owner_last_name" placeholder="Enter Farm Owner Last Name" value="" autocomplete="off">
                                <?php if ($errors->has('farm_owner_last_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('farm_owner_last_name'); ?>
                                
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Contact Number<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="Enter Contact Number" value="" autocomplete="off">
                                <?php if ($errors->has('contact_number')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('contact_number'); ?>
                                
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Email Id<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="email_id" name="email_id" placeholder="Enter Email Id" value="" autocomplete="off">
                                <?php if ($errors->has('email_id')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email_id'); ?>
                                
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Monthly Rent<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="monthly_rent" name="monthly_rent" placeholder="Enter Monthly Rent Amount" value="" autocomplete="off">
                                <?php if ($errors->has('monthly_rent')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('monthly_rent'); ?>
                                
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Address Line 1</label>
                                <textarea class="form-control" id="address_line_one" name="address_line_one" placeholder="Enter Address Line One" value="" rows="5"></textarea>
                                <?php if ($errors->has('address_line_one')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('address_line_one'); ?>
                                
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Address Line 2</label>
                                <textarea class="form-control" id="address_line_two" name="address_line_two" placeholder="Enter Address Line Two" value="" rows="5"></textarea>
                                <?php if ($errors->has('address_line_two')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('address_line_two'); ?>
                                
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Pin Code</label>
                                <input type="text" class="form-control" id="pin_code" name="pin_code" placeholder="Enter Pin Code" value="" autocomplete="off">
                                <?php if ($errors->has('pin_code')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('pin_code'); ?>
                                
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Country<sup style="color: red">*</sup></label>
                                <select class="form-control" id="country_id" name="country_id">
                                    <option value="">Select Country</option>
                                    <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($country->id); ?>"><?php echo e($country->country_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <?php if ($errors->has('country_id')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('country_id'); ?>
                                
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">State<sup style="color: red">*</sup></label>
                                <select class="form-control" id="state_id" name="state_id">
                                    <option value="">Select State</option>
                                </select>
                                <?php if ($errors->has('state_id')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('state_id'); ?>
                                
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">City<sup style="color: red">*</sup></label>
                                <select class="form-control" id="city_id" name="city_id">
                                    <option value="">Select City</option>
                                </select>
                                <?php if ($errors->has('city_id')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('city_id'); ?>
                                
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword4">Profile Picture</label>
                                <input type="file" class="form-control" id="profile_picture" name="profile_picture" accept=".jpeg, .jpg, .png">
                                <br>
                                <label>Preview</label><br>
                                <img id="preview_profile_picture" height="150px" width="150px" src="<?php echo e(url('/public/media/backend/images/no-image.jpg')); ?>">
                                <?php if ($errors->has('profile_picture')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('profile_picture'); ?>
                                
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>


                            <button type="submit" class="btn btn-primary mr-2" id="farm_submit_btn"><i id="farm_submit_loder" style="font-size:15px"></i>  Submit</button>
                            <button class="btn btn-light" id="reset_farm_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jcontent'); ?>
    <script>

        var javascript_site_path = '<?php echo e(url('/')); ?>';
        $(function ()
        {
            $('#reset_farm_form').click(function () {
                $('#add_farm_form')[0].reset();
                $('#preview_profile_picture').attr('src',javascript_site_path+'/public/media/backend/images/no-image.jpg');
            });

            $('#profile_picture').change(function () {
                var uploaded_image = window.URL.createObjectURL(this.files[0]);
                $('#preview_profile_picture').attr('src',uploaded_image);
            });

            $("#farm_owner_first_name").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 65 || key > 90)) {
                    return false;
                }
            });

            $("#farm_owner_last_name").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 65 || key > 90)) {
                    return false;
                }
            });

            $("#contact_number").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105)) {
                    return false;
                }
            });

            $("#monthly_rent").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105)) {
                    return false;
                }
            });

            $('#country_id').change(function () {
                $('#state_id').html('<option value="">Select State</option>');
                $('#city_id').html('<option value="">Select City</option>');
                var country_id = $(this).val();
                $.ajax({
                    url: javascript_site_path+'/admin/fetch/state-according-to-country',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        country_id: country_id
                    },
                    success:function(response)
                    {
                        var html_content = '';
                        if(response.status == "1")
                        {
                            html_content += '<option value="">Select State</option>';
                            $.each(response.data,function (key,value) {
                                html_content += '<option value="'+value.id+'">'+value.state_name+'</option>';
                            });
                            $('#state_id').html(html_content);
                        }
                        else
                        {
                            $('#state_id').html('<option value="">No States Found...</option>');
                        }
                    },
                    beforeSend:function () {
                        $('#state_id').html('<option value="">Fetching States...</option>');
                    },
                    complete:function () {

                    },
                    error:function () {

                    }
                });

            });

            $('#state_id').change(function () {
                var state_id = $(this).val();
                $.ajax({
                    url: javascript_site_path+'/admin/fetch/city-according-to-state',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        state_id: state_id
                    },
                    success:function(response)
                    {
                        var html_content = '';
                        if(response.status == "1")
                        {
                            html_content += '<option value="">Select City</option>';
                            $.each(response.data,function (key,value) {
                                html_content += '<option value="'+value.id+'">'+value.city_name+'</option>';
                            });
                            $('#city_id').html(html_content);
                        }
                        else
                        {
                            $('#city_id').html('<option value="">No Cities Found...</option>');
                        }
                    },
                    beforeSend:function () {
                        $('#city_id').html('<option value="">Fetching Cities...</option>');
                    },
                    complete:function () {

                    },
                    error:function () {

                    }
                });

            });


            $('#add_farm_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'farm_name':{
                        required:true,
                    },
                    'farm_owner_first_name':{
                        required:true,
                    },
                    'farm_owner_last_name':{
                        required:true,
                    },
                    'contact_number':{
                        required:true,
                        digits:true,
                        minlength:10,
                        maxlength:10,
                        remote: {
                            url: javascript_site_path+"/admin/check-contact",
                            type: "post",
                            data:{
                                flag:'1'
                            }
                        }
                    },
                    'email_id':{
                        required:true,
                        email:true,
                        remote: {
                            url: javascript_site_path+"/admin/check-email",
                            type: "post",
                            data:{
                                flag:'1'
                            }
                        }
                    },
                    'monthly_rent':{
                        required:true,
                        digits:true,
                    },
                    'country_id':{
                        required:true,
                    },
                    'state_id':{
                        required:true,
                    },
                    'city_id':{
                        required:true,
                    },

                },
                messages:{
                    'farm_name':{
                        required:"Please enter farm name.",
                    },
                    'farm_owner_first_name':{
                        required:"Please enter farm owner first name.",
                    },
                    'farm_owner_last_name':{
                        required:"Please enter farm owner last name.",
                    },
                    'contact_number':{
                        required:"Please enter contact number.",
                        digits:"Please enter valid contact number.",
                        minlength:"Contact number should not be less than 10 digit.",
                        maxlength:"Contact number should not be greater than 10 digit.",
                        remote:"Contact number already exits."
                    },
                    'email_id':{
                        required:"Please enter email id.",
                        email:"Please enter valid email id.",
                        remote:"Email id already exits."
                    },
                    'monthly_rent':{
                        required:"Please enter monthly rental amount.",
                        digits:"Please enter valid rental amount."
                    },
                    'country_id':{
                        required:"Please select country.",
                    },
                    'state_id':{
                        required:"Please select state.",
                    },
                    'city_id':{
                        required:"Please select city.",
                    },
                },
                submitHandler:function (form) {
                    $('#farm_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#farm_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

        });


    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\farm\src/views/farm-add.blade.php ENDPATH**/ ?>