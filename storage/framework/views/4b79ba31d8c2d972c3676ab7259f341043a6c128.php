<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <?php if(session('success')): ?>
            <div class="alert alert-success" role="alert">
                <?php echo e(session('success')); ?>

            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="<?php echo e(url('/master/dashboard')); ?>">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Add Client</p>
                        </div>
                        <br>

                        <form class="forms-sample" id="add_client_form" method="POST" action="" enctype="multipart/form-data">


                            <div class="form-group">
                                <label for="exampleInputName1">Client Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="client_name" name="client_name" placeholder="Enter Client Name" value="">
                                <?php if ($errors->has('client_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('client_name'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Client Farm Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="farm_name" name="farm_name" placeholder="Enter Client Farm Name" value="">
                                <?php if ($errors->has('farm_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('farm_name'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Email Id<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email Id" value="">
                                <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Contact Number<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="contact_no" name="contact_no" placeholder="Enter Contact Number" value="">
                                <?php if ($errors->has('contact_no')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('contact_no'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Sub Domain<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="sub_domain" name="sub_domain" placeholder="Enter Client Sub Domain" value="">
                                <?php if ($errors->has('sub_domain')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('sub_domain'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Database Name<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="database_name" name="database_name" placeholder="Enter Client Database Name" value="">
                                <?php if ($errors->has('database_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('database_name'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Database Username<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="database_username" name="database_username" placeholder="Enter Client Database Username" value="">
                                <?php if ($errors->has('database_username')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('database_username'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Database Password<sup style="color: red">*</sup></label>
                                <input type="text" class="form-control" id="database_password" name="database_password" placeholder="Enter Client Database Password" value="">
                                <?php if ($errors->has('database_password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('database_password'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Notes</label>
                                <textarea class="form-control" id="notes" name="notes" placeholder="Enter Notes" value="" rows="5"></textarea>
                                <?php if ($errors->has('notes')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('notes'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <button type="submit" class="btn btn-primary mr-2" id="client_submit_btn"><i id="client_submit_loder" style="font-size:15px"></i>  Submit</button>
                            <button class="btn btn-light" id="reset_client_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jcontent'); ?>
    <script>

        var javascript_site_path = '<?php echo e(url('/')); ?>';
        $(function () {
            $('#reset_client_form').click(function () {
                $('#add_client_form')[0].reset();
            });



            $("#contact_no").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105)) {
                    return false;
                }
            });

            $('#farm_name').keyup(function () {
                var value = $(this).val();
                $('#sub_domain').val(value.replace(/\s+/g, '').toLowerCase());
            });


            $('#add_client_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'client_name':{
                        required:true,
                    },
                    'farm_name':{
                        required:true,
                    },
                    'email':{
                        required:true,
                    },
                    'contact_no':{
                        required:true,
                    },
                    'sub_domain':{
                        required:true,
                    },
                    'database_name':{
                        required:true,
                    },
                    'database_username':{
                        required:true,
                    },
                    /*'database_password':{
                        required:true,
                    },*/
                    /*'contact_no':{
                        required:true,
                        digits:true,
                        minlength:10,
                        maxlength:10,
                        remote: {
                            url: javascript_site_path+"/subadmin/check-contact",
                            type: "post",
                            data:{
                                flag:'1'
                            }
                        }
                    },*/
                    /*'email':{
                        required:true,
                        email:true,
                        remote: {
                            url: javascript_site_path+"/subadmin/check-email",
                            type: "post",
                            data:{
                                flag:'1'
                            }
                        }
                    },*/


                },
                messages:{
                    'client_name':{
                        required:"Please enter client name.",
                    },
                    'farm_name':{
                        required:"Please enter client farm name.",
                    },
                    'email':{
                        required:"Please enter email id.",
                    },
                    'contact_no':{
                        required:"Please enter contact number.",
                    },
                    'sub_domain':{
                        required:"Please enter client sub domain.",
                    },
                    'database_name':{
                        required:"Please enter database name.",
                    },
                    'database_username':{
                        required:"Please enter database username.",
                    },
                    /*'database_password':{
                        required:"Please enter database username.",
                    },*/
                    /*'contact_no':{
                        required:"Please enter contact number.",
                        digits:"Please enter valid contact number.",
                        minlength:"Contact number should not be less than 10 digit.",
                        maxlength:"Contact number should not be greater than 10 digit.",
                        remote:"Contact number already exits."
                    },
                    'email':{
                        required:"Please enter email id.",
                        email:"Please enter valid email id.",
                        remote:"Email id already exits."
                    },*/
                },
                submitHandler:function (form) {
                    $('#client_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#client_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

        });


    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\client\src/views/client-add.blade.php ENDPATH**/ ?>