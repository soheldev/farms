<div class="db-head clearfix">
    <div class="bur-menu pull-left">
        <a href="javascript:void(0);"><i class="fa fa-bars"></i></a>
    </div>
    <div class="db-head-menu pull-right">
        <ul class="">
            <li class="db-head-user relative">
                <a href="javascript:void(0);">
                    <?php echo e(Auth::user()->name); ?>

                    <span>
                        <?php if(isset(Auth::user()->userInformation->profile_image)): ?>
                            <img src="<?php echo e(url('/public/farm/'.Auth::user()->userInformation->profile_image)); ?>" alt="profile"/>
                        <?php else: ?>
                            <img src="<?php echo e(url('/public/media/backend')); ?>/images/faces/face5.jpg" alt="profile"/>
                        <?php endif; ?>
                        
                    </span>
                </a>
                <div class="user-det-menu animate">
                    <ul>
                        <li><a href="<?php echo e(url('/vendor/profile')); ?>"> <i class="fa fa-user-o"></i>My Profile</a></li>
                        <li class="divider" role="separator"></li>
                        <li><a href="<?php echo e(url('/vendor/logout')); ?>"> <i class="fa fa-key"></i>Log Out</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div><?php /**PATH C:\xampp\htdocs\farms\resources\views/layouts/vendor/vendor_header.blade.php ENDPATH**/ ?>