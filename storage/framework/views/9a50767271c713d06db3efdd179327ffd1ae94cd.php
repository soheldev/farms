<?php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="<?php echo e(url($user.'/dashboard')); ?>">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="<?php echo e(url($user.'/animal/list')); ?>">List Animals&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">View Animal Information</p>
                        </div>
                        <br>
                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>
                                        Animal Id : <span>#<?php echo e($view_animal->prefix.$view_animal->animal_id); ?></span>
                                    </th>
                                    <th>
                                        Animal Type :
                                        <?php switch($view_animal->animal_type):
                                            case ('0'): ?>
                                            <span>Goat</span>
                                            <?php break; ?>

                                            <?php case ('1'): ?>
                                            <span>Sheep</span>
                                            <?php break; ?>

                                            <?php default: ?>
                                            <span>N/A</span>
                                        <?php endswitch; ?>
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Animal Name : <span><?php echo e($view_animal->animal_name); ?></span>
                                    </th>
                                    <th>
                                        Animal Breed : <span><?php echo e(isset($view_animal->animalBreed)?$view_animal->animalBreed->breed_name:'N/A'); ?></span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Gender :
                                        <?php switch($view_animal->gender):
                                            case ('0'): ?>
                                            <span>Andul</span>
                                            <?php break; ?>

                                            <?php case ('1'): ?>
                                            <span>Khassi</span>
                                            <?php break; ?>

                                            <?php case ('2'): ?>
                                            <span>Female</span>
                                            <?php break; ?>

                                            <?php default: ?>
                                            <span>N/A</span>
                                        <?php endswitch; ?>
                                    </th>
                                    <th>
                                        Age In Months : <span><?php echo e(isset($view_animal->age_in_month)?$view_animal->age_in_month:'N/A'); ?> Months</span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>Date Of Birth : <span><?php echo e(isset($view_animal->date_of_birth)?$view_animal->date_of_birth:'N/A'); ?></span></th>
                                    <th>Entry Date : <span><?php echo e(isset($view_animal->entry_date)?date("d-m-Y", strtotime($view_animal->entry_date)):'N/A'); ?></span></th>
                                </tr>

                                <tr>
                                    <th>Entry Height : <span><?php echo e(isset($view_animal->entry_height)?$view_animal->entry_height:'N/A'); ?> Feet</span></th>
                                    <th>Entry Weight : <span><?php echo e(isset($view_animal->entry_weight)?$view_animal->entry_weight:'N/A'); ?> Kg</span></th>
                                </tr>

                                <tr>
                                    <th>Notes : <span><?php echo e(isset($view_animal->notes)?$view_animal->notes:'N/A'); ?></span></th>
                                    <th>Owner Name : <span><?php echo e(isset($view_animal->animalOwner)?$view_animal->animalOwner->first_name.' '.$view_animal->animalOwner->last_name:'N/A'); ?></span></th>
                                </tr>

                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="<?php echo e(url($user.'/dashboard')); ?>">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="<?php echo e(url($user.'/animal/list')); ?>">List Animals&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">View Animal Progress Chart</p>
                        </div>
                        <br>
                        <div id="animal-chart-legend" class="d-flex justify-content-center pt-3"></div>
                        <canvas id="animal-chart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('jcontent'); ?>
<script>
    var javascript_site_path = '<?php echo e(url('/')); ?>';
    $(function () {
        loadAnimalChart();
    });

    function loadAnimalChart()
    {
        var animal_id = '<?php echo e(Request::segment(4)); ?>';
        $.ajax({
           url: javascript_site_path+'/get/animal-chart-data',
           type: 'POST',
           //dataType: 'JSON',
           data:{
               animal_id: animal_id
           },
           success:function (response) {
               if(response.status == '0')
               {
                   var cashDepositsCanvas = $("#animal-chart").get(0).getContext("2d");
                   var data = {
                       labels: response.month_arr,
                       datasets: [
                           {
                               label: 'Height',
                               data: response.height_arr,
                               borderColor: [
                                   '#ff4747'
                               ],
                               borderWidth: 2,
                               fill: false,
                               pointBackgroundColor: "#fff"
                           },
                           {
                               label: 'Weight',
                               data: response.weight_arr,
                               borderColor: [
                                   '#4d83ff'
                               ],
                               borderWidth: 2,
                               fill: false,
                               pointBackgroundColor: "#fff"
                           },
                       ]
                   };
                   var options = {
                       scales: {
                           yAxes: [{
                               display: true,
                               gridLines: {
                                   drawBorder: false,
                                   lineWidth: 1,
                                   color: "#e9e9e9",
                                   zeroLineColor: "#e9e9e9",
                               },
                               ticks: {
                                   min: 0,
                                   max: 100,
                                   stepSize: 10,
                                   fontColor: "#6c7383",
                                   fontSize: 16,
                                   fontStyle: 300,
                                   padding: 15
                               }
                           }],
                           xAxes: [{
                               display: true,
                               gridLines: {
                                   drawBorder: false,
                                   lineWidth: 1,
                                   color: "#e9e9e9",
                               },
                               ticks : {
                                   fontColor: "#6c7383",
                                   fontSize: 16,
                                   fontStyle: 300,
                                   padding: 15
                               }
                           }]
                       },
                       legend: {
                           display: false
                       },
                       legendCallback: function(chart) {
                           var text = [];
                           text.push('<ul class="dashboard-chart-legend">');
                           for(var i=0; i < chart.data.datasets.length; i++) {
                               text.push('<li><span style="background-color: ' + chart.data.datasets[i].borderColor[0] + ' "></span>');
                               if (chart.data.datasets[i].label) {
                                   text.push(chart.data.datasets[i].label);
                               }
                           }
                           text.push('</ul>');
                           return text.join("");
                       },
                       elements: {
                           point: {
                               radius: 3
                           },
                           line :{
                               tension: 0
                           }
                       },
                       stepsize: 1,
                       layout : {
                           padding : {
                               top: 0,
                               bottom : -10,
                               left : -10,
                               right: 0
                           }
                       }
                   };
                   var cashDeposits = new Chart(cashDepositsCanvas, {
                       type: 'line',
                       data: data,
                       options: options
                   });
                   document.getElementById('animal-chart-legend').innerHTML = cashDeposits.generateLegend();
               }
               else
               {
                   $("#animal-chart-legend").html('<h3>No Progress Recorded Yet...</h3>');
               }
           },
           beforeSend:function () {
               $("#animal-chart-legend").html('<i class="fa fa-spinner fa-pulse fa-2x"></i>&nbsp;<h3> Fetching Animal Progress</h3>');
           }
        });
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($extends, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\animal\src/views/animal-view.blade.php ENDPATH**/ ?>