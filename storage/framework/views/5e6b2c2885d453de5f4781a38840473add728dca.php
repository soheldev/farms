<?php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
?>


<?php $__env->startSection('content'); ?>
    <div class="db-body">
        <form id="permission_form" method="post">
            <?php $__currentLoopData = $permission_arr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model => $permissions): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="inner-forms">
                    <div class="box">
                        <div class="box-title">
                            <h2># <?php echo e($model); ?></h2>
                        </div>
                        <div class="add-customer">
                            <div class="row">
                                <?php $__currentLoopData = $permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $permission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-sm-3 col-xs-12 form-group">
                                        <div class="cust-form relative">
                                            <div class="relative">
                                                <input type="checkbox" <?php if(count($role_permission) > 0 && in_array($permission['id'], $role_permission)): ?> checked <?php endif; ?> name="permissions[]" value="<?php echo e($permission['id']); ?>">
                                                <label for="ch-1"><?php echo e($permission['name']); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <div class="cust-form relative">
                            <ul class="list-inline">
                                <li><button type="button" class="btn cust-btn btn-save" id="role_permission_submit_btn"><i id="role_permission_submit_loder" style="font-size:15px"></i> Save</button></li>
                            </ul>
                        </div>
                    </div>
                </div>
        </form>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jcontent'); ?>
  <script>
      $('#role_permission_submit_btn').click(function () {
          $('#role_permission_submit_loder').addClass("fa fa-spinner fa-pulse");
          $('#permission_form').submit();
      });
  </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make($extends, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\role\src/views/role-permission-view.blade.php ENDPATH**/ ?>