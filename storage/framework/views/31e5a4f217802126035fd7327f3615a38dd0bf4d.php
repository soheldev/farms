<?php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <?php if(session('success')): ?>
            <div class="alert alert-success" role="alert">
                <?php echo e(session('success')); ?>

            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="<?php echo e(url($user.'/dashboard')); ?>">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">Add Bulk Vaccine</p>
                        </div>
                        <br>

                        <form class="forms-sample" id="add_vaccine_utility_form" method="POST" action="" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="exampleInputName1">Select Animal<sup style="color: red">*</sup></label>
                                <select class="form-control animal_select" id="animal_ids" name="animal_ids[]" multiple>
                                    <option value="all">Select All</option>
                                    <?php $__currentLoopData = $animals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $animal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($animal->id); ?>"><?php echo e($animal->animal_name); ?><?php echo e(' ('.$animal->prefix.$animal->animal_id.')'); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <label id="animal_ids-error" class="text-danger" for="animal_ids"></label>
                                <?php if ($errors->has('animal_ids')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('animal_ids'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName1">Select Vaccine<sup style="color: red">*</sup></label>
                                <select class="form-control" id="vaccine_id" name="vaccine_id">
                                    <option></option>
                                    <?php $__currentLoopData = $vaccines; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vaccine): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($vaccine->id); ?>"><?php echo e($vaccine->vaccine_name); ?><?php echo e(' ('.$vaccine->vaccine_id.')'); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <label id="vaccine_id-error" class="text-danger" for="vaccine_id"></label>
                                <?php if ($errors->has('vaccine_id')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('vaccine_id'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>


                            <button type="submit" class="btn btn-primary mr-2" id="vaccine_utility_submit_btn"><i id="vaccine_utility_submit_loder" style="font-size:15px"></i>  Submit</button>
                            <button class="btn btn-light" id="reset_vaccine_utility_form">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jcontent'); ?>
    <script>

        var javascript_site_path = '<?php echo e(url('/')); ?>';
        $(function () {

            $('#animal_ids').select2({
                /*placeholder: "Select Animals",
                allowClear: true*/
                closeOnSelect : false,
                placeholder : "Select Animals",
                allowHtml: true,
                allowClear: true,
                tags: true
            });

            $('#vaccine_id').select2({
                closeOnSelect : true,
                placeholder : "Select Vaccine",
                allowHtml: true,
                allowClear: true,
                tags: true
            });

            $('.animal_select').on("select2:select", function (e) {
                var data = e.params.data.text;
                if(data=='Select All')
                {
                    $(".animal_select > option").prop("selected","selected");
                    $(".animal_select option[value='all']").prop("selected", false);
                    $(".animal_select").trigger("change");
                }
            });

            $('#reset_vaccine_utility_form').click(function () {
                $('#add_vaccine_utility_form')[0].reset();
            });



            $('#add_vaccine_utility_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'animal_ids[]':{
                        required:true,
                    },
                    'vaccine_id':{
                        required:true,
                    },
                    /*'value':{
                        required:true,
                    }*/
                },
                messages:{
                    'animal_ids[]':{
                        required:"Please select animals."
                    },
                    'vaccine_id':{
                        required:"Please select vaccine.",
                    },
                    /*'value':{
                        required:"Please enter vlaue."
                    }*/
                },
                submitHandler:function (form) {
                    $('#vaccine_utility_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#vaccine_utility_submit_btn').attr('disabled',true);
                    form.submit();
                    /*$('#admin_profile_submit_icon').addClass('fa fa-spinner fa-pulse');
                    var form = $('#update_admin_profile_form')[0];
                    console.log(1111);
                    var data = new FormData(form);
                    $.ajax({
                        url: javascript_site_path+'/admin/update/profile',
                        type: 'POST',
                        dataType: 'JSON',
                        processData: false,
                        contentType: false,
                        cache: false,
                        enctype: 'multipart/form-data',
                        data: data,
                        success:function(response){

                        },
                        beforeSend:function () {

                        },
                        complete:function () {

                        },
                        error:function () {

                        }
                    });*/

                }
            });

        });


    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($extends, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\vaccine\src/views/utility-vaccine-add.blade.php ENDPATH**/ ?>