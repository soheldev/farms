<?php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
?>


<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="<?php echo e(url($user.'/dashboard')); ?>">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <a class="text-primary mb-0 hover-cursor" href="<?php echo e(url($user.'/setting/roles')); ?>">List Role&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">View Role</p>
                        </div>
                        <br>
                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>
                                        Role Id : <span># <?php echo e($view_role->id); ?></span>
                                    </th>
                                    <th>
                                        Role Name : <span><?php echo e($view_role->name); ?></span>
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        Slug : <span><?php echo e($view_role->slug); ?></span>
                                    </th>
                                    <th>
                                        Description : <span><?php echo e(isset($view_role->description)?$view_role->description:'N/A'); ?></span>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jcontent'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make($extends, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\role\src/views/role-view.blade.php ENDPATH**/ ?>