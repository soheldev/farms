<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
    <!-- Css File -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/public/media/backend')); ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/public/media/backend')); ?>/assets/css/animated.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/public/media/backend')); ?>/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/public/media/backend')); ?>/assets/css/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/public/media/backend')); ?>/assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/public/media/backend')); ?>/assets/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/public/media/backend')); ?>/assets/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/public/media/backend')); ?>/assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/public/media/backend')); ?>/assets/css/responsive.css">
</head>
<body>
<section class="login-page tent relative back-img flex-model" style="background-image: url(<?php echo e(url('/public/media/backend')); ?>/assets/images/login.jpg);">
    <div class="login flex-model">
        <div class="login-inner">
            <?php if(session('error')): ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo e(session('error')); ?>

                </div>
            <?php endif; ?>
            <div class="main-pg-titl"><h4 class="text-center">Login</h4></div>
            <form id="login_form" action="<?php echo e(url('/login')); ?>" method="POST">
                <?php echo e(csrf_field()); ?>

                <div class="cust-form relative form-group">
                    <span><i class="fa fa-user"></i></span>
                    <input type="text" name="email" id="email" placeholder="Email" class="form-control" value="<?php echo e(old('email')); ?>">
                    <?php if($errors->has('email')): ?>
                        <span class="help-block">
                              <strong><?php echo e($errors->first('email')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
                <div class="cust-form relative">
                    <span><i class="fa fa-lock"></i></span>
                    <input type="password" name="password" id="password" placeholder="Password" class="form-control">
                    <?php if($errors->has('password')): ?>
                        <span class="help-block">
                               <strong><?php echo e($errors->first('password')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

                <div class="remember relative">
                    <ul class="clearfix">
                        <li>
                            <input type="checkbox" name="" class="form-control">
                            <label for="ch-1">Remember Me</label>
                        </li>
                        <li class="forget text-right">
                            <a href="javascript:void(0);"><label for="ch-1">Forgot Password</label></a>
                        </li>
                    </ul>
                </div>
                <div class="cust-from">
                    <button type="submit" class="btn cust-btn btn-blue-1">Login</button>
                </div>

            </form>
        </div>
    </div>
</section>
<!-- Js scroll Script-->
<script>
    (function($){
        $(window).on("load",function(){
            $(".content").mCustomScrollbar();
        });
    })(jQuery);
</script>
<script src="<?php echo e(url('/public/media/backend')); ?>/assets/js/jquery.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/assets/js/wow.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/assets/js/owl.carousel.min.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/assets/js/jquery.bxslider.min.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/assets/js/custom.js"></script>
<script src="<?php echo e(url('/public/media/backend')); ?>/assets/js/jquery.validate.js"></script>
<script>
    $(function () {
        $('#login_form').validate({
            errorClass: 'text-danger',
            rules:{
                'email':{
                    required:true
                },
                'password':{
                    required:true
                }
            },
            messages:{
                'email':{
                    required:'Please enter your email id.'
                },
                'password':{
                    required:'Please enter your password.'
                }
            },
            submitHandler:function (form) {
                form.submit();
            }
        });

    });
</script>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\farms\resources\views/auth/login.blade.php ENDPATH**/ ?>