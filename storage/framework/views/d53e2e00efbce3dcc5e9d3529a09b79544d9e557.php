<?php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
?>


<?php $__env->startSection('content'); ?>
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Add Deworming</h2>
                </div>
                <div class="add-customer">
                    <form id="add_deworming_form" method="POST" action="" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="type" name="type">
                                        <option value="">Deworming Type*</option>
                                        <option value="1">Internal</option>
                                        <option value="2">External</option>
                                    </select>
                                    <?php if ($errors->has('type')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('type'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                                <div class="col-sm-6 col-xs-12 form-group">
                                    <div class="cust-form relative">
                                        <span><i class="fa fa-user"></i></span>
                                        <input type="text" id="deworming_agent_name" name="deworming_agent_name"
                                               placeholder="Enter Deworming Agent Name*" class="form-control">
                                        <?php if ($errors->has('deworming_agent_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('deworming_agent_name'); ?>
                                        <p style="color: red"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6 col-xs-12 form-group">
                                    <div class="cust-form relative">
                                        <span><i class="fa fa-user"></i></span>
                                        <input type="text" id="body_weight" name="body_weight"
                                               placeholder="Body Weight (In KG)*" class="form-control">
                                        <?php if ($errors->has('body_weight')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('body_weight'); ?>
                                        <p style="color: red"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xs-12 form-group">
                                    <div class="cust-form relative">
                                        <span><i class="fa fa-user"></i></span>
                                        <input type="text" id="dosage" name="dosage" placeholder="Dosage (In ML)*"
                                               class="form-control">
                                        <?php if ($errors->has('dosage')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('dosage'); ?>
                                        <p style="color: red"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12 form-group">
                                    <textarea class="form-control" id="deworming_notes" name="deworming_notes"
                                              placeholder="Enter Notes" value="" rows="5"></textarea>
                                    <?php if ($errors->has('deworming_notes')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('deworming_notes'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="cust-form relative">
                                        <ul class="list-inline">
                                            <li>
                                                <button type="submit" class="btn cust-btn btn-save"
                                                        id="deworming_submit_btn"><i id="deworming_submit_loder"
                                                                                     style="font-size:15px"></i> Save
                                                </button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn cust-btn btn-grey"
                                                        id="reset_deworming_form">Cancel
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jcontent'); ?>
    <script>

        var javascript_site_path = '<?php echo e(url('/')); ?>';
        $(function () {
            $('#reset_deworming_form').click(function () {
                $('#add_deworming_form')[0].reset();
            });

            /*$('#type').change(function () {
               var value = $(this).val();
            });*/

            $('#add_deworming_form').validate({
                errorClass: 'text-danger',
                rules: {
                    'type': {
                        required: true,
                    },
                    'deworming_agent_name': {
                        required: true,
                    },
                    'body_weight': {
                        required: true,
                    },
                    'dosage': {
                        required: true,
                    },
                },
                messages: {
                    'type': {
                        required: "Please select deworming type."
                    },
                    'deworming_agent_name': {
                        required: "Please enter deworming agent name."
                    },
                    'body_weight': {
                        required: "Please enter body weight."
                    },
                    'dosage': {
                        required: "Please enter dosage."
                    },
                },
                submitHandler: function (form) {
                    $('#deworming_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#deworming_submit_btn').attr('disabled', true);
                    form.submit();
                }
            });

        });


    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($extends, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\deworming\src/views/deworming-add.blade.php ENDPATH**/ ?>