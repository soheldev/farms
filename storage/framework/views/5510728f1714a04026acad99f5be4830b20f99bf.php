<?php $__env->startSection('content'); ?>
    <div class="db-body">
        <div class="inner-forms">
            <?php if(session('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo e(session('success')); ?>

                </div>
            <?php endif; ?>
            <div class="box">
                <div class="box-title">
                    <h2>Update Profile</h2>
                </div>
                <div class="add-customer">
                    <form id="update_vendor_profile_form" method="POST" action="" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="name" name="name" placeholder="Farm Name*" class="form-control" value="<?php echo e($vendor_profile->name); ?>">
                                    <?php if ($errors->has('name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('name'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                                <input type="hidden" name="id" id="id" value="<?php echo e($vendor_profile->id); ?>">
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="first_name" name="first_name" placeholder="First Name*" class="form-control" value="<?php echo e($vendor_profile->userInformation->first_name); ?>">
                                    <?php if ($errors->has('first_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('first_name'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="last_name" name="last_name" placeholder="Last Name*" class="form-control" value="<?php echo e($vendor_profile->userInformation->last_name); ?>">
                                    <?php if ($errors->has('last_name')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('last_name'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="email" name="email" placeholder="Email Id*" class="form-control" value="<?php echo e($vendor_profile->email); ?>">
                                    <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="mobile_number" name="mobile_number" placeholder="Mobile Number*" class="form-control" value="<?php echo e($vendor_profile->userInformation->mobile_number); ?>">
                                    <?php if ($errors->has('mobile_number')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('mobile_number'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="file" id="profile_image" name="profile_image" accept=".jpeg, .jpg, .png" class="form-control">
                                    <br>
                                    <img id="preview_profile_image" height="50px" width="50px" <?php if(isset($vendor_profile->userInformation->profile_image)): ?> src="<?php echo e(url('/public/farm/'.$vendor_profile->userInformation->profile_image)); ?>" <?php else: ?> src="<?php echo e(url('/public/media/backend/images/no-image.jpg')); ?>" <?php endif; ?>>
                                    <?php if ($errors->has('profile_image')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('profile_image'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                        </div>

                        
                        
                        
                        
                        
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li><button type="submit" class="btn cust-btn btn-save" id="vendor_profile_submit_btn"><i id="vendor_profile_submit_icon" style="font-size:15px"></i> Save</button></li>
                                        <li><button type="button" class="btn cust-btn btn-grey" id="reset_vendor_profile_form">Cancel</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jcontent'); ?>
    <script>

        var javascript_site_path = '<?php echo e(url('/')); ?>';
        $('#update_vendor_profile_form').validate({
            errorClass: 'text-danger',
            rules:{
                'name':{
                    required:true,
                },
                'first_name':{
                    required:true,
                },
                'last_name':{
                    required:true,
                },
                'email':{
                    required:true,
                    remote:{
                        url: javascript_site_path+"/vendor-check-mail-duplication",
                        type: "post",
                        data:{
                            id: $('#id').val(),
                        }
                    },
                },
                'mobile_number':{
                    required:true,
                }
            },
            messages:{
                'name':{
                    required:"Please enter your farm name."
                },
                'first_name':{
                    required:"Please enter your first name."
                },
                'last_name':{
                    required:"Please enter your last name."
                },
                'email':{
                    required:"Please enter your email id.",
                    remote:"Email already exist."
                },
                'mobile_number':{
                    required:"Please enter your mobile number."
                }
            },
            submitHandler:function (form) {
                $('#vendor_profile_submit_icon').addClass("fa fa-spinner fa-pulse");
                $('#vendor_profile_submit_btn').attr('disabled',true);
                form.submit();
            }
        });

        $(function () {

            $('#profile_image').change(function () {
                var uploaded_image = window.URL.createObjectURL(this.files[0]);
                $('#preview_profile_image').attr('src',uploaded_image);
            });

            $('#reset_vendor_profile_form').click(function () {
                $('#update_vendor_profile_form')[0].reset();
            })
        })

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.vendor.vendor', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\vendor\src/views/vendor-profile.blade.php ENDPATH**/ ?>