<?php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
?>

<?php $__env->startSection('content'); ?>
    <div class="db-body">
        <div class="inner-forms">
            <?php if(session('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo e(session('success')); ?>

                </div>
            <?php endif; ?>
            <div class="box ">
                <div class="box-title clearfix">
                    <h2 class="pull-left">List Payments</h2>
                </div>

                <div class="table-resposnive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Animal Id</th>
                            <th>Animal Name</th>
                            <th>Customer Id</th>
                            <th>Customer Name</th>
                            <th>Customer Contact</th>
                            <th>Amount Due</th>
                            <th>Due Date<br><span style="font-size: 8px">YYYY-MM-DD</span></th>
                            <th>Status</th>
                            <?php if($capture_pending_payment_permission == true): ?>
                                <th>Action</th>
                            <?php endif; ?>
                        </tr>
                        </thead>
                        <tbody id="pending_payment_table">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('jcontent'); ?>
    <script>
        var site_path = '<?php echo e(url('/')); ?>';
        var user = '<?php echo e($user); ?>';
        var skip = 0;
        var i = 0;
        var search_value = '';
        var capture_pending_payment_permission = '<?php echo e($capture_pending_payment_permission); ?>';
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/pending-payment-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip,
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td>'+this.id+'</td>';
                            html_content += '<td><a href="'+site_path+''+user+'/animal/view/'+btoa(this.animal_id)+'">'+this.animal_unique_id+'</a></td>';
                            html_content += '<td>'+this.animal_name+'</td>';
                            html_content += '<td><a href="'+site_path+''+user+'/customer/view/'+btoa(this.customer_id)+'">'+this.customer_unique_id+'</a></td>';
                            html_content += '<td>'+this.customer_name+'</td>';
                            html_content += '<td>'+this.contact_no+'</td>';
                            html_content += '<td>'+this.amount_due+'/-</td>';
                            html_content += '<td>'+this.due_date+'</td>';
                            html_content += '<td>Pending</td>';
                            if(capture_pending_payment_permission == true)
                            {
                                html_content += '<td><button title="Make Payment" data-attribute-payment-id="'+btoa(this.id)+'" class="btn btn-primary make_payment_btn">Pay</button></td>';
                            }
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="10"><center>No Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#pending_payment_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 10;
                    fetchRecord(skip);
                }
            };

            $('body').on('click','.make_payment_btn',function () {
                var payment_id = $(this).attr('data-attribute-payment-id');
                var redirect_payment_url = site_path+user+'/payment/pending/process/'+payment_id;
                window.location.href = redirect_payment_url;
            });


            function searchRecord(skip,search_value)
            {
                $.ajax({
                    url: site_path+'/fetch/pending-payment-data',
                    type: 'POST',
                    dataType: 'JSON',
                    data:{
                        skip:skip,
                        search_value:search_value
                    },
                    success:function (response) {
                        console.log(response);
                        var html_content = '';
                        if(response.status == "1")
                        {
                            $.each(response.data,function (index,value) {

                                html_content += '<tr>';
                                html_content += '<td>'+this.id+'</td>';
                                html_content += '<td><a href="'+site_path+''+user+'/animal/view/'+btoa(this.animal_id)+'">'+this.animal_unique_id+'</a></td>';
                                html_content += '<td>'+this.animal_name+'</td>';
                                html_content += '<td><a href="'+site_path+''+user+'/customer/view/'+btoa(this.customer_id)+'">'+this.customer_unique_id+'</a></td>';
                                html_content += '<td>'+this.customer_name+'</td>';
                                html_content += '<td>'+this.contact_no+'</td>';
                                html_content += '<td>'+this.amount_due+'/-</td>';
                                html_content += '<td>'+this.due_date+'</td>';
                                html_content += '<td>Pending</td>';
                                if(capture_pending_payment_permission == true)
                                {
                                    html_content += '<td><button title="Make Payment" data-attribute-payment-id="'+btoa(this.id)+'" class="btn btn-primary make_payment_btn">Pay</button></td>';
                                }
                                html_content += '</tr>';
                            });
                        }
                        else
                        {
                            html_content += '<tr><td colspan="10"><center>No Record Found...</center></td></tr>';
                        }
                        $('#pending_payment_table').html(html_content);

                    },
                    beforeSend:function () {
                        $('#pending_payment_table').html('<tr><td colspan="10"><center><i class="fa fa-spinner fa-pulse fa-2x"><i/></center></td></tr>');
                    },
                    complete:function(){

                    },
                    error:function(){

                    }
                });
            }

            $('#search_text').keyup(function () {
                search_value = $(this).val();
                skip = 0;
                searchRecord(skip,search_value);
            });


        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($extends, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\payment\src/views/payment-pending-list.blade.php ENDPATH**/ ?>