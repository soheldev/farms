<?php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
?>

<?php $__env->startSection('content'); ?>
    <div class="db-body">
        <div class="inner-forms">
            <?php if(session('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo e(session('success')); ?>

                </div>
            <?php endif; ?>
            <div class="box ">
                <div class="box-title clearfix">
                    <h2 class="pull-left">List Deworming Detail Of #<?php echo e($animal_detail->prefix.$animal_detail->animal_id); ?></h2>
                    <?php if($animal_detail->status == '1'): ?>
                        <?php if($add_animal_deworming_permission == true): ?>
                            <div class="top-btn pull-right">
                                <a class="btn cust-btn btn-blue-1" href="<?php echo e(url($user.'/animal/deworming/add/'.Request::segment(4))); ?>"><i class="fa fa-plus"></i> Add Deworming</a>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>

                <div class="table-resposnive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Deworming Id</th>
                            <th>Deworming Agent Name</th>
                            <th>Deworming Dosage</th>
                            <th>Actual Dosage</th>
                            <th>Deworming Date</th>
                            <th>Status</th>
                            <?php if($update_animal_deworming_permission == true || $delete_animal_deworming_permission == true): ?>
                                <th>Action</th>
                            <?php endif; ?>
                        </tr>
                        </thead>
                        <tbody id="animal_deworming_table">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('jcontent'); ?>
    <script>
        var site_path = '<?php echo e(url('/')); ?>';
        var user = '<?php echo e($user); ?>';
        var animal_id = '<?php echo e(Request::segment(4)); ?>';
        var skip = 0;
        var i = 0;
        var search_value = '';
        var update_animal_deworming_permission = '<?php echo e($update_animal_deworming_permission); ?>';
        var delete_animal_deworming_permission = '<?php echo e($delete_animal_deworming_permission); ?>';
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/animal-deworming-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip,
                    animal_id:animal_id
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {
                            html_content += '<tr>';
                            html_content += '<td>'+this.animal_deworming.deworming_id+'</td>';
                            html_content += '<td>'+this.animal_deworming.deworming_agent_name+'</td>';
                            html_content += '<td>'+this.predefine_body_weight+' KG / '+this.predefine_dosage+' ML</td>';
                            if((this.actual_body_weight != null && this.actual_body_weight != '') && (this.actual_dosage != null && this.actual_dosage != ''))
                            {
                                html_content += '<td>'+this.actual_body_weight+' KG / '+this.actual_dosage+'</td>';
                            }
                            else
                            {
                                html_content += '<td>N/A</td>';
                            }
                            if(this.deworming_date != null && this.deworming_date != '')
                            {
                                html_content += '<td>'+this.deworming_date+'</td>';
                            }
                            else
                            {
                                html_content += '<td>N/A</td>';
                            }
                            if(this.deworming_status == "0")
                            {
                                html_content += '<td>Pending</td>';
                            }
                            else if(this.deworming_status == "1")
                            {
                                html_content += '<td>Completed</td>';
                            }
                            else if(this.deworming_status == "2")
                            {
                                html_content += '<td>Missed</td>';
                            }
                            else
                            {
                                html_content += '<td>N/A</td>';
                            }

                            if(update_animal_deworming_permission == true || delete_animal_deworming_permission == true)
                            {
                                html_content += '<td>';
                                html_content += '<ul class="list-inline">';
                                if(update_animal_deworming_permission == true)
                                {
                                    html_content += '<li><a title="Edit" href="'+site_path+''+user+'/animal/deworming/edit/'+animal_id+'/'+btoa(this.id)+'" class="action-btn btn-yellow edit" data-attribute-id="' + this.id + '"><i class="fa fa-pencil "></i></a></li>';
                                }
                                if(delete_animal_deworming_permission == true)
                                {
                                    html_content += '<li><a title="Delete" href="javascript:void(0)" class="action-btn btn-red animal_deworming_delete" data-attribute-id="' + this.id + '"><i class="fa fa-trash"></i></a></li>';
                                }
                                html_content += '</ul>';
                                html_content += '</td>';
                            }

                            //html_content += '<td><center><a title="Edit" href="'+site_path+''+user+'/animal/deworming/edit/'+animal_id+'/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; <a title="delete" href="javascript:void(0)" class="animal_deworming_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></center></td>';
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="7"><center>No Deworming Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#animal_deworming_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 5;
                    fetchRecord(skip);
                }
            };



            $('body').on('click','.animal_deworming_delete',function () {
                var id = btoa($(this).attr('data-attribute-id'));
                var _this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to delete this animal deworming record.!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#71c016',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function(result){
                    if (result.value) {

                        $.ajax({
                            url: site_path + '/delete/animal/deworming',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                id: id,
                            },
                            success: function (response) {
                                if(response.status == '1')
                                {
                                    _this.parent('li').parent('ul').parent('td').parent('tr').remove();
                                    if($('#animal_deworming_table tr').length == 0)
                                    {
                                        $('#animal_deworming_table').append('<tr><td colspan="7" class="no_record_found_row"><center>No Record Found...</center></td></tr>');
                                    }
                                }
                                Swal.fire({
                                    icon: response.icon,
                                    title: response.title,
                                    text: response.text,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            },
                            beforeSend: function () {

                            },
                            complete: function () {

                            },
                            error: function () {

                            }
                        });
                    }
                })
            });

            function searchRecord(skip,search_value)
            {
                $.ajax({
                    url: site_path+'/fetch/animal-data',
                    type: 'POST',
                    dataType: 'JSON',
                    data:{
                        skip:skip,
                        search_value:search_value
                    },
                    success:function (response) {
                        console.log(response);
                        var html_content = '';
                        if(response.status == "1")
                        {
                            $.each(response.data,function (index,value) {

                                html_content += '<tr>';
                                html_content += '<td>'+this.vaccine_id+'</td>';
                                html_content += '<td>'+this.vaccine_id+'</td>';
                                html_content += '<td>'+this.dose_number+'</td>';
                                html_content += '<td>'+this.vacination_date+'</td>';
                                html_content += '<td>'+this.weight_on_vacination_day+'</td>';
                                html_content += '<td>'+this.quantity+'</td>';
                                html_content += '<td><center><a title="Edit" href="'+site_path+''+user+'/animal/vacination/edit/'+animal_id+'/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; <a title="delete" href="javascript:void(0)" class="animal_vacination_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></center></td>';
                                html_content += '</tr>';
                            });
                        }
                        else
                        {
                            html_content += '<tr><td colspan="7"><center>No Vacination Record Found...</center></td></tr>';
                        }
                        $('#animal_vacination_table').html(html_content);

                    },
                    beforeSend:function () {
                        $('#animal_vacination_table').html('<tr><td colspan="6"><center><i class="fa fa-spinner fa-pulse fa-2x"><i/></center></td></tr>');
                    },
                    complete:function(){

                    },
                    error:function(){

                    }
                });
            }

            /*$('#search_text').keyup(function () {
                search_value = $(this).val();
                skip = 0;
                searchRecord(skip,search_value);
            });*/


        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($extends, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\animal\src/views/animal-deworming-list.blade.php ENDPATH**/ ?>