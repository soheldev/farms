<?php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
?>


<?php $__env->startSection('content'); ?>
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Edit Monthly Progress Of <?php echo e($animal_detail->prefix.$animal_detail->animal_id); ?></h2>
                </div>
                <div class="add-customer">
                    <form id="edit_animal_progress_form" method="POST" action="" enctype="multipart/form-data" autocomplete="off">


                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="weight" name="weight"
                                           placeholder="Enter Animal Weight (In Kgs)*" class="form-control"
                                           value="<?php echo e($edit_progress->weight); ?>">
                                    <?php if ($errors->has('weight')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('weight'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="height" name="height" placeholder="Enter Animal Height (In Inch)*"
                                           class="form-control" value="<?php echo e($edit_progress->height); ?>">
                                    <?php if ($errors->has('height')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('height'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="capture_date" name="capture_date" placeholder="Select Date Capture*"
                                           class="form-control"  value="<?php echo e($edit_progress->capture_date); ?>">
                                    <?php if ($errors->has('capture_date')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('capture_date'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                    <textarea class="form-control" id="notes" name="notes"
                                              placeholder="Enter Notes" value="" rows="5"><?php echo e($edit_progress->notes); ?></textarea>
                                <?php if ($errors->has('notes')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('notes'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li>
                                            <button type="submit" class="btn cust-btn btn-save"
                                                    id="edit_animal_progress_submit_btn"><i id="edit_animal_progress_submit_loder"
                                                                                       style="font-size:15px"></i> Save
                                            </button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn cust-btn btn-grey"
                                                    id="reset_edit_animal_progress_form">Cancel
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jcontent'); ?>
    <script>

        var javascript_site_path = '<?php echo e(url('/')); ?>';
        $(function ()
        {
            $( "#capture_date" ).datepicker({
                dateFormat : 'd/m/yy',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });

            $('#reset_edit_animal_progress_form').click(function () {
                $('#edit_animal_progress_form')[0].reset();
            });

            $("#weight").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#height").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });




            $('#edit_animal_progress_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'weight':{
                        required:true,
                    },
                    'height':{
                        required:true,
                    },
                    'capture_date':{
                        required:true,
                    }
                },
                messages:{
                    'weight':{
                        required:"Please enter animal weight.",
                    },
                    'height':{
                        required:"Please enter animal height.",
                    },
                    'capture_date':{
                        required:"Please select date",
                    }
                },
                submitHandler:function (form) {
                    $('#edit_animal_progress_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#edit_animal_progress_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

        });


    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($extends, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\progress\src/views/animal-progress-edit.blade.php ENDPATH**/ ?>