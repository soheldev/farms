<?php
    $segment = Request::segment(2);
    $value = Request::segment(3);

?>

<div class="left-panel pull-left">
    <div class="logo">
        <a href="<?php echo e(url("/vendor/dashboard")); ?>"><?php echo e(Auth::user()->name); ?></a>
    </div>
    <div class="db-menu-list">
        <ul>
            <li class="active">
                <a href="<?php echo e(url("/vendor/dashboard")); ?>"><i class="fa fa-home"></i> Home</a>
            </li>
            <?php if(\Illuminate\Support\Facades\Auth::user()->user_type == '2'): ?>
                <li>
                    <div class="panel-menu panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseOne">
                                <i class="fa fa-cog"></i> Manage Setting
                            </a>
                            <div id="collapseOne" class="panel-collapse <?php if($segment == "setting"): ?> collapse in <?php else: ?> collapse <?php endif; ?>" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="<?php echo e(url('/vendor/setting/roles')); ?>"> Manage Roles</a></li>
                                        <li><a href="<?php echo e(url('/vendor/setting/users')); ?>"> Manage Users</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php endif; ?>

            <?php if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.breeds') == true): ?>
                <li>
                    <a href="<?php echo e(url('/vendor/breed/list')); ?>"><i class="fa fa-users"></i> Manage Breed</a>
                </li>
            <?php endif; ?>

            <?php if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.vaccine') == true): ?>
                <li>
                    <a href="<?php echo e(url('/vendor/vaccine/list')); ?>"><i class="fa fa-medkit"></i> Manage Vaccine</a>
                </li>
            <?php endif; ?>

            <?php if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.deworming') == true): ?>
                <li>
                    <a href="<?php echo e(url('/vendor/deworming/agent/list')); ?>"><i class="fa fa-heartbeat"></i> Manage Deworming</a>
                </li>
            <?php endif; ?>

            <?php if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.disease') == true): ?>
                <li>
                    <a href="<?php echo e(url('/vendor/disease/list')); ?>"><i class="fa fa-stethoscope"></i> Manage Disease</a>
                </li>
            <?php endif; ?>

            <?php if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.customer') == true): ?>
                <li>
                    <a href="<?php echo e(url('/vendor/customer/list')); ?>"><i class="fa fa-user-circle-o"></i> Manage Customers</a>
                </li>
            <?php endif; ?>

            <?php if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.animal') == true || \App\Http\Helpers\CheckPermission::hasPermission('view.exitanimal') == true): ?>
                <li>
                    <div class="panel-menu panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <a data-toggle="collapse" class="collapsed" data-parent="#accordion1" href="#collapseOne1">
                                <i class="fa fa-users"></i> Manage Animal
                            </a>
                            <div id="collapseOne1" class="panel-collapse <?php if($segment == "animal"): ?> collapse in <?php else: ?> collapse <?php endif; ?>" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <?php if(\App\Http\Helpers\CheckPermission::hasPermission('view.animal') == true): ?>
                                            <li><a href="<?php echo e(url('/vendor/animal/list')); ?>"> Manage Active Animal</a></li>
                                        <?php endif; ?>
                                        <?php if(\App\Http\Helpers\CheckPermission::hasPermission('view.exitanimal') == true): ?>
                                             <li><a href="<?php echo e(url('/vendor/animal/exited')); ?>"> Manage Exited Animal</a></li>
                                        <?php endif; ?>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php endif; ?>

            <?php if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.pendingprogress') == true || \App\Http\Helpers\CheckPermission::hasPermission('view.updateprogress') == true || \App\Http\Helpers\CheckPermission::hasPermission('view.progress') == true): ?>
                <li>
                    <div class="panel-menu panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <a data-toggle="collapse" class="collapsed" data-parent="#accordion2" href="#collapseOne2">
                                <i class="fa fa-line-chart"></i> Manage Animals Progress
                            </a>
                            <div id="collapseOne2" class="panel-collapse <?php if($segment == "progress"): ?> collapse in <?php else: ?> collapse <?php endif; ?>" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <?php if(\App\Http\Helpers\CheckPermission::hasPermission('view.pendingprogress') == true): ?>
                                            <li><a href="<?php echo e(url('/vendor/progress/pending/list')); ?>"> Manage Pending Progress</a></li>
                                        <?php endif; ?>
                                        <?php if(\App\Http\Helpers\CheckPermission::hasPermission('view.updateprogress') == true): ?>
                                            <li><a href="<?php echo e(url('/vendor/progress/capture/list')); ?>"> Update Animal Progress</a></li>
                                        <?php endif; ?>
                                        <?php if(\App\Http\Helpers\CheckPermission::hasPermission('view.progress') == true): ?>
                                             <li><a href="<?php echo e(url('/vendor/progress/view/list')); ?>"> View Animal Progress</a></li>
                                        <?php endif; ?>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php endif; ?>

            <?php if(\Illuminate\Support\Facades\Auth::user()->user_type == '2' || \App\Http\Helpers\CheckPermission::hasPermission('view.pendingpayment') == true || \App\Http\Helpers\CheckPermission::hasPermission('view.receivedpayment') == true): ?>
                <li>
                    <div class="panel-menu panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <a data-toggle="collapse" class="collapsed" data-parent="#accordion3" href="#collapseOne3">
                                <i class="fa fa-money"></i> Manage Payments
                            </a>
                            <div id="collapseOne3" class="panel-collapse <?php if($segment == "payment"): ?> collapse in <?php else: ?> collapse <?php endif; ?>" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <?php if(\App\Http\Helpers\CheckPermission::hasPermission('view.pendingpayment') == true): ?>
                                            <li><a href="<?php echo e(url('/vendor/payment/pending/list')); ?>"> Manage Pending Payment</a></li>
                                        <?php endif; ?>
                                        <?php if(\App\Http\Helpers\CheckPermission::hasPermission('view.receivedpayment') == true): ?>
                                            <li><a href="<?php echo e(url('/vendor/payment/received/list')); ?>"> Manage Received Payment</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php endif; ?>

            <?php if(\Illuminate\Support\Facades\Auth::user()->user_type == '2'): ?>
                <li>
                    <div class="panel-menu panel-group" id="accordion4" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <a data-toggle="collapse" class="collapsed" data-parent="#accordion4" href="#collapseOne4">
                                <i class="fa fa-bars"></i> Manage Utility
                            </a>
                            <div id="collapseOne4" class="panel-collapse <?php if($segment == "utility"): ?> collapse in <?php else: ?> collapse <?php endif; ?>" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="<?php echo e(url('/vendor/utility/vaccine')); ?>"> Vaccine Utility</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

            <?php endif; ?>

            
            
            
        </ul>
    </div>
</div><?php /**PATH C:\xampp\htdocs\farms\resources\views/layouts/vendor/vendor_side_nav.blade.php ENDPATH**/ ?>