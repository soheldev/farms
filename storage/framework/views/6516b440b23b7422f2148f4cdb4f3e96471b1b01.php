<?php $__env->startSection('content'); ?>
    <style>
        .my-custom-scrollbar {
            position: relative;
            height: 400px;
            overflow: auto;
        }
        .table-wrapper-scroll-y {
            display: block;
        }
    </style>
    <div class="content-wrapper">
        <?php if(session('success')): ?>
            <div class="alert alert-success" role="alert">
                <?php echo e(session('success')); ?>

            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <a class="text-primary mb-0 hover-cursor" href="<?php echo e(url('/master/dashboard')); ?>">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</a>
                            <p class="text-muted mb-0 hover-cursor">List Clients</p>
                        </div>
                        <br>
                        <div class="pull-right">
                            <a type="button" class="btn btn-success btn-rounded" href="<?php echo e(url('/master/client/add')); ?>">Add New Client</a>
                        </div>
                        <div class="table-responsive pt-3 ">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th>Client Id</th>
                                    <th>Name</th>
                                    <th>Email Id</th>
                                    <th>Contact No</th>
                                    <th>Sub Domain</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody id="client_table">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('jcontent'); ?>
    <script>
        var site_path = '<?php echo e(url('/')); ?>';
        var skip = 0;
        var i = 0;
        function fetchRecord(skip)
        {
            $.ajax({
                url: site_path+'/fetch/client-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td><a href="'+site_path+'/master/client/view/'+btoa(this.id)+'">'+this.client_id+'</a></td>';
                            html_content += '<td>'+this.client_name+'</td>';
                            html_content += '<td>'+this.email+'</td>';
                            html_content += '<td>'+this.contact_no+'</td>';
                            html_content += '<td>'+this.sub_domain+'</td>';
                            if(this.status == '0')
                            {
                                html_content += '<td><a type="button" href="javascript:void(0)" class="btn btn-warning btn-rounded">InActive</a></td>';
                            }
                            else if(this.status == '1')
                            {
                                html_content += '<td><a type="button" href="javascript:void(0)" class="btn btn-success btn-rounded">Active</a></td>';
                            }
                            else
                            {
                                html_content += '<td><a type="button" href="javascript:void(0)" class="btn btn-danger btn-rounded">Blocked</a></td>';
                            }
                            html_content += '<td><center><a title="Edit" href="'+site_path+'/master/client/edit/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; <a title="delete" href="javascript:void(0)" class="client_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></center></td>';
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        if(i == 0)
                        {
                            html_content += '<tr><td colspan="7"><center>No Record Found...</center></td></tr>';
                        }
                        i++;

                    }
                    $('#client_table').append(html_content);

                },
                beforeSend:function () {

                },
                complete:function(){

                },
                error:function(){

                }
            });
        }

        function searchRecord(skip,search_value)
        {
            $.ajax({
                url: site_path+'/fetch/client-data',
                type: 'POST',
                dataType: 'JSON',
                data:{
                    skip:skip,
                    search_value:search_value
                },
                success:function (response) {
                    var html_content = '';
                    if(response.status == "1")
                    {
                        $.each(response.data,function (index,value) {

                            html_content += '<tr>';
                            html_content += '<td><a href="'+site_path+'/master/client/view/'+btoa(this.id)+'">'+this.client_id+'</a></td>';
                            html_content += '<td>'+this.client_name+'</td>';
                            html_content += '<td>'+this.email+'</td>';
                            html_content += '<td>'+this.contact_no+'</td>';
                            html_content += '<td>'+this.sub_domain+'</td>';
                            if(this.status == '0')
                            {
                                html_content += '<td><a type="button" href="javascript:void(0)" class="btn btn-warning btn-rounded">InActive</a></td>';
                            }
                            else if(this.status == '1')
                            {
                                html_content += '<td><a type="button" href="javascript:void(0)" class="btn btn-success btn-rounded">Active</a></td>';
                            }
                            else
                            {
                                html_content += '<td><a type="button" href="javascript:void(0)" class="btn btn-danger btn-rounded">Blocked</a></td>';
                            }
                            html_content += '<td><center><a title="Edit" href="'+site_path+'/master/client/edit/'+btoa(this.id)+'" class="edit" data-attribute-id="'+this.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> &nbsp; <a title="delete" href="javascript:void(0)" class="client_delete" data-attribute-id="'+this.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a></center></td>';
                            html_content += '</tr>';
                        });
                    }
                    else
                    {
                        html_content += '<tr><td colspan="7"><center>No Record Found...</center></td></tr>';
                    }
                    $('#client_table').html(html_content);

                },
                beforeSend:function () {
                    $('#client_table').html('<tr><td colspan="5"><center><i class="fa fa-spinner fa-pulse fa-2x"><i/></center></td></tr>');
                },
                complete:function(){

                },
                error:function(){

                }
            });
        }
        $(function(){
            fetchRecord(skip);
            window.onscroll = function(ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    // you're at the bottom of the page
                    skip = skip + 5;
                    fetchRecord(skip);
                }
            };
            $('body').on('click','.client_delete',function () {
                var id = btoa($(this).attr('data-attribute-id'));
                var _this = $(this);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to delete this client.!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#71c016',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function(result){
                    if (result.value) {

                        $.ajax({
                            url: site_path + '/delete/client',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                id: id,
                            },
                            success: function (response) {
                                if(response.status == '1')
                                {
                                    _this.parent('center').parent('td').parent('tr').remove();
                                    if($('#client_table tr').length == 0)
                                    {
                                        $('#client_table').append('<tr><td colspan="5" class="no_record_found_row"><center>No Record Found...</center></td></tr>');
                                    }
                                }
                                Swal.fire({
                                    icon: response.icon,
                                    title: response.title,
                                    text: response.text,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            },
                            beforeSend: function () {

                            },
                            complete: function () {

                            },
                            error: function () {

                            }


                        });
                    }
                })
            });

            $('#master_search').keyup(function () {
                search_value = $(this).val();
                skip = 0;
                searchRecord(skip,search_value);
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\client\src/views/client-list.blade.php ENDPATH**/ ?>