<?php
    $type = Auth::user()->userInformation->user_type;
    $extends = '';
    $user = '';
    if($type == "1")
    {
        $extends = 'layouts.admin.admin';
        $user = '/admin';
    }
    else
    {
        $extends = 'layouts.vendor.vendor';
        $user = '/vendor';
    }
?>


<?php $__env->startSection('content'); ?>
    <div class="db-body">
        <div class="inner-forms">
            <div class="box">
                <div class="box-title">
                    <h2>Edit Animal Vaccination</h2>
                </div>
                <div class="add-customer">
                    <form id="edit_animal_vacination_form" method="POST" action="" enctype="multipart/form-data" autocomplete="off">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form cust-select relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <select class="form-control" id="vaccine_id" name="vaccine_id">
                                        <option value="">Select Vaccination*</option>
                                        <?php $__currentLoopData = $vacinations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vacine): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($vacine->id); ?>" <?php if($vacine->id == $edit_animal_vacination->vaccine_id): ?> selected <?php endif; ?>><?php echo e($vacine->vaccine_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <?php if ($errors->has('vaccine_id')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('vaccine_id'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="dose_number" name="dose_number"
                                           placeholder="Enter Dose Number*" class="form-control" value="<?php echo e($edit_animal_vacination->dose_number); ?>">
                                    <?php if ($errors->has('dose_number')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('dose_number'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="vacination_date" name="vacination_date"
                                           placeholder="Select Vaccination Date*" class="form-control" value="<?php echo e($edit_animal_vacination->vacination_date); ?>">
                                    <?php if ($errors->has('vacination_date')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('vacination_date'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="weight_on_vacination_day" name="weight_on_vacination_day" placeholder="Enter Weight On Vaccination Day*"
                                           class="form-control" value="<?php echo e($edit_animal_vacination->weight_on_vacination_day); ?>">
                                    <?php if ($errors->has('weight_on_vacination_day')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('weight_on_vacination_day'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xs-12 form-group">
                                <div class="cust-form relative">
                                    <span><i class="fa fa-user"></i></span>
                                    <input type="text" id="quantity" name="quantity"
                                           placeholder="Enter Quantity In ML" class="form-control" value="<?php echo e($edit_animal_vacination->quantity); ?>">
                                    <?php if ($errors->has('quantity')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('quantity'); ?>
                                    <p style="color: red"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 form-group">
                                <textarea class="form-control" id="notes" name="notes"
                                          placeholder="Enter Notes" value="" rows="5"><?php echo e($edit_animal_vacination->notes); ?></textarea>
                                <?php if ($errors->has('notes')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('notes'); ?>
                                <p style="color: red"><?php echo e($message); ?></p>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="cust-form relative">
                                    <ul class="list-inline">
                                        <li>
                                            <button type="submit" class="btn cust-btn btn-save"
                                                    id="edit_animal_vacination_submit_btn"><i id="edit_animal_vacination_submit_loder"
                                                                                         style="font-size:15px"></i> Save
                                            </button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn cust-btn btn-grey"
                                                    id="reset_edit_animal_vacination_form">Cancel
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jcontent'); ?>
    <script>

        var javascript_site_path = '<?php echo e(url('/')); ?>';
        $(function ()
        {
            $( "#vacination_date" ).datepicker({
                dateFormat : 'd/m/yy',
                changeYear: true,
                changeMonth: true,
                maxDate: 0,
                yearRange: '2000:+0',
            });

            $('#reset_edit_animal_vacination_form').click(function () {
                $('#edit_animal_vacination_form')[0].reset();
            });

            $("#dose_number").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#weight_on_vacination_day").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });

            $("#quantity").keydown(function(event) {
                var key = event.charCode || event.keyCode;
                console.log(key);
                if ( (key != 8 || key ==32 )  && (key < 96 || key > 105) && (key != 190) && (key != 9) && (key != 110)) {
                    return false;
                }
            });




            $('#edit_animal_vacination_form').validate({
                errorClass: 'text-danger',
                rules:{
                    'vaccine_id':{
                        required:true,
                    },
                    'dose_number':{
                        required:true,
                    },
                    'vacination_date':{
                        required:true,
                    },
                    'weight_on_vacination_day':{
                        required:true,
                    },
                    'quantity':{
                        required:true,
                    }
                },
                messages:{
                    'vaccine_id':{
                        required:"Please select vacination.",
                    },
                    'dose_number':{
                        required:"Please enter dose number.",
                    },
                    'vacination_date':{
                        required:"Please select vacination date.",
                    },
                    'weight_on_vacination_day':{
                        required:"Please enter weight of animal on vacination day",
                    },
                    'quantity':{
                        required:"Please enter quantity in ml.",
                    },
                },
                submitHandler:function (form) {
                    $('#edit_animal_vacination_submit_loder').addClass("fa fa-spinner fa-pulse");
                    $('#edit_animal_vacination_submit_btn').attr('disabled',true);
                    form.submit();
                }
            });

        });


    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($extends, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\farms\packages\sohel\animal\src/views/animal-vacination-edit.blade.php ENDPATH**/ ?>