<?php
    $segment = Request::segment(2);
    $value = Request::segment(3);

?>
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item <?php if($segment == "dashboard"): ?>active <?php endif; ?>">
            <a class="nav-link <?php if($segment == "dashboard"): ?> hover-cursor <?php endif; ?>" href="<?php echo e(url('/master/dashboard')); ?>">
                <i class="mdi mdi-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>

        <li class="nav-item <?php if($segment == "client"): ?> active <?php endif; ?>">
            <a class="nav-link" href="<?php echo e(url('/master/client/list')); ?>">
                <i class="mdi mdi mdi-account menu-icon"></i>
                <span class="menu-title">Manage Clients</span>
            </a>
        </li>

        

        

        

        

        

        

        

        
        
        
        
        
        
        
        
    </ul>
</nav><?php /**PATH C:\xampp\htdocs\farms\resources\views/layouts/master/master_side_nav.blade.php ENDPATH**/ ?>