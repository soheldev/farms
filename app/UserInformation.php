<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    //
    protected $table = 'user_informations';
    protected $fillable = ['user_id','user_unique_id','first_name','last_name','monthly_rent','country_id','state_id','city_id','address_line_one','address_line_two','pin_code','mobile_number','user_type','user_status','profile_image'];
    protected $with = ['users'];

    public function users()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function userCity()
    {
        return $this->belongsTo('Sohel\City\Model\City','city_id','id');
    }

    public function userState()
    {
        return $this->belongsTo('Sohel\State\Model\State','state_id','id');
    }

    public function userCountry()
    {
        return $this->belongsTo('Sohel\Country\Model\Country','country_id','id');
    }



}
