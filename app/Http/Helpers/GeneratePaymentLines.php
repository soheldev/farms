<?php
/**
 * Created by PhpStorm.
 * User: Sohell
 * Date: 5/3/2020
 * Time: 7:51 AM
 */
namespace App\Http\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sohel\Payment\Model\PaymentReminder;

class GeneratePaymentLines
{
    public function generatePendingCustomerPaymentLine($pending_payments)
    {
        $pending_payment_lines_arr = [];
        $update_payment_reminder_arr = [];
        foreach($pending_payments as $key => $pending_payment)
        {
            $pending_payment_lines_arr[$key]['animal_id'] = $pending_payment->animal_id;
            $pending_payment_lines_arr[$key]['customer_id'] = $pending_payment->paymentReminderAnimalInfo->customer_id;
            $pending_payment_lines_arr[$key]['amount_due'] = $pending_payment->paymentReminderAnimalInfo->monthly_rent;
            $pending_payment_lines_arr[$key]['amount_paid'] = 0;
            $pending_payment_lines_arr[$key]['due_date'] = $pending_payment->payment_due_date;
            $pending_payment_lines_arr[$key]['payment_status'] = '0';
            $pending_payment_lines_arr[$key]['added_by'] = Auth::user()->id;
            $pending_payment_lines_arr[$key]['created_at'] = date('Y-m-d h:m:i');

            $payment_reminder = new \DateTime($pending_payment->payment_due_date);
            $payment_reminder->modify('+1 months');
            $new_payment_due_date = $payment_reminder->format("Y-m-d");
            $update_payment_reminder_arr['payment_due_date'] = $new_payment_due_date;
            DB::table('payments_reminders')->where('id',$pending_payment->id)->update($update_payment_reminder_arr);
        }
        DB::table('payments')->insert($pending_payment_lines_arr);
    }


    public static function generateParticularCustomerPaymentLine($animal_id)
    {
        $added_by = Auth::user()->id;
        $pending_payment = PaymentReminder::where('added_by',$added_by)->where('payment_tracker','0')->where('payment_due_date','<=',date('Y-m-d'))->where('animal_id',$animal_id)->first();
        if(isset($pending_payment))
        {
            $pending_payment_lines_arr = [];
            $update_payment_reminder_arr = [];
            $pending_payment_lines_arr['animal_id'] = $pending_payment->animal_id;
            $pending_payment_lines_arr['customer_id'] = $pending_payment->paymentReminderAnimalInfo->customer_id;
            $pending_payment_lines_arr['amount_due'] = $pending_payment->paymentReminderAnimalInfo->monthly_rent;
            $pending_payment_lines_arr['amount_paid'] = 0;
            $pending_payment_lines_arr['due_date'] = $pending_payment->payment_due_date;
            $pending_payment_lines_arr['payment_status'] = '0';
            $pending_payment_lines_arr['added_by'] = Auth::user()->id;
            $pending_payment_lines_arr['created_at'] = date('Y-m-d h:m:i');

            $payment_reminder = new \DateTime($pending_payment->payment_due_date);
            $payment_reminder->modify('+1 months');
            $new_payment_due_date = $payment_reminder->format("Y-m-d");
            $update_payment_reminder_arr['payment_due_date'] = $new_payment_due_date;
            DB::table('payments_reminders')->where('id',$pending_payment->id)->update($update_payment_reminder_arr);
            DB::table('payments')->insert($pending_payment_lines_arr);
        }

    }
}