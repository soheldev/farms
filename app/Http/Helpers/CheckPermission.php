<?php
/**
 * Created by PhpStorm.
 * User: Sohell
 * Date: 5/6/2020
 * Time: 5:50 AM
 */

namespace App\Http\Helpers;


use App\UserRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sohel\Role\Model\Permission;

class CheckPermission
{
    public static function hasPermission($slug)
    {
        if(Auth::user()->user_type == '1' || Auth::user()->user_type == '2')
        {
            return true;
        }
        else
        {
            $permission_id = Permission::where('slug',$slug)->first('id');
            $role_id = UserRole::where('user_id', Auth::id())->first(['role_id']);
            if(isset($permission_id) && isset($role_id))
            {
                $check_permission = DB::table('role_permissions')->where('role_id',$role_id->role_id)->where('permission_id',$permission_id->id)->first();
                if($check_permission)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}