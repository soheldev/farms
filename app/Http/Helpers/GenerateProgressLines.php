<?php
/**
 * Created by PhpStorm.
 * User: Sohell
 * Date: 5/3/2020
 * Time: 7:51 AM
 */
namespace App\Http\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sohel\Progress\Model\ProgressReminder;

class GenerateProgressLines
{
    public function generatePendingAnimalProgressLine($pending_progress)
    {
        $pending_progress_lines_arr = [];
        $update_progress_reminder_arr = [];
        foreach($pending_progress as $index => $progress)
        {
            $pending_progress_lines_arr[$index]['animal_id'] = $progress->animal_id;
            $pending_progress_lines_arr[$index]['weight'] = 0;
            $pending_progress_lines_arr[$index]['height'] = 0;
            $pending_progress_lines_arr[$index]['due_date'] = $progress->progress_entry_date;
            $pending_progress_lines_arr[$index]['added_by'] = Auth::user()->id;

            $progress_reminder = new \DateTime($progress->progress_entry_date);
            $progress_reminder->modify('+1 months');
            $new_progress_reminder_date = $progress_reminder->format("Y-m-d");
            $update_progress_reminder_arr['progress_entry_date'] = $new_progress_reminder_date;
            DB::table('progress_reminders')->where('id',$progress->id)->update($update_progress_reminder_arr);
        }
        DB::table('animal_progress')->insert($pending_progress_lines_arr);
    }

    public static function generateParticularAnimalProgressLine($animal_id)
    {
        $added_by = Auth::user()->id;
        $pending_progress = ProgressReminder::where('added_by',$added_by)->where('progress_tracker','0')->where('progress_entry_date','<=',date('Y-m-d'))->where('animal_id',$animal_id)->first();
        if(isset($pending_progress))
        {
            $pending_progress_lines_arr = [];
            $update_progress_reminder_arr = [];
            $pending_progress_lines_arr['animal_id'] = $pending_progress->animal_id;
            $pending_progress_lines_arr['weight'] = 0;
            $pending_progress_lines_arr['height'] = 0;
            $pending_progress_lines_arr['due_date'] = $pending_progress->progress_entry_date;
            $pending_progress_lines_arr['added_by'] = Auth::user()->id;

            $progress_reminder = new \DateTime($pending_progress->progress_entry_date);
            $progress_reminder->modify('+1 months');
            $new_progress_reminder_date = $progress_reminder->format("Y-m-d");
            $update_progress_reminder_arr['progress_entry_date'] = $new_progress_reminder_date;
            DB::table('progress_reminders')->where('id',$pending_progress->id)->update($update_progress_reminder_arr);
            DB::table('animal_progress')->insert($pending_progress_lines_arr);
        }
    }
}