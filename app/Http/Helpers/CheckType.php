<?php
/**
 * Created by PhpStorm.
 * User: Sohell
 * Date: 5/6/2020
 * Time: 5:50 AM
 */

namespace App\Http\Helpers;


use Illuminate\Support\Facades\Auth;

class CheckType
{
    public static function checkAuthType()
    {
        $user_redirect = '';
        $auth_type = Auth::user()->userInformation->user_type;
        if($auth_type == "1")
        {
            $user_redirect = "/admin";
        }
        else
        {
            $user_redirect = "/vendor";
        }
        return $user_redirect;
    }
}