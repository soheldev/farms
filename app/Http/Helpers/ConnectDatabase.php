<?php
/**
 * Created by PhpStorm.
 * User: Sohell
 * Date: 5/6/2020
 * Time: 5:50 AM
 */

namespace App\Http\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ConnectDatabase
{

    public static function connectDB($subdomain)
    {
        $check_sub_domain = DB::table('clients')->where('sub_domain',$subdomain)->first();
        if(isset($check_sub_domain))
        {
            $tenant_status = $check_sub_domain->status;
            switch ($tenant_status)
            {
                case '0':
                    Auth::logout();
                    /*return redirect('/login')->with(['error' => 'Your account is not active. Please contact admin']);*/
                    dd('Your account is not active. Please contact admin');
                    break;
                case '1':
                    $database_name = $check_sub_domain->database_name;
                    $database_user_name = $check_sub_domain->database_username;
                    $database_password = isset($check_sub_domain->database_password)?$check_sub_domain->database_password:'';
                    ConnectDatabase::setConnection($database_name,$database_user_name,$database_password);
                    break;
                case '2':
                    Auth::logout();
                    //return redirect('/login')->with(['error' => 'Your account is blocked. Please contact admin']);
                    dd('Your account is blocked. Please contact admin');
                    break;
                default:
                    Auth::logout();
                    dd('Opps something went wrong');
                    //return redirect('/login')->with(['error' => 'Opps something went wrong']);
                    break;

            }
        }
        else
        {
            dd('Not Found');
        }
        //dd(\DB::connection('mysql'));
    }

    public static function setConnection($database_name,$database_user_name,$database_password)
    {
        Session::put('client_db',$database_name);
        Session::put('client_db_user',$database_user_name);
        Session::put('client_db_password',$database_password);
        Config::set('database.connections.mysql.database', $database_name);
        Config::set('database.connections.mysql.username',$database_user_name);
        Config::set('database.connections.mysql.password',$database_password);
        DB::purge('mysql');
        DB::reconnect('mysql');
        //dd(dd(Config::get('database.connections.mysql.database')));
    }
}