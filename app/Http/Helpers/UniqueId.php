<?php
/**
 * Created by PhpStorm.
 * User: Sohell
 * Date: 5/3/2020
 * Time: 7:51 AM
 */
namespace App\Http\Helpers;

class UniqueId
{
    public static function generateUniqueIdentification($limit)
    {
        $code = '';
        for($i = 0; $i < $limit; $i++) { $code .= mt_rand(0, 9); }
        return $code;
    }
}