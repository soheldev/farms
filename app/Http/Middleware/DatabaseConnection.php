<?php

namespace App\Http\Middleware;

use App\Http\Helpers\ConnectDatabase;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DatabaseConnection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        /*$subdomains = explode('.', $request->getHost());
        $subdomain = $subdomains[0];
        $check_sub_domain = DB::table('clients')->where('sub_domain',$subdomain)->first();
        dd($check_sub_domain);
        if(isset($check_sub_domain))
        {
            $database_name = $check_sub_domain->database_name;
            $database_user_name = $check_sub_domain->database_username;
            $database_password = isset($check_sub_domain->database_password)?$check_sub_domain->database_password:'';
            ConnectDatabase::setConnection($database_name,$database_user_name,$database_password);
        }
        else
        {
            dd('Not Found');
        }*/

        return $next($request);
    }
}
