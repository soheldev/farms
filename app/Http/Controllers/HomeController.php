<?php

namespace App\Http\Controllers;

use App\Http\Helpers\ConnectDatabase;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $subdomains = explode('.', $request->getHost());
        $subdomain = $subdomains[0];
        ConnectDatabase::connectDB($subdomain);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->user_type == '0')
        {
            return redirect('/master/dashboard');
        }
        elseif(Auth::user()->user_type == '1')
        {
            return redirect('/admin/dashboard');
        }
        elseif (Auth::user()->user_type == '2' || Auth::user()->user_type == '3')
        {
            $status = Auth::user()->userInformation->user_status;
            //dd($status);
            switch ($status)
            {
                case '0':
                    Auth::logout();
                    return redirect('/login')->with(['error' => 'Your account is not active. Please contact admin']);
                    break;
                case '1':
                    return redirect('/vendor/dashboard');
                    break;
                case '2':
                    Auth::logout();
                    return redirect('/login')->with(['error' => 'Your account is blocked. Please contact admin']);
                    break;
                default:
                    Auth::logout();
                    return redirect('/login')->with(['error' => 'Opps something went wrong']);
                    break;

            }
        }
        elseif (Auth::user()->user_type == '4')
        {
            $status = Auth::user()->customerInformation->status;
            switch ($status)
            {
                case '0':
                    return redirect('/login')->with(['error' => 'Your account is not active. Please contact admin']);
                    break;
                case '1':
                    return redirect('/customer/dashboard');
                    break;
                case '2':
                    return redirect('/login')->with(['error' => 'Your account is blocked. Please contact admin']);
                    break;
                default:
                    return redirect('/login')->with(['error' => 'Opps something went wrong']);
                    break;

            }
        }
        else
        {
            return view('home');
        }

    }

    public function viewChartPage(Request $request)
    {
        $sql = \DB::select("SELECT wp.leave_type concat(substring(date_format(to_date,'%M'),1,3))as to_date,leave_type,sum(no_day) FROM wp_applyleave AS wp WHERE status = 0 GROUP by leave_type");
        dd($sql);
        return view('chart');
    }
}
