<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    //
    protected $table = 'user_roles';
    protected $fillable = ['user_id','role_id'];


    //protected $with = ['users'];

    public function roleUserDetail()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function userRoleName()
    {
        return $this->belongsTo('Sohel\Role\Model\Role','role_id','id');
    }



}
